import json
import math
import os
import re
import numpy as np
import tensorflow as tf
from keras.layers import Dense, BatchNormalization, Activation, Dropout, Input
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split

import gw2
from dps_report_api import EliteInsightData, DpsReportAPI, CombatReplayData, get_ei_data_cached
from logmanagercachereader import LogManagerCache


print("Tensorflow v" + tf.__version__)
print(" build with CUDA:", str(tf.test.is_built_with_cuda()))
print("Num GPUs Available: ", len(tf.config.list_physical_devices('GPU')))
tf.device('/CPU:0')

HISTORY_SIZE = 2
HISTORIES_COUNT = 4

CACHE_PATH = "E:\\GW2LogsData\\"



def get_cr_json(url, *, silent=True):
    if not re.match(r'https://dps\.report/[a-zA-z0-9]{4}-\d{8}-\d{6}_[a-z]{2,5}', url):
        print('invalid url: "%s"' % url)
        return
    url_id_part = url.split('https://dps.report/')[1]
    if not os.path.exists(CACHE_PATH + 'crJsons'):
        os.mkdir(CACHE_PATH + 'crJsons')
    jpath = (CACHE_PATH + 'crJsons/%s.json') % url_id_part
    if os.path.exists(jpath):
        try:
            f = open(jpath, 'r')
            j = json.loads(f.read())
            f.close()
            return CombatReplayData(j)
        except json.decoder.JSONDecodeError:
            print("Failed to load file :", url_id_part)

    else:
        if not silent:
            print('Downloading cr data %s' % url)
        api = DpsReportAPI()
        j = api.get_cr_json_data_by_link(url)
        f = open(jpath, 'w')
        f.write(json.dumps(j.data))
        f.close()
        return j


def get_kerchu_log_urls():
    path = "E:\\GW2LogsData\\kerchuLogs\\log_files"
    cache_file = "E:\\GW2LogsData\\kerchuLogs\\cache_file.json"
    dps_api = DpsReportAPI()
    if os.path.exists(cache_file):
        f = open(cache_file, "r")
        data = json.loads(f.read())
        f.close()
    else:
        data = {}
    for file in os.listdir(path):
        if file not in data:
            print("uploading", file)
            response = dps_api.upload_file(path + "\\" + file)
            data[file] = response.get_permalink()
        f = open(cache_file, "w")
        f.write(json.dumps(data))
        f.close()
    print("done preparing")
    url_list = []
    for fn in data:
        url_list.append(data[fn])
    return url_list


def process_log_url(url, history_size, bomb_index, mechanic_name):
    ei_data = get_ei_data_cached(url)
    if not ei_data:
        print("No EI data")
        return
    crjson = CombatReplayData({})
    if not ei_data.has_combat_replay_data():
        crjson = get_cr_json(url)
        if not crjson:
            print("No EI CR or fallback data")
            return
        print("No EI CR data, use CRJ fallback")
        return
    mechanic_events = ei_data.get_mechanic_events(mechanic_name)
    if not mechanic_events or len(mechanic_events) <= bomb_index:
        print("No events found")
        return
    # first_bomber_name = bomb_events[0]["actor"]
    bomb_time = mechanic_events[bomb_index]["time"]
    bomber_name = mechanic_events[bomb_index]["actor"]
    players = ei_data.players()
    if len(players) != 10:
        return
    x = np.zeros(shape=(13 + history_size * HISTORIES_COUNT, 10), dtype='float32')
    bomber_index = -1
    print(bomb_time / 1000., bomber_name, url)
    player_index = 0
    for player in players:
        is_bomber = player.name == bomber_name
        if is_bomber:
            bomber_index = player_index
        x[0][player_index] = player.toughness_rank
        x[1][player_index] = player.concentration_rank
        x[2][player_index] = player.healing_rank
        x[3][player_index] = player.condition_rank

        es_id = gw2.get_class_id_by_elite_spec_name(player.profession_name)

        x[4:13, player_index] = tf.keras.utils.to_categorical(es_id, 9)

        # if player.get_name() != first_bomber_name and green2_name != player.get_name():

        for i in range(history_size):
            if ei_data.has_combat_replay_data():
                pos = player.get_position_interpolate(bomb_time - i * 1000)
                rotation = player.get_orientation_interpolate(bomb_time - i * 1000)
            else:
                pos = crjson.get_position_interpolate(player.name, bomb_time - i * 1000) #todo this is wrong
                rotation = 0 #todo
            if not pos:
                print("no pos found :/")
                return
                # pos = (0, 0)
            x_pos, y_pos = pos
            hp_percent = player.get_health_percent_at_time(bomb_time - i * 1000)

            if not rotation:
                rotation = 0
            x[13 + i + 0 * history_size][player_index] = x_pos
            x[13 + i + 1 * history_size][player_index] = y_pos
            x[13 + i + 2 * history_size][player_index] = hp_percent
            x[13 + i + 3 * history_size][player_index] = rotation
        player_index += 1
    return x, bomber_index


def create_data(bomb_index=0, *, boss_name="Dhuum", mechanic_name="Bomb"):
    lmc = LogManagerCache()
    logs = lmc.get_logs(target_filter=[boss_name])
    # distanceB1_clm = ABDataColumn('Distance to Bomb 1 explosion')
    X = np.zeros(shape=(0, 13 + HISTORY_SIZE * HISTORIES_COUNT, 10), dtype='float32')
    t = np.zeros(shape=(0, 0), dtype='int32')
    print("Logs:", len(logs))
    total_log_count = 0
    for log_entry in logs:
        total_log_count += 1
        url = log_entry["DpsReportEIUpload"]["Url"]
        if not url:
            continue
        ret = process_log_url(url, HISTORY_SIZE, bomb_index, mechanic_name)
        if not ret:
            continue
        x, y = ret
        X = np.concatenate((X, [x]), axis=0)
        t = np.append(t, y)
    if boss_name == "Dhuum":
        for url in get_kerchu_log_urls():
            total_log_count += 1
            ret = process_log_url(url, HISTORY_SIZE, bomb_index, mechanic_name)
            if not ret:
                continue
            x, y = ret
            X = np.concatenate((X, [x]), axis=0)
            t = np.append(t, y)
    print(X.shape)
    print(t.shape)
    np.savez("Shackle" + str(bomb_index + 1) + "Data", X=X, t=t)
    print("total logs / valid players", total_log_count, X.shape[0] * 10)


def create_model():
    # Delete all previous models to free memory
    tf.keras.backend.clear_session()
    # Sequential model
    model = tf.keras.models.Sequential()
    # Input layer representing the 94 data points for 10 players
    model.add(Input(shape=((13 + HISTORIES_COUNT * HISTORY_SIZE) * 10)))

    # Hidden layer with 400 logistic neurons
    model.add(Dense(units=50))
    model.add(BatchNormalization())  # Batch normalization
    model.add(Activation('relu'))
    model.add(Dropout(0.4))

    # Hidden layer with 60 logistic neurons
    model.add(Dense(units=30))
    model.add(BatchNormalization())  # Batch normalization
    model.add(Activation('relu'))
    model.add(Dropout(0.3))

    # Hidden layer with 30 logistic neurons
    #model.add(tf.keras.layers.Dense(units=30))
    #model.add(tf.keras.layers.BatchNormalization())  # Batch normalization
    #model.add(tf.keras.layers.Activation('relu'))
    #model.add(tf.keras.layers.Dropout(0.1))

    # Softmax output layer over 10 classes
    model.add(Dense(units=10))
    model.add(Activation('softmax'))

    # Learning rule
    optimizer = tf.keras.optimizers.Adam(learning_rate=0.001)

    # Loss function
    model.compile(
        loss='categorical_crossentropy',  # loss function
        optimizer=optimizer,  # learning rule
        metrics=['accuracy']  # show accuracy
    )

    print(model.summary())

    return model


def train(model, X_train, T_train):
    # History tracks the evolution of the metrics during learning
    history = tf.keras.callbacks.History()

    # Training procedure
    model.fit(
        X_train, T_train,  # training data
        batch_size=128,  # batch size
        epochs=100,  # Maximum number of epochs
        validation_split=0.2,  # Perceptage of training data used for validation
        callbacks=[history]  # Track the metrics at the end of each epoch
    )
    plt.figure(figsize=(12, 6))

    plt.subplot(121)
    plt.plot(history.history['loss'], '-r', label="Training")
    plt.plot(history.history['val_loss'], '-b', label="Validation")
    plt.xlabel('Epoch #')
    plt.ylabel('Loss')
    plt.legend()

    plt.subplot(122)
    plt.plot(history.history['accuracy'], '-r', label="Training")
    plt.plot(history.history['val_accuracy'], '-b', label="Validation")
    plt.xlabel('Epoch #')
    plt.ylabel('Accuracy')
    plt.legend()
    plt.show()


def test_model(model, X_train, T_train, X_test, T_test, X_mean, X_std):
    score = model.evaluate(X_test, T_test, verbose=0)
    print('Test loss:', score[0])
    print('Test accuracy:', score[1])

    Y_test = model.predict(X_test)
    c_test = np.argmax(Y_test, axis=-1)
    t_test = np.argmax(T_test, axis=-1)

    misclassification = (c_test != t_test).nonzero()[0]

    correct_classification = (c_test == t_test).nonzero()[0]

    plt.figure(figsize=(10, 8))
    for i in range(min(15, len(misclassification))):
        plt.subplot(5, 3, i + 1)
        plt.imshow(np.rot90((X_test[misclassification[i], :]).reshape((13 + HISTORIES_COUNT * HISTORY_SIZE, 10))),
                   cmap=plt.cm.gray,
                   interpolation='nearest')
        plt.title('predicted:' + str(c_test[misclassification[i]]) + '; actual:' + str(t_test[misclassification[i]]))
        plt.xticks([])
        plt.yticks([])

    plt.figure(figsize=(10, 8))
    for i in range(min(15, len(correct_classification))):
        plt.subplot(5, 3, i + 1)
        plt.imshow(
            np.transpose((X_test[correct_classification[i], :]).reshape((13 + HISTORIES_COUNT * HISTORY_SIZE, 10))),
            cmap=plt.cm.gray,
            interpolation='nearest')
        plt.title('predicted:' + str(c_test[correct_classification[i]]) + '; actual:' + str(
            t_test[correct_classification[i]]))
        plt.xticks([])
        plt.yticks([])
    plt.show()


def permute_rows(X, T, factor):
    rng = np.random.default_rng()
    Xt_in = np.concatenate((X, np.reshape(T, (X.shape[0], 1, 10))), axis=1)
    Xt_out = np.zeros(shape=(Xt_in.shape * np.array([factor, 1, 1])))
    for row in range(X.shape[0]):
        for j in range(factor):
            x_copy = np.copy(Xt_in[row])
            rng.shuffle(x_copy, 1)
            Xt_out[row * factor + j] = x_copy
    return Xt_out[:, :-1], Xt_out[:, -1]


if __name__ == "__main__":
    #create_data(0, boss_name="Dhuum", mechanic_name="Shackles")
    data = np.load("Shackle1Data.npz")
    X = data['X']
    t = data['t']
    print("Source data:", X.shape, t.shape)
    X_mean = X.mean((0, 2))
    X_std = X.std((0, 2))
    for i in range(X_std.shape[0]):
        if math.isnan(X_mean[i]):
            X_mean[i] = 0
        if math.isnan(X_std[i]):
            X_std[i] = 0
        if X_std[i] == 0:
            X_std[i] = 1
        if 4 <= i <= 12: # dont normalize class enum
            X_std[i] = 1

    X -= X_mean[None, :, None]  # normalize mean
    X /= X_std[None, :, None]  # normalize std
    # One-hot encoding
    T = tf.keras.utils.to_categorical(t, 10)
    # split dataset
    X_train, X_test, T_train, T_test = train_test_split(X, T, test_size=0.2)
    # permute player rows to increase network size
    X_train, T_train = permute_rows(X_train, T_train, 100)
    X_test, T_test = permute_rows(X_test, T_test, 20)
    print("Training data:", X_train.shape, T_train.shape)
    print("Test data:", X_test.shape, T_test.shape)
    X_train = X_train.reshape(-1, (13 + HISTORY_SIZE * HISTORIES_COUNT) * 10).astype('float32')
    X_test = X_test.reshape(-1, (13 + HISTORY_SIZE * HISTORIES_COUNT) * 10).astype('float32')

    model = create_model()
    train(model, X_train, T_train)
    test_model(model, X_train, T_train, X_test, T_test, X_mean, X_std)
