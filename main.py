from __future__ import print_function

import asyncio
import re
import sys

import googleapiclient

import discord_log_sender
import splitts2svg
from dps_report_api import DpsReportAPI
from google_spreadsheet import authenticate, get_times
from gw2.gw2 import MIGHT_BUFF_ID, QUICKNESS_BUFF_ID, ALACRITY_BUFF_ID, VULNERABILITY_BUFF_ID
import gw2.raids as raids
import gw2.fractals as fractals
from log_uploader import get_new_logs

# The ID and range of the spreadsheet to use.
SHEET_ID = '19AEHyOVnXCzTlVmmKROu7AHLn9NYN7JzjLCNm7KE9Tc'
TIMES_RANGE = 'Records!C2:C21'


def main(server_name='testseverpv42', channel_name='2', mode='raids', src='links', max_log_age=240, sheet_id=None,
         sheet_range=None, show_buffs=False, thumbnail=None):
    dps_api = DpsReportAPI()
    game_mode = None
    if mode == 'raids':
        game_mode = raids
    elif mode == 'fracs':
        game_mode = fractals
    else:
        print('Invalid game mode.')
        return
    # get logs
    uploaded_logs = None
    if src == 'links':
        logs_str = ''
        print('Please input the logs you want to process')
        no_input = False
        while not no_input:
            line = input()
            logs_str += line + '\n'
            no_input = line == ''
        print('processing ...')
        # find links like https://dps.report/a3Pn-20210115-123051_vg
        log_links = re.findall(r'https://dps\.report/[a-zA-z0-9]{4}-\d{8}-\d{6}_[a-z]{2,5}', logs_str)
        uploaded_logs = get_logs_from_links(log_links, dps_api, game_mode)
    elif src == 'logs':
        uploaded_logs = upload_new_logs(dps_api, max_log_age=max_log_age, game_mode=game_mode)
    else:
        print('No valid log input specified, see --help on how to use -s')
        return
    if len(uploaded_logs) == 0:
        print('No logs found')
        return
    else:
        print('Found %d logs:' % len(uploaded_logs))
    for encounter_id in uploaded_logs:
        print(' ' + game_mode.get_by_encounter_id(encounter_id)['name'] + ': ' + uploaded_logs[
            encounter_id].get_permalink())
    boss_times = get_boss_records(game_mode, sheet_id=sheet_id, times_range=sheet_range)
    log_links_and_times = {}
    instabilities = {}
    i = 0
    display_buffs = []
    display_conditions = []
    if show_buffs:
        display_buffs = [MIGHT_BUFF_ID, QUICKNESS_BUFF_ID, ALACRITY_BUFF_ID]
        display_conditions = [VULNERABILITY_BUFF_ID]
    buffs = {}
    for encounter_id in uploaded_logs:
        i += 1
        print('getting duration %d/%d ...' % (i, len(uploaded_logs)))
        json_data = dps_api.get_json_data_by_id(uploaded_logs[encounter_id].get_id())
        time_ms = json_data.get_duration_ms()
        record_string = ''
        record_time_str = ''
        if boss_times and encounter_id not in boss_times:
            print('"New" Record: ' + uploaded_logs[encounter_id].get_boss() + ' in ' + format_duration_string(time_ms))
            record_string = ' - "New Record"'
        elif boss_times and time_ms < boss_times[encounter_id]:
            print('New Record: ' + uploaded_logs[encounter_id].get_boss() + ' in ' + format_duration_string(
                time_ms) + ' Old record was ' + format_duration_string(boss_times[encounter_id]))
            record_string = ' - New Record by ' + format_duration_string(boss_times[encounter_id] - time_ms)
            record_time_str = 'Old record: ' + format_duration_string(boss_times[encounter_id])
        else:
            record_time_str = 'Record: ' + format_duration_string(boss_times[encounter_id])
        if encounter_id == 'arkk' or encounter_id == 'arriv' or encounter_id == 'siax' or encounter_id == 'ai':  # only main target on some bosses
            buffs[encounter_id] = json_data.get_party_buff_uptimes(
                buffs=display_buffs,
                average_phases=True) | json_data.get_main_targets_buff_uptime(buffs=display_conditions)
        else:
            buffs[encounter_id] = json_data.get_party_buff_uptimes(
                buffs=display_buffs) | json_data.get_targets_buff_uptime(buffs=display_conditions)
        # link, duration string, record string (or empty), record time string
        log_links_and_times[encounter_id] = (
            uploaded_logs[encounter_id].get_permalink(),
            format_duration_string(time_ms),
            record_string,
            record_time_str
        )
        if game_mode == fractals:
            fractal_id = game_mode.get_by_encounter_id(encounter_id)['fractal']
            instabilities[fractal_id] = set()
            for instab in json_data.instabilities:
                if instab in fractals.fractal_instabilities:
                    instabilities[fractal_id] |= {fractals.fractal_instabilities[instab]}
            thumbnail = splitts2svg.create_fractal_png(max_log_age=max_log_age)
    # suppress an on exit error message
    asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
    # send to discord
    sender = discord_log_sender.DiscordLogSender(log_links_and_times,
                                                 instabilities=instabilities,
                                                 guild_name=server_name,
                                                 channel_name=channel_name,
                                                 mode=mode,
                                                 sheet_id=sheet_id,
                                                 buffs=buffs,
                                                 thumbnail=thumbnail)
    sender.start_bot()


def get_boss_records(game_mode, sheet_id, times_range):
    service = authenticate()
    old_time_values = None
    from googleapiclient.errors import HttpError
    try:
        old_time_values = get_times(service, sheet_id, times_range)
    except googleapiclient.errors.HttpError:
        print('Could not get times data (Google API Error)')
        return
    if not old_time_values:
        print('Could not get times data (No data returned)')
        return
    boss_times = {}
    boss_index = 0
    for row in old_time_values:
        if len(row) == 0:
            boss_index += 1
            continue
        time_str = row[0]
        parts = re.findall(r'\d+', time_str)
        time_ms = 1000 * (60 * int(parts[0]) + int(parts[1])) + int(parts[2])
        print('%22s: %s' % (game_mode.encounters[boss_index]['name'], time_str))
        boss_times[game_mode.encounters[boss_index]['id']] = time_ms
        boss_index += 1
    return boss_times


def upload_new_logs(dps_report_api, max_log_age, game_mode):
    logs = get_new_logs(max_log_age)
    uploaded_logs = {}
    i = 0
    for log in logs:
        i += 1
        print('uploading %d/%d ...' % (i, len(logs)))
        response = dps_report_api.upload_file(log)
        if not response.is_success():
            continue
        enemy = game_mode.get_by_enemy_id(response.get_boss_id())
        if not enemy:
            print(response.get_boss() + ' is not a counted boss')
            continue
        uploaded_logs[enemy['id']] = response
    return uploaded_logs


def get_logs_from_links(links, dps_report_api, game_mode):
    uploaded_logs = {}
    for link in links:
        r = dps_report_api.get_upload_data_from_link(link)
        if not r:
            continue
        if not r.is_success():
            continue
        enemy = game_mode.get_by_enemy_id(r.get_boss_id())
        if not enemy:
            continue
        uploaded_logs[enemy['id']] = r
    return uploaded_logs


def format_duration_string(time_ms):
    minutes = int(time_ms / 60000)
    sec = int(time_ms / 1000) % 60
    millis = time_ms % 1000
    return '%02d:%02d.%03d' % (minutes, sec, millis)


def command_line_arg_parser():
    i = 1
    server_name = None
    channel_name = None
    log_src = None
    max_log_age = None
    mode = None
    sheet_id = None
    sheet_range = None
    show_buffs = False
    while i < len(sys.argv):
        arg = sys.argv[i]
        if arg == '-h' or arg == '--help' or arg == '-?':
            print('''Usage:
python main.py -s <servername> -c <channelname> -s (links|logs) [-d <duration>] -m (raids|fracs) -t <google_sheet_id> -r range
 -g servername    specify the server to send the logs in
 -c channelname   specify the channel to send the logs in
 -s links         enter dps report links to process
 -s logs          search the filesystem for new log and uses them
 -a max_log_age   for -s logs: specifies how new the logs must be to be processed
                  <max_log_age> must a number of minutes, default is 240
 -m (raids|fracs) specifies whether to process raid or fractal logs, raids is default
 -t sheet_id      specifies a google sheet id to pull the records from
 -r range         specifies the place of the google sheet to pull the record from e.g. Records!B2:B10
 -b               displays might, alacrity and quickness for each boss
''')
            return
        elif arg == '-g':
            i += 1
            server_name = sys.argv[i]
        elif arg == '-c':
            i += 1
            channel_name = sys.argv[i]
        elif arg == '-s':
            i += 1
            log_src = sys.argv[i]
        elif arg == '-a':
            i += 1
            max_log_age = int(sys.argv[i])
        elif arg == '-m':
            i += 1
            mode = sys.argv[i]
        elif arg == '-t':
            i += 1
            sheet_id = sys.argv[i]
        elif arg == '-r':
            i += 1
            sheet_range = sys.argv[i]
        elif arg == '-b':
            i += 1
            show_buffs = True
        else:
            print('Unknown command line option: %s' % arg)
            return
        i += 1
    main(server_name=server_name, channel_name=channel_name, src=log_src, max_log_age=max_log_age, mode=mode,
         sheet_id=sheet_id, sheet_range=sheet_range, show_buffs=show_buffs)


if __name__ == '__main__':
    command_line_arg_parser()
