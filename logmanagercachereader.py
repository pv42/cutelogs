import json
import os
import re
from datetime import datetime, timedelta
from enum import Enum
from typing import Optional

from typing import List, Dict

CACHE_PATH = os.environ["USERPROFILE"] + "\\AppData\\Local\\ArcdpsLogManager"


class LMEncounter(Enum):
    Unknown = 0
    VG = 11
    Gorseval = 13
    Sabetha = 14
    Slothasor = 21
    BanditTrio = 22
    Matthias = 23
    Escort = 31
    KC = 32
    TC = 33
    Xera = 34
    Cairn = 41
    MO = 42
    Samarog = 43
    Deimos = 44
    SH = 51
    DesminaEscort = 52
    BrokenKing = 53
    Eater = 54
    Eyes = 55
    Dhuum = 56
    ConjuredAmalgamate = 61
    Twins = 62
    Qadim = 63
    Adina = 71
    Sabir = 72
    QTP = 73
    MAMA = 10001
    Siax = 10002
    Ensolyss = 10003
    Artsariiv = 10012
    Skorvald = 10011
    Arkk = 10013
    AiEle = 10022
    AiDark = 10023
    AiBoth = 10024
    Freezie = 20001
    StandardGolem = 30001
    MediumGolem = 30002
    LargeGolem = 30003
    IcebroodConstruct = 40001
    Kodans = 40002
    Fraenir = 40003
    Boneskinner = 40004
    Whisper = 40005
    MaiTrin = 41001
    Ankka = 41002
    MinisterLi = 41003
    DragonVoid = 41004
    Unknown100k = 100000


def convert_time_to_my_timezone(time):
    return time.astimezone(datetime.now().tzinfo)


def to_datetime(string):
    try:
        return convert_time_to_my_timezone(datetime.strptime(string, "%Y-%m-%dT%H:%M:%S%z"))
    except ValueError:  # sometimes without milliseconds
        string = re.sub(r"([0-9]{6})([0-9]*)", r"\1", string)  # strip milliseconds past 6 digits
        return convert_time_to_my_timezone(datetime.strptime(string, "%Y-%m-%dT%H:%M:%S.%f%z"))
    except OSError:
        print("invalid datetime:", string)
        return convert_time_to_my_timezone(datetime(year=2000, month=1, day=1))


def to_timedelta(string):
    parts = string.split(":")
    return timedelta(0, 3600 * int(parts[0]) + 60 * int(parts[1]) + float(parts[2]))


def correct_ch_id_name(name):
    if name.startswith("ch17154"):
        return "Deimos"
    elif name.startswith("ch16247") or name.startswith("ch16246"):
        return "Haunting Statue"
    elif name.startswith("ch16088"):
        return "Berg"
    elif name == "Beschworene Verschmelzung":
        return "Conjured Amalgamate"
    elif name == "ag650":
        return "Cairn the Indomitable"
    else:
        return name


class LogManagerLog:
    def __init__(self, data):
        self.data = data

    def get_filename(self) -> str:
        return self.data["FileName"]

    def get_main_target_name(self) -> str:
        return self.data["MainTargetName"]

    def get_encounter_start_time(self) -> datetime:
        return to_datetime(self.data["EncounterStartTime"])

    def get_dps_report_url(self) -> Optional[str]:
        return self.data["DpsReportEIUpload"]["Url"]

    def is_success(self) -> bool:
        return int(self.data["EncounterResult"]) == 0

    def get_health_percentage(self):
        return self.data["HealthPercentage"]

    def get_map_id(self) -> int:
        return self.data["MapId"]

    def get_duration(self) -> timedelta:
        return to_timedelta(self.data["EncounterDuration"])

    @property
    def players(self):
        player_list = []
        if "Players" in self.data and self.data["Players"] is not None:
            for player_entry in self.data["Players"]:
                player_list.append(LogMangerPlayer(player_entry))
        else:
            print(self.get_dps_report_url(), "has no player attrib")
        return player_list

    @property
    def encounter(self):
        return LMEncounter(self.data["Encounter"])

    def is_cm(self) -> bool:
        return self.data["EncounterMode"] == 2


class LogManagerCache:
    def __init__(self):
        print(CACHE_PATH + "\\LogDataCache.json")
        with open(CACHE_PATH + "\\LogDataCache.json", "rb") as f:
            self.cache = json.loads(f.read())

    def get_logs(self, *, target_filter: Optional[list[str]] = None, success_filter: Optional[list[bool]] = None,
                 encounter_filter: Optional[list[LMEncounter]] = None, cm_filter: Optional[list[bool]] = None,
                 after= None, before=None):
        logs = []
        for fn in self.cache['LogsByFilename']:
            log = LogManagerLog(self.cache['LogsByFilename'][fn])
            if target_filter and correct_ch_id_name(log.get_main_target_name()) not in target_filter:
                continue
            if success_filter and log.is_success() not in success_filter:
                continue
            if cm_filter and log.is_cm() not in cm_filter:
                continue
            if after and log.get_encounter_start_time() < after:
                continue
            if before and log.get_encounter_start_time() > before:
                continue
            if encounter_filter and log.encounter not in encounter_filter:
                continue
            logs.append(log)
        return logs

    def get_all_logs(self) -> List[LogManagerLog]:
        logs = []
        for fn in self.cache['LogsByFilename']:
            logs.append(LogManagerLog(self.cache['LogsByFilename'][fn]))
        return logs

    def get_logs_by_day(self, *, maps_filter=None, target_filter=None) -> Dict[datetime.date, List[LogManagerLog]]:
        days = {}
        logs = self.get_all_logs()
        for log in logs:
            if maps_filter and log.get_map_id() not in maps_filter:
                continue
            if target_filter and correct_ch_id_name(log.get_main_target_name()) not in target_filter:
                continue
            dtime = log.get_encounter_start_time()
            date = dtime.date()
            if date not in days:
                days[date] = []
            days[date].append(log)
        return days


class EliteSpecialization(Enum):
    NoEliteSpec = 0
    Dragonhunter = 1
    Berserker = 2
    Scrapper = 3
    Druid = 4
    Daredevil = 5
    Tempest = 6
    Chronomancer = 7
    Reaper = 8
    Herald = 9
    Soulbeast = 10
    Weaver = 11
    Holosmith = 12
    Deadeye = 13
    Mirage = 14
    Scourge = 15
    Spellbreaker = 16
    Firebrand = 17
    Renegade = 18
    Willbender = 19
    Bladesworn = 20
    Mechanist = 21
    Untamed = 22
    Specter = 23
    Catalyst = 24
    Virtuoso = 25
    Harbinger = 26
    Vindicator = 27
    # base classes
    Guardian = 101
    ES102 = 102
    Engineer = 103
    ES104 = 104
    Thief = 105
    ES106 = 106
    ES107 = 107
    ES108 = 108
    ES109 = 109
    Elementalist = 110
    # extra values for reasons
    Sword = 1001
    NPC = 1002


class LogMangerPlayer:
    def __init__(self, data):
        self.data = data

    def get_account_name(self) -> str:
        return self.data["AccountName"]

    @property
    def name(self) -> str:
        return self.data["Name"]

    def get_elite_spec(self):
        return EliteSpecialization(self.data["EliteSpecialization"])

    def get_guild_guid(self) -> str:
        return self.data["GuildGuid"]
