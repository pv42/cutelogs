# used to retrive past log links
import asyncio
import datetime
import re

import discord


class LogLinkRetriever(discord.Client):
    def __init__(self, guild_name='testseverpv42', channel_name='2', **options):
        super().__init__(**options)
        self.guild_name = guild_name
        self.channel_name = channel_name

    async def on_ready(self):
        print('Connected to discord api as user %s' % self.user)
        await self.get_log_links()
        asyncio.run_coroutine_threadsafe(self.logout(), self.loop)

    async def get_log_links(self):
        guild = None
        async for g in self.fetch_guilds():
            if g.name == self.guild_name:
                guild = g
                break
        if not guild:
            print('No matching guild for "%s" found' % self.guild_name)
            return
        guild = self.get_guild(guild.id)
        channel = None
        channels = await guild.fetch_channels()
        for c in channels:
            if c.name == self.channel_name:
                channel = c
                break
        if not channel:
            print('No matching channel for "%s" found' % self.channel_name)
            return
        msg_counter = 0
        frac_counter = 0
        link_counter = 0
        limit = 5000
        after = datetime.datetime(day=18, month=3, year=2021)
        links = []
        dates = []
        count = 0
        async for message in channel.history(limit=limit, after=after):
            if message.author == self.user and len(message.embeds) > 0:
                daily_links = []
                for field in message.embeds[0].fields:
                    frac_links = re.findall(r'https://dps\.report/[a-zA-z0-9]{4}-\d{8}-\d{6}_[a-z]{2,5}', field.value)
                    for link in frac_links:
                        daily_links.append(link)
                        link_counter += 1
                    frac_counter += 1
                msg_counter += 1
                links.append(daily_links)
                dates.append(message.created_at - datetime.timedelta(hours=6))  # enfore right day
            count += 1
        print("Found %d links in %d fractals in %d days in the last %d messages (limit %d)" % (
        link_counter, frac_counter, msg_counter, count, limit))
        links_to_csv(links, dates)
        return

    def start_bot(self):
        token_file = open('discord_token')
        token = token_file.read()
        token_file.close()
        self.run(token)


def links_to_csv(links, dates):
    f = open('logs.csv', 'w')
    index = 0
    for day in links:
        f.write(str(dates[index].date()))
        f.write(';')  # leave space for date
        for link in day:
            f.write(link)
            f.write(';')
        f.write('\n')
        index += 1
    f.close()


if __name__ == '__main__':
    asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
    llr = LogLinkRetriever(guild_name="The hideout [Cute]", channel_name="fracss")
    llr.start_bot()
