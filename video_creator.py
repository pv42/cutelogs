import gzip
import json
import os
import re
import os.path as path
import zlib
from enum import Enum
from math import sqrt
from typing import Optional

import numpy as np
import cv2
from cv2 import VideoWriter, VideoWriter_fourcc

import requests

import storage
import dps_report_api
from gw2 import raids, strikes, fractals

import logmanagercachereader

# Some notes on how to run this
# - install numpy and opencv-python
# - configure the path of FFMPEG_PATH to point to a ffmpeg binary (download from ffmpeg.org)
# - configure the CACHE_PATH, there will be ~10MB stored for each log processed
# - first run will download/cache json data and combat replay data, this is slow
# - it takes a long time (hours) in a reasonable resolution
# - the video files for each boss will be stored lossless compressed in potential giant files before rendered
# - i pull the logs from storage.py, this has the record logs like
#       fractal_record_logs = {
#       'mama': 'https://dps.report/abcd-20210616-123456_mama',
#       'siax': 'https://dps.report/abcd-20210616-123456_siax',
#       ...
#       'eai': 'https://dps.report/abcd-20210616-123456_ai',
#       'ai': 'https://dps.report/abcd-20210616-123456_ai'
#       }
#    and the list of logs like this
#       raid_logs = [
#           "https://dps.report/defg-20201026-012345_dei",
#           "https://dps.report/hijk-20201102-012345_eyes",
#           ...
#       ]
# - if you want to add other encounters (trio etc) add them to raids.py/fractals.py/strikes.py
# - there is a good chance it won't work some some weird reason anyways
# - you can mostly ignore warnings like "invalid x value 3234.02 in frame 54 in log https://dps.report/abcd-20210106-012345_vg"
#   but there is a bunch of cases where Ai logs positions  are messed up and you should maybe exclude those logs
# - it works poorly on bosses with dynamic phase number like Dhuum, KC, Desmina
#   or this https://dps.report/P4JL-20210714-201358_sloth


FFMPEG_PATH = 'C:\\Users\\pv42\\Documents\\FFMpeg\\ffmpeg-20170312-58f0bbc-win64-static\\bin\\ffmpeg.exe'
CACHE_PATH = "E:\\GW2LogsData\\"
VIDEO_CACHE_PATH = CACHE_PATH + "video_cache\\"

TEXT_COLOR = (255, 255, 255)


class HT_Gamemode:
    def __init__(self):
        self.encounters = [{'id': 'void', 'name': 'The Dragonvoid', 'wing': 2, 'short_name': "Dragonvoid",
                            "lmid": logmanagercachereader.LMEncounter.DragonVoid}]

    def get_by_encounter_id(self, _):
        return self.encounters[0]


HT_CM_GAMEMODE = HT_Gamemode()


def dot(frame, color, x, y, size, opacity=1.):
    color = np.array(color)
    if size == 0:
        return
    for xi in range(int(x - size), int(x + size + 1)):
        for yi in range(int(y - size), int(y + size + 1)):
            if xi < 0 or yi < 0 or xi >= frame.shape[1] or yi >= frame.shape[0]:
                continue
            aa_factor = 1
            d_sq = (x - xi) ** 2 + (y - yi) ** 2
            if d_sq > size ** 2:
                continue
            if d_sq > (size - 0.5) ** 2:
                d = sqrt(d_sq)
                aa_factor = (size - d) * 2
            frame[(yi, xi)] = color * aa_factor * opacity + (1 - aa_factor * opacity) * frame[(yi, xi)]


def special_dot(frame, colors, color_dist, x, y, size, opacity=1.):
    # print(color_dist)
    if size == 0:
        return
    for xi in range(int(x - size), int(x + size + 1)):
        for yi in range(int(y - size), int(y + size + 1)):
            if xi < 0 or yi < 0 or xi >= frame.shape[1] or yi >= frame.shape[0]:
                continue
            aa_factor = 1
            d_sq = (x - xi) ** 2 + (y - yi) ** 2
            if d_sq > size ** 2:
                continue
            if d_sq > (size - 0.5) ** 2:
                d = sqrt(d_sq)
                aa_factor = (size - d) * 2
            c_index = 0
            angle = np.arctan2(xi - x, yi - y) + np.pi
            acc_angel = 0
            for part in color_dist:
                acc_angel += part * 2 * np.pi
                if acc_angel > angle:
                    break
                c_index += 1
            if c_index >= 9:
                color = np.array((127, 127, 127))
            else:
                color = np.array(colors[c_index])
            frame[(yi, xi)] = color * aa_factor * opacity + (1 - aa_factor * opacity) * frame[(yi, xi)]


def get_cr_json(url, *, silent=True) -> Optional[dps_report_api.CombatReplayData]:
    if not re.match(r'https://dps\.report/[a-zA-z0-9]{4}-\d{8}-\d{6}_[a-z]{2,5}', url):
        print('invalid url: "%s"' % url)
        return
    url_id_part = url.split('https://dps.report/')[1]
    if not os.path.exists(CACHE_PATH + 'crJsons'):
        os.mkdir(CACHE_PATH + 'crJsons')
    jpath = (CACHE_PATH + 'crJsons/%s.json') % url_id_part
    gzjpath = jpath + ".gz"
    if os.path.exists(gzjpath):
        if os.path.exists(jpath):
            os.remove(jpath)
            print("deleted " + jpath + " in favor of gz")
        with gzip.open(gzjpath, 'rb') as f:
            crj = None
            try:
                crj = dps_report_api.CombatReplayData(json.loads(f.read()))
            except json.JSONDecodeError:
                print("Json error in", url)
            except zlib.error:
                print("Zlib error in", url)
            return crj
    elif os.path.exists(jpath):
        f = open(jpath, 'rb')
        j = None
        try:
            data = f.read()
            with gzip.open(gzjpath, "wb") as gzf:
                print("Creating gzip " + gzjpath)
                gzf.write(data)
            j = dps_report_api.CombatReplayData(json.loads(data))
        except json.JSONDecodeError:
            print("Json error in", url)
        f.close()
        return j
    else:
        if not silent:
            print('Downloading cr data json %s' % url)
        api = dps_report_api.DpsReportAPI()
        j = api.get_cr_json_data_by_link(url)
        if j:
            with gzip.open(gzjpath, 'wb') as f:
                f.write(bytes(json.dumps(j.data), "utf-8"))
        return j


def get_img(url):
    cache_name = url.replace("/", "-").replace(":", "-")
    if not os.path.exists(CACHE_PATH + 'img_cache'):
        os.mkdir(CACHE_PATH + 'img_cache')
    img_path = (CACHE_PATH + 'img_cache/%s') % cache_name
    if not path.exists(img_path):
        if not url.startswith("https:"):
            url = "https://dps.report" + url
        print("Downloading %s to %s" % (url, img_path))
        f = open(img_path, 'wb')
        response = requests.get(url)
        f.write(response.content)
        f.close()
    return img_path


def clamp(lwr, value, upr):
    if value < lwr:
        return lwr
    if value > upr:
        return upr
    return value


def convert_video_file(in_files, audio_file=None):
    out_file = "render.mp4"
    if len(in_files) == 1:
        in_param = in_files[0]
    else:
        in_param = '\"concat:'
        first = True
        for in_file in in_files:
            if not first:
                in_param += '|'
            first = False
            in_param += in_file
        in_param += '\"'
    if audio_file:
        in_param += " -i \"" + audio_file + "\""
    cmd = f"{FFMPEG_PATH} -i {in_param} -vf scale=2560x1440:flags=lanczos -y -c:v libx264 -b:v 50M {out_file}"
    print(cmd)
    os.system(cmd)


CLASS_COLOR_INDEX_GUARD = 0
CLASS_COLOR_INDEX_THIEF = 1
CLASS_COLOR_INDEX_RANGER = 2
CLASS_COLOR_INDEX_RENE = 3
CLASS_COLOR_INDEX_ELE = 4
CLASS_COLOR_INDEX_WAR = 5
CLASS_COLOR_INDEX_ENGI = 6

CLASS_COLOR_INDEX_MES = 8

class_colors = [
    [255, 222, 10],
    [115, 97, 224],
    [10, 222, 135],
    [41, 41, 161],
    [56, 56, 247],
    [61, 212, 222],
    [41, 115, 227],
    [125, 227, 5],
    [209, 59, 204]
]


def get_actor_class(actor):
    if actor['img'] == '/cache/https_wiki.guildwars2.com_images_c_c9_Dragonhunter_tango_icon_20px.png' or \
            actor['img'] == '/cache/https_wiki.guildwars2.com_images_1_1f_Dragonhunter_tango_icon_200px.png':
        return CLASS_COLOR_INDEX_GUARD
    elif actor['img'] == '/cache/https_wiki.guildwars2.com_images_0_02_Firebrand_tango_icon_20px.png' or \
            actor['img'] == '/cache/https_wiki.guildwars2.com_images_7_73_Firebrand_tango_icon_200px.png':
        return CLASS_COLOR_INDEX_GUARD
    elif actor['img'] == '/cache/https_wiki.guildwars2.com_images_6_6c_Guardian_tango_icon_200px.png' or \
            actor['img'] == '/cache/https_wiki.guildwars2.com_images_8_8c_Guardian_tango_icon_20px.png':
        return CLASS_COLOR_INDEX_GUARD
    elif actor['img'] == '/cache/https_wiki.guildwars2.com_images_3_3a_Willbender_tango_icon_20px.png':
        return CLASS_COLOR_INDEX_GUARD
    elif actor['img'] == '/cache/https_wiki.guildwars2.com_images_7_7c_Soulbeast_tango_icon_20px.png' or \
            actor['img'] == '/cache/https_wiki.guildwars2.com_images_f_f6_Soulbeast_tango_icon_200px.png':
        return CLASS_COLOR_INDEX_RANGER
    elif actor['img'] == '/cache/https_wiki.guildwars2.com_images_6_6d_Druid_tango_icon_200px.png' or \
            actor['img'] == '/cache/https_wiki.guildwars2.com_images_d_d2_Druid_tango_icon_20px.png':
        return CLASS_COLOR_INDEX_RANGER
    elif actor[
        'img'] == '/cache/https_wiki.guildwars2.com_images_thumb_3_33_Untamed_tango_icon_200px.png_20px-Untamed_tango_icon_200px.png':
        return CLASS_COLOR_INDEX_RANGER
    elif actor['img'] == '/cache/https_wiki.guildwars2.com_images_4_43_Ranger_tango_icon_20px.png':
        return CLASS_COLOR_INDEX_RANGER
    elif actor['img'] == '/cache/https_wiki.guildwars2.com_images_7_7c_Renegade_tango_icon_20px.png' or \
            actor['img'] == '/cache/https_wiki.guildwars2.com_images_b_bc_Renegade_tango_icon_200px.png':
        return CLASS_COLOR_INDEX_RENE
    elif actor['img'] == '/cache/https_wiki.guildwars2.com_images_6_67_Herald_tango_icon_20px.png' or \
            actor['img'] == '/cache/https_wiki.guildwars2.com_images_c_c7_Herald_tango_icon_200px.png':
        return CLASS_COLOR_INDEX_RENE
    elif actor['img'] == '/cache/https_wiki.guildwars2.com_images_5_5a_Vindicator_tango_icon_20px.png':
        return CLASS_COLOR_INDEX_RENE
    elif actor['img'] == '/cache/https_wiki.guildwars2.com_images_f_fc_Weaver_tango_icon_20px.png' or \
            actor['img'] == '/cache/https_wiki.guildwars2.com_images_3_31_Weaver_tango_icon_200px.png':
        return CLASS_COLOR_INDEX_ELE
    elif actor['img'] == '/cache/https_wiki.guildwars2.com_images_4_4a_Tempest_tango_icon_20px.png' or \
            actor['img'] == '/cache/https_wiki.guildwars2.com_images_9_90_Tempest_tango_icon_200px.png':
        return CLASS_COLOR_INDEX_ELE
    elif actor['img'] == '/cache/https_wiki.guildwars2.com_images_d_d5_Catalyst_tango_icon_20px.png':
        return CLASS_COLOR_INDEX_ELE
    elif actor['img'] == '/cache/https_wiki.guildwars2.com_images_a_aa_Elementalist_tango_icon_20px.png':
        return CLASS_COLOR_INDEX_ELE
    elif actor['img'] == '/cache/https_wiki.guildwars2.com_images_d_da_Berserker_tango_icon_20px.png' or \
            actor['img'] == '/cache/https_wiki.guildwars2.com_images_8_80_Berserker_tango_icon_200px.png':
        return CLASS_COLOR_INDEX_WAR
    elif actor['img'] == '/cache/https_wiki.guildwars2.com_images_e_ed_Spellbreaker_tango_icon_20px.png' or \
            actor['img'] == '/cache/https_wiki.guildwars2.com_images_7_78_Spellbreaker_tango_icon_200px.png':
        return CLASS_COLOR_INDEX_WAR
    elif actor[
        'img'] == '/cache/https_wiki.guildwars2.com_images_thumb_c_c1_Bladesworn_tango_icon_200px.png_20px-Bladesworn_tango_icon_200px.png':
        return CLASS_COLOR_INDEX_WAR
    elif actor['img'] == '/cache/https_wiki.guildwars2.com_images_d_db_Warrior_tango_icon_200px.png':
        return CLASS_COLOR_INDEX_WAR
    elif actor['img'] == '/cache/https_wiki.guildwars2.com_images_2_28_Holosmith_tango_icon_20px.png' or \
            actor['img'] == '/cache/https_wiki.guildwars2.com_images_a_ae_Holosmith_tango_icon_200px.png':
        return CLASS_COLOR_INDEX_ENGI
    elif actor['img'] == '/cache/https_wiki.guildwars2.com_images_3_3a_Scrapper_tango_icon_200px.png':
        return CLASS_COLOR_INDEX_ENGI
    elif actor['img'] == '/cache/https_wiki.guildwars2.com_images_2_2f_Engineer_tango_icon_200px.png':
        return CLASS_COLOR_INDEX_ENGI
    elif actor[
        'img'] == '/cache/https_wiki.guildwars2.com_images_thumb_8_8a_Mechanist_tango_icon_200px.png_20px-Mechanist_tango_icon_200px.png':
        return CLASS_COLOR_INDEX_ENGI
    elif actor['img'] == '/cache/https_wiki.guildwars2.com_images_c_c9_Deadeye_tango_icon_20px.png' or \
            actor['img'] == '/cache/https_wiki.guildwars2.com_images_b_b0_Deadeye_tango_icon_200px.png':
        return CLASS_COLOR_INDEX_THIEF
    elif actor['img'] == '/cache/https_wiki.guildwars2.com_images_e_e1_Daredevil_tango_icon_20px.png' or \
            actor['img'] == '/cache/https_wiki.guildwars2.com_images_c_ca_Daredevil_tango_icon_200px.png':
        return CLASS_COLOR_INDEX_THIEF
    elif actor['img'] == '/cache/https_wiki.guildwars2.com_images_7_7a_Thief_tango_icon_20px.png' or \
            actor['img'] == '/cache/https_wiki.guildwars2.com_images_1_19_Thief_tango_icon_200px.png':
        return CLASS_COLOR_INDEX_THIEF
    elif actor['img'] == '/cache/https_wiki.guildwars2.com_images_5_5c_Specter_tango_icon_20px.png':
        return CLASS_COLOR_INDEX_THIEF
    elif actor['img'] == '/cache/https_wiki.guildwars2.com_images_1_11_Reaper_tango_icon_20px.png' or \
            actor['img'] == '/cache/https_wiki.guildwars2.com_images_9_95_Reaper_tango_icon_200px.png':
        return 7
    elif actor['img'] == '/cache/https_wiki.guildwars2.com_images_0_06_Scourge_tango_icon_20px.png' or \
            actor['img'] == '/cache/https_wiki.guildwars2.com_images_8_8a_Scourge_tango_icon_200px.png':
        return 7
    elif actor['img'] == '/cache/https_wiki.guildwars2.com_images_7_7f_Harbinger_tango_icon_20px.png':
        return 7
    elif actor['img'] == '/cache/https_wiki.guildwars2.com_images_d_df_Mirage_tango_icon_20px.png' or \
            actor['img'] == '/cache/https_wiki.guildwars2.com_images_a_a9_Mirage_tango_icon_200px.png':
        return CLASS_COLOR_INDEX_MES
    elif actor['img'] == '/cache/https_wiki.guildwars2.com_images_6_62_Virtuoso_tango_icon_20px.png':
        return CLASS_COLOR_INDEX_MES
    elif actor['img'] == '/cache/https_wiki.guildwars2.com_images_f_f4_Chronomancer_tango_icon_20px.png' or \
            actor['img'] == '/cache/https_wiki.guildwars2.com_images_8_8b_Chronomancer_tango_icon_200px.png':
        return CLASS_COLOR_INDEX_MES
    elif actor['img'] == '/cache/https_wiki.guildwars2.com_images_6_60_Mesmer_tango_icon_20px.png':
        return CLASS_COLOR_INDEX_MES
    elif actor['img'] == 'https://i.imgur.com/ck2IsoS.png':  # saul
        return -1
    else:
        print("unknown spec image:", actor['img'])
        return -1


def get_actor_color(actor):
    class_id = get_actor_class(actor)
    if class_id >= 0:
        return class_colors[class_id]
    return [0, 0, 0]


def get_phase_parts(link, phase_blacklist=None, sync_at_phase_end=False):
    if phase_blacklist is None:
        phase_blacklist = []
    j = dps_report_api.get_ei_data_cached(link)
    phases = j.get_main_phase().get_sub_phases(j)
    phase_durations = get_phase_durations_ms(phases, phase_blacklist, sync_at_phase_end)
    return np.array(phase_durations) / sum(phase_durations)


def overlay_transparent(background, overlay, x, y):
    background_width = background.shape[1]
    background_height = background.shape[0]

    if x >= background_width or y >= background_height:
        return background

    h, w = overlay.shape[0], overlay.shape[1]

    if x + w > background_width:
        w = background_width - x
        overlay = overlay[:, :w]

    if y + h > background_height:
        h = background_height - y
        overlay = overlay[:h]

    if overlay.shape[2] < 4:
        overlay = np.concatenate(
            [
                overlay,
                np.ones((overlay.shape[0], overlay.shape[1], 1), dtype=overlay.dtype) * 255
            ],
            axis=2,
        )

    overlay_image = overlay[..., :3]
    mask = overlay[..., 3:] / 255.0

    background[y:y + h, x:x + w] = (1.0 - mask) * background[y:y + h, x:x + w] + mask * overlay_image

    return background


def get_phase_durations_ms(phases, phase_blacklist, sync_at_phase_end) -> np.array:
    phase_durations = []
    last_phase_index = -1
    for i in range(len(phases)):
        if phases[i].name in phase_blacklist:
            continue
        if sync_at_phase_end:
            if last_phase_index >= 0:
                sub = phases[last_phase_index].get_end_time_ms()
                phase_durations.append(phases[i].get_start_time_ms() - sub)
            phase_durations.append(phases[i].get_duration_ms())
        else:
            sub = 0
            if last_phase_index >= 0:
                sub = phases[last_phase_index].get_end_time_ms()
            phase_durations.append(phases[i].get_end_time_ms() - sub)
        last_phase_index = i
    return np.array(phase_durations)


def draw_static_img(frame, height, encounter_name, phase_index, show_group_dps, *, phase_name=None):
    frame = cv2.putText(frame, encounter_name, (0, int(height / 18)),
                        cv2.FONT_HERSHEY_SIMPLEX, height / 400, TEXT_COLOR, 1 + (height // 800), cv2.LINE_AA)
    if phase_name is None:
        phase_name = f"Phase {phase_index + 1}"
    frame = cv2.putText(frame, phase_name, (0, int(height / 8)),
                        cv2.FONT_HERSHEY_SIMPLEX, height / 400, TEXT_COLOR, 1 + (height // 800), cv2.LINE_AA)
    if not show_group_dps:
        dot(frame, [61, 212, 222], height / 30, height / 6.5, height / 38, opacity=0.2)
        dot(frame, [41, 115, 227], height / 30, height / 6.5, height / 60, opacity=0.8)
        dot(frame, [56, 56, 247], height / 30, height / 6.5, height / 120, opacity=1)
        frame = cv2.putText(frame, "10k/40k/100k", (int(height / 20), int(height / 6)),
                            cv2.FONT_HERSHEY_SIMPLEX, height / 800, TEXT_COLOR, 1, cv2.LINE_AA)
    show_class_legend = True
    if show_class_legend:
        class_name = [
            "Guardian",
            "Thief",
            "Ranger",
            "Renegade",
            "Ele",
            "Warrior",
            "Engineer",
            "Necromancer",
            "Mesmer"
        ]
        cv2.putText(frame, "Classes", (int(0.3 * height / 20), int(5 * height / 20)),
                    cv2.FONT_HERSHEY_SIMPLEX, height / 800, TEXT_COLOR, 1, cv2.LINE_AA)
        for i in range(9):
            cv2.rectangle(frame, (int(0.4 * height / 20), int((i * 0.8 + 5.4) * height / 20)),
                          (int(0.6 * height / 20), int((i * 0.8 + 5.6) * height / 20)),
                          class_colors[i], -1)
            cv2.putText(frame, class_name[i], (int(1.1 * height / 20), int((i * 0.8 + 5.7) * height / 20)),
                        cv2.FONT_HERSHEY_SIMPLEX, height / 1200, TEXT_COLOR, 1, cv2.LINE_AA)
    return frame


def get_player_dps_at_time_array(player: dps_report_api.EliteInsightPlayer, duration_ms, target_number: int):
    dps_array = []
    for time_ms in range(0, int(duration_ms), 1000):
        dps = 0
        for ti in range(target_number):
            dps += player.get_target_dps_at_time(time_ms / 1000, target_index=ti)
        dps_array.append(dps)
    return dps_array


def get_player_dps_at_time_from_array(dps_time_array: [int], time_ms: float):
    index = int(time_ms / 1000)
    if index >= len(dps_time_array):
        return 0
    return dps_time_array[index]


def create_movement_video(links, *, boss_id, phase_parts, phase_blacklist=None, video_file='out.avi', height=480,
                          fps=30, duration=30, game_mode, sync_at_phase_end=False, show_group_dps=True,
                          dont_stretch_time=False, record_phase_durations, phase_names, start_still_frames=60):
    if phase_blacklist is None:
        phase_blacklist = []
    crjs = []
    actorss = []
    duration_secs = []
    phase_durationss = []
    target_counts = []
    players_dps_arrays = []
    valid_links = []

    for link in links:
        if boss_id and not (link.endswith(boss_id) or (link.endswith('ai') and boss_id == 'eai') or (
                boss_id == "void" and link.endswith("boss"))):
            # print("Id missmatch skipping", link)
            continue
        j = dps_report_api.get_ei_data_cached(link, silent=False)
        if not j:
            print('could not get json for', link)
            continue
        tn = j.get_target_names()
        if boss_id == 'eai' and 'Dark Ai' in tn and 'Elemental Ai' not in tn:
            # print(link, "is not Elemental Ai but", tn)
            continue
        if boss_id == 'ai' and 'Dark Ai' not in tn and 'Elemental Ai' in tn:
            # print(link, "is not Dark Ai but", tn)
            continue
        crj = get_cr_json(link, silent=False)
        if not crj or not j:
            print('Got no crj data')
            continue
        phases = j.get_main_phase().get_sub_phases(j)
        phase_durations = get_phase_durations_ms(phases, phase_blacklist, sync_at_phase_end)
        if len(phase_durations) != len(phase_parts) and game_mode != HT_CM_GAMEMODE:
            print("Log %s has %d phases, %d expected" % (link, len(phase_durations), len(phase_parts)))
            if len(phase_durations) < len(phase_parts):
                continue
            phase_durations = phase_durations[:len(phase_parts)]
        if len(phase_durations) == 0:  # if no phases add 'fake' phase for whole fight
            phase_durations = [j.duration.total_seconds() * 1000]

        crjs.append(crj)
        actorss.append(crj.get_actors())
        duration_secs.append(j.duration.total_seconds())
        phase_durationss.append(phase_durations)
        players_dps_arrays.append(
            [get_player_dps_at_time_array(p, j.duration.total_seconds() * 1000, len(j.targets)) for p in j.players])
        target_counts.append(len(j.targets))
        valid_links.append(link)
        # print("Duration:", j.get_duration_ms(), "ms")
    record_eid = dps_report_api.get_ei_data_cached(get_records_logs(game_mode)[boss_id])
    if len(phase_parts) == 0:
        phase_parts = [1]  # add part for fake phases
    print("Found", len(crjs), "valid logs")
    actual_width = int(16 / 9 * height)
    bg = cv2.imread(get_img(crjs[0].map_img_link), cv2.IMREAD_UNCHANGED)
    if bg is not None:
        width = int(height * bg.shape[1] / bg.shape[0])
        bg = cv2.resize(bg, (width, height))
    else:
        width = height
    fourcc = VideoWriter_fourcc(*'HFYU')
    video = VideoWriter(video_file, fourcc, float(fps), (actual_width, height))
    minx = 0
    maxx = crjs[0].size[0]
    miny = 0
    maxy = crjs[0].size[1]

    last_frame_time_ms = [0.0] * len(crjs)
    for pre_frame_index in range(start_still_frames):
        frame = np.full((height, actual_width, 3), 50, np.uint8)
        left_offset = int((actual_width - width) / 2)
        if bg is not None:
            frame = overlay_transparent(frame, bg, left_offset, 0)
        if (pre_frame_index + 1) % int(fps * duration / 20) == 0:
            print("Frame", pre_frame_index + 1, "of", int(fps * duration), "phase:", pre_frame_index + 1)
        used_log_number = len(crjs)
        frame = draw_static_img(frame, height, game_mode.get_by_encounter_id(boss_id)['name'], pre_frame_index,
                                show_group_dps, phase_name=phase_names[0])
        frame = cv2.putText(frame, "n=%d" % used_log_number, (0, int(height * 0.99)),
                            cv2.FONT_HERSHEY_SIMPLEX, height / 800, TEXT_COLOR, 1, cv2.LINE_AA)
        video.write(frame)
    for frame_index in range(int(fps * duration)):
        frame = np.full((height, actual_width, 3), 50, np.uint8)
        left_offset = int((actual_width - width) / 2)
        if bg is not None:
            frame = overlay_transparent(frame, bg, left_offset, 0)
        phase_index = 0
        phase_sum = 0
        skipped_number = 0
        while phase_sum + phase_parts[phase_index] < frame_index / (fps * duration):
            phase_sum += phase_parts[phase_index]
            phase_index += 1
        if (frame_index + 1) % int(fps * duration / 20) == 0:
            print("Frame", frame_index + 1, "of", int(fps * duration), "phase:", phase_index + 1)
        if phase_index >= len(phase_parts):
            print('frame ', frame_index, 'has invalid phase')
            continue
        for log_index in range(len(crjs)):
            phase_durations = phase_durationss[log_index]
            if len(phase_durations) <= phase_index:
                skipped_number += 1
        used_log_number = len(crjs) - skipped_number
        frame = draw_static_img(frame, height, game_mode.get_by_encounter_id(boss_id)['name'], phase_index,

                                show_group_dps, phase_name=phase_names[phase_index])
        total_avg_dps = 0
        avg_class_dps_share = np.zeros(10)
        for log_index in range(len(crjs)):
            phase_durations = phase_durationss[log_index]
            if len(phase_durations) <= phase_index:
                continue
            crj = crjs[log_index]
            actors = actorss[log_index]
            fi_offset = sum(phase_parts[:phase_index]) * fps * duration  # frame of phase start
            if dont_stretch_time:
                offset = sum(phase_durations[:phase_index])  # time of phase start
                time_ms = offset + (frame_index - fi_offset) * record_phase_durations[phase_index] / (
                        fps * duration * phase_parts[phase_index])
                if time_ms > offset + phase_durations[phase_index]:
                    used_log_number -= 1
                    continue
            else:
                offset = sum(phase_durations[:phase_index])
                time_ms = offset + phase_durations[phase_index] * (frame_index - fi_offset) / (
                        fps * duration * phase_parts[phase_index])
            player_index = 0
            # print(last_frame_time_ms[log_index], time_ms)
            log_dps = 0
            for actor in actors:
                if actor["type"] == "Player":
                    dps = draw_player(frame, crj, actor, time_ms, target_counts[log_index],
                                      players_dps_arrays[log_index][player_index],
                                      minx, maxx, miny, maxy, height, width, left_offset, last_frame_time_ms[log_index])

                    log_dps += dps
                    class_id = get_actor_class(actor)
                    avg_class_dps_share[class_id] += dps
                    player_index += 1
            total_avg_dps += log_dps
            last_frame_time_ms[log_index] = time_ms
        if show_group_dps:
            if used_log_number > 0:
                total_avg_dps /= used_log_number
                avg_class_dps_share /= used_log_number
            if total_avg_dps > 0:
                avg_class_dps_share /= total_avg_dps
            if total_avg_dps > 100000:
                dps_str = f" {total_avg_dps / 1000:.0f}kDPS"
            else:
                dps_str = f"{total_avg_dps / 1000.0:4.1f}kDPS"
            size = clamp(2.5, sqrt(total_avg_dps) * height / 12000, height / 10)
            if total_avg_dps > 0:
                special_dot(frame, class_colors, avg_class_dps_share, height / 25, height / 5.8, size, opacity=0.8)
            else:
                dot(frame, [41, 115, 227], height / 25, height / 5.8, size, opacity=0.8)
            t_size, _ = cv2.getTextSize(dps_str, cv2.FONT_HERSHEY_SIMPLEX, height / 800, 1)
            frame = cv2.putText(frame, dps_str, (int(height / 3.8 - t_size[0]), int(height / 5.3)),
                                cv2.FONT_HERSHEY_SIMPLEX, height / 800, TEXT_COLOR, 1, cv2.LINE_AA)
        frame = cv2.putText(frame, "n=%d" % used_log_number, (0, int(height * 0.99)),
                            cv2.FONT_HERSHEY_SIMPLEX, height / 800, TEXT_COLOR, 1, cv2.LINE_AA)
        video.write(frame)
    video.release()


class DrawMode(Enum):
    Dots = 0
    Lines = 1


last_frame_time = 0


def draw_player(frame, crj: dps_report_api.CombatReplayData, actor, time_ms: int, log_target_counts,
                players_dps_arrays: [int],
                minx: int, maxx: int, miny: int, maxy: int, height: int, width: int, left_offset: int,
                last_frame_time_ms, draw_mode=DrawMode.Lines):
    pos = crj.get_position_interpolate(actor["id"], time_ms)
    if pos:
        # if frame_index > 2 and (pos[0] < 0 or pos[0] > maxx - 1) and not disable_invalid_pos:
        #    print(f"invalid x value {pos[0]:.2f} in frame {frame_index} in log {valid_links[i]}")
        #    disable_invalid_pos = True
        # if frame_index > 2 and (pos[1] < 0 or pos[1] > maxy - 1) and not disable_invalid_pos:
        #    print(f"invalid y value {pos[1]:.2f} in frame {frame_index} in log {valid_links[i]}")
        #    disable_invalid_pos = True
        x = clamp(minx, pos[0], maxx - 1)
        y = clamp(miny, pos[1], maxy - 1)
        color = get_actor_color(actor)
        dps = get_player_dps_at_time_from_array(players_dps_arrays, time_ms)
        pxl_x = x * width / maxx + left_offset
        pxl_y = y * height / maxy
        if draw_mode == DrawMode.Dots:
            size = clamp(2.5, sqrt(dps) * height / 12000, height / 10)
            dot(frame, color, pxl_x, pxl_y, size, opacity=0.35)
        elif draw_mode == DrawMode.Lines:
            old_pos = crj.get_position_interpolate(actor["id"], last_frame_time_ms)
            if old_pos:
                old_x = clamp(minx, old_pos[0], maxx - 1)
                old_y = clamp(miny, old_pos[1], maxy - 1)
                old_pxl_x = old_x * width / maxx + left_offset
                old_pxl_y = old_y * height / maxy
                cv2.line(frame, (int(pxl_x), int(pxl_y)), (int(old_pxl_x), int(old_pxl_y)), color, 2,
                         lineType=cv2.LINE_AA)
        return dps
    else:
        return 0


def get_records_logs(gm):
    if gm == fractals:
        return storage.fractal_record_logs
    elif gm == raids:
        return storage.record_logs
    elif gm == strikes:
        return storage.strike_record_logs
    elif gm == HT_CM_GAMEMODE:
        return {"void": "https://dps.report/yeRl-20221009-203453_void"}
        #return {"void": "https://dps.report/QSg8-20220826-225229_void"}


def main():
    game_mode = HT_CM_GAMEMODE
    height = 1440 #- 360
    fps = 60 #// 2
    duration = 30 #// 2
    if game_mode == fractals:
        logs = storage.get_frac_logs() + storage.old_frac_logs
    elif game_mode == raids:
        logs = []  # logs = storage.raid_logs
    elif game_mode == strikes:
        logs = storage.strike_logs + list(filter(lambda x: x is not None, [log.get_dps_report_url() for log in
                                                                           storage.get_strikes_from_log_manager()]))
    elif game_mode == HT_CM_GAMEMODE:
        from raid_report.ht_reports import get_kerchu_logs_url, get_kerchu2_logs_url, get_rpfy_log_urls
        # logs = get_rpfy_log_urls().values()
        logs = get_kerchu2_logs_url().values()
    else:
        logs = []
    video_parts = []
    c = True
    for boss in game_mode.encounters:
        if game_mode == raids:
            lmc = logmanagercachereader.LogManagerCache()
            logs = lmc.get_logs(encounter_filter=[boss["lmid"]], success_filter=[True])
            logs = (log.get_dps_report_url() for log in logs)
            logs = list(filter(lambda x: x is not None, logs))
        if boss['id'] in ['tc', 'bk', 'rr']:
            continue
        if boss['id'] == "dhuum":
            c = False
            # continue
        if c:
            pass
            # continue
        tmp_video_file_name = VIDEO_CACHE_PATH + 'out_%s.avi' % boss['id']
        sync_on_end = False
        blacklist = []
        if boss['id'] == 'dei':
            blacklist = ['Burst 1', 'Burst 2', 'Burst 3', 'Burst 4', 'Burst 5', 'Burst 6', 'Burst 7', '100% - 10%']
        if boss['id'] == 'kc':
            blacklist = ['Phase 1', 'Phase 2', 'Phase 3']
        if boss['id'] == 'qpeer':
            blacklist = ['Magma Drop 1', 'Magma Drop 2', 'North Pylon', 'SouthWest Pylon', 'SouthEast Pylon']
        if boss['id'] == 'skor':
            blacklist = ['Split 1', 'Split 2']
            sync_on_end = True
        if boss['id'] == 'arriv':
            blacklist = ['Split 1', 'Split 2']
            sync_on_end = True
        if boss['id'] == 'siax':
            sync_on_end = True
        if boss['id'] == 'enso':
            sync_on_end = True
        if boss['id'] == 'frae':
            blacklist = ['Icebrood']
        if boss['id'] == 'falln':
            # blacklist every thing
            blacklist = ['Phase 1', 'Enrage', 'Phase 2', 'Phase 3', 'Voice and Claw 1']
        if boss['id'] == 'void':
            # blacklist every thing
            blacklist = ['Soo-Won']
        record_eid = dps_report_api.get_ei_data_cached(get_records_logs(game_mode)[boss['id']])
        records_phase_times = get_phase_durations_ms(record_eid.get_main_phase().get_sub_phases(record_eid),
                                                     phase_blacklist=blacklist, sync_at_phase_end=sync_on_end)
        phase_parts = get_phase_parts(get_records_logs(game_mode)[boss['id']], phase_blacklist=blacklist,
                                      sync_at_phase_end=sync_on_end)

        print('Phase parts for', boss['name'], 'are', phase_parts)
        phase_names = [phase.name for phase in record_eid.get_main_phase().get_sub_phases(record_eid)]
        for bli in blacklist:
            if bli in phase_names:
                phase_names.remove(bli)
        print("phases are ", phase_names)
        create_movement_video(logs, boss_id=boss['id'], phase_parts=phase_parts, phase_blacklist=blacklist,
                              video_file=tmp_video_file_name, height=height, fps=fps, duration=duration,
                              game_mode=game_mode, sync_at_phase_end=sync_on_end,
                              record_phase_durations=records_phase_times, dont_stretch_time=game_mode == HT_CM_GAMEMODE,
                              phase_names=phase_names, start_still_frames=0)
        # insert elemetal ai
        if boss['id'] == 'ai':
            bid = 'eai'
            phase_parts = get_phase_parts(get_records_logs(game_mode)[bid], phase_blacklist=blacklist,
                                          sync_at_phase_end=True)
            eai_video_file_name = VIDEO_CACHE_PATH + 'out_%s.avi' % bid
            # create_movement_video(storage.elemental_ai_logs_new + storage.old_frac_logs, boss_id=bid,
            #                      phase_parts=phase_parts, phase_blacklist=blacklist,
            #                      video_file=eai_video_file_name, height=600, fps=30, duration=28, game_mode=game_mode,
            #                      sync_at_phase_end=True)
            video_parts.append(eai_video_file_name)
        video_parts.append(tmp_video_file_name)
    convert_video_file(video_parts)


if __name__ == '__main__':
    main()
