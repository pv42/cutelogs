import html

from gw2 import raids, fractals
import splittlogparser
from splittlogparser import SplittLog
import cairosvg


def get_time_string(time_ms):
    if time_ms == 0:
        return " -:--.---"
    return '%2d:%02d.%03d' % (time_ms / 60000, (time_ms / 1000) % 60, time_ms % 1000)


def get_diff_time_string(time_ms):
    if time_ms == 0:
        return ''
    sign = '+'
    if time_ms < 0:
        time_ms = -time_ms
        sign = '-'
    if time_ms < 10000:  # less than a 10 s
        return '%s%d.%03d' % (sign, (time_ms / 1000) % 60, time_ms % 1000)
    elif time_ms < 60000:  # less than a min
        return '%s%2d.%02d' % (sign, (time_ms / 1000) % 60, time_ms / 10 % 100)
    else:
        return '%s%2d:%02d' % (sign, time_ms / 60000, (time_ms / 1000) % 60)


def splitts2svg_inner(infilepath, *, y_offset=2):
    sp_log = SplittLog(infilepath)
    out_str = '<text x="5" y="%d" style="font-size: 12px;">%s</text>' % (y_offset + 10, sp_log.name)
    index = 0
    y = 22 + y_offset
    for splitt in sp_log.splitts:
        out_str += '<text x="5" y="%d" >%s</text>' % (y, html.escape(splitt['name']))
        if sp_log.is_splitt_better(index):
            if sp_log.is_splitt_improving(index):
                color_class = 'dark-green'
            else:
                color_class = 'dark-green'
        elif sp_log.is_splitt_worse(index):
            if sp_log.is_splitt_improving(index):
                color_class = 'light-red'
            else:
                color_class = 'dark-red'
        else:
            color_class = ''
        if sp_log.is_best_segment(index):
            color_class = 'golden'
        if splitt['time'] > 0:
            diff_str = get_diff_time_string(sp_log.get_time_delta(index))
            out_str += '<text x="160" y="%d" class="%s" text-anchor="end">%s</text>' % \
                       (y, color_class, diff_str)

        if splitt['time'] > 10 * 60 * 1000:
            out_str += '<text x="215" y="%d" textLength="45" text-anchor="end">%s</text>\n' % (y, get_time_string(splitt['time']))
        else:
            out_str += '<text x="215" y="%d" textLength="42" text-anchor="end">%s</text>\n' % (y, get_time_string(splitt['time']))
        y += 10
        index += 1
    return out_str, y


def splitts2svg(infilepaths):
    out_str = '''<?xml version="1.0" encoding="UTF-8"?>
<svg xmlns="http://www.w3.org/2000/svg" 
     xml:space="preserve"
width="220" height="[HEIGHT]"
style="background-color:#333;font-family: sans-serif;font-size: 10px;fill: white;">
    <style type="text/css">
        .dark-red {
            fill: #cc1200;
        }
        .light-red {
            fill: #cc5c52;
        }
        .light-green {
            fill: #52cc73;
        }
        .dark-green {
            fill: #00cc36;
        }
        .golden {
            fill: #d8af1f;
        }
    </style>'''
    y_offset = 2
    for infilepath in infilepaths:
        part_str, y_offset = splitts2svg_inner(infilepath, y_offset=y_offset)
        out_str += part_str
    out_str += '</svg>'
    if y_offset < 8:
        y_offset = 8
    out_str = out_str.replace('[HEIGHT]', str(y_offset - 7))
    return out_str


def create_for_fractals(*, max_log_age):
    in_files = []
    for frac in fractals.fractals:
        f = splittlogparser.get_splitt_for_instance(frac, max_log_age=max_log_age, mode=fractals.fractals)
        if f:
            in_files.append(f)
    outfilepath = "out.svg"
    out_str = splitts2svg(in_files)
    outfile = open(outfilepath, 'w')
    outfile.write(out_str)
    outfile.close()
    return len(in_files)


def create_for_raid(*, max_log_age):
    in_files = []
    for wing in raids.wings:
        f = splittlogparser.get_splitt_for_instance(wing, max_log_age=max_log_age, mode=raids.wings)
        if f:
            in_files.append(f)
    outfilepath = "out.svg"
    out_str = splitts2svg(in_files)
    outfile = open(outfilepath, 'w')
    outfile.write(out_str)
    outfile.close()
    return len(in_files)


def create_fractal_png(*, max_log_age=240):
    if create_for_fractals(max_log_age=max_log_age) > 0:
        cairosvg.svg2png(bytestring=open("out.svg").read().encode('utf-8'), scale=3, write_to="out.png",
                         background_color='#333')
        return "out.png"


def create_raid_png(*, max_log_age=300):
    if create_for_raid(max_log_age=max_log_age) > 0:
        cairosvg.svg2png(bytestring=open("out.svg").read().encode('utf-8'), scale=3, write_to="out.png",
                         background_color='#333')
        return "out.png"


if __name__ == '__main__':
    create_raid_png(max_log_age=2000)
