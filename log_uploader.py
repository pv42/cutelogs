import os
import sys
from datetime import datetime


def get_new_logs(max_log_age=240, log_path=None):
    if not log_path:
        log_path = os.environ['userprofile'] + '\\Documents\\Guild Wars 2\\addons\\arcdps\\arcdps.cbtlogs'
    now = datetime.now()
    logs = []
    for path, dirs, files in os.walk(log_path):
        newest = '<nothing>'
        newest_age = sys.maxsize
        for file in files:
            absolute_path = os.path.join(path, file)
            mtime = os.path.getmtime(absolute_path)
            mtimestamp = datetime.fromtimestamp(mtime)
            file_age = now - mtimestamp
            file_age_hrs = file_age.days * 24 + file_age.seconds / 3600.0
            if newest_age > file_age_hrs:
                newest = absolute_path
                newest_age = file_age_hrs
        if newest_age != sys.maxsize and newest_age < max_log_age / 60.0:
            logs.append(newest)
    return logs


if __name__ == '__main__':
    logs = get_new_logs(max_log_age=240)
    print('found %d logs' % len(logs))
    for log in logs:
        print(log)
