

import os
import sys
from pathlib import Path
from datetime import datetime


class SplittLog():
    def __init__(self, filename):
        infile = open(filename, 'r')
        data = infile.read()
        infile.close()
        rows = data.split('\n')
        self.name = rows[0].split(';')[0]
        self.splitts = []
        for row in rows[2:]:
            cells = row.split(';')
            if len(cells) <= 1:
                break
            splitt_name = cells[1]
            splitt_time = int(cells[2])
            splitt_best = int(cells[3])
            best_segment = int(cells[4])
            self.splitts.append(
                {'name': splitt_name, 'time': splitt_time, 'best_run': splitt_best, 'best_segment': best_segment})

    def is_splitt_better(self, splitt_index):
        return self.get_time_delta(splitt_index) < 0

    def is_splitt_worse(self, splitt_index):
        return self.get_time_delta(splitt_index) > 0

    def is_splitt_improving(self, splitt_index):
        if splitt_index == 0:
            return False
        this_splitt = self.splitts[splitt_index]
        last_splitt = self.splitts[splitt_index - 1]
        return this_splitt['time'] - this_splitt['best_run'] < last_splitt['time'] - last_splitt['best_run']

    def is_best_segment(self, splitt_index):
        last_sp_time = 0
        if splitt_index > 0:
            last_sp_time = self.splitts[splitt_index - 1]['time']
        return self.splitts[splitt_index]['time'] - last_sp_time < self.splitts[splitt_index]['best_segment'] and \
               self.splitts[splitt_index]['best_segment'] > 0

    def get_time_delta(self, splitt_index):
        if self.splitts[splitt_index]['best_run'] == 0:
            return 0
        return self.splitts[splitt_index]['time'] - self.splitts[splitt_index]['best_run']


def get_splitt_for_instance(id, *, max_log_age=240, mode):
    path = Path.home() / 'Documents' / 'Guild Wars 2' / 'addons' / 'gw2autosplits' / 'logs' / mode[id][
        'splittset_name']
    if not path.exists():
        print('Path for splittset not found')
        print(path)
        return
    now = datetime.now()
    logs = []
    if not max_log_age:
        max_log_age = sys.maxsize
    for path, dirs, files in os.walk(path):
        newest = '<nothing>'
        newest_age = sys.maxsize
        for file in files:
            absolute_path = os.path.join(path, file)
            mtime = os.path.getmtime(absolute_path)
            mtimestamp = datetime.fromtimestamp(mtime)
            file_age = now - mtimestamp
            file_age_hrs = file_age.days * 24 + file_age.seconds / 3600.0
            if newest_age > file_age_hrs:
                newest = absolute_path
                newest_age = file_age_hrs
        if newest_age != sys.maxsize and newest_age < max_log_age / 60.0:
            logs.append(newest)
    if len(logs) > 0:
        return logs[0]
    else:
        return


