import math
import os
import re
from datetime import timedelta, date

from enum import Enum

from typing import List, Dict

import dps_report_api
from dps_report_api import EliteInsightPlayer, EliteInsightData, CombatReplayData, get_ei_data_cached
import gw2.gw2
from gw2.gw2 import MIGHT_BUFF_ID, QUICKNESS_BUFF_ID, ALACRITY_BUFF_ID, WEAPON_SWAP_SKILL_ID, DODGE_SKILL_ID1, DODGE_SKILL_ID2, \
    BEEHIVE_SKILL_ID, MIRAGE_CLOAK, SIGNET_OF_WRATH_SKILL, IMPOSSIBLE_ODDS_HIT_ID, PROTECTION_BUFF_ID
import gw2.raids
from logmanagercachereader import LogManagerLog, correct_ch_id_name, EliteSpecialization
import orjson as json

from raid_report.character_builds import get_player_build

CACHE_PATH = "E:\\GW2LogsData\\"

DAMAGE_MOD_ID_SCHOLAR = -202180908

skill_detail_cache = {}


def get_skill_name_by_id(skill_id) -> str:
    return skill_detail_cache[skill_id].name


def get_skill_icon_by_id(skill_id) -> str:
    return skill_detail_cache[skill_id].get_icon_url()


def get_cr_json(url, *, silent=True):
    if not re.match(r'https://dps\.report/[a-zA-z0-9]{4}-\d{8}-\d{6}_[a-z]{2,5}', url):
        print('invalid url: "%s"' % url)
        return
    url_id_part = url.split('https://dps.report/')[1]
    if not os.path.exists(CACHE_PATH + 'crJsons'):
        os.mkdir(CACHE_PATH + 'crJsons')
    jpath = (CACHE_PATH + 'crJsons/%s.json') % url_id_part
    if os.path.exists(jpath):
        try:
            f = open(jpath, 'r')
            j = json.loads(f.read())
            f.close()
            return dps_report_api.CombatReplayData(j)
        except json.JSONDecodeError:
            print("Failed to load file :", url_id_part)
    else:
        if not silent:
            print('Downloading cr data %s' % url)
        api = dps_report_api.DpsReportAPI()
        j = api.get_cr_json_data_by_link(url, silent=silent)
        if j:
            f = open(jpath, 'wb')
            f.write(json.dumps(j.data))
            f.close()
        return j


def create_player_actor_id_dict(ei_data: EliteInsightData, crj):
    player_actor_dict = {}
    if not ei_data:
        return player_actor_dict
    players = ei_data.players
    player_index = 0
    for actor in crj.get_actors():
        if actor["type"] == "Player":
            if not players[player_index].is_fake:
                player_actor_dict[players[player_index].name] = actor["id"]
                if players[player_index].profession_name not in actor["img"]:
                    pass
                    # print("WRN: match", players[player_index].get_profession_name(), "to", actor["img"])
            player_index += 1
    return player_actor_dict


def get_player_position(player: EliteInsightPlayer, ei_data: EliteInsightData, crj: CombatReplayData,
                        player_actor_id_dict, time_ms):
    if ei_data.has_combat_replay_data():
        return player.get_position_interpolate(time_ms)
    else:
        if player.name in player_actor_id_dict:
            return crj.get_position_interpolate(player_actor_id_dict[player.name], time_ms)


def get_player_position_keyframe(player: EliteInsightPlayer, ei_data: EliteInsightData, crj: CombatReplayData,
                                 player_actor_id_dict: dict,
                                 time_index: int):
    if ei_data.has_combat_replay_data():
        return player.get_position_keyframe(time_index)
    else:
        if player.name in player_actor_id_dict:
            return crj.get_position_keyframe(player_actor_id_dict[player.name], time_index)


def get_player_rotation_keyframe(player: EliteInsightPlayer, ei_data: EliteInsightData, crj: CombatReplayData,
                                 player_actor_id_dict: dict,
                                 time_index: int):
    if ei_data.has_combat_replay_data():
        return player.get_orientation_keyframe(time_index)
    else:
        if player.name in player_actor_id_dict:
            return crj.get_rotation_keyframe(player_actor_id_dict[player.name], time_index)


def create_boss_specific_stats(boss_id, log, ei_data, ei_has_cr_data, crj, player_actor_dict, stats):
    if boss_id == "vg":
        stats.vg_blue_tps += sum(ei_data.get_mechanic_counts("Boss TP").values())
        stats.vg_stood_in_green += sum(ei_data.get_mechanic_counts("Green").values())
    elif boss_id == "gors":
        stats.gorse_eggs += sum(ei_data.get_mechanic_counts("Egg").values())
        stats.gorse_slam_hits += sum(ei_data.get_mechanic_counts("Slam").values())
    elif boss_id == "sab":
        radius = 300
        delay = 3000
        bombs = ei_data.get_mechanic_events("Timed Bomb")
        if ei_has_cr_data:
            scale = 1.0 / ei_data.combat_replay_inch_to_pxl
        else:
            scale = 9.345794392523365
        for bomb_ev in bombs:
            bomber = ei_data.get_player_by_name(bomb_ev["actor"])
            b_pos = get_player_position(bomber, ei_data, crj, player_actor_dict, bomb_ev["time"] + delay)
            if not b_pos:
                continue
            for player in ei_data.players:
                p_pos = get_player_position(player, ei_data, crj, player_actor_dict,
                                            bomb_ev["time"] + delay)
                if not p_pos:
                    continue
                dist = math.sqrt((b_pos[0] - p_pos[0]) ** 2 + (b_pos[1] - p_pos[1]) ** 2) * scale
                if player.name != bomber.name and dist < radius:
                    # print(bomber.get_name(), "bombed", player.get_name())
                    stats.sab_hit_by_bomb += 1
                    if player.profession_name == "Druid":
                        stats.sab_druids_hit_by_bomb += 1
    elif boss_id == "trio":
        for player in ei_data.players:
            stats.trio_beehives_thrown += len(player.get_skill_cast(BEEHIVE_SKILL_ID))
    elif boss_id == "matt":
        deaths = ei_data.get_mechanic_events("Dead")
        sac_evs = ei_data.get_mechanic_events("Sacrifice")
        if sac_evs and deaths:
            for ev in sac_evs:
                name = ev["actor"]
                time_ms = ev["time"]
                for death_ev in deaths:
                    if death_ev["actor"] == name and time_ms < death_ev["time"] < time_ms + 10000:
                        acc_name = ei_data.get_player_by_name(name).account_name
                        if acc_name not in stats.mat_successful_sacs_by_account:
                            stats.mat_successful_sacs_by_account[acc_name] = 0
                        stats.mat_successful_sacs_by_account[acc_name] += 1
                        break
    elif boss_id == "xera":
        if ei_data.get_main_phase().get_sub_phases(ei_data)[0] == "Pre Event":
            phases = ei_data.get_main_phase().get_sub_phases(ei_data)[1].get_sub_phases(ei_data)
        else:
            phases = ei_data.get_main_phase().get_sub_phases(ei_data)
        p1end = None
        p2start = None
        if len(phases) == 3 or (len(phases) == 2 and phases[1].name != "Gliding"):
            p1end = phases[0].get_end_time_ms()
            p2start = phases[-1].get_start_time_ms()
        elif len(phases) == 2 and phases[1].name == "Gliding":
            p1end = phases[0].get_end_time_ms()
            p2start = phases[-1].get_end_time_ms()

        if p1end:
            deaths = ei_data.get_mechanic_events("Dead")
            if deaths:
                for death_ev in deaths:
                    if p1end < death_ev["time"] < p2start + 5000:
                        stats.xera_glide_deaths += 1
        else:
            print("Phase detect failed:", log.get_dps_report_url())
    elif boss_id == "dei":
        tears = ei_data.get_mechanic_counts("Tear")
        for player_name in tears:
            stats.deimos_tears_collected += tears[player_name]
            player = ei_data.get_player_by_name(player_name)
            stats.pp_tears_collected.add_data(player.account_name, tears[player_name])
        stats.deimos_oil_triggered += sum(ei_data.get_mechanic_counts("Oil T.").values())
    elif boss_id == "dhuum":
        stats.dhuum_bombs += sum(ei_data.get_mechanic_counts("Bomb").values())
        stats.dhuum_chains += sum(ei_data.get_mechanic_counts("Shackles").values())
    elif boss_id == "twins":
        for player in filter(lambda p: p.profession_name, ei_data.players):
            for cast in player.get_skill_cast(SIGNET_OF_WRATH_SKILL):
                if cast.cast_duration != - cast.time_gained:
                    stats.twins_signet_of_wrath += 1
    elif boss_id == "qadim":
        stats.qadim_hit_on_line += sum(ei_data.get_mechanic_counts("F.Dance").values())
    elif boss_id == "adina":
        stats.adina_blinds += sum(ei_data.get_mechanic_counts("R.Blind").values())
    elif boss_id == "qpeer":
        stats.qpeer_hit_on_line += sum(ei_data.get_mechanic_counts("Pattern.H").values())


def create_movement_stats(boss_id, ei_data, crj, player_actor_dict, stats):
    if not ei_data.has_combat_replay_data() and crj is None:
        print("No movement data")
        return
    for player in ei_data.players:
        if player.profession_name == "NPC" or player.is_fake:
            continue
        time_index = 0
        pos = get_player_position_keyframe(player, ei_data, crj, player_actor_dict, time_index)
        next_pos = get_player_position_keyframe(player, ei_data, crj, player_actor_dict, time_index + 1)
        player_movement = 0
        known_boss_scales = {
            "dhuum": 6.02409,
            "matt": 3.49650,
            "dei": 4.42478,
            "eyes": 8.33333,
            "cairn": 3.66300,
            "mo": 3.40136,
            "sam": 5.46448,
            "gors": 6.06061,
            "sab": 9.34579,
            "sloth": 7.63359,
            "kc": 4.31034,
            "xera": 8.06452,
            "qadim": 10.41667,
            "adina": 3.27869,
            "bk": 6.41026,
            "rr": 26.31579,
            "sh": 4.38596,
            "qpeer": 6.94444,
            "eater": 4.54545,
            "vg": 4.29185,
            "tc": 15.38462,
            "trio": 7.29927,
            "sabir": 6.57895,
            "ca": 5.40541,
            "twins": 12.65823,
            "esc": 20.0,
        }
        if ei_data.has_combat_replay_data():
            scale = 1.0 / ei_data.combat_replay_inch_to_pxl
            if boss_id not in known_boss_scales:
                print(f"unknown boss scale for\"{boss_id}\", using {scale:.5f}")
        else:
            if boss_id in known_boss_scales:
                scale = known_boss_scales[boss_id]
            else:
                # print("No scale for" ,boss_id)
                scale = 0
        # print(scale, log.get_dps_report_url())
        while pos and next_pos:
            player_movement += math.sqrt(
                (pos[0] - next_pos[0]) ** 2 + (pos[1] - next_pos[1]) ** 2) * scale
            time_index += 1
            pos = next_pos
            next_pos = get_player_position_keyframe(player, ei_data, crj, player_actor_dict, time_index + 1)
        stats.total_units_moved += player_movement
        stats.pp_avg_move_units.add_data(player.account_name, player_movement)
        # rotations
        time_index = 0
        rot = get_player_rotation_keyframe(player, ei_data, crj, player_actor_dict, time_index)
        r0 = rot
        c_rot = r0
        next_rot = get_player_rotation_keyframe(player, ei_data, crj, player_actor_dict, time_index + 1)
        while rot and next_rot:
            time_index += 1
            diff = next_rot - rot
            if diff > 180:
                diff -= 360
            if diff < -180:
                diff += 360
            c_rot += diff
            if c_rot - r0 > 360:
                c_rot -= 360
                stats.cw_rotations += 1
            if c_rot - r0 < -360:
                c_rot += 360
                stats.ccw_rotations += 1
            rot = next_rot
            next_rot = get_player_rotation_keyframe(player, ei_data, crj, player_actor_dict, time_index + 1)


def create_personal_stats(boss_id, ei_data: EliteInsightData, crj, player_actor_dict, stats, url):
    deaths = []
    death_evs = ei_data.get_mechanic_events("Dead")
    if death_evs and ei_data.is_success:
        for dead_ev in death_evs:
            if dead_ev["time"] < ei_data.get_duration_ms():
                deaths.append(dead_ev["actor"])
    for player in ei_data.players:
        acc_name = player.account_name
        if ei_data.is_success:
            if boss_id == "twins":
                stats.pp_avg_dps.add_data(acc_name, player.get_all_targets_dps())
                stats.pp_max_dps.add_data(acc_name, player.get_all_targets_dps())
            else:
                stats.pp_avg_dps.add_data(acc_name, player.get_target_dps())
                stats.pp_max_dps.add_data(acc_name, player.get_target_dps())
        stats.pp_sum_dmg.add_data(acc_name, player.get_total_damage())
        stats.pp_condi_cleanse_out.add_data(acc_name, player.get_condi_cleanse_out())
        if player.get_total_healing_out() > 0:
            stats.pp_sum_heal.add_data(acc_name, player.get_total_healing_out())
            stats.pp_max_heal.add_data(acc_name, player.get_total_healing_out())
            stats.pp_avg_hps.add_data(acc_name, player.get_hps())
        stats.pp_res_time.add_data(acc_name, player.get_resurrect_time_s())
        stats.pp_downed_count.add_data(acc_name, player.get_down_count())
        stats.pp_downed_duration.add_data(acc_name, player.get_down_duration_ms())
        stats.pp_presence.add_data(acc_name, 1)
        stats.pp_uniqe_specs_played.add_data(acc_name, player.profession_name)
        stats.pp_max_cc.add_data(acc_name, player.get_total_breakbar_damage())
        stats.pp_avg_cc.add_data(acc_name, player.get_total_breakbar_damage())
        stats.pp_dist_to_sq_mid.add_data(acc_name, -player.get_dist_sq_center())
        len_ms = ei_data.get_duration_ms()
        avg_hp = 0
        for i in range(math.ceil(len_ms / 1000)):
            avg_hp += player.get_health_percent_at_time(i * 1000)
        avg_hp /= math.ceil(len_ms / 1000)
        stats.pp_avg_hp_percent.add_data(acc_name, avg_hp)
        if ei_data.is_success:
            if player.name in deaths:
                stats.pp_kill_survived_percent.add_data(acc_name, 0)
            else:
                stats.pp_kill_survived_percent.add_data(acc_name, 100)
            if acc_name in name_acc_dict:
                name = name_acc_dict[acc_name]
                # print( name, stats.pp_kill_survived_percent[name])


name_acc_dict = {
    "Kerchu.1906": "Kerchu",
    "NotOverlyCheesy.9427": "Ent",
    "Aegas.1069": "Ewayne",
    "CaptainBloodloss.5732": "Ines",
    "rivvyn.6257": "Rivvyn",
    "Warotrix.9124": "Waro",
    "PVzweiundvierzig.7381": "pv42",
    "FunNinja.7403": "Toucan",
    # "Dr Cynosure.1840": "Cyno",
    "RDEF.8513": "RDEF",
    "Lele.3165": "Lele",
    "Mihkael.1954": "Lele",
    "KaptainKittens.6135": "Ben",
    "MoreEwayne.1732": "Ewayne",
    "CaptainSalmon.4361": "Ines",
    "Ker.5304": "Kerchu",
    "flare.8627": "Flare",
    "RisayaKinan.9876": "Risaya"
}


def get_name_from_account(acc_name, name_acc_dict):
    return name_acc_dict[acc_name]


class PersonalStatsMode(Enum):
    MAXIMUM = 0
    SUMME = 1
    AVERAGE = 2
    COUNT_UNIQUE = 3


class PerPersonStat:
    def __init__(self, mode: PersonalStatsMode, *, pp_name_acc_dict=None):
        if pp_name_acc_dict is None:
            pp_name_acc_dict = name_acc_dict
        self.mode = mode
        self.data = {}
        self.data_count = {}
        self.name_acc_dict = pp_name_acc_dict
        for acc in self.name_acc_dict:
            if mode == PersonalStatsMode.COUNT_UNIQUE:
                self.data[pp_name_acc_dict[acc]] = set()
            else:
                self.data[pp_name_acc_dict[acc]] = 0
            self.data_count[pp_name_acc_dict[acc]] = 0

    def add_data(self, acc_name: str, value):
        if acc_name in self.name_acc_dict:
            name = get_name_from_account(acc_name, self.name_acc_dict)
            self.data_count[name] += 1
            if self.mode == PersonalStatsMode.MAXIMUM:
                if value > self.data[name]:
                    self.data[name] = value
            elif self.mode == PersonalStatsMode.COUNT_UNIQUE:
                self.data[name].add(value)
            else:
                self.data[name] += value
        else:
            pass # print(acc_name, "is not in dict")

    def get_arg_best(self):
        arg_best = ""
        best = -1e99
        for name in self.data:
            if self[name] > best and self.data_count[name] > 0:
                arg_best = name
                best = self[name]
        return arg_best

    def get_arg_top(self, count):
        arg_top = [""] * (count+1)
        top = [-1e99] * (count+1)
        for name in self.data:
            if self.data_count[name] > 0:
                arg_top[count] = name
                top[count] = self[name]
                top, arg_top = map(list, zip(*sorted(zip(top, arg_top), reverse=True)))
        return arg_top[:-1]

    def __getitem__(self, acc_name):
        if self.mode == PersonalStatsMode.AVERAGE:
            if self.data_count[acc_name] > 0:
                return self.data[acc_name] / self.data_count[acc_name]
            else:
                return 0
        elif self.mode == PersonalStatsMode.COUNT_UNIQUE:
            return len(self.data[acc_name])
        else:
            return self.data[acc_name]

    def __iter__(self):
        return iter(self.data)


class StatisticsData:
    def __init__(self):
        self.dodges = 0
        self.encounters_completed = 0
        self.wipes = 0
        self.wipes_per_boss = {}
        self.time_wasted_per_boss = {}
        self.total_time = timedelta(0)
        self.total_kill_time = timedelta(0)
        self.total_wipe_time = timedelta(0)
        self.total_walk_time = timedelta(0)
        self.skill_cast_counts = {}
        self.time_per_wing = [timedelta(0)] * 7
        self.total_cc = 0
        self.total_power_dmg = 0
        self.total_condi_dmg = 0
        self.total_damage = 0
        self.weapon_swaps = 0
        self.total_skills_canceled = 0
        self.total_cancel_time = timedelta(0)
        self.total_target_damage_in_kills = 0
        self.total_time_might = timedelta(0)
        self.total_time_quickness = timedelta(0)
        self.total_time_alac = timedelta(0)
        self.low_man_kills = 0
        self.total_gold_created = 0
        self.cm_kills = 0
        self.unique_characters_seen = set()
        self.unique_accounts_seen = set()
        self.unique_guilds_seen = set()
        self.total_units_moved = 0
        self.total_downstates = 0
        self.total_deaths = 0
        self.total_healing = 0
        self.cw_rotations = 0
        self.ccw_rotations = 0
        self.scholar_uptime_active = 0
        self.scholar_uptime_total = 0
        self.scholar_damage_increase = 0
        self.resurrect_time = timedelta(0)
        self.total_damage_taken = 0
        self.total_damage_mitigated_barrier = 0
        self.impossible_odds_hits = 0
        self.build_usage_counts = {}
        self.build_usage_by_acc = {}
        # boss_specific
        self.vg_blue_tps = 0
        self.vg_stood_in_green = 0
        self.gorse_eggs = 0
        self.gorse_slam_hits = 0
        self.sab_hit_by_bomb = 0
        self.sab_druids_hit_by_bomb = 0
        self.trio_beehives_thrown = 0
        self.mat_successful_sacs_by_account = {}
        self.xera_glide_deaths = 0
        self.deimos_tears_collected = 0
        self.deimos_oil_triggered = 0
        self.dhuum_chains = 0
        self.dhuum_bombs = 0
        self.twins_signet_of_wrath = 0
        self.qadim_hit_on_line = 0
        self.adina_blinds = 0
        self.qpeer_hit_on_line = 0
        # for graphs data
        self.kills_per_day = {}
        self.boon_uptimes_kills_per_day_sum = {}  # dict[date,dict[int,float]]
        self.boon_uptimes_kill_per_day_count = {}  # dict[date,int]
        self.quickness_classes_per_day: Dict[date, Dict[str, int]] = {}
        self.kill_time_per_day = {}
        self.wipe_time_per_day = {}
        self.walk_time_per_day = {}
        self.kill_dps_per_spec_sum = {}
        self.kill_dps_per_spec_count = {}
        self.kill_squad_dps_per_day_sum = {}
        self.kill_squad_dps_per_day_count = {}
        self.specs_log_counts = {}
        # personal stats
        self.pp_avg_dps = PerPersonStat(PersonalStatsMode.AVERAGE)
        self.pp_max_dps = PerPersonStat(PersonalStatsMode.MAXIMUM)
        self.pp_sum_dmg = PerPersonStat(PersonalStatsMode.SUMME)
        self.pp_avg_hps = PerPersonStat(PersonalStatsMode.AVERAGE)
        self.pp_condi_cleanse_out = PerPersonStat(PersonalStatsMode.SUMME)
        self.pp_sum_heal = PerPersonStat(PersonalStatsMode.SUMME)
        self.pp_max_heal = PerPersonStat(PersonalStatsMode.MAXIMUM)
        self.pp_res_time = PerPersonStat(PersonalStatsMode.AVERAGE)
        self.pp_sum_skill_casts = PerPersonStat(PersonalStatsMode.SUMME)
        self.pp_avg_skill_casts = PerPersonStat(PersonalStatsMode.AVERAGE)
        self.pp_kill_survived_percent = PerPersonStat(PersonalStatsMode.AVERAGE)
        self.pp_presence = PerPersonStat(PersonalStatsMode.SUMME)
        self.pp_avg_grp_quick = PerPersonStat(PersonalStatsMode.AVERAGE)
        self.pp_avg_hp_percent = PerPersonStat(PersonalStatsMode.AVERAGE)
        self.pp_avg_move_units = PerPersonStat(PersonalStatsMode.AVERAGE)
        self.pp_uniqe_specs_played = PerPersonStat(PersonalStatsMode.COUNT_UNIQUE)
        self.pp_max_cc = PerPersonStat(PersonalStatsMode.MAXIMUM)
        self.pp_avg_cc = PerPersonStat(PersonalStatsMode.AVERAGE)
        self.pp_tears_collected = PerPersonStat(PersonalStatsMode.SUMME)
        self.pp_dist_to_sq_mid = PerPersonStat(PersonalStatsMode.AVERAGE)
        self.pp_scholar_hit_grp_when_heal_kill = PerPersonStat(PersonalStatsMode.AVERAGE)
        self.pp_downed_count = PerPersonStat(PersonalStatsMode.AVERAGE)
        self.pp_downed_duration = PerPersonStat(PersonalStatsMode.AVERAGE)
        # memes
        self.ewayne_puns = 437
        self.new_raids_released = 0
        self.ewayne_cata_alac_quick_data_point = []

    @classmethod
    def create_from_logs(cls, logs: List[LogManagerLog], days_data: List["DayData"]):
        stats = StatisticsData()
        i = 0
        for log in logs:
            print("\b\b\b\b\b%4d " % i, end="")
            i += 1
            StatisticsData.process_log(log, stats)
        for build in stats.build_usage_counts:
            print("%-30s" % build, stats.build_usage_counts[build])
        # for acc in stats.build_usage_by_acc:
        #    print(acc)
        #    for build in stats.build_usage_by_acc[acc]:
        #        print("%-30s" % build, stats.build_usage_by_acc[acc][build])
        #    print("-"*50)
        for day_data in days_data:
            stats.total_walk_time += day_data.total_walk_time
            stats.total_wipe_time += day_data.total_wipe_time
            stats.total_time += day_data.total_walk_time
            stats.total_time += day_data.total_wipe_time
            if day_data.date in stats.walk_time_per_day:
                stats.walk_time_per_day[day_data.date] += day_data.total_walk_time
            else:
                stats.walk_time_per_day[day_data.date] = day_data.total_walk_time
        return stats

    @staticmethod
    def process_log(log: LogManagerLog, stats):
        boss_name = correct_ch_id_name(log.get_main_target_name())
        boss_id = gw2.raids.get_id_by_name(boss_name)
        wing = gw2.raids.get_wing_from_boss(boss_name)
        log_date = log.get_encounter_start_time().date()
        stats.time_per_wing[wing - 1] += log.get_duration()
        if log.is_success():
            stats.total_time += log.get_duration()
            stats.encounters_completed += 1
            stats.total_kill_time += log.get_duration()
            if log.is_cm():
                stats.cm_kills += 1
            if len(log.players) < 10:
                stats.low_man_kills += 1
            wing = gw2.raids.get_wing_from_boss(boss_name)
            if wing == 7:
                stats.total_gold_created += len(log.players) * 4
            else:
                stats.total_gold_created += len(log.players) * 2.3333
            if log.get_encounter_start_time().date() not in stats.kills_per_day:
                stats.kills_per_day[log.get_encounter_start_time().date()] = 0
                stats.kill_time_per_day[log.get_encounter_start_time().date()] = timedelta(0)
            stats.kills_per_day[log.get_encounter_start_time().date()] += 1
            stats.kill_time_per_day[log.get_encounter_start_time().date()] += log.get_duration()
        else:
            stats.wipes += 1
            if boss_id not in stats.wipes_per_boss:
                stats.wipes_per_boss[boss_id] = 1
            else:
                stats.wipes_per_boss[boss_id] += 1
            if log.get_encounter_start_time().date() not in stats.wipe_time_per_day:
                stats.wipe_time_per_day[log.get_encounter_start_time().date()] = timedelta(0)
            stats.wipe_time_per_day[log.get_encounter_start_time().date()] += log.get_duration()
        for player in log.players:
            if player.get_elite_spec().name.startswith("Unknown"):
                print("elite spec unknown for", player.name, player.get_elite_spec())
            stats.unique_characters_seen.add(player.name)
            stats.unique_accounts_seen.add(player.get_account_name())
            stats.unique_guilds_seen.add(player.get_guild_guid())
            if player.get_elite_spec() in stats.specs_log_counts:
                stats.specs_log_counts[player.get_elite_spec()] += 1
            else:
                stats.specs_log_counts[player.get_elite_spec()] = 1
        url = log.get_dps_report_url()
        if url:
            ei_data = get_ei_data_cached(url, silent=False)
            if not ei_data:
                return
            for skill in ei_data.skill_infos:
                if skill.id not in skill_detail_cache:
                    skill_detail_cache[skill.id] = skill
            builds = []
            for player in ei_data.players:
                if player.is_fake or player.profession_name == "NPC":
                    continue
                build_name = get_player_build(player)
                if player.account_name not in stats.build_usage_by_acc:
                    stats.build_usage_by_acc[player.account_name] = {}
                if build_name not in stats.build_usage_by_acc[player.account_name]:
                    stats.build_usage_by_acc[player.account_name][build_name] = 0
                stats.build_usage_by_acc[player.account_name][build_name] += 1
                if "?" in str(build_name):
                    print("%-20s" % player.name, build_name)
                builds.append(build_name)
                stats.total_damage += player.get_total_damage()
                stats.total_power_dmg += player.get_total_power_damage()
                stats.total_condi_dmg += player.get_total_condi_damage()
                stats.total_cc += player.get_total_breakbar_damage()
                stats.total_healing += player.get_total_healing_out()
                if ei_data.is_success:
                    for target_index in range(len(ei_data.targets)):
                        stats.total_target_damage_in_kills += player.get_target_damage(target_index=target_index)
                    spec = None
                    try:
                        spec = EliteSpecialization[player.profession_name]
                    except KeyError as e:
                        print(player.profession_name, e)
                    if spec:
                        if spec not in stats.kill_dps_per_spec_count:
                            stats.kill_dps_per_spec_count[spec] = 0
                            stats.kill_dps_per_spec_sum[spec] = 0
                        stats.kill_dps_per_spec_count[spec] += 1
                        for target_index in range(len(ei_data.targets)):
                            stats.kill_dps_per_spec_sum[spec] += player.get_target_dps(target_index=target_index)
                for target in range(len(ei_data.targets)):
                    scholar = player.get_damage_mods_target(DAMAGE_MOD_ID_SCHOLAR)
                    if scholar:
                        stats.scholar_uptime_total += scholar.total_hit_count
                        stats.scholar_uptime_active += scholar.active_hit_count
                        stats.scholar_damage_increase += scholar.damage_gain
                    if player.healing_rank >= 8 and ei_data.is_success:
                        sub = player.sub_group
                        scholar_sub = 0
                        sub_cnt = 0
                        for p in ei_data.players:
                            if sub == p.sub_group:
                                scholar = p.get_damage_mods_target(DAMAGE_MOD_ID_SCHOLAR)
                                if scholar and scholar.total_hit_count > 0:
                                    scholar_sub += scholar.active_hit_count / scholar.total_hit_count
                                    sub_cnt += 1
                        if 4 <= sub_cnt <= 5:
                            scholar_sub /= sub_cnt
                            scholar_sub *= 100
                            stats.pp_scholar_hit_grp_when_heal_kill.add_data(player.account_name, scholar_sub)
                stats.resurrect_time += timedelta(seconds=player.get_resurrect_time_s())
                stats.total_damage_taken += player.get_damage_taken()
                stats.total_damage_mitigated_barrier += player.get_damage_against_barrier()
                if player.profession_name in ["Renegade", "Herald", "Vindicator", "Revenant"]:
                    for dist_entry in player.data["totalDamageDist"][0]:
                        if dist_entry["id"] == IMPOSSIBLE_ODDS_HIT_ID:
                            stats.impossible_odds_hits += dist_entry["hits"]
            for build in builds:
                if build not in stats.build_usage_counts:
                    stats.build_usage_counts[build] = 0
                stats.build_usage_counts[build] += 1
            # print(builds, url)
            if ei_data.is_success:
                if log_date not in stats.kill_squad_dps_per_day_count:
                    stats.kill_squad_dps_per_day_count[log_date] = 0
                    stats.kill_squad_dps_per_day_sum[log_date] = 0
                sq_dps = 0
                for player in ei_data.players:
                    for target_index in range(len(ei_data.targets)):
                        sq_dps += player.get_target_dps(target_index=target_index)
                stats.kill_squad_dps_per_day_count[log_date] += 1
                stats.kill_squad_dps_per_day_sum[log_date] += sq_dps
                # quickness givers
                quickness_givers = []
                real_player = filter(lambda p: not p.is_fake, ei_data.players)
                players_by_quickness = sorted(real_player,
                                              key=lambda p: -p.get_squad_buff_generation(QUICKNESS_BUFF_ID))
                if log_date not in stats.quickness_classes_per_day:
                    stats.quickness_classes_per_day[log_date] = {}
                for j in range(2):
                    if players_by_quickness[j].get_squad_buff_generation(QUICKNESS_BUFF_ID) > 5:
                        prof = players_by_quickness[j].profession_name
                        if prof not in stats.quickness_classes_per_day[log_date]:
                            stats.quickness_classes_per_day[log_date][prof] = 0
                        stats.quickness_classes_per_day[log_date][prof] += 1
                        quickness_givers.append(players_by_quickness[j])
                for q_player in quickness_givers:
                    stats.pp_avg_grp_quick.add_data(q_player.account_name,
                                                    q_player.get_group_buff_generation(QUICKNESS_BUFF_ID))
            deaths = ei_data.get_mechanic_counts("Dead")
            for player in deaths:
                stats.total_deaths += deaths[player]
            downs = ei_data.get_mechanic_counts("Downed")
            for player in downs:
                stats.total_downstates += downs[player]
            buff_uptimes = ei_data.get_party_buff_uptimes(
                buffs=[MIGHT_BUFF_ID, QUICKNESS_BUFF_ID, ALACRITY_BUFF_ID, PROTECTION_BUFF_ID])
            stats.total_time_might += timedelta(seconds=
                                                ei_data.get_duration_ms() / 1000 * len(ei_data.players) *
                                                buff_uptimes[MIGHT_BUFF_ID])
            stats.total_time_quickness += timedelta(seconds=
                                                    ei_data.get_duration_ms() / 1000 * len(ei_data.players) *
                                                    buff_uptimes[
                                                        QUICKNESS_BUFF_ID] / 100)
            stats.total_time_alac += timedelta(seconds=
                                               ei_data.get_duration_ms() / 1000 * len(ei_data.players) *
                                               buff_uptimes[
                                                   ALACRITY_BUFF_ID] / 100)
            if ei_data.is_success:
                if log_date not in stats.boon_uptimes_kill_per_day_count:
                    stats.boon_uptimes_kill_per_day_count[log_date] = 0
                    stats.boon_uptimes_kills_per_day_sum[log_date] = {MIGHT_BUFF_ID: 0, QUICKNESS_BUFF_ID: 0,
                                                                      ALACRITY_BUFF_ID: 0, PROTECTION_BUFF_ID: 0}
                stats.boon_uptimes_kill_per_day_count[log_date] += 1
                for boon in buff_uptimes:
                    stats.boon_uptimes_kills_per_day_sum[log_date][boon] += buff_uptimes[boon]
            ei_has_cr_data = ei_data.has_combat_replay_data()
            crj = None
            player_actor_dict = None
            if not ei_data.has_combat_replay_data():
                crj = get_cr_json(log.get_dps_report_url(), silent=False)
                if crj is not None:
                    player_actor_dict = create_player_actor_id_dict(ei_data, crj)
            create_movement_stats(boss_id, ei_data, crj, player_actor_dict, stats)
            # cast stats
            for player in ei_data.players:
                sum_per_player = 0
                for skill_id in player.get_skill_ids_used():
                    if skill_id == WEAPON_SWAP_SKILL_ID:
                        stats.weapon_swaps += len(player.get_skill_cast(WEAPON_SWAP_SKILL_ID))
                    elif skill_id == DODGE_SKILL_ID1 or skill_id == DODGE_SKILL_ID2 or skill_id == MIRAGE_CLOAK:
                        stats.dodges += len(player.get_skill_cast(skill_id))
                    else:
                        for cast in player.get_skill_cast(skill_id):
                            if cast.cast_duration == - cast.time_gained and cast.cast_duration.total_seconds() > 0:
                                if cast.cast_duration.total_seconds() > 0.01:  # ignore cancels < 10ms
                                    stats.total_skills_canceled += 1
                                    stats.total_cancel_time += cast.cast_duration
                                    # print(f"cancel {gw2.gw2.get_skill_name(skill_id)}")
                            else:
                                if skill_id not in stats.skill_cast_counts:
                                    stats.skill_cast_counts[skill_id] = 0
                                sum_per_player += 1
                                stats.skill_cast_counts[skill_id] += 1
                stats.pp_sum_skill_casts.add_data(player.account_name, sum_per_player)
                stats.pp_avg_skill_casts.add_data(player.account_name, sum_per_player)
            create_boss_specific_stats(boss_id, log, ei_data, ei_has_cr_data, crj, player_actor_dict, stats)
            create_personal_stats(boss_id, ei_data, crj, player_actor_dict, stats, url)
