import codecs
import math
from datetime import datetime, timedelta

import gw2.raids
import gw2.strikes
import logmanagercachereader
from logmanagercachereader import LogManagerCache
from dps_report_api import get_ei_data_cached, get_ei_data_reduced, EliteInsightDataReduced, EliteInsightData, \
    EliteInsightPlayer

from gw2.gw2 import MIGHT_BUFF_ID, QUICKNESS_BUFF_ID, ALACRITY_BUFF_ID
from character_builds import get_player_build

import html_graph_tool

wipe_reasons = {
    "https://dps.report/2HAl-20220321-221540_vg": "ben got tank",
    "https://dps.report/Nnfs-20220314-222116_vg": "pv was not in green during split",
    "https://dps.report/Rt5t-20220606-221603_vg": "pv blinked out of a blue during split 2",
    "https://dps.report/khSr-20220601-193922_vg": "lack of healing in p3 before green",
    "https://dps.report/Xmwx-20220606-221858_vg": "lack of healing followed by missing seeker controll",

    "https://dps.report/A425-20220321-222341_gors": "charged soul was not stopped",
    "https://dps.report/iVIy-20220425-222252_gors": "charged soul was not stopped",

    "https://dps.report/WHft-20220330-202507_sab": "waro died gliding, later wiped to 4 canons up",
    "https://dps.report/z8xg-20220425-222949_sab": "lost 2 ppl to flamewall while ressing after canon downed to Kernan",

    "https://dps.report/0EHg-20220209-204826_sloth": "failed stealth strat at start",
    "https://dps.report/66o3-20220209-205728_sloth": "ewayne dced (?)",
    "https://dps.report/cQIS-20220221-221308_sloth": "failed stealth strat at start",
    "https://dps.report/bfKF-20220214-215443_sloth": "failed stealth strat at start",
    "https://dps.report/Ds8X-20220321-214410_sloth": "shroom 2 died to ben's gs1",
    "https://dps.report/L2hE-20220418-220409_sloth": "shroom 1 died to ewayne's phantasmal blades",
    "https://dps.report/vmNj-20220418-220518_sloth": "shroom 1 died to ewayne's sword 5",

    "https://dps.report/oUOS-20220314-221336_matt": "ben died while placing fire",
    "https://dps.report/MoRX-20220425-220959_matt": "ben's focus 5 absorbed reflect",

    "https://dps.report/uBd7-20220307-215650_kc": "too slow cc",
    "https://dps.report/fKKl-20220328-222603_kc": "too slow cc",
    "https://dps.report/QlCR-20220328-222715_kc": "first statues merged",

    "https://dps.report/iBSv-20220307-221539_xera": "lost riv to gliding, than didn't clear shards",

    "https://dps.report/ucmO-20220303-212514_mo": "gg since cm was not active",
    "https://dps.report/Sh7q-20220303-212634_mo": "ewayne was to close to the edge of the blue",
    "https://dps.report/AKAD-20220303-212853_mo": "issues with statue, multiple people miss blue",
    "https://dps.report/Tf5H-20220307-211743_mo": "issues with statues",

    "https://dps.report/EUwi-20220509-202446_sh": "gg since cm was not active",

    "https://dps.report/7nTU-20220303-202445_qadim": "ben, waro died to reapers of flesh and apocalypse bringer",
    "https://dps.report/y87z-20220303-203322_qadim": "pv died to mace 2, 33% dps check failed",

    "https://dps.report/nUVI-20220303-191553_sabir": "testing stuff",
    "https://dps.report/KaNj-20220425-192141_sabir": "testing stuff",

    "https://dps.report/qFMB-20220221-193919_adina": "Toucan not behind 1st pillar",
    "https://dps.report/YZSn-20220328-195838_adina": "ewayne gged for unknown reason or died to pillars",
    "https://dps.report/qf4w-20220303-193559_adina": "ewayne got tank as qcata",
    "https://dps.report/u9gI-20220303-193736_adina": "dps check failed (76.9%)",
    "https://dps.report/1G5k-20220321-193613_adina": "dps check failed (75.8%)",
    "https://dps.report/ngPQ-20220328-195430_adina": "dps check failed (50.8%)",
    "https://dps.report/QMvG-20220328-201110_adina": "dps check failed (25.4%)",
    "https://dps.report/mbXy-20220328-194919_adina": "dps check failed (76.0%)",
    "https://dps.report/PLhm-20220328-195636_adina": "Ines and Messalina died to wall",
    "https://dps.report/0IUq-20220328-195749_adina": "Waro died to wall",
    "https://dps.report/4TVm-20220328-195034_adina": "slb got tank instead of pv",
    "https://dps.report/qSqM-20220328-195210_adina": "slb got tank instead of pv",
    "https://dps.report/lqxg-20220321-193709_adina": "ewayne wrong build",
    "https://dps.report/LxCC-20220214-193748_adina": "ewayne (tank) died to mines and then wall",
    "https://dps.report/tfbd-20220328-200036_adina": "pv (tank) died to mines and then wall",
    "https://dps.report/EzfA-20220404-194038_adina": "pv (tank) died to mines and then wall",
    "https://dps.report/t9Ue-20220411-194045_adina": "ines got stuck inside the platform",
    "https://dps.report/Avob-20220425-194709_adina": "ines died to wall",
    "https://dps.report/6EbU-20220425-194553_adina": "kerchu's reflect to late, druid knocked in sand",
}


def get_top_by_mechanic(ei_data: EliteInsightData, mechanic_name: str, count: int, *, filter_before_ms=None,
                        minimum_mech=0):
    if filter_before_ms:
        fixate_counts = ei_data.get_mechanic_counts(mechanic_name, filter_before_ms=filter_before_ms)
    else:
        fixate_counts = ei_data.get_mechanic_counts(mechanic_name)

    def key_fn(player: EliteInsightPlayer):
        if player.name in fixate_counts:
            return fixate_counts[player.name]
        return 0

    players = sorted(ei_data.players, key=key_fn, reverse=True)
    return list(filter(lambda p: (p.name in fixate_counts) and fixate_counts[p.name] >= minimum_mech, players[0:count]))


def get_desmina_tanks(ei_data: EliteInsightData):
    return get_top_by_mechanic(ei_data, "Fixate", 2)


def get_qtp_tank(ei_data: EliteInsightData):
    return get_top_by_mechanic(ei_data, "Fixated", 1, filter_before_ms=ei_data.duration.total_seconds() * 1000 / 2)


def get_toughness_tanks(ei_data: EliteInsightData, count=1):
    l = sorted(filter(lambda p: p.profession_name != "NPC" and not p.is_fake and p.toughness_rank > 0,
                      ei_data.players),
               key=lambda p: p.toughness_rank, reverse=True)
    if len(l) > count:
        return [l[0:min(count, len(l))]]
    else:
        return []


def get_tank_by_direction(ei_data: EliteInsightData, *, polling_rate=150, max_ms=90000):
    if not ei_data.has_combat_replay_data():
        return []
    target = ei_data.targets[0]
    player_scores = {}
    for player in ei_data.players:
        player_scores[player] = 0
    start_offset = 0
    if ei_data.fight_name in ["Dhuum", "Dhuum CM"]:
        start_offset = 130000
    for time_ms in range(target.first_aware_ms + start_offset, min(target.last_aware_ms, max_ms)):
        t_index_target = (time_ms - target.first_aware_ms) // polling_rate
        t_index_player = time_ms // polling_rate
        t_ori = target.get_orientation_keyframe(t_index_target)
        tpos = target.get_position_keyframe(t_index_target)
        if not tpos or not t_ori:
            continue
        tx, ty = tpos
        for player in ei_data.players:
            px, py = player.get_position_keyframe(t_index_player)
            pta = math.atan2(py - ty, px - tx) / math.pi * 180
            delta = math.fabs(t_ori - pta)
            if delta > 180:
                delta = 360 - delta
            player_scores[player] += delta
    min_val = min(player_scores.values())
    return [[player for player in player_scores if player_scores[player] == min_val][0]]


def get_sabetha_canons(ei_data: EliteInsightData):
    return get_top_by_mechanic(ei_data, "Launched", 2)


def get_sloth_shrooms(ei_data: EliteInsightData):
    return get_top_by_mechanic(ei_data, "Slub", 4)


def get_mo_tank(ei_data: EliteInsightData):
    return get_top_by_mechanic(ei_data, "Claim (SAK)", 1)


def get_mo_specials(ei_data: EliteInsightData):
    return get_top_by_mechanic(ei_data, "Protect (SAK)", 1) + get_top_by_mechanic(ei_data, "Dispel (SAK)", 1)


def get_deimos_hk(ei_data):
    def key_fn(player: EliteInsightPlayer):
        return player.get_dist_sq_center()

    players = sorted(ei_data.players, key=key_fn, reverse=True)
    return [players[0]]


def get_ca_collectors(ei_data):
    return get_top_by_mechanic(ei_data, "Shield.C", 1) + get_top_by_mechanic(ei_data, "Sword.C", 1)


def get_dhuum_kite(ei_data):
    # todo
    return []


def qtp_get_kites(ei_data: EliteInsightData):
    return get_top_by_mechanic(ei_data, "Orb caught", 3)


def get_qadim_lamp(ei_data):
    return get_top_by_mechanic(ei_data, "Lamp", 2, minimum_mech=2)


def log_data_to_json(url, ei_data: EliteInsightData):
    players = ""
    tank_players = []
    special_role_players = []
    if ei_data.fight_name in ["Vale Guardian", "Gorseval the Multifarious", "Keep Construct", "Keep Construct CM",
                              "Xera", "ch16246-720", "Deimos", "Deimos CM", "Statue of Ice", "Dhuum", "Dhuum CM",
                              "Cardinal Adina", "Cardinal Adina CM"]:
        tank_players = get_toughness_tanks(ei_data)
        if len(tank_players) == 0:
            tank_players = get_tank_by_direction(ei_data)
        if ei_data.fight_name in ["Deimos", "Deimos CM"]:
            special_role_players = get_deimos_hk(ei_data)
        elif ei_data.fight_name in ["Dhuum", "Dhuum CM"]:
            special_role_players = get_dhuum_kite(ei_data)
    elif ei_data.fight_name == "Sabetha the Saboteur":
        special_role_players = get_sabetha_canons(ei_data)
    elif ei_data.fight_name == "Slothasor":
        special_role_players = get_sloth_shrooms(ei_data)
    elif ei_data.fight_name in ["Mursaat Overseer", "Mursaat Overseer CM"]:
        tank_players = get_mo_tank(ei_data)
        special_role_players = get_mo_specials(ei_data)
    elif ei_data.fight_name in ["Soulless Horror CM", "Soulless Horror", "ag535 CM"]:
        tank_players = get_desmina_tanks(ei_data)
    elif ei_data.fight_name in ["Twin Largos CM", "Twin Largos"]:
        tank_players = get_toughness_tanks(ei_data, count=2)
    elif ei_data.fight_name.startswith("Qadim the Peerless") or ei_data.fight_name == "ag121":
        tank_players = get_qtp_tank(ei_data)
        special_role_players = qtp_get_kites(ei_data)
    elif ei_data.fight_name in ["Conjured Amalgamate", "Conjured Amalgamate CM", "Beschworene Verschmelzung CM"]:
        special_role_players = get_ca_collectors(ei_data)
    elif ei_data.fight_name in ["Qadim", "Qadim CM"]:
        special_role_players = get_qadim_lamp(ei_data)
    elif ei_data.fight_name in ["Boneskinner", "Fraenir of Jormag", "Super Kodan Brothers", "Icebrood Construct",
                                "Aetherblade Hideout", "Aetherblade Hideout CM", "Whisper of Jormag", "Ankka",
                                "Ankka CM", "The Dragonvoid", "Cairn", "Cairn CM"]:
        pass  # no special roles
    else:
        print("Undefined roles function for " + ei_data.fight_name)
    for player in ei_data.players:
        build = get_player_build(player)
        buffs = "\"buff_uptime\":{"
        for buff_id in [QUICKNESS_BUFF_ID, ALACRITY_BUFF_ID]:
            buffs += f"\"{buff_id}\":{player.get_buff_uptime(buff_id)},"
        buffs = buffs[:-1]
        buffs += "},\"group_buff_generation\":{"
        for buff_id in [QUICKNESS_BUFF_ID, ALACRITY_BUFF_ID]:
            buffs += f"\"{buff_id}\":{player.get_group_buff_generation(buff_id)},"
        buffs = buffs[:-1]
        buffs += "},\"off_group_buff_generation\":{"
        for buff_id in [QUICKNESS_BUFF_ID, ALACRITY_BUFF_ID]:
            buffs += f"\"{buff_id}\":{player.get_off_group_buff_generation(buff_id)},"
        buffs = buffs[:-1]
        buffs += "}"

        extra = ""
        roles = []
        if player in tank_players:
            roles.append("tank")
        if player in special_role_players:
            roles.append("special")
        if len(roles) > 0:
            extra = ",\"roles\":["
            for role in roles:
                extra += '"' + role + '",'
            extra = extra[:-1] + "]"
        players += f"{{\"target_cc\":{player.get_all_targets_cc()},\"target_dps\":{player.get_all_targets_dps()}," \
                   f"\"build\":{{\"name\":\"{build.name}\",\"alac\":{str(build.alac).lower()},\"quick\":{str(build.quick).lower()}," \
                   f"\"elite_spec\":\"{build.elite_spec.name}\"}},{buffs},\"sub_group\":{player.sub_group}," \
                   f"\"name\":\"{player.name}\"{extra}}},"
    players = players[:-1]
    ht_dragons = ""
    if ei_data.fight_name in ["The Dragonvoid", "The Dragonvoid CM"]:
        ht_dragons = ", \"ht_dragons\":\""
        ht_boss_names = ["The JormagVoid", "The PrimordusVoid", "The KralkatorrikVoid", "The MordremothVoid",
                         "The ZhaitanVoid", "The SooWonVoid"]
        ht_boss_colors = ["#6495fb", "#ff6d41", "#fe4bff", "#95c62f", "#2a2e0d", "#8c9cc8"]
        for i, tn in enumerate(ht_boss_names):
            for target in ei_data.targets:
                if target.name == tn and target.total_health > 0:
                    color = ht_boss_colors[i]
                    kill_percent = 100 * (1 - target.final_health / target.total_health)
                    final_hpp = 100 * target.final_health / target.total_health
                    ht_dragons += f"<span class='ht_boss_bar' style='background:rgba(0, 0, 0, 0) linear-gradient(to right, {color} {kill_percent:.2f}%, {kill_percent:.2f}%, #cccccc {final_hpp:.2f}%) repeat scroll 0% 0%;'></span>"
        ht_dragons += "\""
    wipe_reason = ""
    if not ei_data.is_success:
        wipe_reason = ",\"wipe_reason\":\"?\""
        if url in wipe_reasons:
            wipe_reason = ",\"wipe_reason\":\" " + wipe_reasons[url] + "\""
        elif ei_data.fight_name in ["The Dragonvoid", "The Dragonvoid CM"]:
            deaths = ei_data.get_mechanic_events("Dead")
            if deaths is not None and len(deaths) > 0:
                pl_name = deaths[0]["actor"]
                player = ei_data.get_player_by_name(pl_name)
                if len(player.data["deathRecap"]) > 0 and "toDown" in player.data["deathRecap"][0]:
                    kill_skill_id = player.data["deathRecap"][0]["toDown"][-1]["id"]
                    skill = ei_data.get_skill_info(kill_skill_id)
                    if skill is not None:
                        sk_name = skill.name
                    else:
                        sk_name = str(kill_skill_id)
                    wipe_reason = ",\"wipe_reason\":\"" + pl_name + " died to " + sk_name + "\""
    data = f"{{\"url\":\"{url}\",\"data\":{{\"duration\":{ei_data.duration.total_seconds() * 1000}," \
           f"\"is_cm\":{str(ei_data.is_cm).lower()},\"start_time\":{ei_data.start_time.timestamp() * 1000}," \
           f"\"players\":[{players}]{wipe_reason}{ht_dragons}}}}},"
    return data


def create_boss_report_ts(encounter, nxt, prev, *, after=datetime(2022, 2, 28).astimezone(datetime.now().tzinfo)):
    lmc = LogManagerCache()
    if "lmid" in encounter:
        logs = lmc.get_logs(encounter_filter=[encounter["lmid"]], after=after)
    else:
        targets = [encounter["name"]]
        if encounter["name"] == "Twin Largos":
            targets = ["Kenut", "Nikare"]
        logs = lmc.get_logs(target_filter=targets, after=after)
    print(f"Found {len(logs)} logs for {encounter['name']}")
    failed_logs = {}
    successful_logs = {}
    skip_count = 0
    for i, log in enumerate(logs):
        print(f"{i + 1:03d}/{len(logs):03d} ...")
        url = log.get_dps_report_url()
        if not url:
            skip_count += 1
            continue
        ei_data = get_ei_data_cached(url)
        if not ei_data:
            print(f"Failed to download {url}")
            continue
        if not ei_data.is_success:
            failed_logs[url] = ei_data
        else:
            successful_logs[url] = ei_data
    if skip_count > 0:
        print(f"Skipped {skip_count} since no url")
    img_url = f"img/boss/{encounter['id']}.png"
    body = "<div id='header'>"
    body += f"<h2><img id='boss-img' src='{img_url}'/><span id='title-text'>{encounter['name']} since {after:%d.%m.%Y}</span></h2>\n"
    body += "<div id='nav'>"
    if prev:
        body += f"<a href='{prev}.html'><span class='prev-boss'>&lt&lt{prev}</span></a> "
    else:
        body += f"<span class='prev-boss'></span> "
    if nxt:
        body += f"<a href='{nxt}.html'><span class='next-boss'>{nxt}&gt&gt</span></a>"
    body += "</div></div>"
    body += "<div id='left-col'><div id='filters' class='border-box'>Filter:" \
            "<div id='date_filter' class='border-box'>" \
            "Date:<br/>" \
            "<span class='date-filter-label'>From</span><input type='date' id='filter_after' name='filter_after'/><br/>" \
            "<span class='date-filter-label'>To</span><input type='date' id='filter_before' name='filter_before'/><br/>" \
            "<button id='btn2021'>2021</button><button id='btn2022'>2022</button>" \
            "<button id='btnPOF'>POF</button><button id='btnEOD'>EOD</button><br/>" \
            "<button id='btnALL'>All</button>" \
            "</div>" \
            "<div class='border-box'>" \
            "<input type='checkbox' id='ckbCM' name='CM' value='CM' checked/>" \
            "<label for='ckbCM'>CM</label>" \
            "<input type='checkbox' id='ckbNM' name='NM' value='NM' checked/>" \
            "<label for='ckbNM'>None-CM</label>" \
            "</div></div>" \
            "<div class='border-box'>" \
            "Navigation"
    wing = 0
    for e in gw2.strikes.encounters + gw2.raids.encounters:
        if wing != e["wing"]:
            body += "<br/>"
            wing = e["wing"]
        cls = "boss-nav-img"
        if e["id"] == encounter["id"]:
            cls = "boss-nav-img-current"
        body += f"<a href='#' onclick='document.getElementsByClassName(\"boss-nav-img-current\")[0].className=\"boss-nav-img\";" \
                f"load_boss(\"{e['name']}\");this.children[0].className=\"boss-nav-img-current\";'>" \
                f"<img src='img/boss/{e['id']}.png' class='{cls}' alt='{e['id']}'/></a>"
    body += "</div></div>"
    body += "<div id='main-content'>"
    # body += kill_time_diagram(successful_logs)
    body += "<div id='kill-time-diagram'></div>"
    body += "<div id='quick-builds'></div>"
    body += "<div id='alac-builds'></div>"
    body += "<div id='dps-builds'></div>"
    body += "<div id='successful-attempts'></div>"
    body += "<div id='fail-attempts'></div>"
    body += "</div>"
    json_data = "{\"failed_logs\":["
    for url in failed_logs:
        json_data += log_data_to_json(url, failed_logs[url])
    if len(failed_logs) > 0:
        json_data = json_data[:-1] + "\n"
    json_data += "],\n"
    json_data += '"successful_logs":['
    for url in successful_logs:
        json_data += log_data_to_json(url, successful_logs[url])
    json_data = json_data[:-1]
    json_data += f"],\"encounter_name\":\"{encounter['name']}\",\"encounter_img\":\"{img_url}\"}}"
    ##### f"<script src='{encounter['name']}.js'></script>" \
    html = f"<!doctype html><html lang='en-us'><head><meta charset='utf-8'>\n" \
           f"<title>{encounter['name']} report</title>\n" \
           f"<link rel='stylesheet' type='text/css' href='boss_report.css' />" \
           f"<link rel='icon' href='{img_url}' type='image/png' id='favicon'/>" \
           f"<script src='ts/site_generator.js'></script>" \
           f"</head>\n" \
           f"<body onload='load()'>{body}</body></html>"
    with codecs.open(f"boss_jsons/{encounter['name']}.json", "w", "utf-8") as jsf:
        jsf.write(json_data)
        print(f"wrote to {encounter['name']}.json")
    with codecs.open(f"{encounter['name']}.html", "w", "utf-8") as f:
        f.write(html)
        print(f"wrote to {encounter['name']}.html")


def table_row(*cells, extra=None):
    data = "<tr>"
    for i, cell in enumerate(cells):
        data += "<td"
        if extra and extra[i]:
            data += " " + extra[i]
        data += ">"
        data += cell
        data += "</td>"
    data += "</tr>"
    return data


def kill_time_diagram(successful_logs):
    data = []
    # record = None
    for log in successful_logs:
        week = log.start_time().isocalendar()[1]
        while len(data) <= week:
            data.append(None)
        data[week] = timedelta(milliseconds=log.get_duration_ms())
    html = "<h3>Kill time</h3>\n"
    html += html_graph_tool.create_line_graph(data, "Kill time") + "\n"
    return html


def get_group_off_group_uptime(source_player, players, buff_id):
    count_in = 0
    count_out = 0
    uptime_sum_in = 0
    uptime_sum_out = 0
    for player in players:
        if player.sub_group() == source_player.sub_group():
            uptime_sum_in += player.get_buff_uptime(buff_id)
            count_in += 1
        else:
            uptime_sum_out += player.get_buff_uptime(buff_id)
            count_out += 1

    return uptime_sum_in / count_in, uptime_sum_out / count_out


def build_icon(build):
    return f"<img src='img/{build.elite_spec.name}_tango_icon_20px.png' />"


def boons_classes(successful_logs, buff_id, name):
    build_count = {}
    uptime_sum = {}
    dps_sum = {}
    cc_sum = {}
    html = f"<h3>{name}</h3>"
    for log in successful_logs:
        for player in log.players():
            uptime_g, uptime_og = get_group_off_group_uptime(player, log.players(), buff_id)  #
            gen_g = player.get_group_buff_generation(buff_id)
            gen_og = player.get_off_group_buff_generation(buff_id)
            if gen_g > 20:
                build = get_player_build(player)
                if build not in build_count:
                    build_count[build] = 0
                    uptime_sum[build] = 0
                    dps_sum[build] = 0
                    cc_sum[build] = 0
                build_count[build] += 1
                if gen_og > 20:
                    uptime_sum[build] += (uptime_g + uptime_og) / 2.  # todo unfair weight
                else:
                    uptime_sum[build] += uptime_g
                dps_sum[build] += player.get_target_dps()
                cc_sum[build] += player.get_target_cc()
    table_id = f"buff_{buff_id}_tbl"
    html += f"<table id='{table_id}'><tr><th style='width:170px;text-align:left;' onclick='sortTable(0,\"{table_id}\")'>Build</th>" \
            f"<th onclick='sortTable(1,\"{table_id}\")'>#</th>" \
            f"<th onclick='sortTable(2,\"{table_id}\")'>Uptime*</th>" \
            f"<th onclick='sortTable(3,\"{table_id}\")'>DPS</th>" \
            f"<th onclick='sortTable(4,\"{table_id}\")'>CC</th></tr>"
    for build in build_count:
        html += table_row(f"{build_icon(build)}{build.name}",
                          str(build_count[build]),
                          f"{uptime_sum[build] / build_count[build]:.1f}%",
                          f"{dps_sum[build] / build_count[build]:.0f}",
                          f"{cc_sum[build] / build_count[build]:.0f}",
                          extra=[None, "class='right-align'", "class='right-align'", "class='right-align'",
                                 "class='right-align'"])
    html += "</table>"
    html += "*Squad or Group depending on build and patch"
    return html


def main():
    from gw2.raids import encounters as raid_encounters
    from gw2.strikes import encounters as strike_encounters
    encounters = raid_encounters #+ strike_encounters[:-1]
    #encounters = [strike_encounters[-1]]
    for i, encounter in enumerate(encounters):
        nxt = None
        prev = None
        if i > 0:
            prev = encounters[i - 1]["name"]
        if i < len(encounters) - 1:
            nxt = encounters[i + 1]["name"]
        create_boss_report_ts(encounter, nxt, prev, after=datetime(2000, 1, 1).astimezone())


if __name__ == "__main__":
    main()
