from enum import Enum

from dps_report_api import EliteInsightPlayer
from gw2.gw2 import QUICKNESS_BUFF_ID, BUFF_BANNER_OF_STRENGTH, BUFF_BANNER_OF_DISCIPLINE, ALACRITY_BUFF_ID
from logmanagercachereader import EliteSpecialization


def get_player_build(player: EliteInsightPlayer) -> "CharacterBuild":
    if player.profession_name == "Druid":
        if player.healing_rank >= 7 or (player.get_weapon(0) == "Staff" or player.get_weapon(2) == "Staff"):
            return HEAL_DRUID
        return CharacterBuild("? Druid", EliteSpecialization.Druid, DamageType.UNSPECIFIED)
    elif player.profession_name == "Mirage":
        if player.get_weapon(0) == "Axe" and player.get_weapon(2) == "Axe":
            return AXE_MIRAGE
        if player.get_weapon(0) == "Staff" and player.get_weapon(2) in ["Staff", "Unknown"]:
            return STAFF_MIRAGE
        if {player.get_weapon(0), player.get_weapon(2)} == {"Staff", "Axe"}:
            return STAXE_MIRAGE
        if player.get_weapon(0) == "Sword":
            return MOBILITY_MIRAGE
        return CharacterBuild("? Mirage", EliteSpecialization.Mirage, DamageType.UNSPECIFIED)
    elif player.profession_name == "Firebrand":
        if player.healing_rank >= 7 or player.get_weapon(0) == "Staff" or player.get_weapon(2) == "Staff":
            return HFB
        elif player.condition_rank >= 6:
            if player.get_squad_buff_generation(QUICKNESS_BUFF_ID) > 5:
                return QFB
            else:
                return CFB
        return CharacterBuild("? FB", EliteSpecialization.Firebrand, DamageType.UNSPECIFIED)
    elif player.profession_name == "Scourge":
        if player.healing_rank >= 5:
            return HEAL_SCG
        elif player.condition_rank >= 8:
            return CONDI_SCG
        return CharacterBuild("? SCG", EliteSpecialization.Scourge, DamageType.UNSPECIFIED)
    elif player.profession_name == "Chronomancer":
        is_quick = player.get_squad_buff_generation(QUICKNESS_BUFF_ID) > 10
        if player.get_total_power_damage() < player.get_total_condi_damage():
            if is_quick:
                return CONDI_QUICK_CHRONO
            else:
                return CONDI_CHRONO
        else:
            if is_quick:
                if player.healing_rank >= 6:
                    return HEAL_CHRONO
                else:
                    return POWER_QUICK_CHRONO
            else:
                return POWER_CHRONO
    elif player.profession_name == "Holosmith":
        if player.get_total_power_damage() > player.get_total_condi_damage():
            if player.get_weapon(0) == "Sword":
                return SWORD_HOLO
            else:
                return RIFLE_HOLO
        else:
            return CONDI_HOLO
    elif player.profession_name == "Daredevil":
        if player.get_weapon(0) == "Staff" or player.get_weapon(2) == "Staff":
            if player.concentration_rank == 0:
                if player.get_total_power_damage() > player.get_total_condi_damage():
                    return STAFF_DD
                else:
                    return STAFF_CONDI_DD
            else:
                return BOON_DD
        if player.get_weapon(0) == "Dagger" and player.get_weapon(0) == "Dagger" and player.condition_rank >= 8:
            return DD_CONDI_DD
        return CharacterBuild("? DD", EliteSpecialization.Daredevil, DamageType.UNSPECIFIED)
    elif player.profession_name == "Berserker":
        banners = 0
        if player.get_squad_buff_generation(BUFF_BANNER_OF_STRENGTH) > 0:
            banners += 1
        if player.get_squad_buff_generation(BUFF_BANNER_OF_DISCIPLINE) > 0:
            banners += 1
        if player.get_total_power_damage() > player.get_total_condi_damage():
            if banners == 0:
                return PWR_BERSERKER_DPS
            else:
                return PWR_BERSERKER_BS
        else:
            if banners == 0:
                return CON_BERSERKER_DPS
            else:
                return CON_BERSERKER_BS
    elif player.profession_name == "Bladesworn":
        banners = 0
        if player.get_squad_buff_generation(BUFF_BANNER_OF_STRENGTH) > 0:
            banners += 1
        if player.get_squad_buff_generation(BUFF_BANNER_OF_DISCIPLINE) > 0:
            banners += 1
        if player.get_total_power_damage() > player.get_total_condi_damage():
            if banners == 0:
                return DPS_SWORN
            else:
                return BANNER_SWORN
        else:
            return CharacterBuild("? Bladesworn", EliteSpecialization.Bladesworn, DamageType.UNSPECIFIED)
    elif player.profession_name == "Renegade":
        if player.condition_rank >= 8:
            if player.get_squad_buff_generation(ALACRITY_BUFF_ID) > 20:
                return RR_CONDI_REN
            else:
                return CONDI_REN
        if player.healing_rank >= 7:
            return CharacterBuild("? Heal Ren", EliteSpecialization.Renegade, DamageType.UNSPECIFIED)
        if player.get_squad_buff_generation(ALACRITY_BUFF_ID) > 20:
            return PWR_ALACRIGADE
        return CharacterBuild("? Ren", EliteSpecialization.Renegade, DamageType.UNSPECIFIED)
    elif player.profession_name == "Scrapper":
        if player.get_squad_buff_generation(QUICKNESS_BUFF_ID) > 20:
            return QRAPPER
        return CharacterBuild("? Scrapper", EliteSpecialization.Scrapper, DamageType.UNSPECIFIED)
    elif player.profession_name == "Tempest":
        if player.healing_rank >= 6:
            return HEAL_TEMPEST
        return CharacterBuild("? Tempest", EliteSpecialization.Tempest, DamageType.UNSPECIFIED)
    elif player.profession_name == "Weaver":
        if player.condition_rank == 0:
            return POWER_WEAVER
        return CONDI_WEAVER
    elif player.profession_name == "Herald":
        if player.condition_rank == 0 and player.healing_rank == 0:
            return POWER_HERALD
        return CharacterBuild("? Herald", EliteSpecialization.Herald, DamageType.UNSPECIFIED)
    elif player.profession_name == "Deadeye":
        if player.get_total_power_damage() > player.get_total_condi_damage():
            return POWER_DEADEYE
        else:
            return CONDI_DEADEYE
    elif player.profession_name == "Reaper":
        if player.get_total_power_damage() > player.get_total_condi_damage():
            return POWER_REAPER
        else:
            return CharacterBuild("? Reaper", EliteSpecialization.Reaper, DamageType.UNSPECIFIED)
    elif player.profession_name == "Dragonhunter":
        return DRAGONHUNTER
    elif player.profession_name == "Spellbreaker":
        banners = 0
        if player.get_squad_buff_generation(BUFF_BANNER_OF_STRENGTH) > 0:
            banners += 1
        if player.get_squad_buff_generation(BUFF_BANNER_OF_DISCIPLINE) > 0:
            banners += 1
        if player.get_total_power_damage() > player.get_total_condi_damage() > 500:
            if banners >= 1:
                return PWR_BANNER_SPB
        return CharacterBuild("? Spellbreaker", EliteSpecialization.Spellbreaker, DamageType.UNSPECIFIED)
    elif player.profession_name == "Soulbeast":
        if player.get_total_power_damage() > player.get_total_condi_damage():
            return POWER_SLB
        else:
            return CONDI_SLB
    elif player.profession_name == "Catalyst":
        if player.get_total_power_damage() > player.get_total_condi_damage():
            if player.get_squad_buff_generation(QUICKNESS_BUFF_ID) > 20:
                return QUICK_CATALYST
            else:
                return POWER_CATALYST
        return CharacterBuild("? Catalyst", EliteSpecialization.Catalyst, DamageType.UNSPECIFIED)
    elif player.profession_name == "Harbinger":
        if player.get_squad_buff_generation(QUICKNESS_BUFF_ID) > 20:
            return QUICKBRINGER
        if 500 < player.get_total_power_damage() < player.get_total_condi_damage():
            return CONDI_HRB
        return CharacterBuild("? Harbinger", EliteSpecialization.Harbinger, DamageType.UNSPECIFIED)
    elif player.profession_name == "Virtuoso":
        if player.get_total_power_damage() > player.get_total_condi_damage():
            return POWER_VIRTUOSO
        else:
            return CONDI_VIRTUOSO
    elif player.profession_name == "Mechanist":
        if player.get_squad_buff_generation(ALACRITY_BUFF_ID) > 20:
            if player.healing_rank >= 6:
                return HEAL_MECHA
            if player.get_total_power_damage() > player.get_total_condi_damage():
                return ALAC_MECHA
            else:
                return CONDI_ALAC_MECHA
        elif player.get_total_power_damage() < player.get_total_condi_damage():
            return CONDI_MECHA
        return CharacterBuild("? Meachnist", EliteSpecialization.Mechanist, DamageType.UNSPECIFIED)
    elif player.profession_name == "Specter":
        if player.get_total_condi_damage() > player.get_total_power_damage():
            if player.get_squad_buff_generation(ALACRITY_BUFF_ID) > 20:
                return ALAC_SPECTER
            else:
                return CONDI_SPECTER
        return CharacterBuild("? Specter", EliteSpecialization.Specter, DamageType.UNSPECIFIED)
    elif player.profession_name == "Willbender":
        if player.get_total_power_damage() > player.get_total_condi_damage():
            return POWER_BENDER
        else:
            if player.get_squad_buff_generation(ALACRITY_BUFF_ID) > 20:
                return CALAC_BENDER
            else:
                return CONDI_BENDER
    elif player.profession_name == "Thief":
        if player.concentration_rank > 5:
            return BOON_THIEF
        else:
            return CharacterBuild("? Thief", EliteSpecialization.Thief, DamageType.UNSPECIFIED)
    elif player.profession_name == "Elementalist":
        return CharacterBuild("? Elementalist", EliteSpecialization.Elementalist, DamageType.UNSPECIFIED)
    elif player.profession_name == "Guardian":
        return CharacterBuild("? Guardian", EliteSpecialization.Guardian, DamageType.UNSPECIFIED)
    else:
        return CharacterBuild("?? " + player.profession_name, EliteSpecialization.NoEliteSpec, DamageType.UNSPECIFIED)


class DamageType(Enum):
    UNSPECIFIED = 0
    CONDI = 1
    POWER = 2


class CharacterBuild:
    def __init__(self, name, elite_spec: EliteSpecialization, damage: DamageType, *, quick=False, alac=False):
        self.quick = quick
        self.alac = alac
        self.name = name
        self.elite_spec = elite_spec

    def __repr__(self):
        return self.name

    def __str__(self):
        return self.name

    def __eq__(self, other):
        if isinstance(other, CharacterBuild):
            return self.name == other.name and self.alac == other.alac \
                   and self.elite_spec == other.elite_spec and self.quick == other.quick
        return False

    def __hash__(self):
        return hash(self.name) + hash(self.elite_spec) * 0x101 + hash(self.alac) * 0x10002 + hash(self.quick) * 0x20003



# red class
HEAL_TEMPEST = CharacterBuild("Heal Tempest", EliteSpecialization.Tempest, DamageType.UNSPECIFIED)
POWER_WEAVER = CharacterBuild("Power Weaver", EliteSpecialization.Weaver, DamageType.POWER)
CONDI_WEAVER = CharacterBuild("Condi Weaver", EliteSpecialization.Weaver, DamageType.CONDI)
QUICK_CATALYST = CharacterBuild("Quick Catalyst", EliteSpecialization.Catalyst, DamageType.POWER, quick=True)
POWER_CATALYST = CharacterBuild("Power Catalyst", EliteSpecialization.Catalyst, DamageType.POWER)
# green class
HEAL_DRUID = CharacterBuild("Heal Druid", EliteSpecialization.Druid, DamageType.UNSPECIFIED)
POWER_SLB = CharacterBuild("Power Soulbeast", EliteSpecialization.Soulbeast, DamageType.POWER)
CONDI_SLB = CharacterBuild("Condi Soulbeast", EliteSpecialization.Soulbeast, DamageType.CONDI)
# pink class
AXE_MIRAGE = CharacterBuild("Axe Mirage", EliteSpecialization.Mirage, DamageType.CONDI)
STAFF_MIRAGE = CharacterBuild("Staff Mirage", EliteSpecialization.Mirage, DamageType.CONDI, alac=True)
STAXE_MIRAGE = CharacterBuild("Staxe Mirage", EliteSpecialization.Mirage, DamageType.CONDI, alac=True)
MOBILITY_MIRAGE = CharacterBuild("Mobility Mirage", EliteSpecialization.Mirage, DamageType.UNSPECIFIED)
POWER_VIRTUOSO = CharacterBuild("Power Virtuoso", EliteSpecialization.Virtuoso, DamageType.POWER)
CONDI_VIRTUOSO = CharacterBuild("Condi Virtuoso", EliteSpecialization.Virtuoso, DamageType.CONDI)
HEAL_CHRONO = CharacterBuild("Heal Chrono", EliteSpecialization.Chronomancer, DamageType.UNSPECIFIED, quick=True)
CONDI_QUICK_CHRONO = CharacterBuild("Condi Quick Chrono", EliteSpecialization.Chronomancer, DamageType.CONDI,
                                    quick=True)
CONDI_CHRONO = CharacterBuild("Condi Chrono", EliteSpecialization.Chronomancer, DamageType.CONDI)
POWER_CHRONO = CharacterBuild("Power Chrono", EliteSpecialization.Chronomancer, DamageType.POWER)
POWER_QUICK_CHRONO = CharacterBuild("Power Quick Chrono", EliteSpecialization.Chronomancer, DamageType.POWER,
                                    quick=True)
# blue class
HFB = CharacterBuild("Heal FB", EliteSpecialization.Firebrand, DamageType.UNSPECIFIED, quick=True)
QFB = CharacterBuild("Quickness FB", EliteSpecialization.Firebrand, DamageType.CONDI, quick=True)
CFB = CharacterBuild("Condi FB", EliteSpecialization.Firebrand, DamageType.CONDI)
DRAGONHUNTER = CharacterBuild("Dragonhunter", EliteSpecialization.Dragonhunter, DamageType.POWER)
POWER_BENDER = CharacterBuild("Power Wilbender", EliteSpecialization.Willbender, DamageType.POWER)
CONDI_BENDER = CharacterBuild("Condi Wilbender", EliteSpecialization.Willbender, DamageType.CONDI)
CALAC_BENDER = CharacterBuild("Condi Alacbender", EliteSpecialization.Willbender, DamageType.CONDI, alac=True)
# dark green class
HEAL_SCG = CharacterBuild("Heal Scourge", EliteSpecialization.Scourge, DamageType.UNSPECIFIED)
CONDI_SCG = CharacterBuild("Condi Scourge", EliteSpecialization.Scourge, DamageType.CONDI)
POWER_REAPER = CharacterBuild("Power Reaper", EliteSpecialization.Reaper, DamageType.POWER)
QUICKBRINGER = CharacterBuild("Quickbringer", EliteSpecialization.Harbinger, DamageType.CONDI, quick=True)
CONDI_HRB = CharacterBuild("Condi Harbinger", EliteSpecialization.Harbinger, DamageType.CONDI)
# gray class
POWER_DEADEYE = CharacterBuild("Power Deadeye", EliteSpecialization.Deadeye, DamageType.POWER)
CONDI_DEADEYE = CharacterBuild("Condi Deadeye", EliteSpecialization.Deadeye, DamageType.CONDI)
STAFF_DD = CharacterBuild("Staff DD", EliteSpecialization.Daredevil, DamageType.POWER)
BOON_DD = CharacterBuild("Boon DD", EliteSpecialization.Daredevil, DamageType.POWER, quick=True)
STAFF_CONDI_DD = CharacterBuild("Condi Staff DD", EliteSpecialization.Daredevil, DamageType.CONDI)
DD_CONDI_DD = CharacterBuild("D/D Condi DD", EliteSpecialization.Daredevil, DamageType.CONDI)
ALAC_SPECTER = CharacterBuild("Alac Specter", EliteSpecialization.Specter, DamageType.CONDI, alac=True)
CONDI_SPECTER = CharacterBuild("Condi Specter", EliteSpecialization.Specter, DamageType.CONDI)
# orange class
SWORD_HOLO = CharacterBuild("Sword Holo", EliteSpecialization.Holosmith, DamageType.POWER)
RIFLE_HOLO = CharacterBuild("Rifle Holo", EliteSpecialization.Holosmith, DamageType.POWER)
CONDI_HOLO = CharacterBuild("Condi Holo", EliteSpecialization.Holosmith, DamageType.CONDI)
QRAPPER = CharacterBuild("Qrapper", EliteSpecialization.Scrapper, DamageType.UNSPECIFIED, quick=True)
HEAL_MECHA = CharacterBuild("Heal Meachanist", EliteSpecialization.Mechanist, DamageType.UNSPECIFIED, alac=True)
ALAC_MECHA = CharacterBuild("Power Alac Meachanist", EliteSpecialization.Mechanist, DamageType.POWER, alac=True)
CONDI_ALAC_MECHA = CharacterBuild("Condi Alac Meachanist", EliteSpecialization.Mechanist, DamageType.CONDI, alac=True)
CONDI_MECHA = CharacterBuild("Condi Meachanist", EliteSpecialization.Mechanist, DamageType.CONDI)
#yellow class
PWR_BANNER_SPB = CharacterBuild("Bannerbreaker", EliteSpecialization.Spellbreaker, DamageType.POWER)
PWR_BERSERKER_DPS = CharacterBuild("Power DPS Berserker", EliteSpecialization.Berserker, DamageType.POWER)
PWR_BERSERKER_BS = CharacterBuild("Power BS Berserker", EliteSpecialization.Berserker, DamageType.POWER)
CON_BERSERKER_DPS = CharacterBuild("Condi DPS Berserker", EliteSpecialization.Berserker, DamageType.CONDI)
CON_BERSERKER_BS = CharacterBuild("Condi BS Berserker", EliteSpecialization.Berserker, DamageType.CONDI)
DPS_SWORN = CharacterBuild("Powersworn", EliteSpecialization.Bladesworn, DamageType.POWER)
BANNER_SWORN = CharacterBuild("Bannersworn", EliteSpecialization.Bladesworn, DamageType.POWER)
# rev class
RR_CONDI_REN = CharacterBuild("Condi RR", EliteSpecialization.Renegade, DamageType.CONDI, alac=True)
CONDI_REN = CharacterBuild("Condi Renegade", EliteSpecialization.Renegade, DamageType.CONDI)
PWR_ALACRIGADE = CharacterBuild("Power Alacrigade", EliteSpecialization.Renegade, DamageType.POWER, alac=True)
POWER_HERALD = CharacterBuild("Power Herald", EliteSpecialization.Herald, DamageType.POWER)
BOON_THIEF = CharacterBuild("Boon Thief", EliteSpecialization.Thief, DamageType.POWER)
