import math
from datetime import timedelta
from string import Template

# from deprecated.classic import deprecated


class PercentTemplate(Template):
    delimiter = "%"


def strfdelta(time_delta: timedelta, fmt: str):
    d = {"D": time_delta.days}
    hours, rem = divmod(time_delta.seconds, 3600)
    minutes, seconds = divmod(rem, 60)
    d["H"] = '{:02d}'.format(hours)
    d["M"] = '{:02d}'.format(minutes)
    d["m"] = '{:02d}'.format(minutes + hours * 60 + time_delta.days * 60 * 24)
    d["S"] = '{:02d}'.format(seconds)
    d['s1'] = '{:04.1f}'.format(time_delta.microseconds / 100000 + seconds)
    t = PercentTemplate(fmt)
    return t.substitute(**d)


def create_pie_chart(data, colors, names):
    data_sum = data[0] - data[0]  # hack to get correct data type
    for d in data:
        data_sum += d
    try:
        _ = data_sum / data_sum
    except ZeroDivisionError:
        print("Warning: Pie chart sum is 0")
        return "<div style='width: 170px;height: 170px;display: table-cell;'>Error</div>"
    conic = ""
    angle = 0
    for i in range(len(data)):
        degree = data[i] / data_sum * 360
        if i == len(data) - 1:
            conic += "%s %.1fdeg %.1fdeg " % (colors[i], angle + 0.5, angle + degree - 0.5)
        else:
            conic += "%s %.1fdeg %.1fdeg, " % (colors[i], angle + 0.5, angle + degree - 0.5)
            angle += degree
    text = "<div style='margin: 5px;display: table-cell;width: 170px;height: 170px;border-radius: 50%%;" \
           "background-image: conic-gradient(%s);'></div>" % conic
    text += "<div class='pie-chart-legend' >"
    for i in range(len(data)):
        if data[i] / data_sum == 0:
            continue
        if isinstance(data[i], type(timedelta(0))):
            value_str = strfdelta(data[i], "%m:%S")
        else:
            value_str = str(data[i])
        text += "<span style='margin:3px;display:inline-block;width:10px; height:10px;background:%s;'></span><span class='chart-legend-name'>%s</span> %.0f%% %s<br/>" % (
            colors[i], names[i], data[i] / data_sum * 100., value_str)
    text += "</div>"
    return text


def create_multi_line_graph(y_data, name, *, colors=None, extra_content="", y_max_overwrite=None, width=400,
                            x_label="days", y_label=None):
    if colors is None or len(y_data[0]) > len(colors):
        colors = ["red", "blue", "green", "yellow", "brown", "pink", "lightblue", "gray", "orange", "darkgray"] * (
                (len(y_data[0]) + 1) // 10)
    polyls = ["<polyline points='"] * len(y_data[0])
    x = 40
    x_step = (width - 50) / len(y_data)
    if y_max_overwrite is None:
        y_max = y_data[0][0] - y_data[0][0]  # get correct data type
        for data_point in y_data:
            for inner in data_point:
                if inner:
                    y_max = max(inner, y_max)
    else:
        y_max = y_max_overwrite
    for data_point in y_data:
        for i in range(len(data_point)):
            if len(polyls) <= i:
                polyls.append("<polyline points='")
            if data_point[i] is not None:
                polyls[i] += f"{x:.2f},{10 + 280 - data_point[i] / y_max * 0.9 * 280:.2f} "
            else:
                polyls[i] += f"' style='fill:none;stroke:{colors[i]};stroke-width:1.5'/><polyline points='"
        x += x_step
    content = ""
    for i in range(len(polyls)):
        polyls[i] += f"' style='fill:none;stroke:{colors[i]};stroke-width:1.5' />"
        content += polyls[i]
    content += f"<line x1='{width}' x2='{width - 7}' y1='290' y2='295' style='stroke:black;'/>"
    content += f"<line x1='{width}' x2='{width - 7}' y1='290' y2='285' style='stroke:black;'/>"
    content += f"<text text-anchor='end' x='{width}' y='305'>{x_label}</text>"
    content += extra_content
    return create_graph_from_content(content, name, y_max / 0.9, width=width, y_label=y_label)


def create_line_graph(y_data, name, *, x_label="days", width=400):
    if len(y_data) == 0:
        return "ERROR: NO DATA"
    content = "<polyline style='fill:none;stroke:#06617d;stroke-width:1.5' points='"
    x = 40
    x_step = (width - 50) / len(y_data)
    y_max = 1
    for i in range(len(y_data)):
        if y_data[i] is not None:
            y_max = y_data[i] - y_data[i]  # get correct data type
            break
    for data_point in y_data:
        if data_point:
            y_max = max(data_point, y_max)
    for data_point in y_data:
        if data_point:
            content += f"{x:.2f},{10 + 280 - data_point / y_max * 0.9 * 280:.2f} "
        else:
            content += "'/><polyline style='fill:none;stroke:#06617d;stroke-width:1.5' points='"
        x += x_step
    content += "'/>"
    content += f"<line x1='{width}' x2='{width - 7}' y1='290' y2='295' style='stroke:black;'/>"
    content += f"<line x1='{width}' x2='{width - 7}' y1='290' y2='285' style='stroke:black;'/>"
    content += f"<text text-anchor='end' x='{width}' y='305'>{x_label}</text>"
    return create_graph_from_content(content, name, y_max / 0.9, width=width)


def create_graph_legend(colors, names, cols=1, col_width=110):
    x = 385 - cols * col_width
    rows = math.ceil(len(names) / cols)
    svg = f"<rect x='{x}' y='30' width='{cols * col_width}' height='{rows * 20 + 6}' style='fill:#b9b9b996;stroke: darkgray;'/>"
    y = 40
    for row in range(rows):
        x = 385 - cols * col_width
        for col in range(cols):
            index = row * cols + col
            if index < len(names):
                svg += f"<rect x='{x + 4}' y='{y}' width='10' height='10' style='fill:{colors[index]}'/>"
                svg += f"<text x='{x + 16}' y='{y + 10}'>{names[index]}</text>"

            x += col_width
        y += 20
    return svg


def create_graph_from_content(content, name, y_max, *, width=400, y_label=None):
    if isinstance(y_max, timedelta):
        if y_max < timedelta(seconds=60):
            step = timedelta(seconds=10)
        elif y_max < timedelta(minutes=10):
            step = timedelta(minutes=1)
        elif y_max < timedelta(minutes=20):
            step = timedelta(minutes=2)
        elif y_max < timedelta(minutes=30):
            step = timedelta(minutes=5)
        elif y_max < timedelta(minutes=60):
            step = timedelta(minutes=10)
        elif y_max < timedelta(hours=6):
            step = timedelta(hours=1)
        elif y_max < timedelta(days=1):
            step = timedelta(hours=6)
        else:
            step = timedelta(hours=24)
    else:
        mul = 1
        while True:
            if y_max <= 35 * mul:
                step = 5 * mul
                break
            elif y_max <= 70 * mul:
                step = 10 * mul
                break
            elif y_max <= 150 * mul:
                step = 20 * mul
                break
            else:
                mul *= 10
    steps = int(0.95 * y_max / step)
    svg = f"<svg width='{width}' height='310'>"
    for i in range(1, steps + 1):
        value = i * step
        svg += f"<line x1='37' x2='43' y1='{10 + 280 - value / y_max * 280}' y2='{10 + 280 - value / y_max * 280}' style='stroke:black;'/>"
        svg += f"<line x1='40' x2='{width - 5}' y1='{10 + 280 - value / y_max * 280}' y2='{10 + 280 - value / y_max * 280}' style='stroke:black;stroke-width:0.3;'/>"
        value_str = str(value)
        if (isinstance(value, float) or isinstance(value, int)) and value >= 1e4:
            value_str = str(int(value / 1e3)) + "k"
        svg += f"<text text-anchor='end' x='36' y='{10 + 280 - value / y_max * 280 + 5}'>{value_str}</text>"
    svg += content
    svg += "<line x1='40' x2='40' y1='10' y2='290' style='stroke:black;'/>"
    svg += "<line x1='40' x2='45' y1='10' y2='17' style='stroke:black;'/>"
    svg += "<line x1='40' x2='35' y1='10' y2='17' style='stroke:black;'/>"
    svg += f"<line x1='40' x2='{width}' y1='290' y2='290' style='stroke:black;'/>"
    svg += f"<text style='font-size:1.2em' text-anchor='middle' x='{width / 2 + 20}' y='20'>{name}</text>"
    if y_label is not None:
        svg += f"<text text-anchor='left' x='47' y='22'>{y_label}</text>"
    svg += "</svg>"
    return svg


def _split_graph_core(data, labels, sub_data=None, sub_labels=None, sub_colors=None, *, main_label=None, x_offset=0,
                      y_start=0.0,
                      height_scale=1.0,
                      y_gap=48.0, color="#444466"):
    html = ""
    base_height = 250
    base_width = 160
    data_parts = [d / sum(data) * height_scale for d in data]
    y_left = y_start
    path_d = f"M{x_offset} {y_left}"
    texts = ""
    y_right = y_start - y_gap / 4 * len(data)
    if main_label:
        texts += f"<text x='{x_offset + 4}' y={y_left + base_height / 2 + 5}>{main_label} - {sum(data)}</text>"
    for i, dp in enumerate(data_parts):
        rect_color = color
        if sub_colors:
            rect_color = sub_colors[i]
        html += f"<rect fill='{rect_color}' width='10' height='{base_height * dp}' x='{x_offset + base_width}' y='{y_right}'/>"
        if sub_data is not None and sub_data[i] is not None and len(sub_data[i]) > 1:
            html += _split_graph_core(sub_data[i], sub_labels[i], x_offset=x_offset + base_width + 10, y_start=y_right,
                                      height_scale=dp, y_gap=(y_gap / 5.8), color=sub_colors[i])
        path_d += f"C{x_offset + base_width * 2 / 5} {y_left} {x_offset + base_width * 3 / 5} {y_right} {x_offset + base_width} {y_right}"
        texts += f"<text x='{x_offset + base_width + 9}' y={y_right + base_height / 2 * dp + 5}>{labels[i]} - {data[i]}</text>"
        y_left += base_height * dp
        y_right += base_height * dp
        path_d += f"L{x_offset + base_width} {y_right} C{x_offset + base_width * 3 / 5} {y_right} {x_offset + base_width * 2 / 5} {y_left} {x_offset} {y_left} "
        y_right += y_gap
    path_d += "Z"
    html += f"<path fill='{color}aa' d='{path_d}'/>{texts}"
    return html


# use v2
#todo @deprecated
def create_split_graph(data, labels, sub_data=None, sub_labels=None, sub_colors=None, *, all_label=None):
    html = "<svg width='540' height='540'>"
    html += _split_graph_core(data, labels, sub_data, sub_labels, sub_colors, y_start=16 + 11 * len(data) + 8,
                              main_label=all_label)
    html += "</svg>"
    return html


def _split_graph_corev2(data, labels, *, x_offset=0, y_offset=0, y_scale=1, color, sub_colors):
    path_width = 150
    rect_width = 10
    y_gap = 5

    texts = ""
    html = ""
    data_sum = 0
    y = y_offset
    y_left = y_offset
    path_d = f"M{x_offset} {y_left}"
    for i, d in enumerate(data):
        y_sub = 0
        if type(d) == list:
            d_html, d_sum, y_sub = _split_graph_corev2(
                d, labels[1][i],
                color=sub_colors[0][i],
                x_offset=x_offset + path_width + rect_width,
                y_offset=y,
                y_scale=y_scale, sub_colors=sub_colors[1:])
            html += d_html

        else:
            d_sum = d
        data_sum += d_sum
        rect_color = color
        if len(sub_colors) > 0 and len(sub_colors[0]) > i:
            rect_color = sub_colors[0][i]
        texts += f"<rect fill='{rect_color}' width='{rect_width}' height='{d_sum * y_scale:.2f}' x='{x_offset + path_width}' y='{y:.2f}'/>"
        lbl = "?"
        if len(labels) > 0 and len(labels[0]) > i:
            lbl = labels[0][i]
        texts += f"<text x='{x_offset + path_width + rect_width + 1}' y={y + d_sum / 2 * y_scale + 5:.2f}>{lbl} - {d_sum}</text>"
        path_d += f"C{x_offset + path_width * 2 / 5} {y_left:.2f} {x_offset + path_width * 3 / 5} {y:.2f} {x_offset + path_width} {y:.2f}"
        y += d_sum * y_scale
        y_left += d_sum * y_scale
        path_d += f"L{x_offset + path_width} {y:.2f} C{x_offset + path_width * 3 / 5} {y:.2f} {x_offset + path_width * 2 / 5} {y_left:.2f} {x_offset} {y_left:.2f} "
        y = max(y + y_gap, y_sub, y - d_sum * y_scale + 13)
    path_d += "Z"
    html += f"<path fill='{color}aa' d='{path_d}'/>{texts}"
    return html, data_sum, y


def _sum_recursive(data):
    if type(data) == list:
        data_sum = 0
        for d in data:
            data_sum += _sum_recursive(d)
        return data_sum
    else:
        return data


def create_split_graphv2(data: list, labels, colors, all_label=None):
    rect_width = 10
    base_height = 300

    total = _sum_recursive(data)
    y_scale = base_height / total
    main_color = "#444466"
    dhtml, _, height = _split_graph_corev2(data, labels, color=main_color, sub_colors=colors, x_offset=rect_width,
                                           y_scale=y_scale, y_offset=2)
    html = f"<svg width='540' height='{height}'>" + dhtml
    html += f"<rect fill='{main_color}' width='{rect_width}' height='{base_height}' x='0' y='2'/>"
    if all_label is not None:
        html += f"<text x='{1 + rect_width}' y={2 + base_height / 2 + 5}>{all_label} - {total}</text>"
    html += "</svg>"
    return html


if __name__ == "__main__":
    sgv2 = create_split_graphv2([[[10, 3], 5, 2, 0.1, 0.1, 1], [2, 1]],
                                [["A", "B"],
                                 [[["AA", "AB", "Aa", "Ax", "Axx", "Ay", "Ayaka"], [[["?", "#"]]]], [["2", "1"]]]],
                                [["#00ff00", "#ff0000"], ["#0000ff"]],
                                all_label="total")
    with open("test_split.htm", "w") as f:
        f.write("<!doctype html><html><body>" + sgv2 + "</body>")
