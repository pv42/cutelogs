"use strict";

let failed_logs: Array<Log>;
let successful_logs: Array<Log>;
failed_logs = [];
successful_logs = [];
let encounter_name = "Loading ...";
let encounter_img = "";

const ALACRITY_BUFF_ID = 30328;
const QUICKNESS_BUFF_ID = 1187;

type Maybe<T> = T | undefined;

interface Log {
    url: string;
    data: LogData;

}

interface Player {
    build: PlayerBuild;
    target_cc: number;
    target_dps: number;
    group_buff_generation: object;
    off_group_buff_generation: object;
    sub_group: number;
    buff_uptime: object;
    name: string;
    roles: Array<string>;
}

interface PlayerBuild {
    alac: boolean;
    quick: boolean;
    name: string;
    elite_spec: string;
}

interface LogData {
    start_time: number;
    duration: number;
    players: Array<Player>;
    is_cm: boolean;
    wipe_reason?: string;
    ht_dragons?: string;
}

interface EncounterLogData {
    failed_logs: Array<Log>;
    successful_logs: Array<Log>;
    encounter_name: string;
    encounter_img: string;
}

function loadDataJsonFromUrl(url: string, callback: (EncounterLogData) => void) {
    const xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = "json"
    xhr.onload = function () {
        const code = xhr.status;
        if(code == 200) {
            callback(xhr.response);
        } else {
            alert("Failed to get " + url);
        }
    }
    xhr.send();
}

function load_boss(boss_name) {
    loadDataJsonFromUrl("boss_jsons/" + boss_name + ".json", function (eld: EncounterLogData) {
        console.log("loaded json from \"boss_jsons/" + boss_name + ".json\"");
        successful_logs = eld.successful_logs;
        failed_logs = eld.failed_logs;
        encounter_name = eld.encounter_name;
        encounter_img = eld.encounter_img;
        generate();
    });
}

function sortTable(column_index: number, table_id: string) {
    function shouldSwitchFunction(a: string, b: string) {
        // if one of the elements does not parse into a float compare strings
        const dateRegex = RegExp("\\d\\d\\.\\d\\d\\.\\d\\d");
        if (dateRegex.test(a) && dateRegex.test(b)) {
            const adata = a.match(/\d\d/g);
            const bdata = b.match(/\d\d/g);
            console.log(10000 * +adata[2] + 100 * +adata[1] + +adata[0], 10000 * +bdata[2] + 100 * +bdata[1] + +bdata[0]);
            return 10000 * +adata[2] + 100 * +adata[1] + +adata[0] > 10000 * +bdata[2] + 100 * +bdata[1] + +bdata[0];
        } else if (isNaN(parseFloat(a)) || isNaN(parseFloat(b))) {
            if (a.toLowerCase() > b.toLowerCase()) {
                return true
            }
            // otherwise sort by numbers
        } else {
            if (parseFloat(a) > parseFloat(b)) {
                // If so, mark as a switch and break the loop:
                return true;
            }
        }
        return false;
    }

    let switching, i, x, y, shouldSwitch, dir;
    let switchcount = 0;
    const table = <HTMLTableElement>document.getElementById(table_id);
    switching = true;
    // Set the sorting direction to ascending:
    dir = "desc";
    /* Make a loop that will continue until
    no switching has been done: */
    while (switching) {
        // Start by saying: no switching is done:
        switching = false;
        let rows = table.rows;
        /* Loop through all table rows (except the
        first, which contains table headers): */
        for (i = 1; i < (rows.length - 1); i++) {
            // Start by saying there should be no switching:
            shouldSwitch = false;
            /* Get the two elements you want to compare,
            one from current row and one from the next: */
            x = rows[i].getElementsByTagName("TD")[column_index];
            y = rows[i + 1].getElementsByTagName("TD")[column_index];
            /* Check if the two rows should switch place,
            based on the direction, asc or desc: */
            if (dir === "asc") {
                if (shouldSwitchFunction(x.innerHTML, y.innerHTML)) {
                    shouldSwitch = true;
                    break;
                }
            } else if (dir === "desc") {
                if (shouldSwitchFunction(y.innerHTML, x.innerHTML)) {
                    shouldSwitch = true;
                    break;
                }
            }
        }
        if (shouldSwitch) {
            /* If a switch has been marked, make the switch
            and mark that a switch has been done: */
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
            // Each time a switch is done, increase this count by 1:
            switchcount++;
        } else {
            /* If no switching has been done AND the direction is "asc",
            set the direction to "desc" and run the while loop again. */
            if (switchcount == 0 && dir == "desc") {
                dir = "asc";
                switching = true;
            }
        }
    }
    // update sort marker
    const headers = table.rows[0].getElementsByTagName("TH");
    for (i = 0; i < headers.length; i++) {
        headers[i].innerHTML = headers[i].innerHTML.replace("▲", "").replace("▼", "");
    }
    headers[column_index].innerHTML += dir == "asc" ? "▲" : "▼";
}


function table_row(cells: Array<string>, extra: Array<Maybe<string>>) {
    let data = "<tr>";
    let i = 0;
    for (const index in cells) {
        data += "<td";
        if (extra && extra[i]) {
            data += " " + extra[i];
        }
        data += ">";
        data += cells[index];
        data += "</td>";
        i++;
    }
    data += "</tr>";
    return data;
}

function dist_viz(data: Array<number>, width: number, data_limit: number, style_appendix: string) {
    const sorted = data.sort(function (a, b) {
        return a - b
    });
    const len = sorted.length
    const min = len >= 2 ? sorted[0] : sorted[0] - data_limit / width;
    const max = len >= 2 ? sorted[len - 1] : sorted[0] + data_limit / width;
    const lwr68 = len >= 4 ? sorted[Math.round(0.16 * len - 0.5)] : min;
    const upr68 = len >= 4 ? sorted[Math.round(0.84 * len - 0.5)] : max;
    const lwr95 = len >= 21 ? sorted[Math.round(0.05 * len - 0.5)] : lwr68;
    const upr95 = len >= 21 ? sorted[Math.round(0.95 * len - 0.5)] : upr68;
    let svg = `<div title='Max: ${sorted[len - 1]}'><svg  class='dist-viz' height='20' width='${width}'>`;
    svg += `<line x1='${min * width / data_limit}' x2='${max * width / data_limit}' y1='10' y2='10' style='stroke:black;stroke-width:1' />`
    svg += `<rect class='percentile-95-${style_appendix}' x='${lwr95 * width / data_limit}' y='6' width='${(upr95 - lwr95) * width / data_limit}' height='8' />`
    svg += `<rect class='percentile-68-${style_appendix}' x='${lwr68 * width / data_limit}' y='4' width='${(upr68 - lwr68) * width / data_limit}' height='12' />`
    svg += "</svg></div>"
    return svg;
}

function player_in_log(player: Player): string {
    if(player.roles && player.roles.some(e => e == "tank")) {
        return spec_icon_title_class(player.build.elite_spec,
            player.name + "\n" + player.build.name + "\nD/C: " + player.target_dps + "/" + player.target_cc,
            "icon-tank"
        );
    } else if(player.roles && player.roles.some(e => e == "special")) {
        return spec_icon_title_class(player.build.elite_spec,
            player.name + "\n" + player.build.name + "\nD/C: " + player.target_dps + "/" + player.target_cc,
            "icon-special"
        );
    } else {
        return spec_icon_title(player.build.elite_spec,
            player.name + "\n" + player.build.name + "\nD/C: " + player.target_dps + "/" + player.target_cc
        );
    }
}

function failed_attempts(failed_logs: Array<Log>) {
    if (failed_logs.length === 0) {
        return "<h3>No failed attempts</h3>";
    }
    let html = "<h3>Failed attempts</h3>\n<table id='failed-attempts'><tr><th>Date</th><th>Duration</th>" +
        "<th class='left-align'>Alac</th><th class='left-align'>Quick</th><th class='left-align'>Other</th>" +
        "<th style='padding-left: 0.2em; text-align: left; width: 400px;'>Wipe Reason</th><th>Log</th></tr>\n";
    failed_logs.forEach(function (fail) {
        const data = fail.data;
        const url = fail.url;
        let reason = fail.data.wipe_reason
        if (reason) {
            if(fail.data.ht_dragons) {
                reason = "<div class='ht_boss_bars'>" + fail.data.ht_dragons + "</div><div class='ht_reason'>" +  reason + "</div>";
            }
        } else if(fail.data.ht_dragons) {
            reason = fail.data.ht_dragons;
        }
        let alac_players = fail.data.players.filter(p => p.build.elite_spec != "NoEliteSpec").filter(p => {
            return p.group_buff_generation[ALACRITY_BUFF_ID] + p.off_group_buff_generation[ALACRITY_BUFF_ID] >= 30;
        }).sort((a, b) => {
            return b.group_buff_generation[ALACRITY_BUFF_ID] - a.group_buff_generation[ALACRITY_BUFF_ID];
        });
        let alac_info = "";
        alac_players.forEach(p => {
            alac_info += player_in_log(p);
        });
        let quick_info = "";
        let quick_players = fail.data.players.filter(p => p.build.elite_spec != "NoEliteSpec").filter(p => {
            return p.group_buff_generation[QUICKNESS_BUFF_ID] + p.off_group_buff_generation[QUICKNESS_BUFF_ID] >= 30;
        }).sort((a, b) => {
            return -a.group_buff_generation[QUICKNESS_BUFF_ID] + b.group_buff_generation[QUICKNESS_BUFF_ID];
        });
        quick_players.forEach(p => {
            quick_info += player_in_log(p);
        });
        let other_info = "";
        let other_players = fail.data.players.filter(p => p.build.elite_spec != "NoEliteSpec");
        other_players = other_players.filter(p => {
            return !quick_players.some(qp => qp == p) && !alac_players.some(ap => ap == p);
        });
        other_players.sort((p0, p1) => p1.target_dps - p0.target_dps);
        other_players.forEach(p => {
            other_info += player_in_log(p);
        });

        const pad = data.duration / 1000 < 100 ? 6 : 7;
        const time_str = `${String(data.duration / 1000).padEnd(pad, "0")}s`;
        const date = new Date(data.start_time);
        html += table_row([
                `${date.toLocaleDateString("de-de", {day: "2-digit", month: "2-digit", year: "2-digit"})}`,
                time_str,
                alac_info,
                quick_info,
                other_info,
                reason,
                `<a href='${url}'>Link</a>`],
            [undefined, "class='right-align'", undefined, undefined, undefined, undefined, undefined]);
    });
    html += "</table>";
    return html;
}


function successful_attempts(suc_logs: Array<Log>) {
    if (suc_logs.length === 0) {
        return "<h3>No successful attempts</h3>";
    }
    let html = "<h3>Successful attempts</h3>\n" +
        "<table id='successful-attempts-tbl'><tr>" +
        `<th onclick='sortTable(0, \"successful-attempts-tbl\")'>Date</th>` +
        `<th onclick='sortTable(1, \"successful-attempts-tbl\")'>Duration</th>` +
        "<th class='left-align'>Alac</th><th class='left-align'>Quick</th><th class='left-align'>Other</th>" +
        "<th>Log</th></tr>\n";
    suc_logs.forEach(function (suc) {
        const data = suc.data;
        const url = suc.url;
        let alac_players = suc.data.players.filter(p => p.build.elite_spec != "NoEliteSpec").filter(p => {
            return p.group_buff_generation[ALACRITY_BUFF_ID] + p.off_group_buff_generation[ALACRITY_BUFF_ID] >= 30;
        }).sort((a, b) => {
            return b.group_buff_generation[ALACRITY_BUFF_ID] - a.group_buff_generation[ALACRITY_BUFF_ID];
        });
        let alac_info = "";
        alac_players.forEach(p => {
            alac_info += player_in_log(p);
        });
        let quick_info = "";
        let quick_players = suc.data.players.filter(p => p.build.elite_spec != "NoEliteSpec").filter(p => {
            return p.group_buff_generation[QUICKNESS_BUFF_ID] + p.off_group_buff_generation[QUICKNESS_BUFF_ID] >= 30;
        }).sort((a, b) => {
            return -a.group_buff_generation[QUICKNESS_BUFF_ID] + b.group_buff_generation[QUICKNESS_BUFF_ID];
        });
        quick_players.forEach(p => {
            quick_info += player_in_log(p);
        });
        let other_info = "";
        let other_players = suc.data.players.filter(p => p.build.elite_spec != "NoEliteSpec");
        other_players = other_players.filter(p => {
            return !quick_players.some(qp => qp == p) && !alac_players.some(ap => ap == p);
        });
        other_players.sort((p0, p1) => p1.target_dps - p0.target_dps);
        other_players.forEach(p => {
            other_info += player_in_log(p);
        });
        const pad = data.duration / 1000 < 100 ? 6 : 7;
        const time_str = `${String(data.duration / 1000).padEnd(pad, "0")}s`;
        html += table_row([
                `${new Date(data.start_time).toLocaleDateString("de-de", {day: "2-digit", month: "2-digit", year: "2-digit"})}`,
                time_str,
                alac_info,
                quick_info,
                other_info,
                `<a href='${url}'>Link</a>`],
            [undefined, "class='right-align'", undefined, undefined, undefined, undefined, undefined]);
    });
    html += "</table>";
    return html;
}

function spec_icon_title_class(spec_name: string, title: string, clazz: string): string {
    if (clazz == undefined) {
        return `<img src='img/${spec_name}_tango_icon_20px.png' alt='${spec_name}' title='${title}'/>`;
    } else {
        return `<img src='img/${spec_name}_tango_icon_20px.png' alt='${spec_name}' title='${title}' class='${clazz}'/>`;
    }
}

function spec_icon_title(spec_name: string, title: string): string {
    return spec_icon_title_class(spec_name, title, undefined);
}


function spec_icon(spec_name) {
    return `<img src='img/${spec_name}_tango_icon_20px.png' alt='${spec_name}'/>`;
}

function get_class_from_spec(spec: string): string {
    switch (spec) {
        case "Firebrand":
        case "Dragonhunter":
        case "Willbender":
        case "Guardian":
            return "Guardian";
        case "Berserker":
        case "Spellbreaker":
        case "Bladesworn":
            return "Warrior";
        case "Scrapper":
        case "Holosmith":
        case "Mechanist":
        case "Engineer":
            return "Engineer";
        case "Druid":
        case "Soulbeast":
        case "Untamed":
            return "Ranger";
        case "Daredevil":
        case "Thief":
        case "Deadeye":
        case "Specter":
            return "Thief";
        case "Tempest":
        case "Weaver":
        case "Catalyst":
            return "Elementalist";
        case "Chronomancer":
        case "Mirage":
        case "Virtuoso":
            return "Mesmer";
        case "Reaper":
        case "Scourge":
        case "Harbinger":
            return "Necromancer";
        case "Herald":
        case "Renegade":
        case "Vindicator":
            return "Revenant";
        default:
            return "Unknown";
    }
}

function dps_builds(successful_logs: Array<Log>) {
    let builds = {};
    let build_count = {};
    let dps_sum = {};
    let total_dps_max = 0;
    let dps_max = {};
    let cc_sum = {};
    let dpss = {};
    let html = "<h3>DPS Builds</h3>";
    for (const log_index in successful_logs) {
        const log = successful_logs[log_index];
        for (const player_index in log.data.players) {
            const player = log.data.players[player_index];
            const build = player.build;
            if (!build.alac && !build.quick) {
                if (!(build.name in build_count)) {
                    build_count[build.name] = 0;
                    dps_sum[build.name] = 0;
                    dps_max[build.name] = 0;
                    cc_sum[build.name] = 0;
                    builds[build.name] = build;
                    dpss[build.name] = [];
                }
                build_count[build.name] += 1;
                dps_sum[build.name] += player.target_dps;
                dpss[build.name].push(player.target_dps);
                cc_sum[build.name] += player.target_cc;
                if (player.target_dps > dps_max[build.name]) {
                    dps_max[build.name] = player.target_dps;
                }
                if (player.target_dps > total_dps_max) {
                    total_dps_max = player.target_dps;
                }
            }
        }
    }
    const table_id = "dps_tbl";
    html += `<table id='${table_id}'><tr><th style='width:170px;text-align:left;' onclick='sortTable(0,\"${table_id}\")'>Build</th>` +
        `<th onclick='sortTable(1,\"${table_id}\")' style="width: 25px;">#</th>` +
        `<th onclick='sortTable(2,\"${table_id}\")'>DPS</th>` +
        `<th>DPS VIZ</th>` +
        `<th onclick='sortTable(4,\"${table_id}\")'>CC</th></tr>`;
    for (const build_name in build_count) {
        html += table_row([`${spec_icon(builds[build_name].elite_spec)}${build_name}`,
                build_count[build_name],
                `${Math.round(dps_sum[build_name] / build_count[build_name])}`,

                dist_viz(dpss[build_name], 100, total_dps_max, get_class_from_spec(builds[build_name].elite_spec)),
                `${Math.round(cc_sum[build_name] / build_count[build_name])}`],
            [undefined, "class='right-align'", "class='right-align'", undefined,
                "class='right-align'"]);
    }
    html += "</table>";
    return html;
}

function get_group_off_group_uptime(source_player: Player, players: Array<Player>, buff_id: number) {
    let count_in = 0;
    let count_out = 0;
    let uptime_sum_in = 0;
    let uptime_sum_out = 0;
    for (const player_index in players) {
        const player = players[player_index];
        if (player.sub_group == source_player.sub_group) {
            uptime_sum_in += player.buff_uptime[buff_id];
            count_in += 1;
        } else {
            uptime_sum_out += player.buff_uptime[buff_id];
            count_out += 1;
        }
    }
    return [uptime_sum_in / count_in, uptime_sum_out / count_out];
}

function create_graph_from_content(content: string, name: string, y_max: number, y_is_time: boolean) {
    let step: number;
    if (y_is_time) {
        if (y_max < 60 * 1000)              //  1min
            step = 10 * 1000;               // 10sec
        else if (y_max < 8 * 60 * 1000)     //  8min
            step = 60 * 1000;               //  1min
        else if (y_max < 20 * 60 * 1000)    // 20min
            step = 2 * 60 * 1000;           //  2min
        else if (y_max < 30 * 60 * 1000)    // 30min
            step = 5 * 60 * 1000;           //  5min
        else if (y_max < 60 * 60 * 1000)
            step = 10 * 60 * 1000;
        else if (y_max < 6 * 60 * 60 * 1000)
            step = 60 * 60 * 1000;
        else if (y_max < 24 * 60 * 60 * 1000)
            step = 6 * 60 * 60 * 1000;
        else
            step = 24 * 60 * 60 * 1000;
    } else {
        let mul = 1;
        step = 0;
        while (true) {
            if (y_max <= 35 * mul) {
                step = 5 * mul;
                break
            } else if (y_max <= 70 * mul) {
                step = 10 * mul;
                break
            } else if (y_max <= 150 * mul) {
                step = 20 * mul;
                break
            } else {
                mul *= 10;
            }
        }
    }
    let steps = Math.floor(0.95 * y_max / step);
    let svg = "<svg width='400' height='310'>"
    for (let i = 1; i < steps + 1; i++) {
        let value = i * step;
        svg += `<line x1='37' x2='43' y1='${10 + 280 - value / y_max * 280}' y2='${10 + 280 - value / y_max * 280}' style='stroke:black;'/>`
        svg += `<line x1='40' x2='395' y1='${10 + 280 - value / y_max * 280}' y2='${10 + 280 - value / y_max * 280}' style='stroke:black;stroke-width:0.3;'/>`
        let value_str = value.toString();
        if (!y_is_time && value >= 1e4) {
            value_str = Math.floor(value / 1e3).toString() + "k";
        } else if (y_is_time) {
            const secs = Math.round(value / 1000)
            value_str = Math.round(secs / 60) + ":" + String(secs % 60).padStart(2, "0");
        }
        svg += `<text text-anchor='end' x='36' y='${10 + 280 - value / y_max * 280 + 5}'>${value_str}</text>`;
    }
    svg += content
    svg += "<line x1='40' x2='40' y1='10' y2='290' style='stroke:black;'/>";
    svg += "<line x1='40' x2='45' y1='10' y2='17' style='stroke:black;'/>";
    svg += "<line x1='40' x2='35' y1='10' y2='17' style='stroke:black;'/>";
    svg += "<line x1='40' x2='400' y1='290' y2='290' style='stroke:black;'/>";
    svg += `<text style='font-size:1.2em' text-anchor='middle' x='220' y='20'>${name}</text>`;
    svg += "</svg>";
    return svg;
}

function create_line_graph(y_data: Array<Maybe<number>>, name: string, y_is_time) {
    if (y_data.length === 0) {
        return "ERROR: NO DATA";
    }
    let content = "<polyline style='fill:none;stroke:#06617d;stroke-width:1.5' points='";
    let x = 40;
    const x_step = 350 / y_data.length;
    let y_max = 1;
    for (let i = 0; i < y_data.length; i++) {
        if (y_data[i] !== undefined) {
            y_max = y_data[i] - y_data[i]; // correct data type
            break;
        }
    }
    for (let i = 0; i < y_data.length; i++) {
        const data_point = y_data[i];
        if (data_point)
            y_max = Math.max(data_point, y_max);
    }
    for (let i = 0; i < y_data.length; i++) {
        const data_point = y_data[i];
        if (data_point) {
            content += `${x},${10 + 280 - data_point / y_max * 0.9 * 280} `;
        } else {
            content += "'/><polyline style='fill:none;stroke:#06617d;stroke-width:1.5' points='";
        }
        x += x_step;
    }
    content += "'/>";
    content += "<line x1='400' x2='393' y1='290' y2='295' style='stroke:black;'/>";
    content += "<line x1='400' x2='393' y1='290' y2='285' style='stroke:black;'/>";
    content += "<text text-anchor='end' x='400' y='305'>weeks</text>";
    return create_graph_from_content(content, name, y_max / 0.9, y_is_time);
}

function boons_classes(successful_logs: Array<Log>, buff_id: number, name: string) {
    let builds = {};
    let build_count = {};
    let dps_sum = {};
    let dps_max = {};
    let cc_sum = {};
    let uptime_sum = {};
    let total_dps_max = 0;
    let dpss = {};
    let html = `<h3>${name}</h3>`;
    for (const log_index in successful_logs) {
        const log = successful_logs[log_index];
        for (const player_index in log.data.players) {
            const player = log.data.players[player_index];
            const build = player.build;
            const uptimes = get_group_off_group_uptime(player, log.data.players, buff_id)
            const uptime_g = uptimes[0];
            const uptime_og = uptimes[1];
            let gen_g = player.group_buff_generation[buff_id];
            let gen_og = player.off_group_buff_generation[buff_id];
            if (gen_g > 20) {
                if (!(build.name in build_count)) {
                    build_count[build.name] = 0;
                    dps_sum[build.name] = 0;
                    dps_max[build.name] = 0;
                    cc_sum[build.name] = 0;
                    uptime_sum[build.name] = 0;
                    builds[build.name] = build;
                    dpss[build.name] = []
                }
                build_count[build.name] += 1;
                dps_sum[build.name] += player.target_dps;
                cc_sum[build.name] += player.target_cc;
                dpss[build.name].push(player.target_dps);
                if (player.target_dps > total_dps_max) {
                    total_dps_max = player.target_dps;
                }
                if (player.target_dps > dps_max[build.name]) {
                    dps_max[build.name] = player.target_dps;
                }
                if (gen_og > 20) {
                    uptime_sum[build.name] += (uptime_g + uptime_og) / 2.0;  // todo unfair weight
                } else {
                    uptime_sum[build.name] += uptime_g;
                }
            }
        }
    }
    const table_id = `buff_${buff_id}_tbl`;
    html += `<table id='${table_id}'><tr><th style='width:170px;text-align:left;' onclick='sortTable(0,\"${table_id}\")'>Build</th>` +
        `<th onclick='sortTable(1,\"${table_id}\")' style="width: 25px;">#</th>` +
        `<th onclick='sortTable(2,\"${table_id}\")'>DPS</th>` +
        `<th>DPS VIZ</th>` +
        `<th onclick='sortTable(4,\"${table_id}\")'>CC</th>` +
        `<th onclick='sortTable(5,\"${table_id}\")'>Uptime*</th></tr>`;
    for (const build_name in build_count) {
        html += table_row([`${spec_icon(builds[build_name].elite_spec)}${build_name}`,
                build_count[build_name],

                `${Math.round(dps_sum[build_name] / build_count[build_name])}`,
                dist_viz(dpss[build_name], 100, total_dps_max, get_class_from_spec(builds[build_name].elite_spec)),
                `${Math.round(cc_sum[build_name] / build_count[build_name])}`,
                `${Math.round(uptime_sum[build_name] / build_count[build_name] * 10) / 10}%`],
            [undefined, "class='right-align'", "class='right-align'", "class='right-align'",
                "class='right-align'", "class='right-align'"]);
    }
    html += "</table>"
    html += "*Squad or Group depending on build and patch"
    return html
}

function load() {
    const inpFA = <HTMLInputElement>document.getElementById('filter_after')
    const inpFB = <HTMLInputElement>document.getElementById('filter_before')
    inpFA.value = "2020-01-01";
    let tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    inpFB.valueAsDate = tomorrow;
    document.getElementById('filter_after').onchange = generate;
    document.getElementById('filter_before').onchange = generate;
    document.getElementById("btn2021").onclick = function () {
        inpFA.value = "2021-01-01";
        inpFB.value = "2021-12-31";
        generate();
    }
    document.getElementById("btn2022").onclick = function () {
        inpFA.value = "2022-01-01";
        inpFB.value = "2022-12-31";
        generate();
    }
    document.getElementById("btnPOF").onclick = function () {
        inpFA.value = "2017-09-22";
        inpFB.value = "2022-02-27"
        generate();
    }
    document.getElementById("btnEOD").onclick = function () {
        inpFA.value = "2022-02-28";
        inpFB.valueAsDate = tomorrow;
        generate();
    }
    document.getElementById("btnALL").onclick = function () {
        inpFA.value = "2000-01-01";
        inpFB.value = "2050-01-01";
        generate();
    }
    document.getElementById("ckbCM").onchange = function () {
        (<HTMLInputElement>document.getElementById("ckbNM")).checked = true;
        generate();
    }
    document.getElementById("ckbNM").onchange = function () {
        (<HTMLInputElement>document.getElementById("ckbCM")).checked = true;
        generate();
    }
    load_boss("Icebrood Construct");
}

function generate() {
    document.getElementById("title-text").innerHTML = encounter_name;
    (<HTMLImageElement>document.getElementById("boss-img")).src = encounter_img;
    (<HTMLLinkElement>document.getElementById("favicon")).href = encounter_img;
    document.title = encounter_name;
    const after = (<HTMLInputElement>document.getElementById('filter_after')).valueAsDate;
    const before = (<HTMLInputElement>document.getElementById('filter_before')).valueAsDate;
    const allowCM = (<HTMLInputElement>document.getElementById('ckbCM')).checked;
    const allowNM = (<HTMLInputElement>document.getElementById('ckbNM')).checked;
    let suc_filtered = successful_logs.filter(e => {
        return new Date(e.data.start_time) > after && new Date(e.data.start_time) < before && (allowCM && e.data.is_cm || allowNM && !e.data.is_cm);
    });
    let fail_filtered = failed_logs.filter(e => {
        return new Date(e.data.start_time) > after && new Date(e.data.start_time) < before && (allowCM && e.data.is_cm || allowNM && !e.data.is_cm);
    });
    const kill_times = Array.from(suc_filtered, e => e.data.duration);
    document.getElementById("kill-time-diagram").innerHTML = create_line_graph(kill_times, "Kill time", true);
    document.getElementById("fail-attempts").innerHTML = failed_attempts(fail_filtered);
    document.getElementById("successful-attempts").innerHTML = successful_attempts(suc_filtered);
    document.getElementById("dps-builds").innerHTML = dps_builds(suc_filtered);
    document.getElementById("alac-builds").innerHTML = boons_classes(suc_filtered, ALACRITY_BUFF_ID, "Alacrity");
    document.getElementById("quick-builds").innerHTML = boons_classes(suc_filtered, QUICKNESS_BUFF_ID, "Quickness");
    sortTable(1, "buff_30328_tbl");
    sortTable(1, "buff_1187_tbl");
    sortTable(1, "dps_tbl");
}