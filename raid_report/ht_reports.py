import json
import math
import os
from datetime import timedelta
from enum import Enum
from math import sqrt
from string import Template

from dps_report_api import DpsReportAPI, get_ei_data_cached, EliteInsightData, EliteInsightPlayer
from gw2 import gw2
from gw2.gw2 import WEAPON_SWAP_SKILL_ID, DODGE_SKILL_ID2, MIRAGE_CLOAK, DODGE_SKILL_ID1
from logmanagercachereader import LogManagerCache, LMEncounter
from raid_report.stats_creator import PerPersonStat, PersonalStatsMode
from raid_report.html_graph_tool import create_line_graph, create_split_graphv2, create_multi_line_graph, \
    create_graph_legend

skip = [
    "https://dps.report/ZJ6t-20220308-232635_boss",
    "https://dps.report/hvHy-20220308-232852_boss",
    "https://dps.report/9B4D-20220308-234153_boss",
    "https://dps.report/FUJS-20220309-214549_boss",
    "https://dps.report/y4EQ-20220314-210353_void",
    "https://dps.report/pflw-20220321-215952_void",
    "https://dps.report/HJuE-20220322-230855_void",
    "https://dps.report/ZIUg-20220324-215848_void",
    "https://dps.report/87Ss-20220330-215553_void",
    "https://dps.report/S7rB-20220401-190101_void",
    "https://dps.report/4nRd-20220401-190425_void",
    "https://dps.report/jf3X-20220401-191018_void",
    "https://dps.report/4T4N-20220401-191813_void",
    "https://dps.report/74M4-20220401-192721_void",
    "https://dps.report/lxc6-20220401-193054_void",
    "https://dps.report/P9sB-20220401-194320_void",
    "https://dps.report/Hk4v-20220404-205450_void",
    "https://dps.report/yQql-20220405-234147_void",
    "https://dps.report/YVCQ-20220406-204020_void",
    "https://dps.report/nML0-20220406-204926_void",
    "https://dps.report/3PbC-20220406-205800_void",
    "https://dps.report/k7A5-20220406-210608_void",
    "https://dps.report/WspE-20220409-175937_void",
    "https://dps.report/95rS-20220413-202452_void",
    "https://dps.report/4d8g-20220417-094720_void",
    "https://dps.report/V0aw-20220421-204345_void",
    "https://dps.report/cmMi-20220421-205352_void",
    "https://dps.report/F1Yf-20220421-210808_void",
    "https://dps.report/LtyB-20220425-224647_void",
    "https://dps.report/gPlR-20220429-231402_void",
    "https://dps.report/k4uU-20220503-225404_void",
    "https://dps.report/Svp8-20220503-230624_void",
    "https://dps.report/ybpJ-20220507-203521_void",
    "https://dps.report/frBJ-20220507-205445_void",
    "https://dps.report/IH40-20220511-215625_void",
    "https://dps.report/0I9x-20220515-171128_void",
    "https://dps.report/FbmH-20220517-193747_void",
    "https://dps.report/3lgX-20220519-213801_void",
    "https://dps.report/bR0J-20220523-214004_void",
    "https://dps.report/xToz-20220527-202704_void",
    "https://dps.report/4PLu-20220531-230147_void",
    "https://dps.report/fl24-20220604-140618_void",
    "https://dps.report/i71e-20220604-142817_void",
    "https://dps.report/2vcT-20220608-201437_void",
    "https://dps.report/RIgh-20220612-215249_void",
    "https://dps.report/RbPd-20220616-214039_void",
    "https://dps.report/NDbV-20220616-215410_void",
    "https://dps.report/uHih-20220620-195112_void",

    "https://dps.report/abun-20230223-012117_void",
    "https://dps.report/vyKk-20230216-222430_void",

    "https://dps.report/XhEX-20230114-222653_void",
    "https://dps.report/ASQW-20230114-223533_void",
    "https://dps.report/iVDr-20230114-224522_void",
    "https://dps.report/ZExZ-20230114-225102_void",
    "https://dps.report/X8KQ-20230114-225843_void",
    "https://dps.report/s3Bn-20230114-231401_void",
    "https://dps.report/3tkI-20230114-232524_void",
    "https://dps.report/c64X-20230114-233523_void",
    "https://dps.report/TyOD-20230124-210824_void",
    "https://dps.report/6vpT-20230124-211020_void",
    "https://dps.report/PQSr-20230124-211146_void",
    "https://dps.report/yMl3-20230124-211524_void",
    "https://dps.report/tNV5-20230124-212238_void",
    "https://dps.report/L3xc-20230124-213052_void",
    "https://dps.report/oJy3-20230124-213432_void",
    "https://dps.report/xx9p-20230124-213813_void",
    "https://dps.report/C4QJ-20230124-214554_void",
    "https://dps.report/hOMI-20230124-215422_void",
    "https://dps.report/oDg3-20230124-215816_void",
    "https://dps.report/Jo6H-20230205-234921_void",
    "https://dps.report/56ix-20230205-235931_void",
    "https://dps.report/b046-20230206-000644_void",
    "https://dps.report/4BvB-20230206-000921_void",
    "https://dps.report/I6nW-20230206-001151_void",
    "https://dps.report/n1LG-20230206-002459_void",
    "https://dps.report/KEm6-20230206-004020_void",
    "https://dps.report/jRoe-20230206-004519_void",
    "https://dps.report/ED7H-20230206-004829_void",
    "https://dps.report/Y8RH-20230206-010021_void",
    "https://dps.report/RKlF-20230208-223643_void",
    "https://dps.report/LuOE-20230208-223741_void",
    "https://dps.report/dPjY-20230208-224110_void",
    "https://dps.report/CRld-20230208-224554_void",
    "https://dps.report/ndJM-20230208-225120_void",
    "https://dps.report/vUz2-20230208-225702_void",
    "https://dps.report/1FLi-20230208-230212_void",
    "https://dps.report/Rznr-20230208-230442_void",
    "https://dps.report/JnAT-20230208-231348_void",
    "https://dps.report/NqHT-20230208-232639_void",
    "https://dps.report/SXR1-20230215-223656_void",
    "https://dps.report/qMVk-20230216-210704_void",
    "https://dps.report/ehrM-20230216-211000_void",
    "https://dps.report/XOoW-20230216-211825_void",
    "https://dps.report/y37D-20230216-212923_void",
    "https://dps.report/j8DO-20230216-213815_void",
    "https://dps.report/4xLU-20230216-215101_void",
    "https://dps.report/LK44-20230216-220344_void",
    "https://dps.report/UFI6-20230222-224430_void",
    "https://dps.report/kula-20230223-003354_void",
    "https://dps.report/epGH-20230223-004356_void",
    "https://dps.report/Szkn-20230223-005651_void",
    "https://dps.report/mNXi-20230223-010346_void",
    "https://dps.report/cNhK-20230223-010749_void",
]
DAMAGE_MOD_ID_SCHOLAR = -202180908

KICK_SKILL_ID = 27266

CR_CACHE_PATH = "E:\\GW2LogsData\\"

klog_path = "E:\\GW2LogsData\\kerchu_ht_logs\\"


def upload_file_with_list_cache(path):
    with open(path + "list.json", "r") as f:
        log_list = json.loads(f.read())
    print(f"loaded list with {len(log_list)} logs")
    for filename in os.scandir(path):
        if filename.is_file() and filename.path[-6:] == ".zevtc":
            if filename.path not in log_list:
                print("uploading " + filename.path)
                resp = log_list[filename.path] = DpsReportAPI().upload_file(filename.path)
                if not resp:
                    print("failed")
                    continue
                print("uploaded to " + resp.get_permalink())
                link = resp.get_permalink()
                log_list[filename.path] = link
                with open(path + "list.json", "w") as f:
                    json.dump(log_list, f)
    return log_list


def get_kerchu_logs_url():
    return upload_file_with_list_cache(klog_path)


klog_path21 = "E:\\GW2LogsData\\kerchu2-dragonvoid.txt"
klog_path22 = "E:\\GW2LogsData\\kerchu_ht2_2_logs\\"


def get_kerchu22_logs_url():
    return upload_file_with_list_cache(klog_path22)


def get_kerchu2_logs_url():
    with open(klog_path21) as f:
        log_urls = f.read().split("\n")
    # create map with fake filenames
    "".split()
    logs2 = get_kerchu22_logs_url()
    # none_dup_logs2 = {}
    for fn2 in logs2:
        url2 = logs2[fn2]
        found_dup = False
        for url in log_urls:
            if url.split("-")[1] == url2.split("-")[1]:
                hhmm1 = int(url.split("-")[2][0:4])
                hhmm2 = int(url2.split("-")[2][0:4])
                if hhmm1 == hhmm2:
                    print("Found duplicate", url, url2)
                    found_dup = True
                    break
                if abs(hhmm1 - hhmm2) < 1:
                    print("poss dup:", url, url2)
        if not found_dup:
            # none_dup_logs2[fn2] = logs2[fn2]
            log_urls.append(url2)
    log_urls.sort(key=lambda url: int(url.split("-")[1] + url.split("-")[2].split("_")[0]))
    log_urls = filter(lambda url: url not in skip, log_urls)
    logs = {}
    i = 0
    for log in log_urls:
        logs["ffn" + str(i)] = log
        i += 1
    return logs


def get_rpfy_log_urls():
    lmc = LogManagerCache()
    log_list = lmc.get_logs(encounter_filter=[LMEncounter.DragonVoid], cm_filter=[True])
    logs = {}
    for log in log_list:
        if log.get_dps_report_url() and log.get_dps_report_url() not in get_rpfy_fake_succsess_links():
            logs[log.get_filename()] = log.get_dps_report_url()
    return logs


def get_rpfy_fake_succsess_links():
    return ["https://dps.report/LgG8-20220925-222629_void",
            "https://dps.report/rdH6-20220915-220404_void",
            "https://dps.report/I6o8-20220915-205905_void"]
    # return ["https://dps.report/I6o8-20220915-205905_void",
    #        "https://dps.report/qiD7-20220915-214910_void",
    #        "https://dps.report/z92I-20221005-214525_void",
    #        "https://dps.report/akPC-20220915-212946_void",
    #        "https://dps.report/Xpml-20220915-212946_void",
    #        "https://dps.report/rdH6-20220915-220404_void",
    #        "https://dps.report/VyHN-20221009-201703_void",
    #        "https://dps.report/1drF-20220929-212430_void",
    #        "https://dps.report/M2J4-20220928-224300_void",
    #        "https://dps.report/TJ8b-20220929-220513_void"]


name_acc_dict = {
    "Kerchu.1906": "Kerchu",
    "NotOverlyCheesy.9427": "Ent",
    # "Aegas.1069": "Ewayne",
    "CaptainBloodloss.5732": "Ines",
    "rivvyn.6257": "Rivvyn",
    "Warotrix.9124": "Waro",
    "PVzweiundvierzig.7381": "pv42",
    "FunNinja.7403": "Toucan",
    # "RDEF.8513": "RDEF",
    "Lele.3165": "Lele",
    "Mihkael.1954": "Lele",
    "KaptainKittens.6135": "Ben (Kittens)",
    "MoreEwayne.1732": "Ewayne",
    "CaptainSalmon.4361": "Ines",
    "Ker.5304": "Kerchu",
    "flare.8627": "Flare",
    "manaflare.5260": "Flare",
    "Messalina.5829": "Flare",
    # "RisayaKinan.9876": "Risaya",
    # k2
    "Dartas.8520": "Darta",
    "LockNarad.3571": "LockNarad",
    "Akvel.5084": "Akvel",
    "ShinRa Alex.6547": "ShinRa Alex",
    "Gren.2573": "Gren",
    "Annabiki.2351": "Annabiki",
    "Infrared.3179": "Infrared",
    "Marmeladka.9017": "Marmeladka",
    "Kisana.4318": "Kisana",
    # "Grendaline.4076": "Grendaline",
    # "Dagon.2793": "Dagon",
    "CombinE FiftyFour.2547": "CombinE 54",
    # "schmilblick.7084": "schmilblick",
    # "oOlExiiT.9520": "oOlExiiT",
    # "Pinky Donkey.9472": "Pinky Donkey",
    # "Kilmisae.9543": "Kilmisae",
    # "Perseus.6053": "Perseus",
    # "KoKo.5147": "KoKo",
    # "HeyItsLavi.3172": "Lavi",
    # "David.6905": "David",
    # "lumi.5203": "lumi",
    # "Paranoiak.1732": "Paranoiak",
    # "Begrasmarmelt Begackeiert.4273": "Begrasmarmelt ...",
    "Hinn.5389": "Hinn",
    # "Kuzan.6059": "Kuzan",
    "luria.1265": "luria",
    "Sin.5306": "Sin",
    "Casidhe.4596": "Casidhe",
    # "Heyuo.2306": "Heyuo",
    # "Kujama.7261": "Kujama",
    # "Tyranokhan.1905": "Tyranokhan",
    # "PpeppinoCz.9258": "PpeppinoCz",
    # "Scykron.7435": "Scykron",
    # "beng.7594": "Beng",
    # "Naglik.9147": "Naglik",
    "HellyS.3129": "HellyS"
    # k1
    # "CDrakulus.6529": "CDrakulus",
    # "Andulias.9516": "Adulias",
    # "Huncex.1203": "Hunex",
    # "Hunex.3619": "Hunex",
    # "kres.7609": "kres",
    # "Gio Leonheart.1675": "Leonheart",
    # "Unidentified.8329": "Unidentified",
    # "Amidar.2814": "Amidar",
    # "necrobionic.3805": "necrobionic",
    # "Bobby.5261": "Bobby",
    # "Mijkzor.2748": "Mijkzor",
    # "Ylana.8461": "Ylana",
    # "beng.7594": "Beng",
    # "kobi.5236": "kobi",
    # "Flyingbeggars.9406": "Flyingbeggars",
    # "leodrago.9251": "leodrago",
    # "Aeron.6247": "Aeron",
    # "xxxime.9401": "xxxime",
    # "Schokonelke.4387": "SchokoNelke",
    # "Hatsune.4815": "Hatsune"

}

succsess_log = "https://dps.report/LVXC-20230226-204554_void"


class PercentTemplate(Template):
    delimiter = "%"


class StatisticsData:
    def __init__(self):
        self.log_count = 0
        self.dc_count = 0
        self.total_time = timedelta(0)
        self.total_power_damage = 0
        self.total_condi_damage = 0
        self.scholar_uptime_sum = 0
        self.scholar_uptime_count = 0
        self.total_damage_taken = 0
        self.mordem_shokwave = 0
        self.greens_missed = 0
        self.greens_done = 0
        self.fallen_off = 0
        self.skill_cast_counts = {}
        self.weapon_swaps = 0
        self.dodges = 0
        self.total_skills_canceled = 0
        self.total_cancel_time = timedelta(0)
        self.chined = 0
        self.chomped = 0
        self.bees = 0
        self.goop = 0
        self.kicks = 0
        self.pp_shokewave = PerPersonStat(PersonalStatsMode.SUMME, pp_name_acc_dict=name_acc_dict)
        self.pp_chin = PerPersonStat(PersonalStatsMode.SUMME, pp_name_acc_dict=name_acc_dict)
        self.pp_chomp = PerPersonStat(PersonalStatsMode.SUMME, pp_name_acc_dict=name_acc_dict)
        self.pp_bees = PerPersonStat(PersonalStatsMode.SUMME, pp_name_acc_dict=name_acc_dict)
        self.pp_goop = PerPersonStat(PersonalStatsMode.SUMME, pp_name_acc_dict=name_acc_dict)
        self.pp_present = PerPersonStat(PersonalStatsMode.SUMME, pp_name_acc_dict=name_acc_dict)
        self.pp_avg_dps = PerPersonStat(PersonalStatsMode.AVERAGE, pp_name_acc_dict=name_acc_dict)
        self.pp_sum_dmg = PerPersonStat(PersonalStatsMode.SUMME, pp_name_acc_dict=name_acc_dict)
        self.pp_max_heal = PerPersonStat(PersonalStatsMode.MAXIMUM, pp_name_acc_dict=name_acc_dict)
        self.pp_sum_heal = PerPersonStat(PersonalStatsMode.SUMME, pp_name_acc_dict=name_acc_dict)
        self.pp_avg_hps = PerPersonStat(PersonalStatsMode.AVERAGE, pp_name_acc_dict=name_acc_dict)
        self.pp_sum_skill_casts = PerPersonStat(PersonalStatsMode.SUMME, pp_name_acc_dict=name_acc_dict)
        self.pp_avg_skill_casts = PerPersonStat(PersonalStatsMode.AVERAGE, pp_name_acc_dict=name_acc_dict)
        self.pp_avg_res_time = PerPersonStat(PersonalStatsMode.AVERAGE, pp_name_acc_dict=name_acc_dict)
        self.pp_avg_cc = PerPersonStat(PersonalStatsMode.AVERAGE, pp_name_acc_dict=name_acc_dict)
        self.pp_dist_to_sq_mid = PerPersonStat(PersonalStatsMode.AVERAGE, pp_name_acc_dict=name_acc_dict)
        self.pp_condi_cleanse_out = PerPersonStat(PersonalStatsMode.SUMME, pp_name_acc_dict=name_acc_dict)
        self.pp_avg_hp_percent = PerPersonStat(PersonalStatsMode.AVERAGE, pp_name_acc_dict=name_acc_dict)
        self.pp_unique_specs_played = PerPersonStat(PersonalStatsMode.COUNT_UNIQUE, pp_name_acc_dict=name_acc_dict)
        self.pp_fallen_off_first = PerPersonStat(PersonalStatsMode.SUMME, pp_name_acc_dict=name_acc_dict)
        self.dps_counted_phase = ["Jormag", "Primordus", "Kralkatorrik", "Mordremoth", "Zhaitan", "Soo-Won"]
        self.pp_avg_dps_per_phase = {}
        self.pp_max_dps_per_phase = {}
        for dragon in self.dps_counted_phase:
            self.pp_avg_dps_per_phase[dragon] = PerPersonStat(PersonalStatsMode.AVERAGE, pp_name_acc_dict=name_acc_dict)
            self.pp_max_dps_per_phase[dragon] = PerPersonStat(PersonalStatsMode.MAXIMUM, pp_name_acc_dict=name_acc_dict)


def format_per_person_stat_entry(pp_stat, best_name, to_seconds, negate, show_as_float):
    if best_name != "":
        best_val = pp_stat[best_name]
        if negate:
            best_val = -best_val
        if to_seconds:
            if best_val > 60:
                best_val = timedelta(seconds=int(best_val))
            else:
                best_val = f"{best_val:.2f}s"
        else:
            if show_as_float:
                best_val = f"{best_val:.1f}"
            else:
                best_val = f"{int(best_val):,}".replace(",", " ")
    else:
        best_val = 0
    return best_val


def create_person_stats_entry(name, pp_stat: PerPersonStat, *, to_seconds=False, negate=False, unit="",
                              show_as_float=False, bg_color=None):
    top_names = pp_stat.get_arg_top(17)
    style = ""
    if bg_color:
        style = f" style='background: {bg_color};'"
    cps_entry = f"<div class='personal-stat-entry'{style}>{name}<br/>"
    for pname in top_names:
        if pname == "":
            cps_entry += "<br/>"
            continue
        val = format_per_person_stat_entry(pp_stat, pname, to_seconds, negate, show_as_float)
        cps_entry += f"<br/>{pname} - {val:}{unit}"
    cps_entry += "<br/>"
    cps_entry += "</div>"
    return cps_entry


def create_personal_stats(stats: StatisticsData):
    out_str = ""
    out_str += create_person_stats_entry("Average DPS", stats.pp_avg_dps)
    for dragon in stats.dps_counted_phase:
        out_str += create_person_stats_entry(f"Average {dragon} DPS", stats.pp_avg_dps_per_phase[dragon],
                                             bg_color=get_boss_color(dragon) + "33")
    out_str += "<br/>"
    for dragon in stats.dps_counted_phase:
        out_str += create_person_stats_entry(f"Maximum {dragon} DPS", stats.pp_max_dps_per_phase[dragon],
                                             bg_color=get_boss_color(dragon) + "33")
    out_str += "<br/>"
    out_str += create_person_stats_entry("Total damage", stats.pp_sum_dmg)
    out_str += create_person_stats_entry("Total healing", stats.pp_sum_heal)
    out_str += create_person_stats_entry("Max Healing in a attempt", stats.pp_max_heal)
    # out_str += create_person_stats_entry("Average HPS", stats.pp_avg_hps, show_as_float=True)
    out_str += create_person_stats_entry("Hit by Mordremoth shockwave<br/>(multihits against invuln)",
                                         stats.pp_shokewave)
    out_str += create_person_stats_entry("Chinned", stats.pp_chin)
    out_str += create_person_stats_entry("Chomped", stats.pp_chomp)
    out_str += create_person_stats_entry("Beeed", stats.pp_bees)
    # out_str += create_person_stats_entry("Gooped", stats.pp_goop)
    out_str += create_person_stats_entry("Attendance", stats.pp_present)
    out_str += "<br/>"
    out_str += create_person_stats_entry("Skills cast", stats.pp_sum_skill_casts)
    out_str += create_person_stats_entry("Skills cast per fight", stats.pp_avg_skill_casts)
    out_str += create_person_stats_entry("Average res time", stats.pp_avg_res_time, show_as_float=True)

    # out_str += create_person_stats_entry("Average movement", stats.pp_avg_move_units, unit="units")
    # out_str += "<br/>"
    # out_str += create_person_stats_entry("Quickness given (Group)", stats.pp_avg_grp_quick, unit="%",
    #                                     show_as_float=True)
    out_str += create_person_stats_entry("Average breakbar damage", stats.pp_avg_cc)
    out_str += create_person_stats_entry("Distance to squad center", stats.pp_dist_to_sq_mid, negate=True, unit="units")
    out_str += create_person_stats_entry("Average HP", stats.pp_avg_hp_percent, unit="%", show_as_float=True)
    out_str += create_person_stats_entry("Unique Professions played", stats.pp_unique_specs_played)
    out_str += create_person_stats_entry("Allied condis cleansed", stats.pp_condi_cleanse_out)
    out_str += create_person_stats_entry("First to fall of", stats.pp_fallen_off_first)
    return out_str


def create_stats_entry(name, value, *, show_always=False):
    if isinstance(value, timedelta):
        value = timedelta(seconds=int(value.total_seconds()))
    if isinstance(value, int) and value >= 1e4:
        value = f"{int(value):,}".replace(",", " ")
    clazz = "stats-entry-hidden"
    if show_always:
        clazz = "stats-entry"
    return f"<div class='{clazz}'><span>{name}</span><span>{value}</span></div>\n"


skill_detail_cache = {}


def get_skill_name_by_id(skill_id) -> str:
    return skill_detail_cache[skill_id].name


def get_skill_icon_by_id(skill_id) -> str:
    return skill_detail_cache[skill_id].icon_url


def get_skill_entry(skill_id):
    name = get_skill_name_by_id(skill_id)
    name = name.replace("'", "")
    return f"<a href='https://wiki.guildwars2.com/index.php?search={name}'>" \
           f"<img class='skill-icon' src='{get_skill_icon_by_id(skill_id)}'/>{name}</a>"


def create_stats_content(stats: StatisticsData):
    out_str = "<div class='stats-inner'>"
    out_str += create_stats_entry("Attempts", stats.log_count, show_always=True)
    out_str += create_stats_entry("Total time", stats.total_time, show_always=True)
    out_str += create_stats_entry("Total power damage", stats.total_power_damage, show_always=True)
    out_str += create_stats_entry("Total condi damage", stats.total_condi_damage, show_always=True)
    out_str += create_stats_entry("Total damage taken", stats.total_damage_taken, show_always=True)
    out_str += create_stats_entry("Jumping is hard", stats.mordem_shokwave, show_always=True)
    out_str += create_stats_entry("Chomp", stats.chomped, show_always=True)
    out_str += create_stats_entry("Chinned", stats.chined, show_always=True)
    out_str += create_stats_entry("Not the bees", stats.bees, show_always=True)
    out_str += create_stats_entry("Greens missed", stats.greens_missed, show_always=True)
    out_str += create_stats_entry("Greens done", stats.greens_done, show_always=True)
    out_str += create_stats_entry("Fallen off the arena", stats.fallen_off, show_always=True)
    # out_str += create_stats_entry("Total damage taken", stats., show_always=True)
    # out_str += create_stats_entry("Total damage taken", stats.total_damage_taken, show_always=True)
    out_str += create_stats_entry("Scholar uptime (hits)",
                                  f"{100 * stats.scholar_uptime_count / stats.scholar_uptime_sum:.1f}%")
    out_str += create_stats_entry("Disconnects", stats.dc_count, show_always=True)
    out_str += create_stats_entry("Unique skills cast", len(stats.skill_cast_counts))
    out_str += create_stats_entry("Total skills cast", sum(stats.skill_cast_counts.values()), show_always=True)
    out_str += create_stats_entry("Total skills canceled", stats.total_skills_canceled)
    out_str += create_stats_entry("Total time skills canceled", stats.total_cancel_time)
    skills_sorted = sorted(stats.skill_cast_counts, key=stats.skill_cast_counts.__getitem__)
    skills_sorted_no_aa = []
    for skill_id in skills_sorted:
        if not gw2.is_skill_auto_attack(skill_id):
            skills_sorted_no_aa.append(skill_id)
    out_str += create_stats_entry("Most used skill",
                                  f"{get_skill_entry(skills_sorted[-1])} ({stats.skill_cast_counts[skills_sorted[-1]]})",
                                  show_always=True)
    for i in range(2, 11):
        cnt_num_ext = "th"
        if i == 2:
            cnt_num_ext = "nd"
        if i == 3:
            cnt_num_ext = "rd"
        out_str += create_stats_entry(f"{i}{cnt_num_ext} most used skill",
                                      f"{get_skill_entry(skills_sorted[-i])} ({stats.skill_cast_counts[skills_sorted[-i]]})")
    least_count = stats.skill_cast_counts[skills_sorted[0]]
    least_str = get_skill_entry(skills_sorted[0])
    i = 1
    while stats.skill_cast_counts[skills_sorted[i]] == least_count:
        least_str += ", " + get_skill_entry(skills_sorted[i])
        i += 1
    out_str += create_stats_entry("Least used skills", f"{least_str} ({least_count})")
    out_str += create_stats_entry("Most used non-auto-attack",
                                  f"{get_skill_entry(skills_sorted_no_aa[-1])} ({stats.skill_cast_counts[skills_sorted_no_aa[-1]]})",
                                  show_always=True)
    for i in range(2, 11):
        cnt_num_ext = "th"
        if i == 2:
            cnt_num_ext = "nd"
        if i == 3:
            cnt_num_ext = "rd"
        out_str += create_stats_entry(f"{i}{cnt_num_ext} most used non-auto-attack",
                                      f"{get_skill_entry(skills_sorted_no_aa[-i])} ({stats.skill_cast_counts[skills_sorted_no_aa[-i]]})")
    out_str += create_stats_entry("Weapon swaps", stats.weapon_swaps)
    out_str += create_stats_entry("Dodges (incl. Mirage Cloak)", stats.dodges)
    # out_str += create_stats_entry("Total quickness applied", stats.total_time_quickness)
    # out_str += create_stats_entry("Total alacrity applied", stats.total_time_alac)
    # out_str += create_stats_entry("Total might applied", stats.total_time_might)
    # specs_sorted = sorted(stats.specs_log_counts, key=stats.specs_log_counts.__getitem__)
    # out_str += create_stats_entry("Most played specialization",
    #                              specs_sorted[-1].name + " (" + str(stats.specs_log_counts[specs_sorted[-1]]) + ")",
    #                              show_always=True)

    out_str += "</div>"
    out_str += "<button id='btnEvenMoreStats' onclick='toggleEvenMoreStats()'>Show even more stats</button>"
    return out_str


def get_boss_color(boss_name):
    ht_boss_colors = ["#6495fb", "#ff6d41", "#fe4bff", "#95c62f", "#2a2e0d", "#8c9cc8"]
    if boss_name in ["Purification 1", "Heart 1 Breakbar"]:
        return "#86b7ff"
    elif boss_name == "Jormag":
        return ht_boss_colors[0]
    elif boss_name == "Primordus":
        return ht_boss_colors[1]
    elif boss_name == "Kralkatorrik":
        return ht_boss_colors[2]
    elif boss_name == "Mordremoth":
        return ht_boss_colors[3]
    elif boss_name == "Zhaitan":
        return ht_boss_colors[4]
    elif boss_name == "Soo-Won":
        return ht_boss_colors[5]
    else:
        return "#d584d2"


def get_phase_boss(phase_name):
    if phase_name in ["Purification 3", "Purification 4", "Success", "Soo-Won 2", "Soo-Won 1"]:
        return "Soo-Won"
    return phase_name


def try_get_down_skill(player, death):
    if len(player.data["deathRecap"]) > 0:
        if "toDown" in player.data["deathRecap"][0]:
            for sk_dmg_data in player.data["deathRecap"][0]["toDown"]:
                if death["time"] - sk_dmg_data["time"] < 10000:
                    return sk_dmg_data["id"]


class ChinChompBees(Enum):
    Nothing = 0
    Chin = 1
    Chomp = 2
    Bees = 3
    Goop = 4


def get_chin_chomp(player: EliteInsightPlayer, eid: EliteInsightData) -> ChinChompBees:
    CHOMP_SKILL_ID = 65704
    CHIN_SKILL_ID = 65427
    BEES_SKILL_ID = 67703
    for dr in player.get_death_recaps():
        for dre in dr.get_to_down() + dr.get_to_kill():
            sk_id = dre.skill_id
            if sk_id == CHIN_SKILL_ID:
                return ChinChompBees.Chin
            if sk_id == CHOMP_SKILL_ID:
                return ChinChompBees.Chomp
            if sk_id == BEES_SKILL_ID:
                return ChinChompBees.Bees
            # if sk_id == GOOP_SKILL_ID:
            #    return  ChinChompBees.Goop
    return ChinChompBees.Nothing


def get_players_alive_at_time(ei_data: EliteInsightData, time_ms):
    return sum((1 for p in ei_data.players if p.get_health_percent_at_time(time_ms) > 0))


def get_wipe_reason(ei_data: EliteInsightData):
    # checks for greens / delayed wipes
    downs = ei_data.get_mechanic_events("Downed")
    jormag_green_times = [20200, 60500]
    primordus_green_times = [28700, 82800]
    zhaitan_green_times = [0, 32700, 65300]
    for phase in ei_data.phases:
        greens = None
        if phase.name == "Jormag":
            greens = jormag_green_times
        elif phase.name == "Primordus":
            greens = primordus_green_times
        elif phase.name == "Zhaitan":
            greens = zhaitan_green_times
        if greens is not None:
            phase_start = phase.get_start_time_ms()
            for green_time in greens:
                if get_players_alive_at_time(ei_data, green_time - 1000) < 10:
                    break
                green_down_count = 0
                for down in downs:
                    down_phase_time = down["time"] - phase_start
                    if green_time - 1000 < down_phase_time < green_time + 1000:
                        green_down_count += 1
                        if green_down_count >= 4:
                            return "Green", None
        # check for prev deads
        if get_players_alive_at_time(ei_data, phase.get_start_time_ms()) < 10:
            return "Previous phase", None

    # check for individual death
    deaths = ei_data.get_mechanic_events("Dead")
    if deaths is not None and len(deaths) > 0:
        pl_name = deaths[0]["actor"]
        player = ei_data.get_player_by_name(pl_name)
        # check for fall of:
        for i in range(deaths[0]["time"] // 150 - 10, deaths[0]["time"] // 150 + 2):
            pos = player.get_position_keyframe(i)
            if not pos:
                continue
            x, y = pos
            if sqrt((x - 370) ** 2 + (y - 370) ** 2) > 390:
                return "Fallen off", player
        kill_skill_id = try_get_down_skill(player, deaths[0])
        if kill_skill_id is None:
            pl_name = deaths[1]["actor"]
            player = ei_data.get_player_by_name(pl_name)
            kill_skill_id = try_get_down_skill(player, deaths[1])
        if kill_skill_id is None:
            return None, None
        skill = ei_data.get_skill_info(kill_skill_id)
        if skill is not None:
            sk_name = skill.name
        else:
            sk_name = str(kill_skill_id)
        if sk_name == "Targeted Expulsion":
            return "Spread", None
        if sk_name == "Void Pool":
            return "Red", None
        if sk_name == "Influence of the Void":
            return "Black", None
        if sk_name == "Jaws of Destruction":
            return "Chomp", None
        if sk_name == "Lava Slam":
            return "Chin", None
        if sk_name == "Shock Wave":
            return "Shock Wave Damage", None
        if sk_name == "Stomp":
            return "Giants", None
        if sk_name == "Putrid Deluge":
            return "small Reds", None
        if sk_name == "Magic Discharge":
            return "Heart Explosion", None
        if sk_name == "Swarm of Mordremoth":
            return "Bees", None
        if sk_name in ["Breath of Jormag", "Grasp of Jormag", "Void Explosion", "Share the Void", "65704", "Ice Spike",
                       "65427", "65017", "Branding Beam", "Crystal Barrage", "Void Discharge"]:  # not working/ to minor
            return None, None
        return sk_name, None
    return None, None


def strfdelta(tdelta: timedelta, fmt):
    d = {"days": tdelta.days}
    d["hours"], rem = divmod(tdelta.seconds, 3600)
    min, sec = divmod(rem, 60)
    d["minutes"] = f"{min:02d}"
    d["seconds"] = f"{sec:02d}"
    d["millis"] = f"{tdelta.microseconds // 1000:03d}"
    return fmt.format(**d)


def get_greens_done(ei_data: EliteInsightData, wipe_reason):
    green_times = {"Jormag": [20200, 60500], "Primordus": [28700, 82800], "Zhaitan": [0, 32700, 65300],
                   "Soo-Won 1": [9500, 55300], "Soo-Won 2": [17000, 62600]}
    green_count = 0
    for phase in ei_data.phases:
        if phase.name in green_times:
            for green_time in green_times[phase.name]:
                if green_time < phase.get_end_time_ms() - phase.get_start_time_ms():
                    green_count += 1
    if wipe_reason == "Green":
        green_count -= 1
    return green_count


def create_stats_and_log_content(logs, stats, wipe_phase_counts, day_log_count, day_log_duration_sum, day_dps_sum,
                                 reason_count_phases, rcp_wr, best_phase_times, best_phase_logs,
                                 sum_phase_times, count_phase_times, day_phase_count, day_phase_duration_sum,
                                 attempts_boss_percent):
    count = 0
    i = 0
    logs_content = ""
    best_attempt_percent = 0
    for fn in logs:
        i += 1
        print(i, "/", len(logs))
        if logs[fn] in skip:
            continue
        ei_data = get_ei_data_cached(logs[fn], silent=False)
        if ei_data.is_cm:
            if ei_data.is_success:
                print(logs[fn], "is suc")
            count += 1
            for skill in ei_data.skill_infos:
                if skill.id not in skill_detail_cache:
                    skill_detail_cache[skill.id] = skill
            wipe_reason = "Success"
            wipe_player = None
            if not ei_data.is_success:
                v = get_wipe_reason(ei_data)
                wipe_reason, wipe_player = v
            if wipe_reason is None:
                wipe_reason = "Other"
            logs_content += f"<tr><td>{ei_data.start_time.date()}</td><td>{ei_data.start_time.time()}</td>" \
                            f"<td>{strfdelta(ei_data.duration, '{minutes}:{seconds}.{millis}')}</td>" \
                            f"<td>{wipe_reason}</td> <td><a target='_blank' href='{logs[fn]}'>Log</a></td></tr>\n"
            if wipe_reason == "Green":
                stats.greens_missed += 1
            if wipe_reason == "Fallen off":
                stats.pp_fallen_off_first.add_data(wipe_player.account_name, 1)
                stats.fallen_off += 1
            day = ei_data.start_time.date()
            if day not in day_log_count:
                day_log_count[day] = 0
                day_log_duration_sum[day] = timedelta(0)
                day_dps_sum[day] = 0
                day_phase_count[day] = {}
                day_phase_duration_sum[day] = {}
            day_log_count[day] += 1
            day_log_duration_sum[day] += ei_data.duration
            day_dps_sum[day] += ei_data.get_squad_dps()
            stats.log_count += 1
            stats.total_time += ei_data.duration

            # self.scholar_uptime_sum = 0
            # self.scholar_uptime_count = 0

            last_phase = ei_data.get_main_phase().get_sub_phases(ei_data)[-1]
            wipe_phase_name = last_phase.name
            stats.greens_done += get_greens_done(ei_data, wipe_reason)
            if wipe_phase_name == "Soo-Won":
                wipe_phase_name = "Soo-Won 1"
            if ei_data.is_success:
                wipe_phase_name = "Success"
            if wipe_phase_name not in wipe_phase_counts:
                wipe_phase_counts[wipe_phase_name] = 0

            for rcp in reason_count_phases:
                if wipe_phase_name == rcp:
                    if wipe_reason not in rcp_wr[rcp]:
                        rcp_wr[rcp][wipe_reason] = 0
                    rcp_wr[rcp][wipe_reason] += 1

            wipe_phase_counts[wipe_phase_name] += 1

            attempt_boss_percent = 0
            for target in ei_data.targets:
                if target.name.startswith("The") and target.name.endswith("Void"):
                    attempt_boss_percent += 100 * (1 - target.final_health / target.total_health)
            best_attempt_percent = max(best_attempt_percent, attempt_boss_percent)
            attempts_boss_percent.append([attempt_boss_percent, best_attempt_percent])

            for p in ei_data.players:
                # if p.account_name not in name_acc_dict:
                #    print(p.account_name)
                stats.total_power_damage += p.get_total_power_damage()
                stats.total_condi_damage += p.get_total_condi_damage()
                stats.total_damage_taken += p.get_damage_taken()
                stats.pp_present.add_data(p.account_name, 1)
                stats.pp_avg_dps.add_data(p.account_name, p.get_all_targets_dps())
                stats.pp_sum_dmg.add_data(p.account_name, p.get_total_damage())
                stats.pp_max_heal.add_data(p.account_name, p.get_total_healing_out())
                stats.pp_sum_heal.add_data(p.account_name, p.get_total_healing_out())
                stats.pp_avg_hps.add_data(p.account_name, p.get_hps())
                stats.pp_avg_res_time.add_data(p.account_name, p.get_resurrect_time_s())
                stats.pp_avg_cc.add_data(p.account_name, p.get_total_breakbar_damage())
                stats.pp_dist_to_sq_mid.add_data(p.account_name, -p.get_dist_sq_center())
                stats.pp_condi_cleanse_out.add_data(p.account_name, p.get_condi_cleanse_out())
                avg_hp = 0
                for j in range(math.ceil(ei_data.duration.total_seconds())):
                    avg_hp += p.get_health_percent_at_time(j * 1000)
                avg_hp /= math.ceil(ei_data.duration.total_seconds())
                stats.pp_avg_hp_percent.add_data(p.account_name, avg_hp)
                stats.pp_unique_specs_played.add_data(p.account_name, p.profession_name)
                for dragon in stats.dps_counted_phase:
                    phase_index = -1
                    dragon_phase = None
                    for j, phase in enumerate(ei_data.phases):
                        if phase.name == dragon:
                            phase_index = j
                            dragon_phase = phase
                    if phase_index >= 0:
                        stats.pp_avg_dps_per_phase[dragon].add_data(p.account_name,
                                                                    p.get_all_targets_dps(phase=phase_index))
                        if phase_index < ei_data.get_main_phase().get_sub_phases_indices()[-1] or ei_data.is_success:
                            stats.pp_max_dps_per_phase[dragon].add_data(p.account_name,
                                                                        p.get_all_targets_dps(phase=phase_index))
                chinchompbees = get_chin_chomp(p, ei_data)
                if chinchompbees == ChinChompBees.Chomp:
                    stats.pp_chomp.add_data(p.account_name, 1)
                    stats.chomped += 1
                elif chinchompbees == ChinChompBees.Chin:
                    stats.pp_chin.add_data(p.account_name, 1)
                    stats.chined += 1
                elif chinchompbees == ChinChompBees.Bees:
                    stats.pp_bees.add_data(p.account_name, 1)
                    stats.bees += 1
                elif chinchompbees == ChinChompBees.Goop:
                    stats.pp_goop.add_data(p.account_name, 1)
                    stats.goop += 1
                for target in range(len(ei_data.targets)):
                    scholar = p.get_damage_mods_target(DAMAGE_MOD_ID_SCHOLAR)
                    if scholar:
                        stats.scholar_uptime_sum += scholar.total_hit_count
                        stats.scholar_uptime_count += scholar.active_hit_count
                sum_per_player = 0
                for skill_id in p.get_skill_ids_used():
                    if skill_id == WEAPON_SWAP_SKILL_ID:
                        stats.weapon_swaps += len(p.get_skill_cast(WEAPON_SWAP_SKILL_ID))
                    elif skill_id in [DODGE_SKILL_ID1, MIRAGE_CLOAK, DODGE_SKILL_ID2]:
                        stats.dodges += len(p.get_skill_cast(skill_id))
                    else:
                        for cast in p.get_skill_cast(skill_id):
                            if cast.cast_duration == - cast.time_gained and cast.cast_duration > timedelta(
                                    milliseconds=10):
                                stats.total_skills_canceled += 1
                                stats.total_cancel_time += cast.cast_duration
                                # print(f"cancel {gw2.gw2.get_skill_name(skill_id)}")
                            else:
                                if skill_id not in stats.skill_cast_counts:
                                    stats.skill_cast_counts[skill_id] = 0
                                sum_per_player += 1
                                stats.skill_cast_counts[skill_id] += 1
                stats.pp_sum_skill_casts.add_data(p.account_name, sum_per_player)
                stats.pp_avg_skill_casts.add_data(p.account_name, sum_per_player)
            for ev in ei_data.get_mechanic_events("ShckWv.H"):
                stats.pp_shokewave.add_data(ei_data.get_player_by_name(ev["actor"]).account_name, 1)
                stats.mordem_shokwave += 1
            for _ in ei_data.get_mechanic_events("DC"):
                stats.dc_count += 1
            for phase in ei_data.phases:
                if timedelta(milliseconds=phase.get_end_time_ms() + 5000) > ei_data.duration and not ei_data.is_success:
                    continue
                if phase.name not in best_phase_times:
                    best_phase_times[phase.name] = phase.get_duration_ms()
                    best_phase_logs[phase.name] = logs[fn]
                    sum_phase_times[phase.name] = 0
                    count_phase_times[phase.name] = 0
                if phase.name not in day_phase_count[day]:
                    day_phase_count[day][phase.name] = 0
                    day_phase_duration_sum[day][phase.name] = 0
                if best_phase_times[phase.name] > phase.get_duration_ms():
                    best_phase_times[phase.name] = phase.get_duration_ms()
                    best_phase_logs[phase.name] = logs[fn]
                sum_phase_times[phase.name] += phase.get_duration_ms()
                count_phase_times[phase.name] += 1
                day_phase_count[day][phase.name] += 1
                day_phase_duration_sum[day][phase.name] += phase.get_duration_ms()
    return logs_content


def main():
    logs = get_kerchu2_logs_url()  # get_rpfy_log_urls()
    stats = StatisticsData()
    best_phase_times = {}
    best_phase_logs = {}
    sum_phase_times = {}
    count_phase_times = {}
    wipe_phase_counts = {"Jormag": 0, "Primordus": 0, "Kralkatorrik": 0}  # force this to be first
    reason_count_phases = ["Jormag", "Primordus", "Kralkatorrik", "Purification 2", "Mordremoth", "Zhaitan"]
    rcp_wr = {}
    for rcp in reason_count_phases:
        rcp_wr[rcp] = {}

    day_log_count = {}
    day_log_duration_sum = {}
    day_dps_sum = {}
    day_phase_count = {}
    day_phase_duration_sum = {}
    attempt_boss_percent = []
    logs_content = "<table><tr><th>Date</th><th>Time</th><th>Duration</th><th>Wipe Reason</th><th>Link</th></tr>"
    logs_content += create_stats_and_log_content(logs, stats, wipe_phase_counts, day_log_count, day_log_duration_sum,
                                                 day_dps_sum, reason_count_phases, rcp_wr, best_phase_times,
                                                 best_phase_logs, sum_phase_times, count_phase_times, day_phase_count,
                                                 day_phase_duration_sum, attempt_boss_percent)
    logs_content += "</table>"
    avg_len_per_day = []
    avg_dps_per_day = []
    avg_phase_len_per_day = []  # [day_index][phase_index]
    avg_phase_len_days = {}
    avg_phase_len_phase_names = []
    avg_phase_len_colors = []
    for day in day_log_count:
        avg_len_per_day.append(day_log_duration_sum[day] / day_log_count[day])
        avg_dps_per_day.append(day_dps_sum[day] / day_log_count[day])
    day_index = 0
    interesting_phases = ["Purification 1", "Heart 1 Breakbar", "Jormag", "Primordus", "Kralkatorrik", "Purification 2",
                          "Mordremoth", "Zhaitan", "Purification 3", "Soo-Won 1", "Purification 4", "Soo-Won 2",
                          "Sum of bests"]
    for day in day_phase_count:
        avg_phase_len_per_day.append([])
        for phase_name in day_phase_count[day]:
            if phase_name not in interesting_phases or phase_name == "Soo-Won 2":
                continue
            if phase_name not in avg_phase_len_phase_names:
                avg_phase_len_phase_names.append(phase_name)
                avg_phase_len_colors.append(get_boss_color(get_phase_boss(phase_name)))
            phase_index = avg_phase_len_phase_names.index(phase_name)
            if day not in avg_phase_len_days:
                avg_phase_len_days[day] = len(avg_phase_len_days)
            day_index = avg_phase_len_days[day]
            while len(avg_phase_len_per_day[day_index]) <= phase_index:
                avg_phase_len_per_day[day_index].append(None)
            avg_phase_len_per_day[day_index][phase_index] = timedelta(
                milliseconds=day_phase_duration_sum[day][phase_name] / day_phase_count[day][phase_name])
            if phase_index > 0:
                avg_phase_len_per_day[day_index][phase_index] += avg_phase_len_per_day[day_index][phase_index - 1]
        day_index += 1
    graph_content = create_line_graph(avg_len_per_day, "average duration per day") + "\n"
    graph_content += create_line_graph(avg_dps_per_day, "average dps per day") + "\n"

    graph_content += create_multi_line_graph(avg_phase_len_per_day, "average successful phase length",
                                             colors=avg_phase_len_colors, y_max_overwrite=timedelta(minutes=15),
                                             extra_content=create_graph_legend(avg_phase_len_colors,
                                                                               avg_phase_len_phase_names, 3)) + "\n"
    graph_content += "<br/>"
    best_attempt_colors = ["#06617d", "red"]
    graph_content += create_multi_line_graph(attempt_boss_percent, "Attempt progress", colors=best_attempt_colors,
                                             width=1200, x_label="attempts", y_label="%",
                                             extra_content=create_graph_legend(best_attempt_colors, ["attempt", "best"],
                                                                               2)) + "\n"
    graph_content += "<br/>"
    phase_reached = []
    sub_phases = []
    sub_phases_names = []
    phase_reached_name = []
    split_graph_colors = []
    v2data = []
    for name in wipe_phase_counts:
        boss_name = get_phase_boss(name)
        if len(phase_reached) == 0 or phase_reached_name[-1] != boss_name:
            phase_reached_name.append(boss_name)
            if boss_name in ["Jormag", "Primordus", "Kralkatorrik", "Purification 2", "Mordremoth", "Zhaitan"]:
                sub_phases.append(list(rcp_wr[boss_name].values()))
                sub_phases_names.append(list(rcp_wr[boss_name].keys()))
                v2data.append(list(rcp_wr[boss_name].values()))
            else:
                sub_phases.append([])
                sub_phases[-1].append(wipe_phase_counts[name])
                sub_phases_names.append([])
                sub_phases_names[-1].append(name)
                v2data.append([])
                v2data[-1].append(wipe_phase_counts[name])
            phase_reached.append(wipe_phase_counts[name])
            split_graph_colors.append(get_boss_color(boss_name))
        else:
            phase_reached[-1] += wipe_phase_counts[name]
            sub_phases[-1].append(wipe_phase_counts[name])
            v2data[-1].append(wipe_phase_counts[name])
            sub_phases_names[-1].append(name)
    spn_v2 = []
    for spn in sub_phases_names:
        spn_v2.append([spn])
    graph_content += create_split_graphv2(v2data, [phase_reached_name, spn_v2], [split_graph_colors],
                                          all_label="Total") + "\n"
    main_template = PercentTemplate(
        "<!doctype html>\n<html><head><meta charset='utf-8'><title>Harvest Temple Report</title><style>\n" +
        "%CSS\n"
        "</style>\n" +
        "<script>\n" +
        "%JS\n" +
        "</script>" +
        "</head>\n<body>\n" +
        "<h1>Harvest Temple</h1>\n" +
        "<div onclick='toggleTimelines()' class='section-header' id='timelines-header'>Logs</div>\n" +
        "<div id='timelines'>%LOGS_CONTENT\n</div>\n" +
        "<div onclick='toggleRecords()' class='section-header' id='records-header'>Records</div>\n" +
        "<div id='records'>%RECORDS_CONTENT</div>" +
        "<div onclick='toggleStats()' class='section-header' id='stats-header'>Statistics</div>\n" +
        "<div id='stats'>%STATS_CONTENT</div>\n" +
        "<div onclick='toggleGraphs()' class='section-header' id='graphs-header'>Graphs</div>\n" +
        "<div id='graphs' class='ht-graphs'>%GRAPHS_CONTENT</div>\n" +
        "<div onclick='togglePersonal()' class='section-header' id='personal-header'>Best Players</div>\n" +
        "<div id='personal'>%PERSONAL_CONTENT</div>\n"
        "<div onclick='toggleVideo()' class='section-header' id='video-header'>Video</div>\n" +
        "<div id='video'>Please watch in 1440p to avoid terrible Youtube compression<br/>" +
        "<iframe width='1440' height='720' src='https://www.youtube.com/embed/l3IvG_sSYLo' title='YouTube video player' frameborder='0' allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>" +
        "</div>\n" +
        "</body></html>")
    with open("raid_report.css", "r") as css_file:
        css_content = css_file.read()
    personal = create_personal_stats(stats)
    records_content = "<table><tr><th>Phase</th><th>Best time</th><th>Best log</th><th>Avg time</th></tr>"
    sum_of_best = 0
    sum_of_avg = 0
    for phase in best_phase_times:
        if phase in interesting_phases:
            sum_of_best += best_phase_times[phase]
            sum_of_avg += sum_phase_times[phase] / count_phase_times[phase]
    best_phase_times["Sum of bests"] = sum_of_best
    best_phase_logs["Sum of bests"] = "https://youtu.be/dQw4w9WgXcQ"
    sum_phase_times["Sum of bests"] = sum_of_avg
    count_phase_times["Sum of bests"] = 1
    for phase in best_phase_times:
        if phase in interesting_phases:
            records_content += f"<tr><td>{phase}</td><td>{best_phase_times[phase] / 1000.0:.1f}s</td>" \
                               f"<td><a target='_blank' href=\"{best_phase_logs[phase]}\">Log</a></td>" \
                               f"<td>{sum_phase_times[phase] / 1000.0 / count_phase_times[phase]:.1f}s</td></tr>"
    records_content += "</table>"
    stats_content = create_stats_content(stats)
    js_file = open("script.js", "r")
    js_content = js_file.read()
    js_file.close()
    html = main_template.substitute(
        **{"JS": js_content, "CSS": css_content, "LOGS_CONTENT": logs_content, "STATS_CONTENT": stats_content,
           "RECORDS_CONTENT": records_content,
           "PERSONAL_CONTENT": personal,
           "GRAPHS_CONTENT": graph_content})
    with open("ht_k2_report.html", "w") as html_out:
        html_out.write(html)


if __name__ == '__main__':
    main()
