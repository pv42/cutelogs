const MAX_NUM_WINGS = 10

function setHoverListeners(arr) {
    for (let i = 0; i < arr.length; i++) {
        arr[i].onmouseenter = function (args) {
            activateHover(arr[i].offsetLeft - document.getElementById("timelines").scrollLeft, arr[i].offsetTop + 40, arr[i].getElementsByClassName("hidden-data")[0].innerHTML);
        }
        arr[i].onmouseleave = function (args) {
            deactivateHover();
        }
    }
}

let tlBaseHeight;
let recsBaseHeight;
let statsBaseHeight;
let graphsBaseHeight;
let personalBaseHeight;
let videoBaseHeight;
let evenMoreStats = false;

window.onload = function () {
    const ch = document.getElementsByTagName('body')[0].getElementsByClassName('timeline');
    for (let i = 0; i < ch.length; i++) {
        ch[i].onclick = function () {
            const extra = ch[i].parentElement.getElementsByClassName('day-details')[0];
            if (extra.style.display) {
                extra.style.removeProperty('display');
                ch[i].parentElement.style.removeProperty('padding-bottom');
                tlBaseHeight -= 193;
                document.getElementById("timelines").style.maxHeight = tlBaseHeight + "px";
            } else {
                extra.style.setProperty('display', 'inherit');
                ch[i].parentElement.style.setProperty('padding-bottom', '5px');
                tlBaseHeight += 193;
                document.getElementById("timelines").style.maxHeight = tlBaseHeight + "px";
            }
        }
    }
    const bbs = document.getElementsByClassName('boss-box');
    setHoverListeners(bbs);
    const wbs = document.getElementsByClassName('walk-box');
    setHoverListeners(wbs);
    for (let i = 1; i < MAX_NUM_WINGS; i++) {
        const wths = document.getElementsByClassName('wing-header-' + i)
        for (let j = 0; j < wths.length; j++) {
            wths[j].onclick = function (args) {
                for (let k = 0; k < MAX_NUM_WINGS; k++) {
                    const tbeles = args.target.parentElement.getElementsByClassName('wing-table-' + k);
                    if (tbeles.length > 0) {
                        tbeles[0].style.removeProperty('display');
                    }
                    const hdeles = args.target.parentElement.getElementsByClassName('wing-header-' + k);
                    if (hdeles.length > 0) {
                        hdeles[0].style.removeProperty('background');
                    }
                }
                args.target.parentElement.getElementsByClassName('wing-table-' + i)[0].style.setProperty('display', 'block');
                args.target.parentElement.getElementsByClassName('wing-header-' + i)[0].style.setProperty('background', '#817f76');
            }
        }
    }

    const tlDiv = document.getElementById("timelines");
    tlBaseHeight = tlDiv.offsetHeight + 20;
    tlDiv.style.setProperty("max-height", tlBaseHeight + "px");
    tlDiv.style.setProperty("transition-duration", "400ms");
    const recsDiv = document.getElementById("records");
    recsBaseHeight = recsDiv.offsetHeight + 20; // 20 for scrollbar
    recsDiv.style.setProperty("max-height", recsBaseHeight + "px");
    recsDiv.style.setProperty("transition-duration", "400ms");
    const statsDiv = document.getElementById("stats");
    statsBaseHeight = statsDiv.offsetHeight + 20; // 20 for scrollbar
    statsDiv.style.setProperty("max-height", statsBaseHeight + "px");
    statsDiv.style.setProperty("transition-duration", "400ms");
    const graphsDiv = document.getElementById("graphs");
    graphsBaseHeight = graphsDiv.offsetHeight + 20; // 20 for scrollbar
    graphsDiv.style.setProperty("max-height", graphsBaseHeight + "px");
    graphsDiv.style.setProperty("transition-duration", "400ms");
    const personalDiv = document.getElementById("personal");
    personalBaseHeight = personalDiv.offsetHeight + 20; // 20 for scrollbar
    personalDiv.style.setProperty("max-height", personalBaseHeight + "px");
    personalDiv.style.setProperty("transition-duration", "400ms");
    const videoDiv = document.getElementById("video");
    if(videoDiv) {
        videoBaseHeight = personalDiv.offsetHeight + 20; // 20 for scrollbar
        videoDiv.style.setProperty("max-height", videoBaseHeight + "px");
        videoDiv.style.setProperty("transition-duration", "400ms");
    }
    toggleTimelines();
}

function activateHover(x, y, content) {
    const hb = document.getElementById('hover-box');
    hb.innerHTML = content;
    hb.style.setProperty('left', -20 + x + 'px');
    hb.style.setProperty('top', 100 +y + 'px');
    hb.style.setProperty('display', 'block');
}

function deactivateHover() {
    const hb = document.getElementById('hover-box');
    hb.style.removeProperty('display');
}

function toggleTimelines() {
    const tlDiv = document.getElementById("timelines");
    if (tlDiv.style.maxHeight === "0px") {
        tlDiv.style.setProperty("max-height", tlBaseHeight + "px");
    } else {
        tlDiv.style.setProperty("max-height", "0px");
    }
}

function toggleRecords() {
    const recsDiv = document.getElementById("records");
    if (recsDiv.style.maxHeight === "0px") {
        recsDiv.style.setProperty("max-height", recsBaseHeight + "px");
    } else {
        recsDiv.style.setProperty("max-height", "0px");
    }
}
function toggleStats() {
    const statsDiv = document.getElementById("stats");
    if (statsDiv.style.maxHeight === "0px") {
        statsDiv.style.setProperty("max-height", statsBaseHeight + "px");
    } else {
        statsDiv.style.setProperty("max-height", "0px");
    }
}

function toggleGraphs() {
    const graphsDiv = document.getElementById("graphs");
    if (graphsDiv.style.maxHeight === "0px") {
        graphsDiv.style.setProperty("max-height", graphsBaseHeight + "px");
    } else {
        graphsDiv.style.setProperty("max-height", "0px");
    }
}

function togglePersonal() {
    const personalDiv = document.getElementById("personal");
    if (personalDiv.style.maxHeight === "0px") {
        personalDiv.style.setProperty("max-height", personalBaseHeight + "px");
    } else {
        personalDiv.style.setProperty("max-height", "0px");
    }
}

function toggleVideo() {
    const videoDiv = document.getElementById("video");
    if (videoDiv.style.maxHeight === "0px") {
        videoDiv.style.setProperty("max-height", videoBaseHeight + "px");
    } else {
        videoDiv.style.setProperty("max-height", "0px");
    }
}

function toggleEvenMoreStats() {
    evenMoreStats = !evenMoreStats;
    const btn = document.getElementById("btnEvenMoreStats");
    if(evenMoreStats) {
        btn.innerHTML = "Less Stats";
    } else {
        btn.innerHTML = "Show even more stats";
    }
    const divs = document.getElementsByClassName('stats-entry-hidden')
    for(let index=0 ;index < divs.length; index++)  {
        const statsDiv = divs[index];
        if(evenMoreStats) {
            statsDiv.style.setProperty("display", "inline-block");
        } else {
            statsDiv.style.removeProperty("display");
        }
    }
    const statsDiv = document.getElementById("stats");
    statsDiv.style.removeProperty("max-height" );
    statsDiv.style.removeProperty("transition-duration" );
    statsBaseHeight = statsDiv.offsetHeight + 20; // 20 for scrollbar
    statsDiv.style.setProperty("max-height", statsBaseHeight + "px");
    statsDiv.style.setProperty("transition-duration", "400ms");
}