import datetime

from datetime import timedelta
from string import Template

from typing import Dict, List

import gw2.raids
from gw2.gw2 import RAID_WING_MAP_IDS, MIGHT_BUFF_ID, ALACRITY_BUFF_ID, QUICKNESS_BUFF_ID, PROTECTION_BUFF_ID, \
    BLADETURN_REQUIEM, DEVOURING_CUT
from gw2.raids import get_wing_from_boss, get_display_name
from logmanagercachereader import LogManagerCache, correct_ch_id_name, LogManagerLog, EliteSpecialization
from raid_report.html_graph_tool import create_pie_chart, create_graph_from_content, create_line_graph, \
    create_multi_line_graph, create_graph_legend
from raid_report.stats_creator import StatisticsData, PerPersonStat, get_skill_name_by_id, get_skill_icon_by_id


class PercentTemplate(Template):
    delimiter = "%"


def strfdelta(time_delta: timedelta, fmt: str):
    d = {"D": time_delta.days}
    hours, rem = divmod(time_delta.seconds, 3600)
    minutes, seconds = divmod(rem, 60)
    d["H"] = '{:02d}'.format(hours)
    d["M"] = '{:02d}'.format(minutes)
    d["m"] = '{:02d}'.format(minutes + hours * 60 + time_delta.days * 60 * 24)
    d["S"] = '{:02d}'.format(seconds)
    d['s1'] = '{:04.1f}'.format(time_delta.microseconds / 100000 + seconds)
    t = PercentTemplate(fmt)
    return t.substitute(**d)


def create_bar_graph_spec_dps(stats):
    def get_s_dps(spec: EliteSpecialization):
        if spec in stats.kill_dps_per_spec_count:
            return stats.kill_dps_per_spec_sum[spec] / stats.kill_dps_per_spec_count[spec]
        return 0

    return create_specs_bar_graph(get_s_dps, "Elite Spec average DPS (Kills only)")


def create_specs_bar_graph(data_souce_function, name):
    elite_spec_groups = [
        [EliteSpecialization.Dragonhunter, EliteSpecialization.Firebrand, EliteSpecialization.Willbender],
        [EliteSpecialization.Berserker, EliteSpecialization.Spellbreaker, EliteSpecialization.Bladesworn],
        [EliteSpecialization.Scrapper, EliteSpecialization.Holosmith, EliteSpecialization.Mechanist],
        [EliteSpecialization.Druid, EliteSpecialization.Soulbeast, EliteSpecialization.Untamed],
        [EliteSpecialization.Daredevil, EliteSpecialization.Deadeye, EliteSpecialization.Specter],
        [EliteSpecialization.Tempest, EliteSpecialization.Weaver, EliteSpecialization.Catalyst],
        [EliteSpecialization.Chronomancer, EliteSpecialization.Mirage, EliteSpecialization.Virtuoso],
        [EliteSpecialization.Reaper, EliteSpecialization.Scourge, EliteSpecialization.Harbinger],
        [EliteSpecialization.Herald, EliteSpecialization.Renegade, EliteSpecialization.Vindicator],
    ]
    class_colors = [
        [255, 222, 10],  # g
        [61, 212, 222],  # w
        [41, 115, 227],  # en
        [10, 222, 135],  # ra
        [115, 97, 224],  # t
        [56, 56, 247],  # el
        [209, 59, 204],  # m
        [125, 227, 5],  # n
        [41, 41, 161],  # re
    ]
    max_spec_cnt = max([data_souce_function(i) for lst in elite_spec_groups for i in lst])
    upr = max(int((1.1 * max_spec_cnt) / 10) * 10, 1)
    content = ""
    x = 42
    for i in range(len(elite_spec_groups)):
        j = 0
        for elite_spec in elite_spec_groups[i]:
            if j == 1:
                color = f"rgb({class_colors[i][2] - 10},{class_colors[i][1] - 10},{class_colors[i][0] - 10})"
            elif j == 2:
                color = f"rgb({class_colors[i][2] - 20},{class_colors[i][1] - 20},{class_colors[i][0] - 20})"
            else:
                color = f"rgb({min(int(class_colors[i][2] + 10), 255)},{min(int(class_colors[i][1] + 10), 255)},{min(int(class_colors[i][0] + 10), 255)})"
            j += 1
            height = data_souce_function(elite_spec) / upr * 280
            content += f"<rect x='{x}' y='{10 + 280 - height}' width='11' height='{height}' style='fill:{color};' />"
            x += 12
        x += 3
    return create_graph_from_content(content, name, upr)


def create_bar_graph_specs_usage(stats):
    def get_s_cnt(spec: EliteSpecialization):
        if spec in stats.specs_log_counts:
            return stats.specs_log_counts[spec]
        return 0

    return create_specs_bar_graph(get_s_cnt, "Elite Spec Usage")


def create_line_graph_boons(stats: StatisticsData, year):
    x = 40
    x_step = 350 / 52
    first_mon = None
    for i in range(7):
        first_mon = datetime.date(day=i + 1, month=1, year=year)
        if first_mon.weekday() == 0:
            break
    week_data = []
    relevant_boons = list(
        stats.boon_uptimes_kills_per_day_sum[list(stats.boon_uptimes_kill_per_day_count.keys())[0]].keys())
    for week in range(52):
        mon = first_mon + timedelta(days=7 * week)
        wed = first_mon + timedelta(days=7 * week + 2)
        week_stats_sum = {}
        week_stats_cnt = 0
        for buff in relevant_boons:
            week_stats_sum[buff] = 0
        if mon in stats.boon_uptimes_kill_per_day_count:
            week_stats_cnt += stats.boon_uptimes_kill_per_day_count[mon]
            for buff in relevant_boons:
                week_stats_sum[buff] += stats.boon_uptimes_kills_per_day_sum[mon][buff]
        if wed in stats.boon_uptimes_kill_per_day_count:
            week_stats_cnt += stats.boon_uptimes_kill_per_day_count[wed]
            for buff in relevant_boons:
                week_stats_sum[buff] += stats.boon_uptimes_kills_per_day_sum[wed][buff]
        if week_stats_cnt > 0:
            week_data.append([week_stats_sum[buff] / week_stats_cnt for buff in week_stats_sum])
        else:
            week_data.append([None] * len(relevant_boons))
    polyls = ["<polyline points='"] * len(relevant_boons)
    content = ""
    for data_point in week_data:
        for i in range(len(data_point)):
            value = data_point[i]
            if value:
                boon_max = 100
                if relevant_boons[i] == MIGHT_BUFF_ID:
                    boon_max = 25
                polyls[i] += f"{x},{10 + 280 - value / boon_max * 280 * 0.9} "
            else:
                color = ["#dd9b05", "#ae33ae", "#A12929", "blue"][(i % 4)]
                polyls[i] += f"' style='fill:none;stroke:{color};stroke-width:1.5'/><polyline points='"
        x += x_step
    for i in range(len(polyls)):
        color = ["#dd9b05", "#ae33ae", "#A12929", "blue"][(i % 4)]
        polyls[i] += f"' style='fill:none;stroke:{color};stroke-width:1.5'/>"
        content += polyls[i]
    content += "<line x1='400' x2='393' y1='290' y2='295' style='stroke:black;'/>"
    content += "<line x1='400' x2='393' y1='290' y2='285' style='stroke:black;'/>"
    content += "<rect x='270' y='170' width='125' height='90' style='fill:#b9b9b996;stroke: darkgray;'/>"
    content += "<rect x='275' y='180' width='10' height='10' style='fill:#dd9b05'/>"
    content += "<rect x='275' y='200' width='10' height='10' style='fill:#ae33ae'/>"
    content += "<rect x='275' y='220' width='10' height='10' style='fill:#A12929'/>"
    content += "<rect x='275' y='240' width='10' height='10' style='fill:blue'/>"

    def get_boon_name(boon_id):
        if boon_id == MIGHT_BUFF_ID:
            return "Might (x25)"
        if boon_id == QUICKNESS_BUFF_ID:
            return "Quickness"
        if boon_id == ALACRITY_BUFF_ID:
            return "Alacrity"
        if boon_id == PROTECTION_BUFF_ID:
            return "Protection"

    content += f"<text x='290' y='190'>{get_boon_name(relevant_boons[0])}</text>"
    content += f"<text x='290' y='210'>{get_boon_name(relevant_boons[1])}</text>"
    content += f"<text x='290' y='230'>{get_boon_name(relevant_boons[2])}</text>"
    content += f"<text x='290' y='250'>{get_boon_name(relevant_boons[3])}</text>"
    content += "<text text-anchor='end' x='400' y='305'>weeks</text>"
    return create_graph_from_content(content, "Boon uptimes (Kills only)", 100 / 0.9)


def create_line_graph_kill_time(stats, timelines, year):
    first_mon = None
    for i in range(7):
        first_mon = datetime.date(day=i + 1, month=1, year=year)
        if first_mon.weekday() == 0:
            break
    week_data = []
    y_max = timedelta(0)

    def get_timeline(date):
        for timeline in timelines:
            if timeline.date == date:
                return timeline

    for week in range(52):
        mon = first_mon + timedelta(days=7 * week)
        wed = first_mon + timedelta(days=7 * week + 2)
        week_kills = 0
        time = timedelta(0)
        if mon in stats.kills_per_day:
            week_kills += stats.kills_per_day[mon]
            mtl = get_timeline(mon)
            time += mtl.time_line[-1]["time"] - mtl.time_line[0]["time"]
        if wed in stats.kills_per_day:
            week_kills += stats.kills_per_day[wed]
            wtl = get_timeline(wed)
            time += wtl.time_line[-1]["time"] - wtl.time_line[0]["time"]
        if week_kills > 0:
            week_data.append(time / week_kills)
            y_max = max(time / week_kills, y_max)
        else:
            week_data.append(None)
    return create_line_graph(week_data, "Time per kill per week")


def create_line_graph_kill_squad_dps(stats: StatisticsData, year):
    first_mon = None
    for i in range(7):
        first_mon = datetime.date(day=i + 1, month=1, year=year)
        if first_mon.weekday() == 0:
            break
    week_data = []
    y_max = 0
    for week in range(52):
        mon = first_mon + timedelta(days=7 * week)
        wed = first_mon + timedelta(days=7 * week + 2)
        count = 0
        dps = 0
        if mon in stats.kill_squad_dps_per_day_count:
            count += stats.kill_squad_dps_per_day_count[mon]
            dps += stats.kill_squad_dps_per_day_sum[mon]
        if wed in stats.kill_squad_dps_per_day_count:
            count += stats.kill_squad_dps_per_day_count[wed]
            dps += stats.kill_squad_dps_per_day_sum[wed]
        if count > 0:
            week_data.append(dps / count)
            y_max = max(dps / count, y_max)
        else:
            week_data.append(None)
    return create_line_graph(week_data, "Average squad DPS per week (Kills only)")


def create_line_graph_quickness_givers(stats: StatisticsData, year):
    first_mon = None
    for i in range(7):
        first_mon = datetime.date(day=i + 1, month=1, year=year)
        if first_mon.weekday() == 0:
            break
    all_q_profs = set()
    for week in range(52):
        mon = first_mon + timedelta(days=7 * week)
        wed = first_mon + timedelta(days=7 * week + 2)
        for day in [mon, wed]:
            if day in stats.quickness_classes_per_day:
                for prof in stats.quickness_classes_per_day[day]:
                    all_q_profs.add(prof)
    week_data = []
    all_q_profs = list(all_q_profs)
    for week in range(52):
        mon = first_mon + timedelta(days=7 * week)
        wed = first_mon + timedelta(days=7 * week + 2)
        counts = [0] * len(all_q_profs)
        for day in [mon, wed]:
            if day in stats.quickness_classes_per_day:
                for i in range(len(all_q_profs)):
                    if all_q_profs[i] in stats.quickness_classes_per_day[day]:
                        counts[i] += stats.quickness_classes_per_day[day][all_q_profs[i]]
        week_sum = sum(counts)
        if week_sum > 0:
            week_data.append([100 * count / week_sum for count in counts])
        else:
            week_data.append([None] * len(all_q_profs))
    prof_colors = {
        "Chronomancer": "rgb(214,69,219)",
        "Firebrand": "rgb(0,212,245)",
        "Thief": "rgb(194,67,85)",
        "Daredevil": "rgb(234,107,125)",
        "Scrapper": "rgb(237,125,51)",
        "Catalyst": "rgb(227,36,36)",
        "Specter": "rgb(204,77,95)",
        "Herald": "rgb(171,51,51)",
        "Harbinger": "rgb(5, 227, 125)"
    }
    colors = []
    names = []
    for prof in all_q_profs:
        names.append(prof)
        if prof in prof_colors:
            colors.append(prof_colors[prof])
        else:
            colors.append("gray")
    return create_multi_line_graph(week_data, "Quickness Source %", colors=colors,
                                   extra_content=create_graph_legend(colors, names, cols=3), y_max_overwrite=110)


def create_stacked_line_graph_time_distribution(stats, year):
    content = ""
    first_mon = None
    for i in range(7):
        first_mon = datetime.date(day=i + 1, month=1, year=year)
        if first_mon.weekday() == 0:
            break
    week_data = []
    for week in range(52):
        mon = first_mon + timedelta(days=7 * week)
        wed = first_mon + timedelta(days=7 * week + 2)
        kill_time = timedelta(0)
        wipe_time = timedelta(0)
        walk_time = timedelta(0)
        if mon in stats.kill_time_per_day:
            kill_time += stats.kill_time_per_day[mon]
        if mon in stats.walk_time_per_day:
            walk_time += stats.walk_time_per_day[mon]
        if mon in stats.wipe_time_per_day:
            wipe_time += stats.wipe_time_per_day[mon]
        if wed in stats.kill_time_per_day:
            kill_time += stats.kill_time_per_day[wed]
        if wed in stats.walk_time_per_day:
            walk_time += stats.walk_time_per_day[wed]
        if wed in stats.wipe_time_per_day:
            wipe_time += stats.wipe_time_per_day[wed]
        week_data.append((kill_time, wipe_time, walk_time))
    x = 40
    max_valid_x = x
    x_step = 350 / 52
    polys = [""] * 3
    polys[0] += f"{x},35 "
    polys[2] += f"{x},290 "
    for data in week_data:
        if data:
            dsum = data[0] + data[1] + data[2]
            if dsum > timedelta(0):
                spl1 = 35 + 255 * data[0] / dsum
                spl2 = 35 + 255 - 255 * data[2] / dsum
                polys[0] += f"{x},{spl1} "
                polys[1] = f"{x},{spl1} " + polys[1] + f"{x},{spl2} "
                polys[2] = f"{x},{spl2} " + polys[2]
                max_valid_x = x
        x += x_step
    polys[0] += f"{max_valid_x},35' style='fill:#8ba82999;stroke-width:0'/>"
    polys[1] += "' style='fill:#ca424299;stroke-width:0'/>"
    polys[2] += f"{max_valid_x},290' style='fill:#4A463899;stroke-width:0'/>"
    for p in polys:
        content += "<polygon points='" + p
    content += "<line x1='400' x2='393' y1='290' y2='295' style='stroke:black;'/>"
    content += "<line x1='400' x2='393' y1='290' y2='285' style='stroke:black;'/>"
    content += create_graph_legend(["#8ba82999", "#ca424299", "#4A463899"], ["Kills", "Wipes", "Between\nBosses"],
                                   col_width=125)
    content += "<text text-anchor='end' x='400' y='305'>weeks</text>"
    return create_graph_from_content(content, "Time distribution %", 110)


def create_graphs_content(stats, timelines, year):
    return create_bar_graph_specs_usage(stats) + "\n" + \
           create_bar_graph_spec_dps(stats) + "\n" + \
           create_line_graph_kill_squad_dps(stats, year) + "\n" + \
           create_stacked_line_graph_time_distribution(stats, year) + "<br/>\n" + \
           create_line_graph_kill_time(stats, timelines, year) + "\n" + \
           create_line_graph_boons(stats, year) + "\n" + \
           create_line_graph_quickness_givers(stats, year)


def create_day_html(tl_day: "DayData", time_scale, wipe_color, kill_color, walk_color, wing_colors,
                    records: Dict[str, timedelta]):
    day_template = PercentTemplate("<div class='day'><span class='date'>%DATE</span>%DAY_CONTENT</div>\n")
    day_content = ""
    wing_data = {}
    wipe_duration = timedelta(0)
    wing_times = [timedelta(0)] * 8  # [0] refers to wing transitions
    total_potential_time_save = timedelta(0)
    day_content += "<div class='timeline'>"

    start = tl_day.time_line[0]["time"]
    start_hour = 19
    offset = ((start.hour - start_hour) * 3600 + (start.minute - 25) * 60 + start.second) * time_scale
    boss_index = 0
    for i in range(len(tl_day.time_line) - 1):
        entry = tl_day.time_line[i]
        next_entry = tl_day.time_line[i + 1]
        duration = next_entry["time"] - entry["time"]
        if duration.seconds == 0:
            continue
        wing = get_wing_from_boss(entry["target"])
        next_wing = get_wing_from_boss(next_entry["target"])
        if wing == next_wing:
            wing_times[wing] += duration
        else:
            wing_times[0] += duration
        name = get_display_name(entry["target"])
        next_name = get_display_name(next_entry["target"])
        width = duration.seconds * time_scale
        if width > 700:
            width = 700
        if entry["type"] == "kill" or entry["type"] == "abort":
            best_time = records["walk_%s_%s" % (entry["target"], next_entry["target"])]
            if wing == next_wing:
                day_content += "<span class='walk-box' style='background:linear-gradient(%s 80%%, %s 20%%);width: %.2fpx;'>" % (
                    walk_color, wing_colors[wing - 1], width)
                day_content += "<span class='hidden-data'>%s → %s<br/>Time: %s<br/>save potential: %s (%.0f%%)</span>" % (
                    name, next_name, strfdelta(duration, "%m:%S"), strfdelta(duration - best_time, "%m:%S"),
                    -100. * (1. - best_time / duration))
                day_content += "</span>"
            else:
                day_content += "<span class='walk-box' style='background:" + walk_color + ";width: " + str(
                    width) + "px;'>"
                day_content += "<span class='hidden-data'>Wing %d → Wing %d<br/>Time: %s<br/>save potential: %s (%.0f%%)</span>" % (
                    wing, next_wing, strfdelta(duration, "%m:%S"), strfdelta(duration - best_time, "%m:%S"),
                    -100. * (1. - best_time / duration))
                day_content += "</span>"
            total_potential_time_save += duration - best_time
        elif next_entry["type"] == "kill":
            best_time = records["kill_%s" % gw2.raids.get_display_name(entry["target"])]
            if wipe_duration.seconds == 0:
                day_content += "<span class='boss-box' style='margin-left:%.2fpx;background:linear-gradient(%s 80%%, %s 20%%);width: %.2fpx;'>" \
                               "<span class='boss-name' style='top:0px;'>%s</span>" % (
                                   offset, kill_color, wing_colors[wing - 1], width, name)
                day_content += "<span class='hidden-data'>%s %s<br/>Kill time: %s (100%%)<br/>save potential: %s (%.0f%%)</span>" % (
                    name, tl_day.date.strftime(" %d.%m.%Y"), strfdelta(duration, "%m:%S"),
                    strfdelta(duration - best_time, "%m:%S"), -100. * (1. - best_time / duration))
                day_content += "</span>"
                wipe_count = 0
                offset = 0
            else:
                wipe_width = wipe_duration.seconds * time_scale
                total_width = width + wipe_width
                day_content += "<span class='boss-box' style='margin-left:%.2fpx;width:%.2fpx;'>" % (
                    offset, total_width)
                offset = 0
                day_content += "<span class='timeline-box' style='background:linear-gradient(%s 80%%, %s 20%%);width:%.2fpx;'></span>" % (
                    wipe_color, wing_colors[wing - 1], wipe_width - 0.01)
                day_content += "<span class='timeline-box' style='background:linear-gradient(%s 80%%, %s 20%%);width:%.2fpx;'></span>" % (
                    kill_color, wing_colors[wing - 1], width - 0.01)

                day_content += "<span class='boss-name'>%s</span>" % name
                kill_time_percent = 100 * duration / (duration + wipe_duration)
                wipe_time_percent = 100 * wipe_duration / (duration + wipe_duration)
                wipe_count = tl_day.wipe_counts[boss_index]
                day_content += "<span class='hidden-data'>%s %s<br/>Kill time: %s (%.0f%%)<br/>%d wipes: %s (%.0f%%)<br/>save potential: %s (%.0f%%)</span>" % (
                    name, tl_day.date.strftime(" %d.%m.%Y"), strfdelta(duration, "%m:%S"), kill_time_percent,
                    wipe_count, strfdelta(wipe_duration, "%m:%S"), wipe_time_percent,
                    strfdelta(wipe_duration + duration - best_time, "%m:%S"),
                    -100. * (1. - best_time / (duration + wipe_duration)))
                day_content += "</span>"
            if wing not in wing_data:
                wing_data[wing] = []
            wing_data[wing].append({"boss": next_entry["target"], "time": duration, "log": next_entry["log"]})
            boss_index += 1
            total_potential_time_save += duration + wipe_duration - best_time
            wipe_duration = timedelta(0)
        elif next_entry["type"] == "abort":
            day_content += "<span class='boss-box' style='margin-left:%.2fpx;background:linear-gradient(%s 80%%, %s 20%%);width: %.2fpx;'>" \
                           "<span class='boss-name' style='top:0px;'>%s</span>" % (
                               offset, wipe_color, wing_colors[wing - 1], width, name)
            wipe_count = tl_day.wipe_counts[boss_index]
            day_content += "<span class='hidden-data'>%s %s<br/>Wipe time: %s (100%%)<br/>%d wipes</span>" % (
                name, tl_day.date.strftime(" %d.%m.%Y"), strfdelta(duration, "%m:%S"), wipe_count + 1)
            day_content += "</span>"
            total_potential_time_save += duration + wipe_duration
            wipe_duration = timedelta(0)
            boss_index += 1
            offset = 0
        else:
            wipe_duration = duration
            continue
    day_content += "</div>"

    day_content += "<div class='day-details'>"
    detail_data = {
        "START": tl_day.time_line[0]["time"].strftime("%H:%M:%S"),
        "END": tl_day.time_line[-1]["time"].strftime("%H:%M:%S"),
        "DURATION": strfdelta(tl_day.time_line[-1]["time"] - tl_day.time_line[0]["time"], "%m:%S"),
        "KILLCOUNT": tl_day.kill_count,
        "POTENTIAL_SAVE": strfdelta(total_potential_time_save, "%H:%M:%S")
    }
    day_content += PercentTemplate("<span style='display:table-cell;padding-right:10px;'><table>"
                                   "<tr><td>Start</td><td>%START</td></tr>"
                                   "<tr><td>End</td><td>%END</td></tr>"
                                   "<tr><td>Duration</td><td>%DURATION</td></tr>"
                                   "<tr><td>Kills</td><td>%KILLCOUNT</td></tr>"
                                   "<tr><td>Potential save</td><td>%POTENTIAL_SAVE</td></tr>"
                                   "</table></span>").substitute(
        detail_data)
    diagram_data = [tl_day.total_kill_time, tl_day.total_walk_time, tl_day.total_wipe_time]
    diagram_names = ["Kill time", "Walk Time", "Wipe Time"]

    day_content += create_pie_chart(diagram_data, [kill_color, walk_color, wipe_color], diagram_names)
    diagram_data = wing_times
    diagram_names = ["Changing", "Wing 1", "Wing 2", "Wing 3", "Wing 4", "Wing 5", "Wing 6", "Wing 7"]
    day_content += create_pie_chart(diagram_data,
                                    ["#777"] + wing_colors,
                                    diagram_names)
    wing_content = "<div style='display:table-cell;padding-left:10px;top: -8px;position: relative;'>"
    for wing in wing_data:
        wing_content += "<span class='wing-header wing-header-%d'>Wing %d</span>" % (wing, wing)
    for wing in wing_data:
        wing_content += "<table class='wing-table wing-table-%d'>" % wing
        for boss in wing_data[wing]:
            log_str = "No Log"
            if boss['log'] is not None:
                log_str = "<a href='%s' target='_blank'>Log</a>" % boss["log"]
            record_color_str = ""
            if boss["time"] == records['kill_' + gw2.raids.get_display_name(boss["boss"])]:
                record_color_str = " style='color:#b5850a;'"
            wing_content += "<tr><td class='boss-name-cell'>%s</td><td%s>%s (+%s)</td><td>%s</td></tr>" % (
                boss["boss"], record_color_str, strfdelta(boss["time"], "%m:%S"),
                strfdelta(boss["time"] - records['kill_' + gw2.raids.get_display_name(boss["boss"])], "%m:%s1"),
                log_str)
        wing_content += "</table>"
    wing_content += "</div>"
    day_content += wing_content
    day_content += "</div>"
    d = {"DATE": tl_day.date.strftime(" %d.%m."), "DAY_CONTENT": day_content}
    return day_template.substitute(**d), records, wing_times


def is_wing_completed(wing: int, timeline_data: list):
    required = []
    for encounter in gw2.raids.encounters:
        if encounter["wing"] == wing:
            required.append(get_display_name(encounter["name"]))
    req_count = len(required)
    for tl_entry in timeline_data:
        if tl_entry["type"] == "kill" and get_display_name(tl_entry["target"]) in required:
            req_count -= 1
    return req_count == 0


def create_stats_entry(name, value, *, show_always=False):
    if isinstance(value, timedelta):
        value = timedelta(seconds=int(value.total_seconds()))
    if isinstance(value, int) and value >= 1e4:
        value = f"{int(value):,}".replace(",", " ")
    clazz = "stats-entry-hidden"
    if show_always:
        clazz = "stats-entry"
    return f"<div class='{clazz}'><span>{name}</span><span>{value}</span></div>\n"


def get_skill_entry(skill_id):
    name = get_skill_name_by_id(skill_id)
    name = name.replace("'", "")
    return f"<a href='https://wiki.guildwars2.com/index.php?search={name}'>" \
           f"<img class='skill-icon' src='{get_skill_icon_by_id(skill_id)}'/>{name}</a>"


def create_stats_content(stats: StatisticsData):
    out_str = "<div class='stats-inner'>"
    out_str += create_stats_entry("Encounters completed", stats.encounters_completed, show_always=True)
    out_str += create_stats_entry("CMs completed", stats.cm_kills)
    out_str += create_stats_entry("Wipes", stats.wipes, show_always=True)
    wipes_sorted = sorted(stats.wipes_per_boss, key=stats.wipes_per_boss.__getitem__)
    if len(wipes_sorted) >= 1:
        out_str += create_stats_entry("Most wipes",
                                      get_display_name(
                                          gw2.raids.get_by_encounter_id(wipes_sorted[-1])["name"]) + " (" + str(
                                          stats.wipes_per_boss[wipes_sorted[-1]]) + ")", show_always=True)
        out_str += create_stats_entry("2nd most wipes",
                                      get_display_name(
                                          gw2.raids.get_by_encounter_id(wipes_sorted[-2])["name"]) + " (" + str(
                                          stats.wipes_per_boss[wipes_sorted[-2]]) + ")")
        out_str += create_stats_entry("3rd most wipes",
                                      get_display_name(
                                          gw2.raids.get_by_encounter_id(wipes_sorted[-3])["name"]) + " (" + str(
                                          stats.wipes_per_boss[wipes_sorted[-3]]) + ")")

        out_str += create_stats_entry("Least wipes",
                                      get_display_name(
                                          gw2.raids.get_by_encounter_id(wipes_sorted[0])["name"]) + " (" + str(
                                          stats.wipes_per_boss[wipes_sorted[0]]) + ")")
    out_str += create_stats_entry("Time in raids", stats.total_time, show_always=True)
    out_str += create_stats_entry("Time in kills", stats.total_kill_time)
    out_str += create_stats_entry("Time in wipes", stats.total_wipe_time)
    out_str += create_stats_entry("Time inbetween bosses", stats.total_walk_time)
    out_str += create_stats_entry("Total damage dealt", stats.total_damage, show_always=True)
    out_str += create_stats_entry("Total power damage dealt", stats.total_power_dmg)
    out_str += create_stats_entry("Total condi damage dealt", stats.total_condi_dmg)
    out_str += create_stats_entry("Total target damage in kills", stats.total_target_damage_in_kills)
    out_str += create_stats_entry("Total target damage increase due to scholar", int(stats.scholar_damage_increase))
    out_str += create_stats_entry("Scholar uptime (hits)",  f"{100 * stats.scholar_uptime_active / stats.scholar_uptime_total:.1f}%"if stats.scholar_uptime_total != 0 else "-")
    out_str += create_stats_entry("Total breakbar damage", int(stats.total_cc))
    out_str += create_stats_entry("Total damage taken", stats.total_damage_taken)
    out_str += create_stats_entry("Total damage against barrier", stats.total_damage_mitigated_barrier)
    out_str += create_stats_entry("Total healing (extension required)", int(stats.total_healing))
    out_str += create_stats_entry("Total skills cast", sum(stats.skill_cast_counts.values()), show_always=True)
    out_str += create_stats_entry("Total skills canceled", stats.total_skills_canceled)
    out_str += create_stats_entry("Total time skills canceled", stats.total_cancel_time)
    out_str += create_stats_entry("Unique skills cast", len(stats.skill_cast_counts))
    skills_sorted = sorted(stats.skill_cast_counts, key=stats.skill_cast_counts.__getitem__)
    skills_sorted_no_aa = []
    for skill_id in skills_sorted:
        if not gw2.gw2.is_skill_auto_attack(skill_id):
            skills_sorted_no_aa.append(skill_id)
    out_str += create_stats_entry("Most used skill",
                                  f"{get_skill_entry(skills_sorted[-1])} ({stats.skill_cast_counts[skills_sorted[-1]]})",
                                  show_always=True)
    for i in range(2, 11):
        cnt_num_ext = "th"
        if i == 2:
            cnt_num_ext = "nd"
        if i == 3:
            cnt_num_ext = "rd"
        out_str += create_stats_entry(f"{i}{cnt_num_ext} most used skill",
                                      f"{get_skill_entry(skills_sorted[-i])} ({stats.skill_cast_counts[skills_sorted[-i]]})")
    least_count = stats.skill_cast_counts[skills_sorted[0]]
    least_str = get_skill_entry(skills_sorted[0])
    i = 1
    while stats.skill_cast_counts[skills_sorted[i]] == least_count:
        least_str += ", " + get_skill_entry(skills_sorted[i])
        i += 1
    out_str += create_stats_entry("Least used skills", f"{least_str} ({least_count})")
    out_str += create_stats_entry("Most used non-auto-attack",
                                  f"{get_skill_entry(skills_sorted_no_aa[-1])} ({stats.skill_cast_counts[skills_sorted_no_aa[-1]]})",
                                  show_always=True)
    for i in range(2, 11):
        cnt_num_ext = "th"
        if i == 2:
            cnt_num_ext = "nd"
        if i == 3:
            cnt_num_ext = "rd"
        out_str += create_stats_entry(f"{i}{cnt_num_ext} most used non-auto-attack",
                                      f"{get_skill_entry(skills_sorted_no_aa[-i])} ({stats.skill_cast_counts[skills_sorted_no_aa[-i]]})")
    out_str += create_stats_entry("Weapon swaps", stats.weapon_swaps)
    out_str += create_stats_entry("Dodges (incl. Mirage Cloak)", stats.dodges)
    out_str += create_stats_entry("Total quickness applied", stats.total_time_quickness)
    out_str += create_stats_entry("Total alacrity applied", stats.total_time_alac)
    out_str += create_stats_entry("Total might applied", stats.total_time_might)
    specs_sorted = sorted(stats.specs_log_counts, key=stats.specs_log_counts.__getitem__)
    out_str += create_stats_entry("Most played specialization",
                                  specs_sorted[-1].name + " (" + str(stats.specs_log_counts[specs_sorted[-1]]) + ")",
                                  show_always=True)
    out_str += create_stats_entry("Least played specialization",
                                  specs_sorted[0].name + " (" + str(stats.specs_log_counts[specs_sorted[0]]) + ")")
    out_str += create_stats_entry("Total gold dropped", int(stats.total_gold_created))
    out_str += create_stats_entry("New raids released", stats.new_raids_released)
    # out_str += create_stats_entry("Good Ewayne puns", stats.ewayne_puns)
    out_str += create_stats_entry("Surprisingly high odds (Impossible Odds Hits)", stats.impossible_odds_hits)
    out_str += create_stats_entry("Kills with less than 10 people", stats.low_man_kills)
    out_str += create_stats_entry("Accounts seen", len(stats.unique_accounts_seen))
    out_str += create_stats_entry("Characters seen", len(stats.unique_characters_seen), show_always=True)
    out_str += create_stats_entry("Guilds seen", len(stats.unique_guilds_seen))
    meters_str = f"{int(0.0254 * stats.total_units_moved):,}".replace(",", " ")
    out_str += create_stats_entry("Meters moved",
                                  f"{meters_str} ({stats.total_units_moved / 1200:.0f} Blinks)", show_always=True)
    out_str += create_stats_entry("Clockwise rotations", stats.cw_rotations)
    out_str += create_stats_entry("Counter clockwise rotations", stats.ccw_rotations)
    out_str += create_stats_entry("Downstates", stats.total_downstates, show_always=True)
    out_str += create_stats_entry("Total resurrect time", stats.resurrect_time)
    out_str += create_stats_entry("Most natural conclusions reached", stats.total_deaths)
    for i in range(7):
        out_str += create_stats_entry(f"Time in Wing {i + 1:d}", stats.time_per_wing[i])
    out_str += create_stats_entry("VG's blue TPs", stats.vg_blue_tps)
    out_str += create_stats_entry("People in VG greens", stats.vg_stood_in_green)
    out_str += create_stats_entry("Unblocked gorse slams", stats.gorse_slam_hits)
    out_str += create_stats_entry("People egged", stats.gorse_eggs)
    out_str += create_stats_entry("People bombed on Sabetha", stats.sab_hit_by_bomb)
    out_str += create_stats_entry("Druids bombed on Sabetha", stats.sab_druids_hit_by_bomb)
    out_str += create_stats_entry("Beehives thrown", stats.trio_beehives_thrown)
    out_str += create_stats_entry("People sacrificed to Matthias", sum(stats.mat_successful_sacs_by_account.values()))
    sacs_sorted = sorted(stats.mat_successful_sacs_by_account, key=stats.mat_successful_sacs_by_account.__getitem__)
    if len(sacs_sorted) > 0:
        out_str += create_stats_entry("Most sacrificed on Matthias",
                                      f"{sacs_sorted[-1]} ({stats.mat_successful_sacs_by_account[sacs_sorted[-1]]})")
    out_str += create_stats_entry("People that can't glide", stats.xera_glide_deaths)
    out_str += create_stats_entry("Tears collected", stats.deimos_tears_collected)
    out_str += create_stats_entry("Blacks triggered", stats.deimos_oil_triggered)
    out_str += create_stats_entry("People chained by Dhuum", stats.dhuum_chains)
    out_str += create_stats_entry("Bombs on Dhuum", stats.dhuum_bombs)
    out_str += create_stats_entry("Signets shared on Largos", stats.twins_signet_of_wrath)
    out_str += create_stats_entry("People on lines (Qadim)", stats.qadim_hit_on_line)
    out_str += create_stats_entry("People blinded on Adina", stats.adina_blinds)
    out_str += create_stats_entry("People on lines (QTP)", stats.qpeer_hit_on_line)
    out_str += "</div>"
    out_str += "<button id='btnEvenMoreStats' onclick='toggleEvenMoreStats()'>Show even more stats</button>"
    return out_str


def create_person_stats_entry(name, pp_stat: PerPersonStat, *, to_seconds=False, negate=False, unit="",
                              show_as_float=False):
    best_name = pp_stat.get_arg_best()
    if best_name != "":
        best_val = pp_stat[best_name]
        if negate:
            best_val = -best_val
        if to_seconds:
            if best_val > 60:
                best_val = timedelta(seconds=int(best_val))
            else:
                best_val = f"{best_val:.2f}s"
        else:
            if show_as_float:
                best_val = f"{best_val:.1f}"
            else:
                best_val = f"{int(best_val):,}".replace(",", " ")
    else:
        best_val = 0
    return f"<div class='personal-stat-entry'>{name}<br/>{best_name}<br/>{best_val:}{unit}</div>"


def create_personal_stats(stats: StatisticsData):
    out_str = create_person_stats_entry("Average DPS", stats.pp_avg_dps)
    out_str += create_person_stats_entry("DPS in a fight", stats.pp_max_dps)
    out_str += create_person_stats_entry("Total damage", stats.pp_sum_dmg)
    out_str += create_person_stats_entry("Total healing", stats.pp_sum_heal)
    out_str += create_person_stats_entry("Healing in a fight", stats.pp_max_heal)
    out_str += create_person_stats_entry("Average HPS", stats.pp_avg_hps)
    out_str += "<br/>"
    out_str += create_person_stats_entry("Allied condis cleansed", stats.pp_condi_cleanse_out)
    out_str += create_person_stats_entry("Mean time resurrecting", stats.pp_res_time, to_seconds=True)
    out_str += create_person_stats_entry("Skills cast", stats.pp_sum_skill_casts)
    out_str += create_person_stats_entry("Skills cast per fight", stats.pp_avg_skill_casts)
    out_str += create_person_stats_entry("Survival Rate in Kills", stats.pp_kill_survived_percent, unit="%",
                                         show_as_float=True)
    out_str += create_person_stats_entry("Number of fights", stats.pp_presence)
    out_str += create_person_stats_entry("Average movement", stats.pp_avg_move_units, unit="units")
    out_str += "<br/>"
    out_str += create_person_stats_entry("Quickness given (Group)", stats.pp_avg_grp_quick, unit="%",
                                         show_as_float=True)
    out_str += create_person_stats_entry("Average breakbar damage", stats.pp_avg_cc)
    out_str += create_person_stats_entry("Distance to squad center", stats.pp_dist_to_sq_mid, negate=True, unit="units")
    out_str += create_person_stats_entry("Average HP", stats.pp_avg_hp_percent, unit="%", show_as_float=True)
    out_str += create_person_stats_entry("Unique Professions played", stats.pp_uniqe_specs_played)
    out_str += create_person_stats_entry("Tears collected", stats.pp_tears_collected)
    out_str += create_person_stats_entry("Scholar uptime as healer", stats.pp_scholar_hit_grp_when_heal_kill, unit='%',
                                         show_as_float=True)
    for name in stats.pp_scholar_hit_grp_when_heal_kill:
        print(name, stats.pp_scholar_hit_grp_when_heal_kill[name])
    return out_str


def create_html_report(days_data: List["DayData"], records, record_logs, year, full_clear_record, relevant_logs,
                       file_name="report.html"):
    time_scale = 0.13
    main_template = PercentTemplate(
        "<!doctype html>\n<html><head><meta charset='utf-8'><title>%YEAR Raid Report </title><style>\n" +
        "%CSS\n"
        "</style>\n" +
        "<script>\n" +
        "%JS\n" +
        "</script>" +
        "</head>\n<body>\n" +
        "<h1>pv's Raid Report %YEAR</h1>\n" +
        "<div id='hover-box'></div>" +
        "<div onclick='toggleTimelines()' class='section-header' id='timelines-header'>Timelines</div>\n" +
        "<div id='timelines'>%TIMELINES_CONTENT\n</div>\n" +
        "<div onclick='toggleRecords()' class='section-header' id='records-header'>Records</div>\n" +
        "<div id='records'>%RECORDS_CONTENT</div>" +
        "<div onclick='toggleStats()' class='section-header' id='stats-header'>Statistics</div>\n" +
        "<div id='stats'>%STATS_CONTENT</div>\n" +
        "<div onclick='toggleGraphs()' class='section-header' id='graphs-header'>Graphs</div>\n" +
        "<div id='graphs'>%GRAPHS_CONTENT</div>\n" +
        "<div onclick='togglePersonal()' class='section-header' id='personal-header'>Best Players</div>\n" +
        "<div id='personal'>%PERSONAL_CONTENT</div>\n"
        "</body></html>")
    wipe_color = "#ff5e5e"
    kill_color = "#aec951"
    walk_color = "#8C8369"
    wing_colors = ["#DE5B49",
                   "#E37B40",
                   "#F0CA4D",
                   "#347F1F",
                   "#41a7b0",
                   "#2A42A9",
                   "#4f0080"]
    timelines_content = ""
    for i in range(8):
        timelines_content += "<div class='time-marker' style='left:%dpx;'></div>" % (i * 30 * 60 * time_scale + 64)
    wing_record_times = [timedelta(999999999)] * 7
    wing_record_dates = [datetime.date(1970, month=1, day=1)] * 7
    for tl_day in days_data:
        day_content, records, day_wing_times = create_day_html(tl_day, time_scale, wipe_color, kill_color,
                                                               walk_color,
                                                               wing_colors, records)
        timelines_content += day_content
        for i in range(7):
            if day_wing_times[i + 1] < wing_record_times[i] and is_wing_completed(i + 1, tl_day.time_line):
                wing_record_times[i] = day_wing_times[i + 1]
                wing_record_dates[i] = tl_day.date
    timelines_content += "<div style='height:60px;'></div>"
    css_file = open("raid_report/raid_report.css", "r")
    css_content = css_file.read()
    css_file.close()
    js_file = open("raid_report/script.js", "r")
    js_content = js_file.read()
    js_file.close()
    records_content = "<div style='margin-top:3px;'><table><tr><th>Boss</th><th>Our Best</th><th>Log</th><th>World Record</th></tr>"
    for record_id in records:
        if record_id.startswith("kill"):
            boss_name = record_id.replace("kill_", "")
            if boss_name in ["TC", "Trio", "Eyes", "Eater"]:
                continue
            global_record = gw2.raids.get_global_boss_record(gw2.raids.get_id_by_name(boss_name))
            global_record_time_str = ""
            if global_record:
                global_record_time_str = strfdelta(global_record, "%m:%S")
            log_str = ""
            if record_id in record_logs and record_logs[record_id]:
                log_str = "<a href='%s' target='_blank'>Log</a>" % record_logs[record_id]
            records_content += "<tr><td>%s</td><td class='time-cell'>%s</td><td>%s</td><td class='time-cell'>%s</td></tr>" % (
                boss_name,
                strfdelta(records[record_id], "%m:%S"),
                log_str,
                global_record_time_str)
    records_content += "</table></div><div><table><tr><th>Wing</th><th>Our Best<span class='superscript'>*</span></th><th>Day</th><th>World Record<span class='superscript'>**</span></th></tr>"
    for i in range(7):
        records_content += "<tr><td>%s</td><td class='time-cell'>%s</td><td>%s</td><td class='time-cell'>%s</td></tr>" % (
            i + 1,
            strfdelta(wing_record_times[i], "%m:%S"),
            wing_record_dates[i].strftime("%d.%m.%Y"),
            strfdelta(gw2.raids.get_global_wing_record(i + 1), "%m:%S"))
    records_content += "</table><span class='superscript'>*</span> starts at first boss<br/><span class='superscript'>**</span> starts on first movement, outdated<br/>Does not account for: Spirit run skip, Escort, Gate<br/>Boss times may differ by up to 3s compared to log times<br/>Xera does not include pre phase.</div>"
    records_content += "<div class='full-record'>Fastest Full-Clear:<br/><span>%s</span><br/>in<br/><span>%s</span><br/>WR: %s</div>" % (
        full_clear_record[0].strftime("%d.%m.%Y"),
        strfdelta(full_clear_record[1], "%H:%M:%S"),
        "2:04:00"
    )
    stats_data = StatisticsData.create_from_logs(relevant_logs, days_data)
    #print("pp_res_time")
    #for k in stats_data.pp_res_time:
    #    print(k, stats_data.pp_res_time[k])
    #print("pp_downed_count")
    #for k in stats_data.pp_downed_count:
    #    print(k, stats_data.pp_downed_count[k])
    #print("pp_downed_duration")
    #for k in stats_data.pp_downed_duration:
    #    print(k, stats_data.pp_downed_duration[k])
    stats_content = create_stats_content(stats_data)
    graphs_content = create_graphs_content(stats_data, days_data, year)
    b = {"TIMELINES_CONTENT": timelines_content, "CSS": css_content, "JS": js_content, "YEAR": year,
         "RECORDS_CONTENT": records_content, "STATS_CONTENT": stats_content, "GRAPHS_CONTENT": graphs_content,
         "PERSONAL_CONTENT": create_personal_stats(stats_data)}
    text = main_template.substitute(**b)
    file_path = f"raid_report/{file_name}"
    file = open(file_path, "wb")
    file.write(bytes(text, 'utf-8'))
    file.close()


def append_kill_data(time_line, total_kill_time, total_walk_time, total_wipe_time, kill_count, wipe_count,
                     target_name, last_target_name, start_time, last_start_time, last_end_time, is_kill,
                     show_detail,
                     times_per_part, log_url):
    if is_kill:
        time_line.append({"type": "startKill", "target": last_target_name, "time": last_start_time})
        time_line.append({"type": "kill", "target": last_target_name, "time": last_end_time, "log": log_url})
        if show_detail:
            print("%s start kill %-25s after %d wipes" % (
                last_start_time.strftime("%T"), last_target_name, wipe_count))
            print("%s       kill %-25s in %s" % (
                last_end_time.strftime("%T"), last_target_name,
                strfdelta(last_end_time - last_start_time, "%m:%S")))
        kill_count += 1
        total_kill_time += last_end_time - last_start_time
        times_per_part["kill_%s" % gw2.raids.get_display_name(last_target_name)] = last_end_time - last_start_time
        if last_target_name != target_name:
            total_walk_time += start_time - last_end_time
            if show_detail:
                print("add walk of", start_time - last_end_time, " ", last_target_name, "->", target_name)
            times_per_part["walk_%s_%s" % (last_target_name, target_name)] = start_time - last_end_time
    else:
        time_line.append({"type": "abort", "target": last_target_name, "time": last_end_time})
        if show_detail:
            print("%s abort      %-25s after %d wipes" % (
                last_end_time.strftime("%T"), last_target_name, wipe_count))
        total_wipe_time += last_end_time - last_start_time
        if last_target_name != target_name:
            total_walk_time += start_time - last_end_time
            times_per_part["walk_%s_%s" % (last_target_name, target_name)] = start_time - last_end_time
    return time_line, total_kill_time, total_walk_time, total_wipe_time, kill_count, times_per_part


class DayData:
    def __init__(self, kill_count: int, total_kill_time, total_wipe_time: timedelta,
                 total_walk_time: timedelta, time_line: list, wipe_counts: list,
                 times_per_part, date: datetime):
        self.date = date
        self.kill_count = kill_count
        self.total_kill_time = total_kill_time
        self.total_wipe_time = total_wipe_time
        self.total_walk_time = total_walk_time
        self.time_line = time_line
        self.wipe_counts = wipe_counts
        self.times_per_part = times_per_part


def process_day(day_logs: List[LogManagerLog], date, *, show_summary=False, show_detail=False):
    list.sort(day_logs, key=lambda item: item.get_encounter_start_time())
    total_wipe_time = timedelta()
    total_kill_time = timedelta()
    total_walk_time = timedelta()
    last_target_name = ""
    last_log_url = None
    target_name = last_target_name
    wipe_count = 0
    wipe_counts = []
    times_per_part = {}
    kill_count = 0
    last_start_time = None
    last_end_time = None
    start_time = None
    time_line = []
    is_kill = True
    for log in day_logs:
        target_name = correct_ch_id_name(log.get_main_target_name())
        start_time = log.get_encounter_start_time()
        duration = log.get_duration()
        is_last_kill = is_kill
        is_kill = True
        if correct_ch_id_name(log.get_main_target_name()) not in ["Zane", "Berg", "Haunting Statue"]:
            is_kill = log.is_success()
            # if not is_kill and last_target_name != target_name and last_target_name != "":
            #    print(log.data["EncounterResult"], "%-15s" % log.get_main_target_name()[0:14], log.get_encounter_start_time(),
            #          log.get_dps_report_url())
        end_time = start_time + duration
        if last_target_name != target_name:
            if last_target_name != "":
                time_line, total_kill_time, total_walk_time, total_wipe_time, kill_count, times_per_part = \
                    append_kill_data(time_line, total_kill_time, total_walk_time, total_wipe_time, kill_count,
                                     wipe_count, target_name, last_target_name, start_time, last_start_time,
                                     last_end_time, is_last_kill,
                                     show_detail, times_per_part, last_log_url)
                wipe_counts.append(wipe_count)
            wipe_count = 0
            time_line.append({"type": "startBoss", "target": target_name, "time": start_time})
            if show_detail:
                print("%s start      %-25s" % (start_time.strftime("%T"), target_name))
        else:
            wipe_count += 1
            total_wipe_time += start_time - last_start_time
        # print("%-25s %s" % (log["MainTargetName"], startTime.strftime("%T")))
        last_target_name = target_name
        last_start_time = start_time
        last_end_time = end_time
        last_log_url = log.get_dps_report_url()
    if last_target_name != "":
        time_line, total_kill_time, total_walk_time, total_wipe_time, kill_count, times_per_part = \
            append_kill_data(time_line, total_kill_time, total_walk_time, total_wipe_time, kill_count,
                             wipe_count, target_name, last_target_name, start_time, last_start_time, last_end_time,
                             is_kill, show_detail, times_per_part, day_logs[-1].get_dps_report_url())
        wipe_counts.append(wipe_count)
    total_time = total_kill_time + total_wipe_time + total_walk_time
    if total_time == timedelta(0):
        total_time = timedelta(0, 1)
    if show_summary:
        print("Total time     : " + strfdelta(total_time, "%H:%M:%S") + " (%d kills)" % kill_count)
        print("Total kill time: " + strfdelta(total_kill_time, "%H:%M:%S") + "(%2.0f%%)" % (
                100.0 * total_kill_time / total_time))
        print("Total wipe time: " + strfdelta(total_wipe_time, "%H:%M:%S") + "(%2.0f%%)" % (
                100.0 * total_wipe_time / total_time))
        print("Total walk time: " + strfdelta(total_walk_time, "%H:%M:%S") + "(%2.0f%%)" % (
                100.0 * total_walk_time / total_time))
    return DayData(kill_count, total_kill_time, total_wipe_time, total_walk_time, time_line, wipe_counts,
                   times_per_part, date)


def main(*, year=2023, weekdays=None):
    if weekdays is None:
        weekdays = [0, 2, 3]
    print("Creating raid report for %d for weekdays %s" % (year, str(weekdays)))
    lmc = LogManagerCache()
    data = lmc.get_logs_by_day(maps_filter=RAID_WING_MAP_IDS)
    full_days = 0
    full_clear_record_time = timedelta(9999999)
    full_clear_record_day = datetime.date(1970, month=1, day=1)
    avg_kill = timedelta(0)
    avg_wipe = timedelta(0)
    avg_walk = timedelta(0)
    timelines = []
    records = {}
    record_logs = {}
    relevant_logs = []
    for encounter in gw2.raids.encounters:
        records["kill_" + get_display_name(encounter["name"])] = timedelta(99999999)  # presort
    for day in data:
        if day.year != year or day.weekday() not in weekdays:  # filter out non-monday/-wednesday and wrong year
            continue
        if year == 2022 and day.day in [3, 9] and day.month == 2:  # filter non-static days / kc training
            continue
        if year == 2021 and day.day in [6, 8] and day.month == 12:  # filter non-static days
            continue
        if year == 2020 and day.day == 1 and day.month == 7:
            continue
        if year == 2020 and day.day == 9 and day.month == 9:
            continue
        if year == 2020 and day.day == 16 and day.month == 11:
            continue
        if year == 2020 and day.day == 18 and day.month == 11:
            continue
        if year == 2020 and day.day == 21 and day.month == 12:
            continue
        if year == 2022 and day.day in [6, 20] and day.month == 6:
            data[day] = list(filter(lambda d: d.get_encounter_start_time().hour >= 18, data[day]))
        if year == 2022 and day.day == 18 and day.month == 7:
            data[day] = list(filter(lambda d: d.get_encounter_start_time().hour >= 19, data[day]))
        day_data = process_day(data[day], day, show_detail=False)
        if not 19 <= day_data.time_line[0]['time'].hour <= 23:
            print(f"Skipping {day_data.time_line[0]['time']}")
            continue
        relevant_logs += data[day]
        for part_name in day_data.times_per_part:
            # filter out broken twins log
            if part_name == "kill_Twins" and day_data.times_per_part[part_name].seconds <= 60:
                continue
            if part_name == "walk_Deimos_Keep Construct" and day_data.times_per_part[part_name].seconds <= 180 and year <= 2022:
                continue
            # filter out escord skip
            if part_name == "walk_Deimos_Keep Construct" and day_data.times_per_part[part_name].seconds <= 180 and year <= 2022:
                continue
            # filter out wrong vg
            if part_name == "kill_Vale Guardian" and day_data.times_per_part[part_name].seconds <= 60:
                continue
            # filter out dhuum rq
            if part_name == "kill_Dhuum" and day_data.times_per_part[part_name].seconds <= 3 * 60 + 45:
                continue
            if part_name == "kill_Slothasor" and day_data.times_per_part[
                part_name].seconds < 60:  # filter out loth bug
                continue
            # filter out sloth bug
            if part_name == "kill_Keep Construct" and day_data.times_per_part[part_name].seconds < 120:
                continue
            if part_name == "kill_Deimos" and day_data.times_per_part[part_name].seconds < 180:  # filter out deimos rq
                continue
            if part_name not in records or records[part_name] > day_data.times_per_part[part_name]:
                records[part_name] = day_data.times_per_part[part_name]
                record_log = None
                for tl_entry in day_data.time_line:
                    if part_name.split("_")[0] == "kill" and tl_entry["type"] == "kill" and get_display_name(
                            tl_entry["target"]) == part_name.split("_")[1]:
                        record_log = tl_entry["log"]
                record_logs[part_name] = record_log
        timelines.append(day_data)
        if day_data.kill_count >= 22:
            full_days += 1
            avg_kill += day_data.total_kill_time
            avg_walk += day_data.total_walk_time
            avg_wipe += day_data.total_wipe_time
            day_total = day_data.total_kill_time + day_data.total_walk_time + day_data.total_wipe_time
            if day_total < full_clear_record_time:
                full_clear_record_time = day_total
                full_clear_record_day = day
    for part_name in records:
        pass
        # print(part_name + ":" + strfdelta(records[part_name], "%m:%S"))
    if full_days > 0:
        avg_kill /= full_days
        avg_wipe /= full_days
        avg_walk /= full_days
        avg_tot = avg_kill + avg_wipe + avg_walk
        print("Found %d full days" % full_days)
        print("AVG TOTAL    : " + strfdelta(avg_tot, "%H:%M:%S") + " (%2.0f%%)" % (100.0 * avg_tot / avg_tot))
        print("AVG kill time: " + strfdelta(avg_kill, "%H:%M:%S") + " (%2.0f%%)" % (100.0 * avg_kill / avg_tot))
        print("AVG wipe time: " + strfdelta(avg_wipe, "%H:%M:%S") + " (%2.0f%%)" % (100.0 * avg_wipe / avg_tot))
        print("AVG walk time: " + strfdelta(avg_walk, "%H:%M:%S") + " (%2.0f%%)" % (100.0 * avg_walk / avg_tot))
        print("Best FC: " + strfdelta(full_clear_record_time, "%H:%M:%S") + " on " + full_clear_record_day.strftime(
            "%d.%m.%Y"))
    timelines.sort(key=lambda item: item.date)
    create_html_report(timelines, records, record_logs, year, (full_clear_record_day, full_clear_record_time),
                       relevant_logs, file_name=f"{year}.html")


if __name__ == "__main__":
    main(year=2023, weekdays=[0, 2])
