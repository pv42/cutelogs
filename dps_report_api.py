import datetime
import zlib
from typing import Optional, List, Generator

import orjson as json
from json import decoder as jsondecoder
import os
import re

import requests
import gzip

from gw2.gw2 import QUICKNESS_BUFF_ID, ALACRITY_BUFF_ID, MIGHT_BUFF_ID, FURY_BUFF_ID, BUFF_BANNER_OF_STRENGTH, \
    BUFF_BANNER_OF_DISCIPLINE, PROTECTION_BUFF_ID

CACHE_PATH = "E:\\GW2LogsData\\"


def get_ei_data_cached(url, *, silent=True) -> Optional["EliteInsightData"]:
    if not re.match(r'https://dps\.report/[a-zA-z0-9]{4}-\d{8}-\d{6}_[a-z]{2,5}', url):
        print('invalid url: "%s"' % url)
        return
    url_id_part = url.split('https://dps.report/')[1]
    if not os.path.exists(CACHE_PATH + 'logJsons'):
        os.mkdir(CACHE_PATH + 'logJsons')
    jpath = (CACHE_PATH + 'logJsons' + os.path.sep + '%s.json') % url_id_part
    gzjpath = jpath + ".gz"
    if os.path.exists(gzjpath):
        if os.path.exists(jpath):
            os.remove(jpath)
            print("deleted " + jpath + " in favor of gz")
        with gzip.open(gzjpath, 'rb') as f:
            j = None
            try:
                j = EliteInsightData(json.loads(f.read()))
            except json.JSONDecodeError:
                print("Json error in", url)
            except zlib.error:
                print("Zlib error in", url)
            return j
    elif os.path.exists(jpath):
        f = open(jpath, 'rb')
        j = None
        try:
            data = f.read()
            with gzip.open(gzjpath, "wb") as gzf:
                print("Creating gzip " + gzjpath)
                gzf.write(data)
            j = EliteInsightData(json.loads(data))
        except json.JSONDecodeError:
            print("Json error in", url)
        f.close()
        return j
    else:
        if not silent:
            print('Downloading json %s' % url)
        api = DpsReportAPI()
        j = api.get_json_data_by_link(url)
        if j:
            with gzip.open(gzjpath, 'wb') as f:
                f.write(json.dumps(j.data))
        return j


def get_ei_data_reduced(url, *, silent=True) -> Optional["EliteInsightDataReduced"]:
    if not re.match(r'https://dps\.report/[a-zA-z0-9]{4}-\d{8}-\d{6}_[a-z]{2,5}', url):
        print('invalid url: "%s"' % url)
        return
    url_id_part = url.split('https://dps.report/')[1]
    if not os.path.exists(CACHE_PATH + 'logJsonsReduced'):
        os.mkdir(CACHE_PATH + 'logJsonsReduced')
    jpath = (CACHE_PATH + 'logJsonsReduced/%s.json.gz') % url_id_part
    if os.path.exists(jpath):
        with gzip.open(jpath, 'rb') as gzf:
            j = None
            try:
                j = EliteInsightDataReduced(json.loads(gzf.read()))
            except json.JSONDecodeError:
                print("Json error in", url)
            return j
    else:
        if not silent:
            print('Creating reduced data for %s' % url)
        full_json = get_ei_data_cached(url, silent=silent)
        if not full_json:
            return
        keys_to_copy = ["eliteInsightsVersion", "triggerID", "fightName", "fightIcon", "arcVersion", "gW2Build",
                        "language", "languageID", "recordedBy", "timeStart", "timeEnd", "timeStartStd", "timeEndStd",
                        "duration", "success", "isCM", "players"]
        del_player_keys = ["targetDamage1S", "targetPowerDamage1S", "targetConditionDamage1S", "targetBreakbarDamage1S",
                           "conditionDamage1S", "powerDamage1S", "damage1S", "breakbarDamage1S",
                           "selfBuffsActive", "groupBuffsActive", "offGroupBuffsActive", "squadBuffsActive",
                           "targetDamageDist", "selfBuffs", "buffUptimesActive", "damageModifiersTarget",
                           "extHealingStats", "healthPercents", "barrierPercents", "combatReplayData",
                           "totalDamageDist", "totalDamageTaken", "rotation"]
        whitelisted_buffs = [QUICKNESS_BUFF_ID, ALACRITY_BUFF_ID, MIGHT_BUFF_ID, FURY_BUFF_ID, BUFF_BANNER_OF_STRENGTH,
                             BUFF_BANNER_OF_DISCIPLINE, PROTECTION_BUFF_ID]

        j = {}
        for key in keys_to_copy:
            j[key] = full_json.data[key]
            if key == "players":
                for player in j[key]:
                    for pkey in del_player_keys:
                        if pkey in player:
                            del player[pkey]
                    new_buffs = []
                    for buff_data in player["buffUptimes"]:
                        if buff_data["id"] in whitelisted_buffs:
                            new_buffs.append(buff_data)
                    player["buffUptimes"] = new_buffs
        if full_json:
            with gzip.open(jpath, 'wb') as gzf:
                gzf.write(json.dumps(j))
        return EliteInsightDataReduced(j)


class DpsReportAPI:
    dps_report_url = 'https://dps.report'

    def upload_file(self, path) -> "DpsReportResponse":
        url_path = '/uploadContent'
        file = {
            'file': open(path, 'rb')
        }
        params = {
            'json': 1,
            'generator': 'ei',
        }
        response = requests.post(self.dps_report_url + url_path, params, files=file)
        if response.status_code == 200:
            return DpsReportResponse(response.json())
        print('FAILED UPLOADING FILE: CODE %d' % response.status_code)
        print(response.text)

    def get_json_data_by_id(self, dps_report_id):
        url_path = '/getJson'
        params = {
            'id': dps_report_id,
        }
        response = requests.get(self.dps_report_url + url_path, params)
        if response.status_code == 200:
            return EliteInsightData(response.json())
        print("FAILED GETTING JSON")
        print(response.status_code)
        print(response.text)

    @staticmethod
    def get_cr_json_data_by_link(link, silent=True):
        response = requests.get(link)
        if response.status_code == 200:
            parts = response.text.split("var _logData = ")
            if len(parts) != 2:
                if not silent:
                    print("Got %d parts in html parser pattern 1, 2 expected -> trying back up" % len(parts))
                parts = response.text.split("var logData = ")
                if len(parts) != 2:
                    print("Got %d parts in html parser bu pattern 1, 2 expected" % len(parts))
                    return
                parts = parts[1].split(";\r\n    <")
                if len(parts) <= 2:
                    print("Got %d parts in html parser bu pattern 2, >=2 expected" % len(parts))
                    return
            else:  # default parts2
                parts = parts[1].split(";\r\n        var logData = _logData;")
                if len(parts) != 2:
                    if not silent:
                        print("Got %d parts in html parser pattern 2, 2 expected -> trying backup" % len(parts))
                    parts = response.text.split("var _logData = ")[1].split(";")
                    if len(parts) <= 2:
                        print("Got %d parts in html parser pattern 2 bu 2, >=2 expected" % len(parts))
                        return
            str = parts[0]
            j = json.loads(str)
            if 'crData' not in j:
                print("No crData found")
                return
            return CombatReplayData(j['crData'])
        print("FAILED GETTING CR JSON")
        print(response.status_code)
        print(response.text)

    def get_json_data_by_link(self, link):
        url_path = '/getJson'
        params = {
            'permalink': link,
        }
        try:
            response = requests.get(self.dps_report_url + url_path, params)
            if response.status_code == 200:
                try:
                    return EliteInsightData(response.json())
                except jsondecoder.JSONDecodeError as e:
                    print("JSON parser error in", link)
                    print(e)
            print("FAILED GETTING JSON for", link, "from", response.url)
            print(response.status_code)
            if len(response.text) < 5000:
                print(response.text)
        except MemoryError as e:
            print("FAILED GETTING JSON for", link, "from", self.dps_report_url + url_path, str(params))
            print(e)

    def get_upload_data_from_link(self, link):
        url_path = '/getUploadMetadata'
        params = {
            'permalink': link,
        }
        response = requests.get(self.dps_report_url + url_path, params)
        if response.status_code == 200:
            return DpsReportResponse(response.json())
        print('FAILED TO GET DATA: CODE %d' % response.status_code)
        print(response.text)


class DpsReportResponse:
    def __init__(self, data):
        self.data = data

    def get_id(self):
        return self.data['id']

    def is_json_available(self):
        return self.data['encounter']['jsonAvailable']

    def get_boss_id(self):
        return self.data['evtc']['bossId']

    def is_success(self) -> bool:
        return self.data['encounter']['success']

    def get_permalink(self) -> str:
        return self.data['permalink']

    def get_boss(self):
        return self.data['encounter']['boss']

    def get_account_names(self):
        account_names = []
        for char_name in self.data['players']:
            account_names.append(self.data['players'][char_name]['display_name'])
        return account_names

    def is_cm(self) -> bool:
        return self.data['encounter']['isCm']


class EliteInsightDamageDist:
    def __init__(self, data):
        self.data = data

    @property
    def total_damage(self) -> int:
        return self.data["totalDamage"]

    @property
    def total_cc(self) -> int:
        return self.data["totalBreakbarDamage"]

    @property
    def min_hit(self) -> int:
        return self.data["min"]

    @property
    def max_hit(self) -> int:
        return self.data["max"]

    @property
    def hit_count(self) -> int:
        return self.data["hits"]

    @property
    def connected_hit_count(self) -> int:
        return self.data["connectedHits"]

    @property
    def crit_count(self) -> int:
        return self.data["crit"]

    @property
    def glancing_count(self) -> int:
        return self.data["glance"]

    @property
    def flanking_count(self) -> int:
        return self.data["flank"]

    @property
    def against_moving_count(self) -> int:
        return self.data["againstMoving"]

    @property
    def miss_count(self) -> int:
        return self.data["missed"]

    @property
    def invulned_count(self) -> int:
        return self.data["invulned"]

    @property
    def interrupted_count(self) -> int:
        return self.data["interrupted"]

    @property
    def evaded_count(self) -> int:
        return self.data["evaded"]

    @property
    def block_count(self) -> int:
        return self.data["blocked"]

    @property
    def barrier_damage(self) -> int:
        return self.data["shieldDamage"]

    @property
    def crit_damage(self) -> int:
        return self.data["critDamage"]

    @property
    def skill_id(self) -> int:
        return self.data["id"]

    @property
    def is_indirect(self) -> bool:
        return self.data["indirectDamage"]

    def __repr__(self):
        return f"[{self.skill_id}]: {self.total_damage} dmg in {self.hit_count} hits"


class EliteInsightSupportStats:
    def __init__(self, data):
        self.data = data

    @property
    def resurrect_count(self) -> int:
        return self.data["resurrects"]

    @property
    def resurrect_time_s(self) -> float:
        return self.data["resurrectTime"]

    @property
    def condi_cleanse(self) -> int:
        return self.data["condiCleanse"]

    @property
    def condis_cleansed_duration_s(self) -> float:
        return self.data["condiCleanseTime"]

    @property
    def self_condi_cleanse(self) -> int:
        return self.data["condiCleanseSelf"]

    @property
    def self_condi_cleanse_duration_s(self) -> float:
        return self.data["condiCleanseTimeSelf"]

    @property
    def boons_striped_from(self) -> int:
        return self.data["boonStrips"]

    @property
    def boons_striped_from_duration_s(self) -> float:
        return self.data["boonStripsTime"]


class EliteInsightGameplayStats:
    def __init__(self, data):
        self.data = data

    @property
    def total_hit_count(self) -> int:
        return self.data["totalDamageCount"]

    @property
    def direct_hit_count(self):
        return self.data["directDamageCount"]

    @property
    def connected_direct_hit_count(self):
        return self.data["connectedDirectDamageCount"]

    @property
    def connected_hit_count(self):
        return self.data["connectedDamageCount"]

    @property
    def critable_hit_count(self):
        return self.data["critableDirectDamageCount"]

    @property
    def crit_hit_count(self):
        return self.data["totalDamageCount"]

    @property
    def critical_damage(self):
        return self.data["criticalDmg"]

    @property
    def flanking_hits(self):
        return self.data["flankingRate"]

    @property
    def against_moving_hits(self):
        return self.data["againstMovingRate"]

    @property
    def glancing_hits(self):
        return self.data["glanceRate"]

    @property
    def missed_hits(self):
        return self.data["missed"]

    @property
    def evaded_hits(self):
        return self.data["evaded"]

    @property
    def blocked_hits(self):
        return self.data["blocked"]

    @property
    def interrupts(self):
        return self.data["interrupts"]

    @property
    def invulned_hits(self):
        return self.data["invulned"]

    @property
    def target_kills(self):
        return self.data["killed"]

    @property
    def target_downs(self):
        return self.data["downed"]


class EliteInsightDamageModData:
    def __init__(self, data):
        self.data = data

    @property
    def id(self):
        return self.data["id"]

    @property
    def total_hit_count(self, *, phase_index=0) -> int:
        return self.data["damageModifiers"][phase_index]["totalHitCount"]

    @property
    def active_hit_count(self, *, phase_index=0) -> int:
        return self.data["damageModifiers"][phase_index]["hitCount"]

    @property
    def damage_gain(self, *, phase_index=0) -> float:
        return self.data["damageModifiers"][phase_index]["damageGain"]

    @property
    def total_damage(self, *, phase_index=0) -> int:
        return self.data["damageModifiers"][phase_index]["totalDamage"]


class EliteInsightConsumable:
    def __init__(self, data):
        self.data = data

    @property
    def stack_count(self):
        return self.data["stack"]

    @property
    def remaining_time_ms(self):
        return self.data["duration"]

    @property
    def start_time_ms(self):
        return self.data["time"]

    @property
    def id(self):
        return self.data["id"]


class EliteInsightMinion:
    def __init__(self, data):
        self.data = data

    @property
    def name(self):
        return self.data["name"]

    # todo totalDamage totalTargetDamage
    # todo totalBreakbarDamage totalTargetBreakbarDamage
    # todo totalShieldDamage totalTargetShieldDamage
    # todo totalDamageDist targetDamageDist
    # todo rotation


class EliteInsightActor:
    def __init__(self, data):
        self.data = data

    @property
    def name(self) -> str:
        return self.data["name"]

    @property
    def total_health(self) -> int:
        return self.data["totalHealth"]

    @property
    def condition_rank(self) -> int:
        return self.data["condition"]

    @property
    def concentration_rank(self) -> int:
        return self.data["concentration"]

    @property
    def healing_rank(self) -> int:
        return self.data["healing"]

    @property
    def toughness_rank(self) -> int:
        return self.data["toughness"]

    @property
    def hitbox_height(self) -> int:
        return self.data["hitboxHeight"]

    @property
    def hitbox_width(self) -> int:
        return self.data["hitboxWidth"]

    @property
    def instance_id(self) -> int:
        return self.data["instanceID"]

    def get_minions(self) -> list[EliteInsightMinion]:
        return [EliteInsightMinion(minion) for minion in self.data["minions"]]

    @property
    def is_fake(self) -> bool:
        if "isFake" not in self.data:
            return False
        return self.data["isFake"]

    def get_total_damage(self, phase_index=0) -> int:
        return self.data['dpsAll'][phase_index]['damage']

    def get_total_power_damage(self, phase_index=0) -> int:
        return self.data['dpsAll'][phase_index]['powerDamage']

    def get_total_condi_damage(self, phase_index=0) -> int:
        return self.data['dpsAll'][phase_index]['condiDamage']

    def get_total_breakbar_damage(self, phase_index=0):
        return self.data['dpsAll'][phase_index]['breakbarDamage']

    def get_dist_sq_center(self, *, phase_index=0) -> float:
        if self.data["statsAll"][phase_index]["stackDist"] == "NaN":
            return 0
        return self.data["statsAll"][phase_index]["stackDist"]

    def get_total_damage_dist(self, *, phase_index=0) -> Generator[EliteInsightDamageDist, None, None]:
        return (EliteInsightDamageDist(skill_data) for
                skill_data in self.data["totalDamageDist"][phase_index])

    def get_health_percent_at_time(self, time_ms) -> float:
        hp = 100
        for hp_datapoint in self.data["healthPercents"]:
            if hp_datapoint[0] > time_ms:
                break
            hp = hp_datapoint[1]
        return hp

    def get_position_keyframe(self, time_index):
        if len(self.data["combatReplayData"]["positions"]) <= time_index:
            return
        return self.data["combatReplayData"]["positions"][time_index]

    def get_position_interpolate(self, time_ms, *, polling_rate=150):
        p1 = self.get_position_keyframe(int(time_ms / polling_rate))
        p2 = self.get_position_keyframe(int(time_ms / polling_rate) + 1)
        if not p1:
            return
        if not p2:
            return p1
        w2 = (time_ms / polling_rate) - int(time_ms / polling_rate)
        w1 = 1.0 - w2
        return p1[0] * w1 + p2[0] * w2, p1[1] * w1 + p2[1] * w2

    def get_orientation_keyframe(self, time_index) -> Optional[float]:
        if "orientations" not in self.data["combatReplayData"] or self.data["combatReplayData"]["orientations"] is None:
            return
        if len(self.data["combatReplayData"]["orientations"]) <= time_index:
            return
        return self.data["combatReplayData"]["orientations"][time_index]

    def get_orientation_interpolate(self, time_ms, *, polling_rate=150) -> Optional[float]:
        r1 = self.get_orientation_keyframe(int(time_ms / polling_rate))
        r2 = self.get_orientation_keyframe(int(time_ms / polling_rate) + 1)
        if not r1:
            return
        if not r2:
            return r1
        if abs(r1 - r2) > 180:
            if r1 > r2:
                r2 += 180
            else:
                r1 += 180
        w2 = (time_ms / polling_rate) - int(time_ms / polling_rate)
        w1 = 1.0 - w2
        r = r1 * w1 + r2 * w2
        if r > 180:
            return r - 180
        return r


class EliteInsightSkillCast:
    def __init__(self, data, skill_id: int):
        self.data = data
        self.skill_id_ = skill_id

    @property
    def cast_time(self) -> datetime.timedelta:
        return datetime.timedelta(milliseconds=self.data["castTime"])

    @property
    def cast_duration(self) -> datetime.timedelta:
        return datetime.timedelta(milliseconds=self.data["duration"])

    @property
    def time_gained(self) -> datetime.timedelta:
        return datetime.timedelta(milliseconds=self.data["timeGained"])

    @property
    def quickness_uptime(self) -> float:
        return self.data["quickness"]


class EliteInsightDeathRecapEntry:
    def __init__(self, data):
        self.data = data

    @property
    def skill_id(self) -> int:
        return self.data["id"]

    @property
    def is_indirect_damage(self) -> bool:
        return self.data["indirectDamage"]

    @property
    def damage_source_name(self) -> str:
        return self.data["src"]

    @property
    def damage(self) -> int:
        return self.data["damage"]

    @property
    def time(self) -> datetime.timedelta:
        return datetime.timedelta(milliseconds=self.data["time"])


class EliteInsightDeathRecap:
    def __init__(self, data):
        self.data = data

    @property
    def death_time(self) -> datetime.timedelta:
        return datetime.timedelta(milliseconds=self.data["deathTime"])

    def get_to_down(self) -> list[EliteInsightDeathRecapEntry]:
        if "toDown" in self.data:
            return [EliteInsightDeathRecapEntry(entry) for entry in self.data["toDown"]]
        return []

    def get_to_kill(self) -> list[EliteInsightDeathRecapEntry]:
        if "toKill" in self.data:
            return [EliteInsightDeathRecapEntry(entry) for entry in self.data["toKill"]]
        return []

class EliteInsightPlayer(EliteInsightActor):
    def __init__(self, data):
        super().__init__(data)

    @property
    def account_name(self) -> str:
        return self.data["account"]

    @property
    def sub_group(self) -> int:
        return self.data["group"]

    @property
    def is_commander(self) -> bool:
        return self.data["hasCommanderTag"]

    @property
    def profession_name(self) -> str:
        return self.data["profession"]

    @property
    def is_friendly_npc(self) -> bool:
        return self.data["friendlyNPC"]

    @property
    def guild_id(self) -> str:
        return self.data["guildID"]

    def get_weapon(self, slot: int) -> str:
        return self.data["weapons"][slot]

    def get_target_dps(self, *, target_index=0, phase=0):
        return self.data['dpsTargets'][target_index][phase]['dps']

    def get_target_damage(self, *, target_index=0, phase_index=0):
        return self.data['dpsTargets'][target_index][phase_index]['damage']

    def get_target_cc(self, *, target_index=0, phase=0):
        return self.data['dpsTargets'][target_index][phase]['breakbarDamage']

    def get_all_targets_dps(self, *, phase=0):
        sum = 0
        for target_dps in self.data['dpsTargets']:
            if phase < len(target_dps):
                sum += target_dps[phase]['dps']
        return sum

    def get_all_targets_cc(self, *, phase=0):
        sum = 0
        for target_dps in self.data['dpsTargets']:
            sum += target_dps[phase]['breakbarDamage']
        return sum

    # Todo (condi, power)x(dps, damage), everything with actor

    def get_target_dps_at_time(self, time: float, *, target_index=0, phase_index=0) -> int:
        index = int(time)
        if index >= len(self.data['targetDamage1S'][target_index][phase_index]) - 1:
            return 0
        return self.data['targetDamage1S'][target_index][phase_index][index + 1] - \
               self.data['targetDamage1S'][target_index][phase_index][
                   index]

    def get_target_power_dps_at_time(self, time: float, *, target_index=0, phase_index=0) -> int:
        index = int(time)
        if index >= len(self.data['targetPowerDamage1S'][target_index][phase_index]) - 1:
            return 0
        return self.data['targetPowerDamage1S'][target_index][phase_index][index + 1] - \
               self.data['targetPowerDamage1S'][target_index][phase_index][
                   index]

    # Todo [target](dps, condi, power, cc)_dps_at_time

    def get_target_damage_dist(self, *, target_index=0, phase_index=0) -> Generator[EliteInsightDamageDist, None, None]:
        return (EliteInsightDamageDist(skill_data) for
                skill_data in self.data["targetDamageDist"][target_index][phase_index])

    def get_target_stats(self, *, target_index=0, phase_index=0) -> EliteInsightGameplayStats:
        return EliteInsightGameplayStats(self.data["statsTargets"][target_index][phase_index])

    def get_support_stats(self, *, phase_index=0) -> EliteInsightSupportStats:
        return EliteInsightSupportStats(self.data["support"][phase_index])

    def get_damage_mods(self, damage_mod_id) -> EliteInsightDamageModData:
        for damage_mod in self.data["damageModifiers"]:
            if damage_mod["id"] == damage_mod_id:
                return EliteInsightDamageModData(damage_mod)

    def get_damage_mods_target(self, damage_mod_id, target_index=0) -> Optional[EliteInsightDamageModData]:
        for damage_mod in self.data["damageModifiersTarget"][target_index]:
            if damage_mod["id"] == damage_mod_id:
                return EliteInsightDamageModData(damage_mod)

    # todo
    @property
    def buffs(self):
        return self.data['buffUptimes']

    # todo selfBuffs groupBuffs offGroupBuffs squadBuffs buffUptimesActive selfBuffsActive

    def get_death_recaps(self) -> list[EliteInsightDeathRecap]:
        if "deathRecap" in self.data:
            return [EliteInsightDeathRecap(dr) for dr in self.data["deathRecap"]]
        return []

    def get_consumables(self) -> list[EliteInsightConsumable]:
        if "consumables" not in self.data:
            return []
        return [EliteInsightConsumable(con) for con in self.data["consumables"]]

    def active_time_ms(self, *, phase_index=0) -> int:
        return self.data["activeTimes"][phase_index]

    ######################################

    def get_buff_uptime(self, buff_id, *, phase_index=0) -> float:
        for uptimes in self.data['buffUptimes']:
            if uptimes["id"] == buff_id:
                return uptimes["buffData"][phase_index]["uptime"]
        return 0.

    def get_squad_buff_generation(self, buff_id, *, phase_index=0) -> float:
        if "squadBuffs" in self.data:
            for generation in self.data["squadBuffs"]:
                if generation["id"] == buff_id:
                    return generation["buffData"][phase_index]["generation"]
        return 0.

    def get_group_buff_generation(self, buff_id, *, phase_index=0) -> float:
        if "groupBuffs" in self.data:
            for generation in self.data["groupBuffs"]:
                if generation["id"] == buff_id:
                    return generation["buffData"][phase_index]["generation"]
        return 0.

    def get_off_group_buff_generation(self, buff_id, *, phase_index=0) -> float:
        if "offGroupBuffs" in self.data and self.data["offGroupBuffs"] is not None:
            for generation in self.data["offGroupBuffs"]:
                if generation["id"] == buff_id:
                    return generation["buffData"][phase_index]["generation"]
        return 0.

    def get_skill_cast(self, skill_id):
        if "rotation" not in self.data:
            return []
        for skill in self.data["rotation"]:
            if skill["id"] == skill_id:
                return [EliteInsightSkillCast(cast, skill["id"]) for cast in skill["skills"]]
        return []

    def get_skill_ids_used(self) -> List[int]:
        skill_ids = []
        if "rotation" in self.data and self.data["rotation"] is not None:
            for skill in self.data["rotation"]:
                skill_ids.append(skill["id"])
        return skill_ids

    def get_total_healing_out(self, phase_index=0) -> int:
        if "extHealingStats" in self.data:
            return self.data["extHealingStats"]["outgoingHealing"][phase_index]["healing"]
        return 0

    def get_hps(self, phase_index=0) -> int:
        if "extHealingStats" in self.data:
            return self.data["extHealingStats"]["outgoingHealing"][phase_index]["hps"]
        return 0

    def get_resurrect_time_s(self, *, phase_index=0) -> float:
        return self.data["support"][phase_index]["resurrectTime"]

    def get_down_duration_ms(self, *, phase_index=0) -> int:
        return self.data["defenses"][phase_index]["downDuration"]

    def get_down_count(self, *, phase_index=0) -> int:
        return self.data["defenses"][phase_index]["downCount"]

    def get_condi_cleanse_out(self, *, phase_index=0) -> int:
        return self.data["support"][phase_index]["condiCleanse"]

    def get_damage_taken(self, *, phase_index=0) -> int:
        return self.data["defenses"][phase_index]["damageTaken"]

    def get_damage_against_barrier(self, *, phase_index=0) -> int:
        return self.data["defenses"][phase_index]["damageBarrier"]

    def __repr__(self):
        return f"EIPlayer: {self.name}"


class EliteInsightNPC(EliteInsightActor):
    def __init__(self, data):
        super().__init__(data)

    @property
    def id(self) -> int:
        return self.data["id"]

    @property
    def final_health(self) -> int:
        return self.data["finalHealth"]

    @property
    def damage_done_percent(self) -> float:
        return self.data["healthPercentBurned"]

    @property
    def first_aware_ms(self) -> int:
        return self.data["firstAware"]

    @property
    def last_aware_ms(self) -> int:
        return self.data["lastAware"]

    @property
    def is_enemy_player(self) -> bool:
        return self.data["lastAware"]

    # todo buffs, breakbarPercents

    def __repr__(self):
        return f"EIActor: {self.name}"


class EliteInsightPhase:
    def __init__(self, data):
        self.data = data

    def get_start_time_ms(self) -> int:
        return int(self.data['start'])

    def get_end_time_ms(self) -> int:
        return int(self.data['end'])

    def get_duration_ms(self) -> int:
        return self.get_end_time_ms() - self.get_start_time_ms()

    @property
    def name(self) -> str:
        return self.data['name']

    def get_sub_phases(self, ei_data) -> List['EliteInsightPhase']:
        if 'subPhases' in self.data:
            return [ei_data.phases[i] for i in self.data['subPhases']]
        else:
            return []

    def get_sub_phases_indices(self):
        if 'subPhases' in self.data:
            return self.data['subPhases']
        else:
            return []

    @property
    def is_break_bar_phase(self) -> bool:
        return self.data['breakbarPhase']


class EliteInsightDataReduced:
    def __init__(self, data):
        self.data = data
        self.players_cache = None

    @property
    def fight_name(self):
        return self.data['fightName']

    @property
    def players(self) -> list[EliteInsightPlayer]:
        if self.players_cache is None:
            self.players_cache = [EliteInsightPlayer(player) for player in self.data['players']]
        return self.players_cache

    #todo @deprecated()
    def get_duration_ms(self):
        duration_string = str(self.data['duration'])
        parts = re.findall(r'\d+', duration_string)
        return 1000 * (60 * int(parts[0]) + int(parts[1])) + int(parts[2])

    @property
    def duration(self) -> datetime.timedelta:
        duration_string = str(self.data['duration'])
        parts = re.findall(r'\d+', duration_string)
        ms = 1000 * (60 * int(parts[0]) + int(parts[1])) + int(parts[2])
        return datetime.timedelta(milliseconds=ms)

    @property
    def instabilities(self):
        if not self.data['presentFractalInstabilities']:
            return []
        return self.data['presentFractalInstabilities']

    def get_target_names(self):
        names = []
        for target in self.data['targets']:
            names.append(target['name'])
        return names

    def get_player_by_name(self, name) -> Optional[EliteInsightPlayer]:
        return next(filter(lambda p: p.name == name, self.players), None)

    def get_player_by_account_name(self, ac_name):
        return next(filter(lambda p: p.account_name() == ac_name, self.players), None)

    @property
    def is_cm(self):
        return self.data["isCM"]

    @property
    def is_success(self):
        return self.data['success']

    @property
    def start_time(self):
        if "timeStartStd" in self.data:
            return datetime.datetime.strptime(self.data["timeStartStd"], "%Y-%m-%d %H:%M:%S %z")
        else:
            return datetime.datetime.strptime(self.data["timeStart"], "%Y-%m-%d %H:%M:%S %z")

    @property
    def end_time(self):
        if "timeEndStd" in self.data:
            return datetime.datetime.strptime(self.data["timeEndStd"], "%Y-%m-%d %H:%M:%S %z")
        else:
            return datetime.datetime.strptime(self.data["timeEnd"], "%Y-%m-%d %H:%M:%S %z")

    def get_party_buff_uptimes(self, *, buffs=None):
        if buffs is None:
            return []
        uptimes = {}
        for buff in buffs:
            uptimes[buff] = 0
        players = self.players
        player_count = len(players)
        for player in players:
            player_buffs = player.buffs
            for buff in player_buffs:
                if buff['id'] in buffs:
                    uptimes[buff['id']] += buff['buffData'][0]['uptime'] / player_count
        return uptimes

    def has_combat_replay_data(self):
        return False


class EliteInsightData(EliteInsightDataReduced):

    def __init__(self, data):
        super().__init__(data)

    @property
    def targets(self) -> list[EliteInsightNPC]:
        return [EliteInsightNPC(d) for d in self.data['targets']]

    @property
    def phases(self):
        phases = [EliteInsightPhase(phase) for phase in self.data['phases']]
        return phases

    def get_main_phase(self):
        return self.phases[0]

    def has_combat_replay_data(self):
        return "combatReplayMetaData" in self.data

    @property
    def combat_replay_inch_to_pxl(self):
        return self.data["combatReplayMetaData"]["inchToPixel"]

    @property
    def skill_infos(self):
        return (SkillInformation(self.data["skillMap"][sid_ext], int(sid_ext[1:])) for sid_ext in self.data["skillMap"])

    def get_skill_info(self, skill_id: int):
        if f"s{skill_id}" not in self.data["skillMap"]:
            return None
        return SkillInformation(self.data["skillMap"][f"s{skill_id}"], skill_id)

    def get_party_buff_uptimes(self, *, buffs=None, average_phases=False):
        if buffs is None:
            return []
        uptimes = {}
        for buff in buffs:
            uptimes[buff] = 0
        players = self.players
        player_count = len(players)
        for player in players:
            player_buffs = player.buffs
            for buff in player_buffs:
                if buff['id'] in buffs:
                    if average_phases:
                        phases = self.get_main_phase().get_sub_phases(self)
                        total_duration = 0.0
                        for phase in phases:
                            total_duration += phase.get_duration_ms()
                        for phase_index in range(len(phases)):
                            weight = phases[phase_index].get_duration_ms() / player_count / total_duration
                            uptimes[buff['id']] += buff['buffData'][phase_index]['uptime'] * weight
                    else:
                        uptimes[buff['id']] += buff['buffData'][0]['uptime'] / player_count
        return uptimes

    def get_targets_buff_uptime(self, *, buffs=None):
        if buffs is None:
            buffs = []
        uptimes = {}
        for buff in buffs:
            uptimes[buff] = 0
        targets = self.targets
        target_count = len(targets)
        for target in targets:
            target_buffs = target.data['buffs']
            for buff in target_buffs:
                if buff['id'] in buffs and buff['buffData'][0]['presence'] > 0:
                    if buff['buffData'][0]['presence'] > 0:
                        uptimes[buff['id']] += buff['buffData'][0]['uptime'] / target_count * 100.0 / \
                                               buff['buffData'][0]['presence']
                    else:
                        uptimes[buff['id']] = -1
        return uptimes

    def get_main_targets_buff_uptime(self, *, buffs=None):
        if buffs is None:
            buffs = []
        uptimes = {}
        for buff in buffs:
            uptimes[buff] = 0
        targets = [self.targets[0]]  # first target is main target
        target_count = len(targets)
        for target in targets:
            target_buffs = target.data['buffs']
            for buff in target_buffs:
                if buff['id'] in buffs and buff['buffData'][0]['presence'] > 0:
                    if buff['buffData'][0]['presence'] > 0:
                        uptimes[buff['id']] += buff['buffData'][0]['uptime'] / target_count * 100.0 / \
                                               buff['buffData'][0]['presence']
                    else:
                        uptimes[buff['id']] = -1
        return uptimes

    # warning death (or any) events can be after the fight
    def get_mechanic_events(self, mechanic_name: str) -> Optional[list]:
        if "mechanics" in self.data:
            for mechanic in self.data["mechanics"]:
                if mechanic["name"] == mechanic_name:
                    return mechanic["mechanicsData"]
        return []

    def get_mechanic_counts(self, mechanic_name, *, filter_before_ms=None, filter_after_ms=None) -> dict[str, int]:
        if filter_before_ms is None:
            filter_before_ms = self.get_duration_ms()
        mechanic_counts = {}
        if "mechanics" in self.data:
            for mechanic in self.data["mechanics"]:
                if mechanic["name"] == mechanic_name:
                    for instance in mechanic["mechanicsData"]:
                        if filter_before_ms and instance["time"] > filter_before_ms:
                            continue
                        if filter_after_ms and instance["time"] < filter_after_ms:
                            continue
                        if not instance["actor"] in mechanic_counts:
                            mechanic_counts[instance["actor"]] = 0
                        mechanic_counts[instance["actor"]] += 1
                    break
        return mechanic_counts

    def get_buff_data(self, buff_id):
        if f"b{buff_id}" not in self.data["buffMap"]:
            return None
        return EIBuffData(self.data["buffMap"][f"b{buff_id}"], buff_id)

    def get_squad_dps(self, *, phase_index=0):
        return sum(player.get_all_targets_dps(phase=phase_index) for player in self.players)


class EIBuffData:
    def __init__(self, data, buff_id):
        self.data = data
        self._id = buff_id

    @property
    def id(self) -> int:
        return self._id

    @property
    def name(self) -> str:
        return self.data["name"]

    @property
    def icon_url(self) -> str:
        return self.data["icon"]

    @property
    def is_stacking(self) -> bool:
        return self.data["stacking"]

    @property
    def is_conversion_based_healing(self) -> bool:
        return self.data["conversionBasedHealing"]

    @property
    def is_hybrid_healing(self) -> bool:
        return self.data["hybridHealing"]

    @property
    def descriptions(self) -> list[str]:
        return self.data["descriptions"]

    def __eq__(self, other):
        return self.id == other.id


class SkillInformation:
    def __init__(self, data, skill_id):
        self.data = data
        self._id = skill_id

    @property
    def name(self):
        return self.data["name"]

    #todo @deprecated()
    def get_name(self):
        return self.data["name"]

    #todo @deprecated()
    def get_icon_url(self):
        return self.data["icon"]

    @property
    def id(self):
        return self._id

    @property
    def icon_url(self):
        return self.data["icon"]

    @property
    def is_auto_attack(self):
        return self.data["autoAttack"]

    @property
    def can_crit(self):
        return self.data["canCrit"]

    def __repr__(self):
        return f"{self.name} [{self.id}]"


class CombatReplayData:
    def __init__(self, data):
        self.data = data

    def get_position_keyframe(self, actor_id, time_index):
        for actor in self.data["actors"]:
            if 'id' not in actor or actor['id'] != actor_id:
                continue
            if len(actor["positions"]) < time_index * 2 + 2:
                return
            x = actor["positions"][time_index * 2]
            y = actor["positions"][time_index * 2 + 1]
            return x, y

    def get_position_interpolate(self, actor_id, time_ms):
        p1 = self.get_position_keyframe(actor_id, int(time_ms / 150))
        p2 = self.get_position_keyframe(actor_id, int(time_ms / 150) + 1)
        if not p1:
            return
        if not p2:
            return p1
        w2 = (time_ms / 150.0) - int(time_ms / 150)
        w1 = 1.0 - w2
        return p1[0] * w1 + p2[0] * w2, p1[1] * w1 + p2[1] * w2

    def get_rotation_keyframe(self, actor_id, time_index):
        for actor in self.data["actors"]:
            if actor["type"] != "Facing" or 'connectedTo' not in actor or actor['connectedTo'] != actor_id:
                continue
            if len(actor["facingData"]) <= time_index:
                return
            return actor["facingData"][time_index]

    def get_actors(self):
        actors = []
        for actor in self.data["actors"]:
            if 'id' not in actor:
                continue
            ac = {
                "img": actor["img"],
                "type": actor["type"],
                "id": actor["id"]
            }
            if 'group' in actor:
                ac["group"] = actor["group"]
            if 'start' in actor:
                ac["start"] = actor["start"]
            if 'end' in actor:
                ac["end"] = actor["end"]
            actors.append(ac)
        return actors

    def get_all_actor_entries(self):
        return self.data["actors"]

    @property
    def size(self) -> list[int]:
        return self.data["sizes"]

    @property
    def map_img_link(self):
        return self.data['maps'][0]['link']


example_response = {
    "id": "M9kd-20210313-233016",
    "permalink": "https:\/\/dps.report\/M9kd-20210313-233016_ai",
    "uploadTime": 1615737939,
    "encounterTime": 1615674456,
    "generator": "Elite Insights",
    "generatorId": 1,
    "generatorVersion": 2,
    "language": "en",
    "languageId": 0,
    "evtc": {
        "type": "EVTC",
        "version": "20210223",
        "bossId": 23254
    },
    "players": {
        "Make The Guard": {
            "display_name": "Necro Boys Friend.8509",
            "character_name": "Make The Guard",
            "profession": 1,
            "elite_spec": 62
        },
        "Lyxiera": {
            "display_name": "lynnraven.1540",
            "character_name": "Lyxiera",
            "profession": 4,
            "elite_spec": 55
        },
        "Rayna Caslin": {
            "display_name": "PVzweiundvierzig.7381",
            "character_name": "Rayna Caslin",
            "profession": 9,
            "elite_spec": 63
        },
        "Kitsune Gami": {
            "display_name": "Reyruko.4538",
            "character_name": "Kitsune Gami",
            "profession": 1,
            "elite_spec": 62
        },
        "Iofiel Evaniscus": {
            "display_name": "Aegas.1069",
            "character_name": "Iofiel Evaniscus",
            "profession": 4,
            "elite_spec": 55
        }
    },
    "encounter": {
        "uniqueId": "ghzXI3\/Y+\/lzXzYeeQOh6A",
        "success": True,
        "duration": 160,
        "compDps": 93161,
        "numberOfPlayers": 5,
        "numberOfGroups": 1,
        "bossId": 23254,
        "boss": "Ai",
        "isCm": True,
        "gw2Build": 112716,
        "jsonAvailable": True
    },
    "error": None,
    "userToken": "n3hdolhenip3n57m4osk3hsdfqjt0fee"
}

if __name__ == '__main__':
    api = DpsReportAPI()
    testResponse = DpsReportResponse(example_response)
    if testResponse.is_json_available():
        extraJson = api.get_json_data_by_id(testResponse.get_id())
        print(str(extraJson.get_duration_ms()))
