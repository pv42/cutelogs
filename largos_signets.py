import dps_report_api
from logmanagercachereader import LogManagerCache, EliteSpecialization

signet_of_wrath_cast = 9151
signet_of_wrath_buff_pi = 9237
signet_of_wrath_buff = 9100
alacrity = 30328


def main():
    twin_logs = LogManagerCache().get_logs(target_filter=["Nikare"], success_filter=[True])
    print(len(twin_logs))
    index = 0
    for log in twin_logs:
        found_fb_ewayne = False
        ewayne_sub = 0
        max_sub = 0
        for player in log.players():
            max_sub = max(max_sub, player.data["Subgroup"])
            if player.account_name() in [":Aegas.1069",
                                             ":MoreEwayne.1732"] and player.get_elite_spec() == EliteSpecialization.Firebrand:
                found_fb_ewayne = True

                ewayne_sub = player.data["Subgroup"]
                break
        if not found_fb_ewayne:
            continue
        if max_sub == 1:
            print("invalid max sub")
        ewayne_sub += 2 - max_sub
        dps_log = dps_report_api.get_ei_data_cached(log.get_dps_report_url())
        for player in dps_log.players():
            if player.account_name() in ["Aegas.1069", "MoreEwayne.1732"]:
                alac_uptimes = []

                signet_casts_times = []
                for buff_data in player.data["buffUptimesActive"]:

                    if buff_data["id"] == alacrity:
                        for phase_index in range(len(buff_data["buffData"])):
                            alac_uptimes.append(buff_data["buffData"][phase_index]["uptime"] / 100)
                        # print(alac_uptimes)
                next_possible_cast = player.data["rotation"][0]["skills"][0]["castTime"]
                for skill_casts in player.data["rotation"]:
                    if skill_casts["id"] == signet_of_wrath_cast:
                        for cast in skill_casts["skills"]:
                            if cast["duration"] < 500:
                                continue
                            signet_casts_times.append(cast["castTime"])
                            # print("cast at %5.1fs earliest %5.1fs dt=%6.2fs" % (cast["castTime"] / 1000, next_possible_cast/1000, (cast["castTime"] - next_possible_cast)/1000))
                            next_possible_cast = cast["castTime"] + 20000 - 4000 * alac_uptimes[0]
                if len(signet_casts_times) == 0:
                    continue
                print()
                print(log.get_encounter_start_time(), log.get_dps_report_url())
                cast_index = 0
                casts_per_phase = {}
                max_casts_per_phase = {}
                last_phase_end = 0
                for phase_index in dps_log.get_main_phase().get_sub_phases_indices():
                    phase = dps_log.phases()[phase_index]
                    if ewayne_sub == 1 and phase.name in ["Kenut P2", "Kenut P3"]:
                        continue
                    if ewayne_sub == 2 and phase.name in ["Nikare P2", "Nikare P3"]:
                        continue
                    avg_cd = (20000 - 4000 * alac_uptimes[phase_index])
                    casts_per_phase[phase_index] = 0
                    cd_overlap = 0
                    if cast_index > 0:
                        cd_overlap = signet_casts_times[cast_index - 1] + avg_cd - phase.get_start_time_ms()
                    if cd_overlap < 0:
                        cd_overlap = 0
                    while len(signet_casts_times) > cast_index and phase.get_end_time_ms() > signet_casts_times[
                        cast_index]:
                        # print("cast in p" , phase_index," at " , signet_casts_times[cast_index])
                        casts_per_phase[phase_index] += 1
                        cast_index += 1

                    max_casts_per_phase[phase_index] = int(1 + (phase.get_duration_ms() - cd_overlap) / avg_cd)
                    last_phase_end = phase.get_end_time_ms()
                    print("Phase %d %-10s: %d/%d casts" % (
                        phase_index, phase.name, casts_per_phase[phase_index], max_casts_per_phase[phase_index]))
                tot_c = 0
                tot_m = 0
                for phase_index in casts_per_phase:
                    tot_m += max_casts_per_phase[phase_index]
                    tot_c += casts_per_phase[phase_index]
                print("Total: %d/%d casts" % (tot_c, tot_m))

        index += 1
        if index == 2:
            pass  # break


if __name__ == "__main__":
    main()
