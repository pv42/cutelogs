import datetime

import numpy as np

from logmanagercachereader import LogManagerCache
from dps_report_api import DpsReportAPI, get_ei_data_cached


def main(*, self_player_name='Rayna Caslin', max_condi_rank=5):
    ignored_targets = ['Arkk', 'Artsariiv', 'Boneskinner', 'Ensolyss of the Endless Torment', 'Fraenir of Jormag',
                       'MAMA', 'Skorvald', 'Whisper of Jormag', 'Voice of the Fallen', 'Claw of the Fallen',
                       'Nightmare Oratuss', 'Elemental Ai', 'Dark Ai', 'Sorrowful Spellcaster',
                       'Skorvald the Shattered', 'ch17632-333', 'ch17632-336', 'ch17632-348', 'ch17632-342',
                       'ch17632-343', 'ch17632-371', 'ch17632-367', 'ch17632-360', 'ch17632-370', 'ch17632-359', # <- these are skorvalds
                       'Icebrood Construct']

    lmc = LogManagerCache()
    logs = lmc.get_logs()
    dps_per_boss = {}
    i = 0
    today_dps_str = ""
    for log in logs:
        i += 1
        found_myself = False
        if log.get_main_target_name() in ignored_targets:
            continue
        if not log.data['Players']:
            continue
        for player in log.data['Players']:
            if player['Name'] == self_player_name:
                found_myself = True
        if not found_myself:
            continue
        if not log.is_success():
            continue
        url = log.get_dps_report_url()
        if not url:
            continue
        log_data = get_ei_data_cached(url)
        if not log_data or not log_data.is_success():
            continue
        if log_data.targets[0].name in ignored_targets:
            print(log_data.targets[0].name, log.get_main_target_name())
            continue
        for player in log_data.players():
            if player.name == self_player_name:
                if player.condition_rank >= max_condi_rank:
                    continue
                if datetime.datetime.now(datetime.timezone.utc) - log_data.start_time() < datetime.timedelta(
                        days=7):
                    today_dps_str += "%-20s %5.0f\n" % (log_data.targets[0].name[:20], player.get_target_dps())
                if log_data.targets[0].name not in dps_per_boss:
                    dps_per_boss[log_data.targets[0].name] = np.array([])
                dps_per_boss[log_data.targets[0].name] = np.append(
                    dps_per_boss[log_data.targets[0].name], player.get_target_dps())
                # print(log_data.get_targets()[0]['name'], player.get_target_dps())
        print('\b' * 6 + "%4.1f%%" % (100. * i / len(logs)), end='')
    print()
    print(today_dps_str)
    print("Boss                   #   Min   25p   Avg   75p   Max")
    for key in dps_per_boss:
        if (len(dps_per_boss[key]) > 5):
            print("%-20s %3d %5.0f %5.0f %5.0f %5.0f %5.0f" % (
                key[:20], len(dps_per_boss[key]), np.min(dps_per_boss[key]), np.percentile(dps_per_boss[key], 25),
                np.mean(dps_per_boss[key]), np.percentile(dps_per_boss[key], 75), np.max(dps_per_boss[key])))


if __name__ == '__main__':
    main()

'''
relevant in order
Boss                   #   Min   25p   Avg   75p   Max
Cardinal Adina        63  3042  4514  5550  6446  8504
Conjured Amalgamate   61   343 10675 12177 14237 18455
Qadim                 63  1396  2507  2999  3400  4353
Samarog               55  1612  5112  5925  6980  8493
Deimos                50  5326  7803  9051 10227 12422
Keep Construct        48  7560  9423 10286 11092 12449
Slothasor             45  4214  5994  6790  7729  9515
Vale Guardian         44  4169  6412  7746  9352 11222

all
Boss                   #   Min   25p   Avg   75p   Max
Berg                  30   401  1054  1422  1793  2728
Broken King           43    57   614  2905  4280  8236
Cardinal Adina        63  3042  4514  5550  6446  8504
Cardinal Sabir         7  2913  4336  4952  5364  7685
Conjured Amalgamate   61   343 10675 12177 14237 18455
Deimos                50  5326  7803  9051 10227 12422
Desmina                6     0     0     0     0     0
Eater of Souls        45   687  2854  3656  4547  7134
Eye of Fate           13     0  2804  3266  4003  4749
Gorseval the Multifa  36  4787  7395  8082  8957 10729
Twisted Castle        42     0     0     0     0     0
Keep Construct        48  7560  9423 10286 11092 12449
Matthias Gabrel        9  2886  4532  5001  5946  6781
Qadim                 63  1396  2507  2999  3400  4353
Qadim the Peerless     8  8359  9506 10148 10561 11854
Sabetha the Saboteur  30  5681  8176  9050 10189 11637
Samarog               55  1612  5112  5925  6980  8493
Slothasor             45  4214  5994  6790  7729  9515
Vale Guardian         44  4169  6412  7746  9352 11222
Xera                  15  2805  4502  5324  6250  7960
River of Souls        34     0     0     0     0     0
Eye of Judgment       31  1176  3908  4600  5460  7057
'''
