import asyncio
import re
import secrets
from datetime import date

import discord
from gw2 import raids, fractals
import splittlogparser


class DiscordLogSender(discord.Client):
    def __init__(self, log_links_and_times, *, instabilities=None, guild_name='testseverpv42', channel_name='2',
                 mode='raids', sheet_id=None, buffs=None, thumbnail=None, **options):
        super().__init__(**options)
        if instabilities is None:
            instabilities = {}
        self.emote_server = None
        self.log_links_and_times = log_links_and_times
        self.instabilities = instabilities
        self.guild_name = guild_name
        self.channel_name = channel_name
        self.mode = mode
        self.sheet_id = sheet_id
        self.buffs = buffs
        self.thumbnail = thumbnail

    async def on_ready(self):
        print('Connected to discord api as user %s' % self.user)
        await self.invoke_send_logs()
        asyncio.run_coroutine_threadsafe(self.logout(), self.loop)
        # await self.close()

    async def invoke_send_logs(self):
        if len(self.log_links_and_times) == 0:
            print('Not sending empty log report')
            return
        guild = await get_server_by_name(self, self.guild_name)
        if not guild:
            print('No matching guild for "%s" found' % self.guild_name)
            return
        guild = self.get_guild(guild.id)
        channel = None
        channels = await guild.fetch_channels()
        for c in channels:
            if c.name == self.channel_name:
                channel = c
                break
        if not channel:
            print('No matching channel for "%s" found' % self.channel_name)
            return

        if self.mode == 'raids':
            await channel.send(embed=await self.get_raid_embed())
        elif self.mode == 'fracs':
            await channel.send(embed=await self.get_fractal_embed())
        else:
            print('Invalid game node %s' % self.mode)

    def start_bot(self):
        token_file = open('discord_token')
        token = token_file.read()
        token_file.close()
        self.run(token)

    async def get_encounter_emote(self, encounter_id):
        emote_name = 'boss_%s' % encounter_id
        if not self.emote_server:
            self.emote_server = await get_emote_server(self)
        emote = None
        for e in await self.emote_server.fetch_emojis():
            if e.name == emote_name:
                emote = e
                break
        if not emote:
            print("Could not find emote for boss %s" % encounter_id)
            return ''
        else:
            return emote

    async def get_instab_emote(self, instab):
        emote_name = 'instab_%s' % instab
        if not self.emote_server:
            self.emote_server = await get_emote_server(self)
        emote = None
        for e in await self.emote_server.fetch_emojis():
            if e.name == emote_name:
                emote = e
                break
        if not emote:
            print("Could not find emote for instability %s" % instab)
            return ''
        else:
            return emote

    async def get_buff_emote(self, buff_id):
        emote_name = 'buff_%d' % buff_id
        if not self.emote_server:
            self.emote_server = await get_emote_server(self)
        emote = None
        for e in await self.emote_server.fetch_emojis():
            if e.name == emote_name:
                emote = e
                break
        if not emote:
            print("Could not find emote for buff %s" % buff_id)
            return str(buff_id)
        else:
            return emote

    async def get_raid_embed(self):
        embed = discord.Embed(title=str(date.today()))
        embed.set_footer(text=get_random_raid_footer())
        content = ''
        wing = 1
        for boss in raids.encounters:
            if boss['wing'] != wing and len(content) > 0:
                embed.add_field(name='W%d' % wing, value=content, inline=False)
                content = ''
            if boss['id'] in self.log_links_and_times:
                wing = boss['wing']
                content += await self.get_boss_string(boss)

        if len(content) > 0:
            embed.add_field(name='W%d' % wing, value=content, inline=False)
        return embed

    async def add_fractal_embed_field(self, embed, fractal, content):
        instabilities_str = ''
        if fractal in self.instabilities:
            for instability in self.instabilities[fractal]:
                instabilities_str += str(await self.get_instab_emote(instability))
        # todo move somewhere else
        fractal_time_string = ''

        split_logfile = splittlogparser.get_splitt_for_instance(fractal, mode=fractals.fractals)
        if split_logfile:
            split_log = splittlogparser.SplittLog(split_logfile)
            last_time = split_log.splitts[-1]['time']
            if last_time > 0:
                from main import format_duration_string
                fractal_time_string = format_duration_string(last_time)
        embed.add_field(
            name='%s %s %s' % (fractals.fractals[fractal]['display_name'], instabilities_str, fractal_time_string),
            value=content, inline=False)

    async def upload_image_file(self, filepath, *, ext='png'):
        server = await get_server_by_name(self, IMAGE_SERVER)
        if not server:
            print('No matching server for "%s" found' % self.channel_name)
            return ERROR_IMAGE_URL
        channel = await get_channel_by_name(server, IMAGE_CHANNEL)
        if not channel:
            print('No matching channel for "%s" found' % self.channel_name)
            return ERROR_IMAGE_URL
        try:
            await channel.send(file=discord.File(filepath, filename='image.' + ext))
            async for msg in channel.history(limit=1):
                url = msg.attachments[0].url
                if isinstance(url, str):
                    return url
        except Exception as ex:
            print('Could not uplodad image: %s' % str(ex))
        return ERROR_IMAGE_URL

    async def get_fractal_embed(self):
        embed = discord.Embed(title='%title% ' + str(date.today()))
        embed.set_footer(text=get_random_frac_footer())
        if self.thumbnail:
            if not re.match('https?://\S+\.\S+/\S+', self.thumbnail):  # url check is a bit unclean but good enough
                self.thumbnail = await self.upload_image_file(filepath=self.thumbnail)
            embed.set_thumbnail(url=self.thumbnail)
        content = ''
        fractal = ''
        for encounter in fractals.encounters:
            if encounter['fractal'] != fractal and len(content) > 0:
                await self.add_fractal_embed_field(embed, fractal, content)
                content = ''
            if encounter['id'] in self.log_links_and_times:
                fractal = encounter['fractal']
                content += await self.get_boss_string(encounter)
        if len(content) > 0:
            await self.add_fractal_embed_field(embed, fractal, content)
        if self.sheet_id:
            embed.url = 'https://docs.google.com/spreadsheets/d/%s/' % self.sheet_id
        return embed

    async def get_boss_string(self, encounter):
        buff_str = ''
        if self.buffs and self.buffs[encounter['id']] and len(self.buffs[encounter['id']]) > 0:
            buff_str = '└'
            for buff_id in self.buffs[encounter['id']]:
                buff_str += '%s %.1f ' % (await self.get_buff_emote(buff_id), self.buffs[encounter['id']][buff_id])
            buff_str += '\n'
        return "%s **[%s](%s '%s')** | **Time:  **%s %s\n%s" % (
            await self.get_encounter_emote(encounter['id']),
            encounter['name'],
            self.log_links_and_times[encounter['id']][0],
            self.log_links_and_times[encounter['id']][3],
            self.log_links_and_times[encounter['id']][1],
            self.log_links_and_times[encounter['id']][2],
            buff_str
        )


EMOTE_SERVER = 'testseverpv42'
IMAGE_SERVER = 'testseverpv42'
IMAGE_CHANNEL = 'imgur'
ERROR_IMAGE_URL = 'https://cdn2.bulbagarden.net/upload/9/98/Missingno_RB.png'


async def get_server_by_name(client, name):
    async for guild in client.fetch_guilds():
        if guild.name == name:
            return guild


async def get_emote_server(client):
    return await get_server_by_name(client, EMOTE_SERVER)


async def get_channel_by_name(server, name):
    channels = await server.fetch_channels()
    for c in channels:
        if c.name == name:
            return c


GENERAL_FOOTERS = [
    "can we gg ?",
    "now with randomly chosen footers",
    "BIG",
    "Have you heard of The Cure?",
    "my wrath is permeating",
    "yes",
    "I agree",
    "hammergamming",
    "peepoperformance",
    "ranged strat",
    "RIP Lt. Soulcleave",
    "lele is in the redneck",
    "youtu.be/dQw4w9WgXcQ",
    "Remember to hydrate",
    "Hey boys it's Komir",
    "Let's go to hell and kill Satan.",
    "I am now a god - Your god - I deserve this",
    "I think i'm sooo pretty",
    "Wow that's quality armor",
    "The game client is unable to gain access to the log-in server at this time.",
    "youtu.be/DxmkAoLC6_4",
    "I am the only sane person in this universe"
]

RAID_FOOTERS = [
    "with Professionals",
    "6 Raids a year",
    "Unbreakable bell broke",
    "\"Weekly\""
]

FRACTAL_FOOTERS = [
    "suck my condi",
    "suck my power",
    "boonstrip tomorrow",
    "I never mess this up",
    "imagine you're a happy poop in a basement",
    "it's fine",
    "pizza flavored",
    "your condi sucks",
    "your power sucks",
    "totally trolling!",
    "Dont you dare turning the boss!",
    "condi is now doubled",
    "fractal god btw",
    "fractal champion btw",
    "better than Deepstone",
    "lele is peepo sleepo",
    "chonking",
    "unsnekified",
    "Area Cleanse Condition",
    "approved by Syra Stahlklinge",
    "Socially awkward",
    "RGB",
    "One Ahooo Pack",
    "give quickness",
    "give alacrity",
    "give superspeed",
    "Portal? Portal?"
    "give might",
    "fire field?",
    "I love Siren's Reef",
    "instabilities are fun",
    "green class op",
    "blue class op",
    "rip red class",
    "who is pink class",
    "no pain no dps from me i guess",
    "www.twitch.tv/makemonni",
    "daily dye giveaways",
    "71 days till fractal good",
    "let me shovel this no more",
    "i block this every time",
    "you will get stabbed with a frozen banana",
    "<bonk> cheese.",
    "full sulfur reflex",
    "Lynn go BS",
    "What is air phase ?",
    "Hey kids, wanna fuck up some shades?",
    "Full Ya Boi precast",
    "get more sleep Rey",
    "pv's statistic service",
    "nonestopshop",
    "Ma i want to slay",
    "Moa btw",
    "pretty cool huh"
    "caw caw",
    "I did not get the singularity",
    "something about protection",
    "Your weakness makes me laugh",
    "You made it!",
    "Fear the Collective!",
    "Horrik!!! Unleash the cannons! - Aye aye captain",
    "Sorry, you will never be a Fractal God :-(",
    "The revolution is glorious!"
]


def get_random_raid_footer():
    footers = GENERAL_FOOTERS + RAID_FOOTERS
    return secrets.choice(footers)


def get_random_frac_footer():
    footers = GENERAL_FOOTERS + FRACTAL_FOOTERS
    return secrets.choice(footers)
