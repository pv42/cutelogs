FURY_BUFF_ID = 725
MIGHT_BUFF_ID = 740
QUICKNESS_BUFF_ID = 1187
ALACRITY_BUFF_ID = 30328
PROTECTION_BUFF_ID = 717
VULNERABILITY_BUFF_ID = 738
BEEHIVE_SKILL_ID = 34533
ZEALOTS_FIRE_ID = 9089
ZEALOTS_FLAME = 9104
ORB_OF_WRAITH_ID = 9098
RICOCHET = 12466
SHATTERSHOT_ID = 40497
WEAPON_SWAP_SKILL_ID = -2
DODGE_SKILL_ID1 = 65001
DODGE_SKILL_ID2 = 23275
FIRE_STRIKE = 39964
BLOOD_CURSE = 10698
PUTRID_CURSE = 10552
RENDING_CURSE = 10699
CORE_CLEAVE = 45047
PREPARATION_THRUST = 29057
BRUTAL_BLADE = 29256
RIFT_SLASH = 28964
MISERY_SWIPE = 27066
MIND_SLASH_ID = 10170
MIND_GASH = 10171
MIND_SPIKE = 10172
CHOP = 14369
LONG_RANGE_SHOT = 12510
BOMB = 5842
STAFF_STRIKE = 30614
LIGHT_STRIKE = 44588
DECAPITATE = 30851
DUAL_SHOT = 14431
WINDS_OF_CHAOS = 10273
MANIFEST_SAND_SHADE_OLD = 42297
MANIFEST_SAND_SHADE = 44946
BLEEDING_EDGE = 44602
SYMBOL_OF_PUNISHMENT = 9090
STAFF_BASH = 30135
WEAKENING_CHARGE = 29911
PUNISHING_STRIKES = 30434
CROSS_FIRE = 12470
SWORD_OF_JUSTICE = 44846
MIRAGE_CLOAK = -17
CHAOS_VORTEX = 40184
CHARGED_STRIKE = 44681
SIGNET_OF_WRATH_SKILL = 9151
SEARING_FISSURE = 28357
BLOODBANE_PATH = 40175
ANGUISH_SWIPE = 26730
SYMBOL_OF_VENGEANCE = 40624
FLAME_RUSH = 46618
SEARING_SLASH_FIRE_BRAND = 43826
THROW_AXE = 14398
IMPOSSIBLE_ODDS_HIT_ID = 49108
DOUBLE_CHOP = 14370
SOLAR_BEAM = 31710
STRIKE_GUARD_GS = 9137
CYCLONE_AXE = 14421
ILLUSIONARY_LEAP = 10173
PHANTASMAL_SWORDSMAN = 10174
SPLINTER_SHOT = 12526
BATTLE_STANDARD = 14569
BRANCH_BASH = 1114
ME_NOTE_SKILL = 16637
SIGNET_OF_COURAGE = 30461
AQUA_SIPHON = 41167
SLIPSTREAM = 10325
STAB = 10315
TE_NOTE_SKILL = 16641
GUN_FLAME = 29644
VOLLEY = 14416
BRUTAL_SHOT = 34296
IMPALE = 14498
RIPOSTE = 14400

SIGNET_OF_MERCY = 9163
RENEWAL_OF_WATER = 5763
GEAR_SHIELD = 5998
GLYPH_OF_ELEMENTAL_HARMONY = 34651
TAILORED_VICTORY = 44637
SHACKLE = 9212
DEVOURING_CUT = 62672
BLADETURN_REQUIEM = 62597
HEAVENS_PALM = 62561
RUSHING_JUSTICE = 62668
ELIXIR_S = 5861
HOUNDS_OF_BALTHAZAR = 12363
GLYPH_OF_UNITY_CA = 31740  # ca maybe ?
SPLIT_SURGE = 44241
STRENGTH_OF_THE_PACK = 12516
STONE_SHARDS = 5500
SUN_EDGE = 43476
LACERATING_CHOP = 44791
WILD_STRIKE = 13087
SPATIAL_SURGE = 10219
LOTUS_STRIKE = 13108
FLYING_CUTTER = 62510
TAINTED_BOLTS = 62611
DOUBLE_STRIKE = 13004
MACE_STRIKE = 63186
MACE_SMASH_MECH = 63077
BARRIER_BLAST = 63174
VICIOUS_SHOT = 62517
RIFLE_BURST = 6003
TRANQUILIZER_DART = 5934

BUFF_BANNER_OF_STRENGTH = 14417
BUFF_BANNER_OF_DISCIPLINE = 14449


RAID_WING_MAP_IDS = [1062, 1149, 1156, 1188, 1264, 1303, 1323]


def get_skill_name(skill_id):
    if skill_id == ZEALOTS_FIRE_ID:
        return "Zealot's Fire"
    elif skill_id == ORB_OF_WRAITH_ID:
        return "Orb of Wrath"
    elif skill_id == RICOCHET:
        return "Ricochet"
    elif skill_id == SHATTERSHOT_ID:
        return "Shattershot"
    elif skill_id == MIND_SLASH_ID:
        return "Mind Slash"
    elif skill_id == BLOOD_CURSE:
        return "Blood Curse"
    elif skill_id == ZEALOTS_FLAME:
        return "Zealot's Flame"
    elif skill_id == DECAPITATE:
        return "Decapitate"
    elif skill_id in [MANIFEST_SAND_SHADE, MANIFEST_SAND_SHADE_OLD]:
        return "Manifest Sand Shade"
    elif skill_id == SYMBOL_OF_PUNISHMENT:
        return "Symbol of Punishment"
    elif skill_id == WEAKENING_CHARGE:
        return "Weakening Charge"
    elif skill_id == SWORD_OF_JUSTICE:
        return "Sword of Justice"
    elif skill_id == WINDS_OF_CHAOS:
        return "Winds of Chaos"
    elif skill_id == MIRAGE_CLOAK:
        return "Mirage Cloak"
    elif skill_id == CHAOS_VORTEX:
        return "Chaos Vortex"
    elif skill_id == PREPARATION_THRUST:
        return "Preparation Thrust"
    elif skill_id == CHARGED_STRIKE:
        return "Charged Strike"
    elif skill_id == CORE_CLEAVE:
        return "Core Cleave"
    elif skill_id == CHOP:
        return "Chop"
    elif skill_id == SEARING_FISSURE:
        return "Searing Fissure"
    elif skill_id == BLOODBANE_PATH:
        return "Bloodbane Path"
    elif skill_id == ANGUISH_SWIPE:
        return "Anguish Swipe"
    elif skill_id == MIND_GASH:
        return "Mind Gash"
    elif skill_id == FLAME_RUSH:
        return "Flame Rush"
    elif skill_id == SYMBOL_OF_VENGEANCE:
        return "Symbol of Vengeance"
    elif skill_id == STAFF_STRIKE:
        return "Staff Strike"
    elif skill_id == SEARING_SLASH_FIRE_BRAND:
        return "Searing Slash"
    elif skill_id == BRUTAL_BLADE:
        return "Brutal Blade"
    elif skill_id == THROW_AXE:
        return "Throw Axe"
    elif skill_id == DOUBLE_CHOP:
        return "Double Chop"
    elif skill_id == MIND_SPIKE:
        return "Mind Spike"
    elif skill_id == SOLAR_BEAM:
        return "Solar Beam"
    elif skill_id == STRIKE_GUARD_GS:
        return "Strike"
    elif skill_id == PHANTASMAL_SWORDSMAN:
        return "Phantasmal Swordsman"
    elif skill_id == CYCLONE_AXE:
        return "Cyclone Axe"
    elif skill_id == ILLUSIONARY_LEAP:
        return "Illusionary Leap"
    elif skill_id == SPLINTER_SHOT:
        return "Splinter Shot"
    elif skill_id == BATTLE_STANDARD:
        return "Battle Standard"
    elif skill_id == BRANCH_BASH:
        return "Branch bash"
    elif skill_id == ME_NOTE_SKILL:
        return "Me (Bell)"
    elif skill_id == SIGNET_OF_COURAGE:
        return "Signet of Courage"
    elif skill_id == AQUA_SIPHON:
        return "Aqua Siphon"
    elif skill_id == SLIPSTREAM:
        return "Slipstream"
    elif skill_id == STAB:
        return "Stab"
    elif skill_id == TE_NOTE_SKILL:
        return "Te (Bell)"
    elif skill_id == GUN_FLAME:
        return "Gun Flame"
    elif skill_id == VOLLEY:
        return "Volley"
    elif skill_id == BRUTAL_SHOT:
        return "Brutal Shot"
    elif skill_id == IMPALE:
        return "Impale"
    elif skill_id == RIPOSTE:
        return "Riposte"
    elif skill_id == SIGNET_OF_MERCY:
        return "Riposte"
    elif skill_id == RENEWAL_OF_WATER:
        return "Renewal of Water"
    elif skill_id == GEAR_SHIELD:
        return "Gear Shield"
    elif skill_id == GLYPH_OF_ELEMENTAL_HARMONY:
        return "Glyph of Elemental Harmony"
    elif skill_id == TAILORED_VICTORY:
        return "Tailored Victory"
    elif skill_id == SHACKLE:
        return "Shackel"
    elif skill_id == DEVOURING_CUT:
        return "Devouring Cut"
    elif skill_id == BLADETURN_REQUIEM:
        return "Bladeturn Requiem"
    elif skill_id == ELIXIR_S:
        return "Elixir S"
    elif skill_id == RUSHING_JUSTICE:
        return "Rushing Justice"
    elif skill_id == HEAVENS_PALM:
        return "Heaven's Palm"
    elif skill_id == HOUNDS_OF_BALTHAZAR:
        return "Hounds of Balthazar"
    elif skill_id == SPLIT_SURGE:
        return "Split Surge"
    elif skill_id == GLYPH_OF_UNITY_CA:
        return "Glyph of Unity (CA)"
    elif skill_id == STONE_SHARDS:
        return "Stone Shards"
    elif skill_id == STRENGTH_OF_THE_PACK:
        return "\"Strength of the Pack!\""

    return f"?({skill_id})"


def is_skill_auto_attack(skill_id):
    return skill_id in [RICOCHET, SHATTERSHOT_ID, ORB_OF_WRAITH_ID, MIND_SLASH_ID, MIND_GASH, MIND_SPIKE, BLOOD_CURSE,
                        CORE_CLEAVE, PREPARATION_THRUST, CHOP, LONG_RANGE_SHOT, BOMB, BRUTAL_BLADE, FIRE_STRIKE,
                        MISERY_SWIPE, STAFF_STRIKE, RIFT_SLASH, LIGHT_STRIKE, DUAL_SHOT, WINDS_OF_CHAOS, PUTRID_CURSE,
                        RENDING_CURSE, BLEEDING_EDGE, STAFF_BASH, PUNISHING_STRIKES, CROSS_FIRE, CHARGED_STRIKE,
                        ANGUISH_SWIPE, SEARING_SLASH_FIRE_BRAND, DOUBLE_CHOP, SOLAR_BEAM, STRIKE_GUARD_GS,
                        SPLINTER_SHOT, SUN_EDGE, LACERATING_CHOP, WILD_STRIKE, SPATIAL_SURGE, LOTUS_STRIKE,
                        FLYING_CUTTER, DOUBLE_STRIKE, MACE_STRIKE, TAINTED_BOLTS, MACE_SMASH_MECH, BARRIER_BLAST,
                        VICIOUS_SHOT, RIFLE_BURST, TRANQUILIZER_DART]


def get_class_id_by_elite_spec_name(es_name):
    if es_name in ["Tempest", "Weaver"]:
        return 0
    if es_name in ["Mirage", "Chronomancer"]:
        return 1
    if es_name in ["Scourge", "Reaper"]:
        return 2
    if es_name in ["Deadeye", "Daredevil", "Thief"]:
        return 3
    if es_name in ["Soulbeast", "Druid"]:
        return 4
    if es_name in ["Scrapper", "Holosmith", "Engineer"]:
        return 5
    if es_name in ["Dragonhunter", "Firebrand"]:
        return 6
    if es_name in ["Warrior", "Berserker"]:
        return 7
    if es_name in ["Renegade", "Herald"]:
        return 8
    raise Exception("unknown elite spec name:" + str(es_name))


def get_elite_spec_name_by_id(spec_id):
    return "EliteSpecId not found :" + str(spec_id)
