import datetime

import srcomapi, srcomapi.datatypes as srcom_dt

SRCOM_GW2_GAME_ID = 'w6jp0x6j'
SRCOM_RAIDS_CAT_ID = 'jdzln3g2'
SRCOM_RAIDS_SUB_CAT_VAR_ID = '789x06q8'
SRCOM_BOSS_CAT_ID_VG = '81pzwpvl'

__ignore_ = {
    'jqzw7e2q': '1 - Spirit Vale',
    '81pzwpvl': '1 - Vale Guardian',
    'xqk3kjd1': '1 - Spirit Woods',
    'gq7oejpl': '1 - Gorseval the Multifarious',
    '21g6r86q': '1 - Sabetha the Saboteur',
    'jqzw7p2q': '2 - Salvation Pass',
    'klrdwpoq': '2 - Slothasor',
    '21dn7zpl': '2 - Bandit Trio',
    '5q8376y1': '2 - Matthias Gabrel',
    'mln76wo1': '3 - Stronghold of the Faithful',
    '4qymep6q': '3 - Siege the Stronghold',
    '810yegol': '3 - Keep Construct',
    '9qjoyv7l': '3 - Twisted Castle',
    'jq6wknvl': '3 - Xera',
    '5lmejv8q': '4 - Bastion of the Penitent',
    'xqk35jk1': '4 - Cairn the Indomitable',
    'gq7opjdl': '4 - Mursaat Overseer',
    '21g6588q': '4 - Samarog',
    'jqzw9p8q': '4 - Deimos',
    '4lxk9r41': '5 - Hall of Chains',
    'klrd9pmq': '5 - Soulless Horror',
    '21dn5z5l': '5 - River of Souls',
    '5q83p631': '5 - Broken King',
    '4qym9p2q': '5 - Eater of Souls',
    'mln79wj1': '5 - Eyes of Darkness',
    '810ypz2l': '5 - Dhuum',
    '810ypg2l': '6 - Mythwright Gambit',
    '9qjo903l': '6 - Conjured Amalgamate',
    'jq6wp0jl': '6 - Twin Largos',
    '5lmepkmq': '6 - Qadim',
    'zqog9421': '7 - The Key of Ahdashim',
    '81wj9zvq': '7 - Cardinal Sabir',
    'zqog9kx1': '7 - Cardinal Adina',
    '013jpdxl': '7 - Qadim the Peerless',
    'p12kpr41': 'Full Clear'}


def main():
    srcom_api = srcomapi.SpeedrunCom()
    gw2 = srcom_api.get_game('w6jp0x6j')
    raids = None
    for cat in gw2.categories:
        if cat.name == "Raids":
            raids = cat
    if not raids:
        return
    sub_cat_var = None
    for var in raids.variables:
        if var.name == "Raid Categories":
            sub_cat_var = var
    sub_cat_choices = sub_cat_var.values['choices']
    print(sub_cat_choices)
    runs = srcom_dt.Leaderboard(srcom_api, data=srcom_api.get(
        "leaderboards/{}/category/{}?embed=variables".format(SRCOM_GW2_GAME_ID, SRCOM_RAIDS_CAT_ID))).runs
    for run in runs:
        for k in run['run'].values:
            v = run['run'].values[k]
            if v in sub_cat_choices:
                pass  # print(sub_cat_choices[v], run['run'].times["primary_t"])
        # print(run['run'].values()[sub_cat_var.id])


def get_boss_record(sr_sub_cat_id):
    srcom_api = srcomapi.SpeedrunCom()
    runs = srcom_dt.Leaderboard(srcom_api, data=srcom_api.get(
        "leaderboards/{}/category/{}?embed=variables".format(SRCOM_GW2_GAME_ID, SRCOM_RAIDS_CAT_ID))).runs
    times = [x['run'].times["primary_t"] for x in runs if x["run"].values[SRCOM_RAIDS_SUB_CAT_VAR_ID] == sr_sub_cat_id]
    return datetime.timedelta(seconds=min(times))


if __name__ == "__main__":
    vg = "81pzwpvl"
    record = get_boss_record(vg)
    print(record)
    main()
