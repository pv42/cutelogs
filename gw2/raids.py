import datetime
from logmanagercachereader import LMEncounter

encounters = [
    {'id': 'vg', 'name': 'Vale Guardian', 'wing': 1, 'enemies': [15438], 'short_name': "VG", "lmid": LMEncounter.VG},
    {'id': 'gors', 'name': 'Gorseval the Multifarious', 'wing': 1, 'enemies': [15429], 'short_name': "Gorse", "lmid": LMEncounter.Gorseval},
    {'id': 'sab', 'name': 'Sabetha the Saboteur', 'wing': 1, 'enemies': [15375], 'short_name': "Sabetha", "lmid": LMEncounter.Sabetha},
    {'id': 'sloth', 'name': 'Slothasor', 'wing': 2, 'enemies': [16123], 'short_name': "Sloth", "lmid": LMEncounter.Slothasor},
    {'id': 'trio', 'name': 'Bandit Trio', 'wing': 2, 'enemies': [], 'short_name': "Trio", "lmid": LMEncounter.BanditTrio},  # todo enemies
    {'id': 'matt', 'name': 'Matthias Gabrel', 'wing': 2, 'enemies': [16115], 'short_name': "Matthias", "lmid": LMEncounter.Matthias},
    {'id': 'esc', 'name': 'Siege the Stronghold', 'wing': 3, 'enemies': [16253], 'short_name': "Escort", "lmid": LMEncounter.Escort},
    {'id': 'kc', 'name': 'Keep Construct', 'wing': 3, 'enemies': [16235], 'short_name': "KC", "lmid": LMEncounter.KC},
    {'id': 'tc', 'name': 'Twisted Castle', 'wing': 3, 'enemies': [], 'short_name': "TC", "lmid": LMEncounter.TC},  # todo enemies
    {'id': 'xera', 'name': 'Xera', 'wing': 3, 'enemies': [16246], 'short_name': "Xera", "lmid": LMEncounter.Xera},
    {'id': 'cairn', 'name': 'Cairn the Indomitable', 'wing': 4, 'enemies': [17194], 'short_name': "Cairn", "lmid": LMEncounter.Cairn},
    {'id': 'mo', 'name': 'Mursaat Overseer', 'wing': 4, 'enemies': [17172], 'short_name': "MO", "lmid": LMEncounter.MO},
    {'id': 'sam', 'name': 'Samarog', 'wing': 4, 'enemies': [17188], 'short_name': "Samarog", "lmid": LMEncounter.Samarog},
    {'id': 'dei', 'name': 'Deimos', 'wing': 4, 'enemies': [17154], 'short_name': "Deimos", "lmid": LMEncounter.Deimos},
    {'id': 'sh', 'name': 'Soulless Horror', 'wing': 5, 'enemies': [19767], 'short_name': "SH", "lmid": LMEncounter.SH},
    {'id': 'bk', 'name': 'Broken King', 'wing': 5, 'enemies': [], 'short_name': "SH", "lmid": LMEncounter.BrokenKing},
    {'id': 'rr', 'name': 'Desmina', 'wing': 5, 'enemies': [], 'short_name': "River", "lmid": LMEncounter.DesminaEscort},
    {'id': 'dhuum', 'name': 'Dhuum', 'wing': 5, 'enemies': [19450], 'short_name': "Dhuum", "lmid": LMEncounter.Dhuum},
    {'id': 'ca', 'name': 'Conjured Amalgamate', 'wing': 6, 'enemies': [43974], 'short_name': "CA", "lmid": LMEncounter.ConjuredAmalgamate},
    {'id': 'twins', 'name': 'Twin Largos', 'wing': 6, 'enemies': [21105, 21089], 'short_name': "Twins", "lmid": LMEncounter.Twins},
    {'id': 'qadim', 'name': 'Qadim', 'wing': 6, 'enemies': [20934], 'short_name': "Qadim", "lmid": LMEncounter.Qadim},
    {'id': 'adina', 'name': 'Cardinal Adina', 'wing': 7, 'enemies': [22006], 'short_name': "Adina", "lmid": LMEncounter.Adina},
    {'id': 'sabir', 'name': 'Cardinal Sabir', 'wing': 7, 'enemies': [21964], 'short_name': "Sabir", "lmid": LMEncounter.Sabir},
    {'id': 'qpeer', 'name': 'Qadim the Peerless', 'wing': 7, 'enemies': [22000], 'short_name': "QTP", "lmid": LMEncounter.QTP}
]


def get_id_by_name(boss_name: str):
    if boss_name == "Nikare":
        return "twins"
    elif boss_name == "Kenut":
        return "twins"
    elif boss_name == "Berg":
        return "trio"
    elif boss_name == "Zane":
        return "trio"
    elif boss_name == "Narella":
        return "trio"
    elif boss_name == "Eye of Judgment":
        return "eyes"
    elif boss_name == "Eye of Fate":
        return "eyes"
    elif boss_name == "Haunting Statue":
        return "tc"
    elif boss_name == "Eater of Souls":
        return "eater"
    elif boss_name == "McLeod the Silent":
        return "esc"
    for encounter in encounters:
        if boss_name == encounter["name"] or boss_name == get_display_name(encounter["name"]):
            return encounter["id"]
    print("No boss id for %s found" % boss_name)


def get_global_wing_record(wing: int):
    return [
        datetime.timedelta(minutes=14, seconds=23),
        datetime.timedelta(minutes=11, seconds=58),
        datetime.timedelta(minutes=12, seconds=7),
        datetime.timedelta(minutes=19, seconds=44),
        datetime.timedelta(minutes=15, seconds=42),
        datetime.timedelta(minutes=16, seconds=30),
        datetime.timedelta(minutes=17, seconds=32)
    ][wing - 1]


def get_global_boss_record(boss_id: str):
    global_records = {
        "vg": datetime.timedelta(minutes=1, seconds=37),
        "gors": datetime.timedelta(minutes=1, seconds=38),
        "sab": datetime.timedelta(minutes=2, seconds=30),
        "sloth": datetime.timedelta(minutes=1, seconds=24),
        "matt": datetime.timedelta(minutes=1, seconds=35),
        "kc": datetime.timedelta(minutes=3, seconds=15),
        "xera": datetime.timedelta(minutes=3, seconds=7),  # official time is 3:56 but towers dont count for log
        "cairn": datetime.timedelta(minutes=0, seconds=52),
        "mo": datetime.timedelta(minutes=0, seconds=56),
        "sam": datetime.timedelta(minutes=2, seconds=51),
        "dei": datetime.timedelta(minutes=2, seconds=9),
        "sh": datetime.timedelta(minutes=1, seconds=27),
        "rr": datetime.timedelta(minutes=2, seconds=2),
        "bk": datetime.timedelta(minutes=1, seconds=40),
        "dhuum": datetime.timedelta(minutes=4, seconds=17),
        "ca": datetime.timedelta(minutes=1, seconds=21),
        "twins": datetime.timedelta(minutes=2, seconds=7),
        "qadim": datetime.timedelta(minutes=4, seconds=33),
        "adina": datetime.timedelta(minutes=2, seconds=22),
        "sabir": datetime.timedelta(minutes=2, seconds=31),
        "qpeer": datetime.timedelta(minutes=3, seconds=28),
    }
    if boss_id in global_records:
        return global_records[boss_id]


def get_wing_from_boss(name):
    wing_dict = {
        "Vale Guardian": 1,
        "Gorseval the Multifarious": 1,
        "Sabetha the Saboteur": 1,
        "Slothasor": 2,
        "Berg": 2,
        "Zane": 2,
        "Narella": 2,
        "Matthias Gabrel": 2,
        "McLeod the Silent": 3,
        "Keep Construct": 3,
        "Haunting Statue": 3,
        "TC": 3,
        "Xera": 3,
        "Cairn the Indomitable": 4,
        "Mursaat Overseer": 4,
        "Samarog": 4,
        "Deimos": 4,
        "Soulless Horror": 5,
        "Desmina": 5,
        "Broken King": 5,
        "Eater of Souls": 5,
        "Eye of Judgment": 5,
        "Eye of Fate": 5,
        "Dhuum": 5,
        "Conjured Amalgamate": 6,
        "Nikare": 6,
        "Kenut": 6,
        "Qadim": 6,
        "Cardinal Adina": 7,
        "Cardinal Sabir": 7,
        "Qadim the Peerless": 7
    }
    if name in wing_dict:
        return wing_dict[name]
    else:
        print("Could not find wing for boss " + name)
        return 0


def get_display_name(name):
    extra_translation_dict = {
        "Berg": "Trio",
        "Zane": "Trio",
        "Haunting Statue": "TC",
        "Soulless Horror": "SH",
        "Desmina": "River",
        "Broken King": "BK",
        "Eater of Souls": "Eater",
        "Eye of Judgment": "Eyes",
        "Eye of Fate": "Eyes",
        "Kenut": "Twins",
        "Nikare": "Twins",
        "Twin Largos": "Twins",
    }
    if name in extra_translation_dict:
        return extra_translation_dict[name]
    else:
        for encounter in encounters:
            if encounter["name"] == name:
                return encounter["short_name"]
    return name


wings = {
    1: {'display_name': 'Wing 1', 'splittset_name': 'Wing 1 - Spirit Vale'},
    2: {'display_name': 'Wing 2', 'splittset_name': 'Wing 2 - Salvation Pass'},
    3: {'display_name': 'Wing 3', 'splittset_name': 'Wing 3 - Stronghold oif the Faithful'},
    4: {'display_name': 'Wing 4', 'splittset_name': 'Wing 4 - Bestion of the Penitent'},
    5: {'display_name': 'Wing 5', 'splittset_name': 'Wing 5 - Hall of Chains'},
    6: {'display_name': 'Wing 6', 'splittset_name': 'Wing 6 - Mythwright Gambit'},
    7: {'display_name': 'Wing 7', 'splittset_name': 'Wing 7 - The Key of Ahdashim'}
}


def get_by_enemy_id(enemy_id):
    for encounter in encounters:
        for eid in encounter['enemies']:
            if eid == enemy_id:
                return encounter
    return None


def get_by_encounter_id(encounter_id):
    for encounter in encounters:
        if encounter['id'] == encounter_id:
            return encounter
    return {'id': encounter_id, 'name': 'Unknown( ' + encounter_id + ' )', 'wing': -1, 'enemies': [],
            'short_name': "UNK"}
