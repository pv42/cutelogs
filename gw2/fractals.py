encounters = [
    {'id': 'mama', 'name': 'MAMA', 'enemies': [17021], 'fractal': 'nm'},
    {'id': 'siax', 'name': 'Siax the Corrupted', 'enemies': [17028], 'fractal': 'nm'},
    {'id': 'enso', 'name': 'Ensolyss of the Endless Torment', 'enemies': [16948], 'fractal': 'nm'},
    {'id': 'skor', 'name': 'Skorvald the Shattered', 'enemies': [17632], 'fractal': 'so'},
    {'id': 'arriv', 'name': 'Artsariiv', 'enemies': [17949], 'fractal': 'so'},
    {'id': 'arkk', 'name': 'Arkk', 'enemies': [17759], 'fractal': 'so'},
    {'id': 'ai', 'name': 'Dark Ai', 'enemies': [23254], 'fractal': 'sp'}
]

# ai phases are weird maybe fix this later

fractals = {
    'nm': {'display_name': 'Nightmare 98', 'splittset_name': 'Nightmare98CM - Nightmare'},
    'so': {'display_name': 'Shattered Observatory 99', 'splittset_name': 'Shattered Observatory'},
    'sp': {'display_name': 'Sunqua Peak 100', 'splittset_name': '100CM - Sunqua Peek'}
}

fractal_instabilities = {
    22228: 'afflicted',
    22277: 'no_pain_no_gain',
    22293: 'last_laugh',
    32942: 'social_awkwardness',
    36204: 'toxic_trail',
    36341: 'adrenaline_rush',
    36386: 'flux_bomb',
    46865: 'vengeance',
    47288: 'toxic_sickness',
    47323: 'hamstrung',
    48296: 'vindicators',
    53673: 'boon_overload',
    53932: 'stick_together',
    54084: 'outflanked',
    54237: 'sugar_rush',
    54477: 'frailty',
    54719: 'we_bleed_fire',
}


def get_by_enemy_id(enemy_id):
    for encounter in encounters:
        for id in encounter['enemies']:
            if id == enemy_id:
                return encounter
    return None


def get_by_encounter_id(encounter_id):
    if encounter_id == 'eai':
        return {'id': 'eai', 'name': 'Elemental Ai'}
    for encounter in encounters:
        if encounter['id'] == encounter_id:
            return encounter
    return None
