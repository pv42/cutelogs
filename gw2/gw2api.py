import json
import requests


class AchievementDto:
    id: int
    name: str
    description: str
    requirement: str
    locked_text: str
    flags: list[str]
    tiers: list
    rewards: list

    def __init__(self, json_def):
        self.__dict__ = json.loads(json_def)

    def __repr__(self):
        return self.name


class AchievementCategoryDto:
    id: int
    name: str
    description: str  # seems to be always empty
    order: int
    icon: str
    achievements: list[int]

    def __init__(self, json_def):
        self.__dict__ = json.loads(json_def)


class DailyAchievementDto:
    pve: list
    pvp: list
    wvw: list
    fractals: list
    special: list

    def __init__(self, json_def):
        self.__dict__ = json.loads(json_def)


class AchievementGroupDto:
    id: str
    name: str
    description: str
    order: int
    categories: list[int]

    def __init__(self, json_def):
        self.__dict__ = json.loads(json_def)


class BackstoryAnswerDto:
    id: str
    title: str
    description: str
    journal: str
    question: int

    def __init__(self, json_def):
        self.__dict__ = json.loads(json_def)


class BackstoryQuestionDto:
    id: str
    title: str
    description: str
    answers: list[str]
    order: int

    def __init__(self, json_def):
        self.__dict__ = json.loads(json_def)


class ColorMaterialDto:
    brightness: int
    contrast: int
    hue: int
    saturation: float
    lightness: float
    rgb: list[int]

    def __init__(self, data_dict):
        self.__dict__ = data_dict

    def __repr__(self):
        return f"rgb:{self.rgb}"


class ColorDto:
    id: int
    name: str
    base_rgb: list[int]
    cloth: ColorMaterialDto
    leather: ColorMaterialDto
    metal: ColorMaterialDto
    fur: ColorMaterialDto
    item: int
    categories: list[str]

    def __init__(self, json_def):
        self.__dict__ = json.loads(json_def)
        self.cloth = ColorMaterialDto(self.cloth)
        self.leather = ColorMaterialDto(self.leather)
        self.metal = ColorMaterialDto(self.metal)
        self.fur = ColorMaterialDto(self.fur)

    def __repr__(self):
        return f"{self.name}"


class SkillFactDto:
    text: str
    type: str
    icon: str
    # optional
    value: [int]
    hit_count: [int]
    dmg_multiplier: [int]
    distance: [int]
    target: [str]
    status: [str]
    description: [str]
    apply_count: [int]
    field_type: [str]
    finisher_type: [str]
    duration: [int]
    percent: [int]
    prefix: [dict]

    def __init__(self, data_dict):
        self.__dict__ = data_dict

    def __repr__(self):
        return f"Fact({self.type}): {self.text}"


class SkillDto:
    name: str
    facts: list
    description: str
    icon: str
    type: str
    weapon_type: str
    professions: list
    slots: str
    flags: list
    id: int
    chat_link: str

    def __init__(self, json_def):
        self.__dict__ = json.loads(json_def)
        for i, _ in enumerate(self.facts):
            self.facts[i] = SkillFactDto(self.facts[i])

    def __repr__(self):
        return f"{self.id}-{self.name}"


class SpecialisationDto:
    name: str
    id: int
    name: str
    profession: str
    elite: bool
    minor_traits: list[int]
    major_traits: list[int]
    icon: str
    background: str
    # elite only
    weapon_trait: int
    profession_icon_big: str
    profession_icon: str

    def __init__(self, json_def):
        self.__dict__ = json.loads(json_def)

    def __repr__(self):
        if self.elite:
            return "Elite Specialisation: " + self.name
        else:
            return "Specialisation: " + self.name


class GW2API:
    """todo:
    /account
    /characters
    /colors
    /commerce
    /continents
    /createsubtoken
    /currencies
    /dailycrafting
    /dungeons
    /emblem
    /emote
    /files
    /finishers
    [d]/gemstore
    /gliders
    /guild
    /home
    /items
    /itemstats
    /legendaryarmory
    /legends
    /mailcarriers
    """

    API_URL = "https://api.guildwars2.com"

    @staticmethod
    def get_achievements_ids() -> list[int]:
        r = requests.get(f"{GW2API.API_URL}/v2/achievements")
        return r.json()

    @staticmethod
    def get_achievement(achievements_id, *, lang="en"):
        r = requests.get(f"{GW2API.API_URL}/v2/achievements/{achievements_id}?lang={lang}")
        ach = AchievementDto(r.text)
        return ach

    @staticmethod
    def get_achievement_category_ids() -> list[int]:
        r = requests.get(f"{GW2API.API_URL}/v2/achievements/categories")
        return r.json()

    @staticmethod
    def get_achievement_category(category_id, *, lang="en") -> AchievementCategoryDto:
        r = requests.get(f"{GW2API.API_URL}/v2/achievements/categories/{category_id}?lang={lang}")
        ach_cat = AchievementCategoryDto(r.text)
        return ach_cat

    @staticmethod
    def get_daily_achievements() -> DailyAchievementDto:
        r = requests.get(f"{GW2API.API_URL}/v2/achievements/daily")
        ach_cat = DailyAchievementDto(r.text)
        return ach_cat

    @staticmethod
    def get_daily_achievements_tomorrow() -> DailyAchievementDto:
        r = requests.get(f"{GW2API.API_URL}/v2/achievements/daily/tomorrow")
        ach_cat = DailyAchievementDto(r.text)
        return ach_cat

    @staticmethod
    def get_achievement_group(group_id) -> AchievementGroupDto:
        r = requests.get(f"{GW2API.API_URL}/v2/achievements/groups/{group_id}")
        ach_cat = AchievementGroupDto(r.text)
        return ach_cat

    @staticmethod
    def get_backstory_question_ids() -> list[int]:
        r = requests.get(f"{GW2API.API_URL}/v2/backstory/questions")
        return r.json()

    @staticmethod
    def get_backstory_answer_ids() -> list[str]:
        r = requests.get(f"{GW2API.API_URL}/v2/backstory/answers")
        return r.json()

    @staticmethod
    def get_backstory_answer(answer_id, *, lang="en") -> BackstoryAnswerDto:
        r = requests.get(f"{GW2API.API_URL}/v2/backstory/answers/{answer_id}?lang={lang}")
        answer = BackstoryAnswerDto(r.text)
        return answer

    @staticmethod
    def get_backstory_question(question_id, *, lang="en") -> BackstoryQuestionDto:
        r = requests.get(f"{GW2API.API_URL}/v2/backstory/question/{question_id}?lang={lang}")
        answer = BackstoryQuestionDto(r.text)
        return answer

    # warning this api endpoint is bugged
    @staticmethod
    def get_game_build() -> int:
        r = requests.get(f"{GW2API.API_URL}/v2/build")
        return r.json()["id"]

    @staticmethod
    def get_achievement_groups() -> list[str]:
        r = requests.get(f"{GW2API.API_URL}/v2/achievements/groups")
        return r.json()

    @staticmethod
    def get_color_ids() -> list[int]:
        r = requests.get(f"{GW2API.API_URL}/v2/colors")
        return r.json()

    @staticmethod
    def get_color(color_id, *, lang="en") -> ColorDto:
        r = requests.get(f"{GW2API.API_URL}/v2/colors/{color_id}?lang={lang}")
        color = ColorDto(r.text)
        return color

    @staticmethod
    def get_specialisation_ids() -> list[int]:
        r = requests.get(f"{GW2API.API_URL}/v2/specializations")
        return r.json()

    @staticmethod
    def get_skill(skill_id, *, lang="en") -> SkillDto:
        r = requests.get(f"{GW2API.API_URL}/v2/skills/{skill_id}?lang={lang}")
        skill = SkillDto(r.text)
        return skill

    @staticmethod
    def get_specialisation(spec_id, *, lang="en") -> SpecialisationDto:
        r = requests.get(f"{GW2API.API_URL}/v2/specializations/{spec_id}?lang={lang}")
        spec = SpecialisationDto(r.text)
        return spec


"""
5 Elite Specialisation: Druid
7 Elite Specialisation: Daredevil
18 Elite Specialisation: Berserker
27 Elite Specialisation: Dragonhunter
34 Elite Specialisation: Reaper
40 Elite Specialisation: Chronomancer
43 Elite Specialisation: Scrapper
48 Elite Specialisation: Tempest
52 Elite Specialisation: Herald
55 Elite Specialisation: Soulbeast
56 Elite Specialisation: Weaver
57 Elite Specialisation: Holosmith
58 Elite Specialisation: Deadeye
59 Elite Specialisation: Mirage
60 Elite Specialisation: Scourge
61 Elite Specialisation: Spellbreaker
62 Elite Specialisation: Firebrand
63 Elite Specialisation: Renegade
64 Elite Specialisation: Harbinger
65 Elite Specialisation: Willbender
66 Elite Specialisation: Virtuoso
67 Elite Specialisation: Catalyst
68 Elite Specialisation: Bladesworn
69 Elite Specialisation: Vindicator
70 Elite Specialisation: Mechanist
71 Elite Specialisation: Specter
"""


def main():
    for i in range(1, 72):
        s = GW2API.get_specialisation(i)
        if s.elite:
            print(i, s)


if __name__ == "__main__":
    main()
