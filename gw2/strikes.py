from logmanagercachereader import LMEncounter

encounters = [
    {'id': 'ice', 'name': 'Icebrood Construct', 'wing': 1, 'short_name': "IC", "lmid": LMEncounter.IcebroodConstruct},
    {'id': 'falln', 'name': 'Super Kodan Brothers', 'wing': 1, 'short_name': "Brothers", "lmid": LMEncounter.Kodans},
    {'id': 'frae', 'name': 'Fraenir of Jormag', 'wing': 1, 'short_name': "Fraenir", "lmid": LMEncounter.Fraenir},
    {'id': 'bone', 'name': 'Boneskinner', 'wing': 1, 'short_name': "Boneskinner", "lmid": LMEncounter.Boneskinner},
    {'id': 'whisp', 'name': 'Whisper of Jormag', 'wing': 1, 'short_name': "Wisper", "lmid": LMEncounter.Whisper},

    {'id': 'trin', 'name': 'Captain Mai Trin', 'wing': 2, 'short_name': "Mai Trin", "lmid": LMEncounter.MaiTrin},
    {'id': 'ankka', 'name': 'Ankka', 'wing': 2, 'short_name': "Ankka", "lmid": LMEncounter.Ankka},
    {'id': 'li', 'name': 'Minister Li', 'wing': 2, 'short_name': "Minister Li", "lmid": LMEncounter.MinisterLi},
    {'id': 'void', 'name': 'The Dragonvoid', 'wing': 2, 'short_name': "Dragonvoid", "lmid": LMEncounter.DragonVoid},
]


def get_by_enemy_id(enemy_id):
    for encounter in encounters:
        for id in encounter['enemies']:
            if id == enemy_id:
                return encounter
    return None


def get_by_encounter_id(encounter_id):
    for encounter in encounters:
        if encounter['id'] == encounter_id:
            return encounter
    return None
