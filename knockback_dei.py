import json
import os
import re
from time import sleep

from logmanagercachereader import LogManagerCache
from dps_report_api import DpsReportAPI, get_ei_data_cached, CombatReplayData, EliteInsightData

import cv2

CACHE_PATH = "E:\\GW2LogsData\\"


def get_cr_json(url, *, silent=True):
    if not re.match(r'https://dps\.report/[a-zA-z0-9]{4}-\d{8}-\d{6}_[a-z]{2,5}', url):
        print('invalid url: "%s"' % url)
        return
    url_id_part = url.split('https://dps.report/')[1]
    if not os.path.exists(CACHE_PATH + 'crJsons'):
        os.mkdir(CACHE_PATH + 'crJsons')
    jpath = (CACHE_PATH + 'crJsons/%s.json') % url_id_part
    if os.path.exists(jpath):
        try:
            f = open(jpath, 'r')
            j = json.loads(f.read())
            f.close()
            return CombatReplayData(j)
        except json.decoder.JSONDecodeError:
            print("Failed to load file :", url_id_part)

    else:
        if not silent:
            print('Downloading cr data %s' % url)
        api = DpsReportAPI()
        j = api.get_cr_json_data_by_link(url)
        f = open(jpath, 'w')
        f.write(json.dumps(j.data))
        f.close()
        return j


def create_player_actor_id_dict(ei_data: EliteInsightData, crj):
    player_actor_dict = {}
    if not ei_data:
        return player_actor_dict
    players = ei_data.players()
    player_index = 0
    for actor in crj.get_actors():
        if actor["type"] == "Player":
            if not players[player_index].is_fake:
                player_actor_dict[players[player_index].name] = actor["id"]
                if players[player_index].profession_name not in actor["img"]:
                    pass
                    # print("WRN: match", players[player_index].get_profession_name(), "to", actor["img"])
            player_index += 1
    return player_actor_dict


def main():
    lmc = LogManagerCache()
    api = DpsReportAPI()
    logs = lmc.get_logs(target_filter=["Deimos"], success_filter=[True])
    logs.reverse()
    img = cv2.imread("E:\\GW2LogsData\\img_cache\\https---i.imgur.com-FFs9cFq.png", cv2.IMREAD_UNCHANGED)
    img = cv2.resize(img, (574, 720))
    for log_index, log in enumerate(logs):
        url = log.get_dps_report_url()
        eid = get_ei_data_cached(url, silent=False)
        mechanic_names = ["10% RSmash", "10% LSmash", "10% DSmash"]
        colors = [(255, 0, 0), (0, 255, 0), (0, 0, 255)]
        colors2 = [(127, 0, 0), (0, 127, 0), (0, 0, 127)]
        # ipd = eid.get_combat_replay_inch_to_pxl()
        if not eid.has_combat_replay_data():
            crj = get_cr_json(url, silent=False)
            player_actor_dict = create_player_actor_id_dict(eid, crj)
            #print(player_actor_dict)
        for i, mechanic_name in enumerate(mechanic_names):
            evs = eid.get_mechanic_events(mechanic_name=mechanic_name)
            if not evs:
                continue
            for ev in evs:
                player = eid.get_player_by_name(ev["actor"])
                if eid.has_combat_replay_data():
                    x, y = player.get_position_keyframe(int(ev["time"] / 150))
                else:
                    x,y = crj.get_position_keyframe(player_actor_dict[player.name()], int(ev["time"] / 150))
                pos = (int(x), int(y))
                img = cv2.circle(img, pos, 3, colors[i], -1)
    # cv2.imshow("window_name", img)
    # cv2.waitKey(0)
    cv2.imwrite("test.jpg", img)


if __name__ == "__main__":
    main()
