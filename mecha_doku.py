import json
import os
import re

import dps_report_api
from logmanagercachereader import LogManagerCache
from dps_report_api import DpsReportAPI, EliteInsightData

CACHE_PATH = "E:\\GW2LogsData\\"


def get_dhuum_bomb_accounts():
    lmc = LogManagerCache()
    data = lmc.get_logs_by_day(target_filter=["Dhuum"])
    pre_ac_counts = {}
    total_ac_present = {}
    pre_es_counts = {}
    total_es_present = {}
    main_ac_counts = {}
    main_ac_present = {}
    main_es_counts = {}
    main_es_present = {}
    day_index = 0
    total_log_count = 0
    for day in data:
        day_index += 1
        print("Processing day %2d of %d with %2d logs .. " % (day_index, len(data), len(data[day])))
        ##if day.year == 2021 or day.weekday() not in [0, 2]:  # filter out none-monday/-wednesday and pre 2021
        ##    continue
        for log in data[day]:
            url = log["DpsReportEIUpload"]["Url"]
            if not url:
                continue
            j = dps_report_api.get_ei_data_cached(url=url)
            if not j:
                continue
            if j.get_duration_ms() < 130000 or j.is_cm():  # filter out pre only and cm attempts
                continue
            total_log_count += 1
            for player in j.players():
                if player.profession_name not in total_es_present:
                    total_es_present[player.profession_name] = 0
                    pre_es_counts[player.profession_name] = 0
                    main_es_present[player.profession_name] = 0
                    main_es_counts[player.profession_name] = 0
                if player.account_name() not in total_ac_present:
                    total_ac_present[player.account_name()] = 0
                    pre_ac_counts[player.account_name()] = 0
                    main_ac_present[player.account_name()] = 0
                    main_ac_counts[player.account_name()] = 0
                total_ac_present[player.account_name()] += 1
                total_es_present[player.profession_name] += 1
            mc = j.get_mechanic_counts("Bomb", filter_before_ms=130000)
            for char_name in mc:
                ac_name = j.get_player_by_name(char_name).account_name()
                pre_ac_counts[ac_name] += 1
                pre_es_counts[j.get_player_by_account_name(ac_name).profession_name] += 1
            mc = j.get_mechanic_counts("Bomb", filter_after_ms=130000)
            for char_name in mc:
                ac_name = j.get_player_by_name(char_name).account_name()
                main_ac_counts[ac_name] += 1
                main_es_counts[j.get_player_by_account_name(ac_name).profession_name] += 1
    print("Processed %d logs" % total_log_count)
    total_ac_list = []
    for ac_name in total_ac_present:
        total_ac_list.append({"name": ac_name,
                              "avg": (pre_ac_counts[ac_name] + main_ac_counts[ac_name]) / total_ac_present[ac_name],
                              "count": (pre_ac_counts[ac_name] + main_ac_counts[ac_name]),
                              "present": total_ac_present[ac_name],
                              "pre_avg": pre_ac_counts[ac_name] / total_ac_present[ac_name],
                              "pre_count": pre_ac_counts[ac_name],
                              "main_avg": main_ac_counts[ac_name] / total_ac_present[ac_name],
                              "main_count": main_ac_counts[ac_name]
                              })
    total_ac_list.sort(key=lambda x: -x["avg"])
    total_es_list = []
    for es in total_es_present:
        total_es_list.append({"name": es,
                              "avg": (pre_es_counts[es] + main_es_counts[es]) / total_es_present[es],
                              "count": (pre_es_counts[es] + main_es_counts[es]),
                              "present": total_es_present[es],
                              "pre_avg": pre_es_counts[es] / total_es_present[es],
                              "pre_count": pre_es_counts[es],
                              "main_avg": main_es_counts[es] / total_es_present[es],
                              "main_count": main_es_counts[es]
                              })
    total_es_list.sort(key=lambda x: -x["avg"])
    print("#logs | total    | pre      | main     | name")
    print("      | avg    # | avg    # | avg    # | ")
    for pair in total_ac_list:
        if pair["present"] >= 10:
            print(" %4d | %.2f %3d | %.2f %3d | %.2f %3d | %s" % (
                pair["present"], pair["avg"], pair["count"], pair["pre_avg"], pair["pre_count"], pair["main_avg"],
                pair["main_count"], pair["name"]))
    print()
    print("#logs | total    | pre      | main     | name")
    print("      | avg    # | avg    # | avg    # | ")
    for pair in total_es_list:
        if pair["present"] >= 3:
            print(" %4d | %.2f %3d | %.2f %3d | %.2f %3d | %s" % (
                pair["present"], pair["avg"], pair["count"], pair["pre_avg"], pair["pre_count"], pair["main_avg"],
                pair["main_count"], pair["name"]))


def get_dhuum_bomb_accounts_from_logs(logs):
    pre_ac_counts = {}
    total_ac_present = {}
    pre_es_counts = {}
    total_es_present = {}
    main_ac_counts = {}
    main_ac_present = {}
    main_es_counts = {}
    main_es_present = {}

    valid_log_count = 0
    total_log_count = 0
    for file in logs:
        total_log_count += 1
        print("Processing %d of %d" % (total_log_count, len(logs)))
        url = logs[file]
        j = dps_report_api.get_ei_data_cached(url=url)
        if not j:
            continue
        if j.get_duration_ms() < 130000:  # filter out pre only and cm attempts
            continue
        valid_log_count += 1
        do_skip = False
        for player in j.players():
            if player.account_name() in ["Aegas.1069", "MoreEwayne.1732", "CaptainBloodloss.5732",
                                             "PVzweiundvierzig.7381"]:
                do_skip = True
                break
        if do_skip:
            continue
        for player in j.players():
            if player.profession_name not in total_es_present:
                total_es_present[player.profession_name] = 0
                pre_es_counts[player.profession_name] = 0
                main_es_present[player.profession_name] = 0
                main_es_counts[player.profession_name] = 0
            if player.account_name() not in total_ac_present:
                total_ac_present[player.account_name()] = 0
                pre_ac_counts[player.account_name()] = 0
                main_ac_present[player.account_name()] = 0
                main_ac_counts[player.account_name()] = 0
            total_ac_present[player.account_name()] += 1
            total_es_present[player.profession_name] += 1
        mc = j.get_mechanic_counts("Bomb", filter_before_ms=130000)
        for char_name in mc:
            ac_name = j.get_player_by_name(char_name).account_name()
            pre_ac_counts[ac_name] += 1
            pre_es_counts[j.get_player_by_account_name(ac_name).profession_name] += 1
        mc = j.get_mechanic_counts("Bomb", filter_after_ms=130000)
        for char_name in mc:
            ac_name = j.get_player_by_name(char_name).account_name()
            main_ac_counts[ac_name] += 1
            main_es_counts[j.get_player_by_account_name(ac_name).profession_name] += 1
    print("Processed %d logs, discaeded %d logs" % (total_log_count, total_log_count - valid_log_count))
    total_ac_list = []
    for ac_name in total_ac_present:
        total_ac_list.append({"name": ac_name,
                              "avg": (pre_ac_counts[ac_name] + main_ac_counts[ac_name]) / total_ac_present[ac_name],
                              "count": (pre_ac_counts[ac_name] + main_ac_counts[ac_name]),
                              "present": total_ac_present[ac_name],
                              "pre_avg": pre_ac_counts[ac_name] / total_ac_present[ac_name],
                              "pre_count": pre_ac_counts[ac_name],
                              "main_avg": main_ac_counts[ac_name] / total_ac_present[ac_name],
                              "main_count": main_ac_counts[ac_name]
                              })
    total_ac_list.sort(key=lambda x: -x["avg"])
    total_es_list = []
    for es in total_es_present:
        total_es_list.append({"name": es,
                              "avg": (pre_es_counts[es] + main_es_counts[es]) / total_es_present[es],
                              "count": (pre_es_counts[es] + main_es_counts[es]),
                              "present": total_es_present[es],
                              "pre_avg": pre_es_counts[es] / total_es_present[es],
                              "pre_count": pre_es_counts[es],
                              "main_avg": main_es_counts[es] / total_es_present[es],
                              "main_count": main_es_counts[es]
                              })
    total_es_list.sort(key=lambda x: -x["avg"])
    print("#logs | total    | pre      | main     | name")
    print("      | avg    # | avg    # | avg    # | ")
    for pair in total_ac_list:
        if pair["present"] >= 10:
            print(" %4d | %.2f %3d | %.2f %3d | %.2f %3d | %s" % (
                pair["present"], pair["avg"], pair["count"], pair["pre_avg"], pair["pre_count"], pair["main_avg"],
                pair["main_count"], pair["name"]))
    print()
    print("#logs | total    | pre      | main     | name")
    print("      | avg    # | avg    # | avg    # | ")
    for pair in total_es_list:
        if pair["present"] >= 3:
            print(" %4d | %.2f %3d | %.2f %3d | %.2f %3d | %s" % (
                pair["present"], pair["avg"], pair["count"], pair["pre_avg"], pair["pre_count"], pair["main_avg"],
                pair["main_count"], pair["name"]))


def prepare_kerchu_logs():
    path = "E:\\GW2LogsData\\kerchuLogs\\log_files"
    cache_file = "E:\\GW2LogsData\\kerchuLogs\\cache_file.json"
    dps_api = DpsReportAPI()
    if os.path.exists(cache_file):
        f = open(cache_file, "r")
        data = json.loads(f.read())
        f.close()
    else:
        data = {}
    for file in os.listdir(path):
        if file not in data:
            print("uploading", file)
            response = dps_api.upload_file(path + "\\" + file)
            data[file] = response.get_permalink()
        f = open(cache_file, "w")
        f.write(json.dumps(data))
        f.close()
    print("done preparing")
    return data


def main():
    data = prepare_kerchu_logs()
    get_dhuum_bomb_accounts_from_logs(data)


if __name__ == "__main__":
    main()
